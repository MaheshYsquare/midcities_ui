wworker_processes  1;

events {
    worker_connections  1024;
}

http {
      server {
        #Send all traffic to https
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name basicdev.lotustms.com;
        return 301 https://$host$request_uri;
      }

      server {
        #SSL configuration
        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;
	    ssl_certificate /etc/nginx/ssl/star_lotustms_com.crt;
        ssl_certificate_key /etc/nginx/ssl/star_lotustms_com.key;
   
        root /usr/share/nginx/html;
        index index.html index.htm index.nginx-debian.html;
        include /etc/nginx/mime.types;
	    server_name basicdev.lotustms.com;
        underscores_in_headers on;
        client_max_body_size 100M;
        ssl_protocols TLSv1.2 TLSv1.3;

        gzip on;
        gzip_min_length 1000;
        gzip_proxied expired no-cache no-store private auth;
        gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        location / {
            try_files $uri $uri/ /index.html;
	        proxy_http_version 1.1;
	        proxy_set_header Upgrade $http_upgrade;
	        proxy_set_header Connection "upgrade";
	        proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
            proxy_connect_timeout      180s;
            proxy_send_timeout         180s;
            proxy_read_timeout         180s;        
	    }

        location /api {
            proxy_pass http://localhost:3000;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
            proxy_connect_timeout      180s;
            proxy_send_timeout         180s;
            proxy_read_timeout         180s;        
	    }

        location /socket.io/ {
            proxy_pass http://localhost:3000;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
            proxy_connect_timeout      180s;
            proxy_send_timeout         180s;
            proxy_read_timeout         180s;        
	    }
    }
}
