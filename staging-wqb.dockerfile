FROM node:10.14.1-alpine as build-stage

WORKDIR /tms-dev-ui

RUN apk add git

RUN npm config set unsafe-perm true

RUN npm install -g pm2 @angular/cli@10.1.7

COPY package.json /tms-dev-ui

RUN npm install

COPY . /tms-dev-ui

RUN ng build --configuration=staging

EXPOSE 80

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY ./key/star_lotustms_com.crt /etc/nginx/ssl/

COPY ./key/star_lotustms_com.key /etc/nginx/ssl/

COPY ./nginx/staging-wqbnginx.conf /etc/nginx/nginx.conf

COPY --from=build-stage /tms-dev-ui/dist /usr/share/nginx/html
