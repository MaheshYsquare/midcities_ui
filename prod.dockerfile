FROM node:10.14.1-alpine as build-stage

WORKDIR /tms-dev-ui

RUN apk add git

RUN npm config set unsafe-perm true

RUN npm install -g pm2 @angular/cli@10.1.7

COPY package.json /tms-dev-ui

RUN npm install

COPY . /tms-dev-ui

RUN ng build --prod --aot

FROM banst/awscli

WORKDIR /root/dist

COPY --from=build-stage /tms-dev-ui/dist/ .

RUN mkdir /root/.aws

COPY .aws/* /root/.aws/

RUN aws s3 sync /root/dist s3://midcitiesr01usohprods3cloudfront/dist --delete

RUN aws cloudfront create-invalidation --distribution-id E1GEHG7GZ7UOV9 --paths /dist* || true

RUN rm -rf /root/.aws || true
