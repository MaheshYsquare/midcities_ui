import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { SharedService } from 'app/shared/service/shared.service';
import { SearchResult } from './search.model';


@Injectable()
export class SearchService {

  private baseURL = environment.baseURL;

  public httpOptions = {};

  private requestStateBehaviour = new BehaviorSubject<any>(null);

  constructor(private httpClient: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  /** Allows subscription to the behavior subject as an observable */
  getState(): Observable<any> {
    return this.requestStateBehaviour.asObservable();
  }

  /**
   * Allows updating the current state of the behavior subject
   * @param request a object representing the current state
   */
  setState(request: any): void {
    this.requestStateBehaviour.next(request);
  }

  advancedSearchAutoSuggest(actionComponentName: String, value: string, searchBy: string): Observable<string[]> {
    const url = `${this.baseURL + actionComponentName}/advancedSearchAutoSuggest?searchBy=${searchBy}`;
    return this.httpClient.post<string[]>(url, { value }, this.httpOptions)
  }

  advancedSearch(actionComponentName: String, request: any): Observable<SearchResult[]> {
    const url = `${this.baseURL + actionComponentName}/advancedSearch`;
    return this.httpClient.post<SearchResult[]>(url, request, this.httpOptions)
  }
}
