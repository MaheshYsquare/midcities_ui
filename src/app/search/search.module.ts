import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchComponent } from './search.component';
import { MaterialModule } from 'app/shared/demo.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchService } from './search.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SearchRoutingModule
  ],
  declarations: [SearchComponent],
  providers: [SearchService]
})
export class SearchModule { }
