import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { MatTableDataSource, MatPaginator, MatDialog, MatSelectChange } from '@angular/material';

import { searchColumns, SearchResult } from './search.model';
import { SearchService } from './search.service';
import { SharedService } from 'app/shared/service/shared.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  public searchComponentName: string = 'searches'; // search endpoint for api

  public searchForm: FormGroup; // reactive formgroup to store the inputs

  public searchColumn: { label: string; value: string; inputType: string }[] = searchColumns; // to display in autocomplete

  public usedSearchColumns: string[] = []; // to store already used column in order to disable the used one

  public searchObject: any = {}; //object to store multiple fields

  public isSearched: boolean = false; // flag is to check whether any searching done by user

  public searchResultDatasource: MatTableDataSource<SearchResult>; //datasource variable for material table to store retrived data

  public searchResultColumns: string[] = ['order_sequence', 'container_number', 'chassis_number',
    'booking_number', 'customer_reference', 'business_name', 'pu_ref', 'current_status']; //columns which need to display in table

  @ViewChild(MatPaginator) searchResultPaginator: MatPaginator; // paginator variable

  public columnLabels = {
    order_sequence: "Order #",
    container_number: "Container #",
    chassis_number: "Chassis #",
    business_name: "Customer",
    pu_ref: "PU Ref",
    booking_number: "Booking #",
    customer_reference: "Reference #"
  }

  public subscription: Subscription;

  public init: boolean = true;

  constructor(private router: Router,
    private searchService: SearchService,
    private sharedService: SharedService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.searchResultDatasource = new MatTableDataSource();

    this.subscription = this.searchService.getState().subscribe(state => {

      if (state && this.init) {

        this.searchForm = new FormGroup({
          "fieldsArray": new FormArray([])
        })

        Object.keys(state).forEach(key => {
          (<FormArray>this.searchForm.get('fieldsArray')).push(this.fieldsArrayFormGroup(key, state[key]));

          this.usedSearchColumns.push(key);
        });

        this.searchObject = state;

        this.searchServiceCall();

        this.init = false;
      } else if (this.init) {
        this.usedSearchColumns[0] = "container_number";

        this.searchForm = new FormGroup({
          "fieldsArray": new FormArray([
            this.fieldsArrayFormGroup("container_number")
          ])
        })

        this.init = false;
      }
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  fieldsArrayFormGroup(column, value?) {
    return new FormGroup({
      column: new FormControl(column),
      values: new FormControl(value)
    })
  }

  getFormArray() {
    return (<FormArray>this.searchForm.get('fieldsArray')).controls;
  }

  onChangeSearchBy(index: number, event: MatSelectChange) {
    this.usedSearchColumns[index] = event.value;
  }

  onClickAddFilter(fields) {
    let column = fields.get('column').value;
    let values = fields.get('values').value;

    if (column && column.trim() && values && values.trim()) {
      (<FormArray>this.searchForm.get('fieldsArray')).push(this.fieldsArrayFormGroup(null));
    } else {
      this.sharedService.openSnackbar("Please complete the current fields to add new one", 3000, "warning-msg");
    }
  }

  onClickDeleteFilter(index: number, event) {
    (<FormArray>this.searchForm.get('fieldsArray')).removeAt(index);
    this.usedSearchColumns = this.usedSearchColumns.filter(temp => temp !== event.column);
  }

  onSearch() {
    let valid = true;

    this.searchObject = {};

    this.searchForm.get("fieldsArray").value.forEach(element => {
      this.searchObject[element.column] = element.values;
    });

    Object.keys(this.searchObject).forEach(key => {
      valid = this.searchObject[key] && this.searchObject[key].trim() ? (valid ? true : false) : false;
    })

    if (Object.keys(this.searchObject).length && valid) {
      this.searchServiceCall();
    } else {
      this.sharedService.openSnackbar('Please Fill the Details to Search', 3000, 'warning-msg');
    }
  }

  searchServiceCall() {
    this.searchService.advancedSearch(this.searchComponentName, this.searchObject).subscribe(response => {
      this.isSearched = Object.keys(this.searchObject).length ? true : false;
      this.searchResultDatasource.data = response;
      setTimeout(() => this.searchResultDatasource.paginator = this.searchResultPaginator);
      this.searchService.setState(this.searchObject);
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg');
    })
  }

  onClickReset() {
    this.isSearched = false;
    this.usedSearchColumns = ["container_number"];
    this.searchForm.reset();
    this.searchForm.get("fieldsArray").value.forEach((element, i) => {
      (<FormArray>this.searchForm.get('fieldsArray')).removeAt(0);
    });
    (<FormArray>this.searchForm.get('fieldsArray')).push(this.fieldsArrayFormGroup("container_number"));
    this.searchService.setState(null);
  }

  onClickOrderNumber(selectedRow: SearchResult) {

    if (selectedRow.current_status === 'Draft') {
      this.router.navigate(['order-management', selectedRow.order_number, selectedRow.container_sequence, 'draft']);
    } else if (selectedRow.current_status === 'Pending') {
      this.router.navigate(['order-management', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Dispatch In Manual' ||
      selectedRow.current_status === 'Dispatch In Auto Schedule' ||
      selectedRow.current_status === 'Dispatch') {
      this.router.navigate(['dispatch', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Ready For Invoice') {
      this.router.navigate(['invoicing', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Invoice Generated') {
      this.router.navigate(['invoicing', 'invoice-management', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Archived') {
      this.router.navigate(['admin-features', 'archived-orders', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Manual Invoice Generated') {
      this.router.navigate(['invoicing', 'invoice-management', 'manual-invoice', selectedRow.invoice_number]);
    } else if (selectedRow.current_status === 'Order Cancelled') {
      this.router.navigate(['admin-features', 'archived-orders', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    } else if (selectedRow.current_status === 'Order Not Generated') {
      this.router.navigate(['admin-features', 'archived-orders', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
    }
  }

}
