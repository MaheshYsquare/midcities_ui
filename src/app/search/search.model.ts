export interface SearchResult {
    order_sequence: string;
    order_number: number;
    invoice_number: number;
    container_sequence: number;
    is_draft: boolean;
    is_dispatch: boolean;
    container_number: string;
    chassis_number: string;
    booking_number: string;
    customer_reference: string;
    current_status: string;
}

export const searchColumns = [
    {
        "label": "Container #",
        "value": "container_number",
        "inputType": "input"
    },
    {
        "label": "Chassis #",
        "value": "chassis_number",
        "inputType": "input"
    },
    {
        "label": "Booking #",
        "value": "booking_number",
        "inputType": "input"
    },
    {
        "label": "Reference #",
        "value": "customer_reference",
        "inputType": "input"
    },
    {
        "label": "Order #",
        "value": "order_sequence",
        "inputType": "input"
    },
    {
        "label": "PU Ref",
        "value": "pu_ref",
        "inputType": "input"
    },
    {
        "label": "Customer",
        "value": "business_name",
        "inputType": "input"
    }
]
