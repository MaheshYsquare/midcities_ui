import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  ElementRef,
  Renderer2,
  OnDestroy
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  MatTableDataSource,
  MatDialogConfig,
  MatDialog,
  MatSort,
  MatSnackBar,
  MatPaginator,
  PageEvent,
  MatCheckboxChange
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import * as moment from 'moment';

import { OrderService } from './order.service';
import { SocketService } from 'app/shared/service/socket.service';
import { OrderData, OrderGetRequest } from 'app/shared/models/order.model';
import { SharedService } from 'app/shared/service/shared.service';
import { ClipboardService } from 'app/shared/service/clipboard.service';
import { Subscription } from 'rxjs';
import { UiSettings, ordersColumns, invoiceColumns, ordersFilterColumns } from 'app/shared/models/display-column.model';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';
import { ManageColumnsComponent } from 'app/shared/modules/order-component/manage-columns/manage-columns.component';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit, OnDestroy {

  public componentName: String = "orders";

  public dispatchFilterEvent: boolean = false;

  public orderInDispatchFilterData: OrderGetRequest['filterData'];

  public isDispatchDataSelected: boolean = false;

  /* Order In Dispatch Table Variables */
  public dispatchFilterDate: Date = null;

  public ordersInDispatchDatasource: MatTableDataSource<OrderData>;

  @ViewChild(MatSort) orderInDispatchSort: MatSort;

  @ViewChild('ordersDispatchTable', { read: ElementRef }) private ordersDispatchTable: ElementRef;

  public ordersInDispatchSelection = new SelectionModel<OrderData>(true, []);

  public selectedOrdersInDispatch: number[] = [];

  public unSelectedOrdersInDispatch: number[] = [];

  public isAllOrdersInDispatchSelected: boolean = false;

  public orderInDispatchColumns: string[] = [];

  public ordersDispatch: UiSettings[] = [];

  @ViewChild('ordersInDispatchPaginator') ordersInDispatchPaginator: MatPaginator;

  public ordersInDispatchLength: number = 0;

  public dispatchOffset: number = 1;

  public dispatchLimit: number = 10;

  public dispatchSortColumn: string;

  public dispatchSortDirection: string;

  public pendingFilterEvent: boolean = false;

  public orderInPendingFilterData: OrderGetRequest['filterData'];

  public isPendingDataSelected: boolean = false;

  /* Orders in Pending Table Varaibles */
  public railTraceHistory: any[] = [];

  public railTraceHistoryTooltip: any;

  public loader: boolean = true;

  public pendingFilterDate: Date = null;

  public ordersInPendingDatasource: MatTableDataSource<OrderData>;

  @ViewChild(MatSort) orderInPendingSort: MatSort;

  @ViewChild('ordersPendingTable', { read: ElementRef }) private ordersPendingTable: ElementRef;

  public ordersInPendingSelection = new SelectionModel<OrderData>(true, []);

  public selectedOrdersInPending: number[] = [];

  public unSelectedOrdersInPending: number[] = [];

  public isAllOrdersInPendingSelected: boolean = false;

  public orderInPendingColumns: string[] = [];

  public ordersPending: UiSettings[] = [];

  @ViewChild('ordersInPendingPaginator') ordersInPendingPaginator: MatPaginator;

  public ordersInPendingLength: number = 0;

  public pendingOffset: number = 1;

  public pendingLimit: number = 10;

  public pendingSortColumn: string;

  public pendingSortDirection: string;

  public draftOrderFilterEvent: boolean = false;

  public draftOrderFilterData: OrderGetRequest['filterData'];

  /* Draft Orders Table Variables */
  public draftOrderDatasource: MatTableDataSource<OrderData>;

  @ViewChild(MatSort) draftOrderSort: MatSort;

  @ViewChild('ordersDraftTable', { read: ElementRef }) private ordersDraftTable: ElementRef;

  public ordersDraftSelection = new SelectionModel<OrderData>(true, []);

  public selectedOrdersDraft: number[] = [];

  public draftOrderColumns: string[] = [];

  public ordersDraft: UiSettings[] = [];

  @ViewChild('draftOrderPaginator') draftOrderPaginator: MatPaginator;

  public draftLength: number = 0;

  public draftOrderOffset: number = 1;

  public draftOrderLimit: number = 10;

  public draftOrderSortColumn: string;

  public draftOrderSortDirection: string;

  public searchFilterValue: string = "";

  public searchFilter: boolean = false;

  public containerToCopy: string[] = [];

  /* orders table Resize Column Variables */
  public ordersDispatchCurrentResizeIndex: number;

  public ordersDispatchPressed: boolean = false;

  public ordersDispatchStartX: number;

  public ordersDispatchStartWidth: number;

  public ordersDispatchIsResizingRight: boolean;

  public ordersDispatchScale: number;

  public ordersPendingCurrentResizeIndex: number;

  public ordersPendingPressed: boolean = false;

  public ordersPendingStartX: number;

  public ordersPendingStartWidth: number;

  public ordersPendingIsResizingRight: boolean;

  public ordersPendingScale: number;

  public ordersDraftCurrentResizeIndex: number;

  public ordersDraftPressed: boolean = false;

  public ordersDraftStartX: number;

  public ordersDraftStartWidth: number;

  public ordersDraftIsResizingRight: boolean;

  public ordersDraftScale: number;

  public sideNavToggleSubscription: Subscription;

  public railTraceSubscription: Subscription;

  public resizableMousemove: () => void;

  public resizableMouseup: () => void;

  public ordersDispatchTimeout: any;

  public ordersPendingTimeout: any;

  public ordersDraftTimeout: any;

  public columnsLabel = {
    order_sequence: "Order #",
    leg_status: "Leg Status",
    leg_number: "Leg #",
    driver_name: "Driver",
    container_size: "Size",
    order_type: "Type",
    container_number: "Container #",
    chassis_number: "Chassis #",
    miles: "Miles",
    order_container_number: "Container #",
    order_chassis_number: "Chassis #",
    order_miles: "Miles",
    leg_type: "Leg Type",
    hazmat_req: "Haz",
    triaxle: "Triaxle",
    order_flag: "Order Flag",
    tags: "Tags",
    pickup_date: "SCH PU",
    pickup_time: "PU Time",
    pu_name: "Pickup",
    dl_name: "Delivery",
    delivery_date: "SCH DL",
    delivery_time: "DL Time",
    order_pickup_date: "SCH PU",
    order_pickup_time: "PU Time",
    order_pu_name: "Pickup",
    order_dl_name: "Delivery",
    order_delivery_date: "SCH DL",
    order_delivery_time: "DL Time",
    order_status: "Order Status",
    container_type: "Container Type",
    p_zip: "PU Zip",
    d_zip: "DL Zip",
    business_name: "Customer",
    rail_cut: "Rail Cut",
    pdm: "PDM",
    dmg: "LFD",
    lfd_date: 'LFD',
    chs: "CHS",
    pu_ref: "PU REF",
    ll: "L/L",
    chs_days: "CHS Days",
    container_name: "Container Owner",
    chassis_name: "Chassis Name",
    created_on: "Created On",
    eta: "ETA",
    action: "Action"
  }

  public isLoading: boolean = true;

  constructor(public dialog: MatDialog,
    private orderService: OrderService,
    private router: Router,
    private sharedService: SharedService,
    private socketService: SocketService,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private clipboardService: ClipboardService,
    private renderer: Renderer2) { }


  ngOnInit() {
    this.ordersInDispatchDatasource = new MatTableDataSource();

    this.ordersInPendingDatasource = new MatTableDataSource();

    this.draftOrderDatasource = new MatTableDataSource();

    this.setDisplayedColumns('ordersDispatch');

    this.setDisplayedColumns('ordersPending');

    this.setDisplayedColumns('ordersDraft');

    this.getOrderDataFromService('dispatch');

    this.getOrderDataFromService('pending');

    this.getOrderDataFromService('draft');

    setTimeout(() => this.getRailTraceHistoryFromService(), 3000);

    this.sideNavToggleSubscription = this.sharedService.getSideNavToggleState().subscribe(isToggled => {
      if (isToggled && !this.isLoading) {
        setTimeout(() => {
          this.setTableResize(this.ordersDispatchTable.nativeElement.clientWidth, 'ordersDispatch');
          this.setTableResize(this.ordersPendingTable.nativeElement.clientWidth, 'ordersPending');
          this.setTableResize(this.ordersDraftTable.nativeElement.clientWidth, 'ordersDraft');
        }, 450);
      }
    })

    this.railTraceSubscription = this.socketService.listenServerThroughSocket('railTraceUpdate')
      .subscribe(response => {
        this.loader = true;
        if (response.isUpdated) this.getOrderDataFromService('pending', true);
        this.getRailTraceHistoryFromService();
      })
  }

  ngOnDestroy() {
    this.sideNavToggleSubscription.unsubscribe();

    this.railTraceSubscription.unsubscribe();
  }

  setDisplayedColumns(table: string) {
    switch (table) {
      case 'ordersDispatch':
        this.orderInDispatchColumns = [];

        this.ordersDispatch = localStorage.getItem('orders_dispatch') ? JSON.parse(localStorage.getItem('orders_dispatch')) : [];

        this.ordersDispatch.forEach((column, index) => {
          column.index = index;
          this.orderInDispatchColumns[index] = column.field;
        });
        break;
      case 'ordersPending':
        this.orderInPendingColumns = [];

        this.ordersPending = localStorage.getItem('orders_pending') ? JSON.parse(localStorage.getItem('orders_pending')) : [];

        this.ordersPending.forEach((column, index) => {
          column.index = index;
          this.orderInPendingColumns[index] = column.field;
        });
        break;
      case 'ordersDraft':
        this.draftOrderColumns = [];

        this.ordersDraft = localStorage.getItem('orders_draft') ? JSON.parse(localStorage.getItem('orders_draft')) : [];

        this.ordersDraft.forEach((column, index) => {
          column.index = index;
          this.draftOrderColumns[index] = column.field;
        });
        break;
    }

  }

  setTableResize(tableWidth: number, table: string) {
    let totWidth;

    switch (table) {
      case 'ordersDispatch':
        totWidth = 0;

        this.ordersDispatch.forEach((column) => {
          totWidth += column.width;
        });
        this.ordersDispatchScale = (tableWidth) / totWidth;
        this.ordersDispatch.forEach((column) => {
          column.width *= this.ordersDispatchScale;
          this.setColumnWidth(column, table);
        });
        break;
      case 'ordersPending':
        totWidth = 0;

        this.ordersPending.forEach((column) => {
          totWidth += column.width;
        });
        this.ordersPendingScale = (tableWidth) / totWidth;
        this.ordersPending.forEach((column) => {
          column.width *= this.ordersPendingScale;
          this.setColumnWidth(column, table);
        });
        break;
      case 'ordersDraft':
        totWidth = 0;

        this.ordersDraft.forEach((column) => {
          totWidth += column.width;
        });

        this.ordersDraftScale = (tableWidth) / totWidth;

        this.ordersDraft.forEach((column) => {
          column.width *= this.ordersDraftScale;
          this.setColumnWidth(column, table);
        });
        break;
    }

  }

  setColumnWidth(column: any, table: string) {
    let columnEls;

    if (table === 'ordersDispatch') {
      columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field} dispatch`));
    } else if (table === 'ordersPending') {
      columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field} pending`));
    } else if (table === 'ordersDraft') {
      columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field} draft`));
    }

    columnEls ? columnEls.forEach((el: HTMLDivElement) => {
      el.style.flex = 'none';
      el.style.width = column.width + 'px';
    }) : null;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTableResize(this.ordersDispatchTable.nativeElement.clientWidth, 'ordersDispatch');
    this.setTableResize(this.ordersPendingTable.nativeElement.clientWidth, 'ordersPending');
    this.setTableResize(this.ordersDraftTable.nativeElement.clientWidth, 'ordersDraft');
  }

  getOrderDataFromService(table: string, noLoader?: boolean): void {
    let request = this.generateRequest(table);

    if (noLoader) request['noLoader'] = true;

    this.sharedService.getOrderList(this.componentName, request)
      .subscribe(response => {
        switch (table) {
          case 'dispatch':
            this.ordersInDispatchDatasource.data = response.data.orderData;
            this.ordersInDispatchLength = response.data.orderPaginationLength;

            this.selectedOrdersInDispatch = this.isAllOrdersInDispatchSelected ?
              this.ordersInDispatchDatasource.data.map(temp => temp.order_container_chassis_id) :
              this.selectedOrdersInDispatch;

            setTimeout(() => {
              this.setTableResize(this.ordersDispatchTable.nativeElement.clientWidth, 'ordersDispatch');
            }, 100);
            break;
          case 'pending':
            this.ordersInPendingDatasource.data = response.data.orderData;
            this.ordersInPendingLength = response.data.orderPaginationLength;
            this.ordersInPendingDatasource.data.forEach(temp => {
              temp.isContainerSelected = this.containerToCopy.some(elem => elem === temp.formated_container);
            });
            this.selectedOrdersInPending = this.isAllOrdersInPendingSelected ?
              this.ordersInPendingDatasource.data.map(temp => temp.order_container_chassis_id) :
              this.selectedOrdersInPending;

            setTimeout(() => {
              this.setTableResize(this.ordersPendingTable.nativeElement.clientWidth, 'ordersPending');
            }, 100);
            break;
          case 'draft':
            this.draftOrderDatasource.data = response.data.orderData;
            this.draftLength = response.data.orderPaginationLength;

            setTimeout(() => {
              this.setTableResize(this.ordersDraftTable.nativeElement.clientWidth, 'ordersDraft');
            }, 100);
            break;
        }
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
  }

  getRailTraceHistoryFromService() {
    this.orderService.getRailTraceHistory(this.componentName)
      .subscribe((response: any) => {
        this.railTraceHistory = response;
        this.railTraceHistoryTooltip = this.generateToolTip();
        this.loader = false;
      }, error => {
        this.loader = false;
      })
  }

  onResizeColumn(event: any, index: number, table: string) {
    this.checkResizing(event, index, table);

    if (table === 'ordersDispatch') {
      this.ordersDispatchCurrentResizeIndex = index;
      this.ordersDispatchPressed = true;
      this.ordersDispatchStartX = event.pageX;
      this.ordersDispatchStartWidth = event.target.clientWidth;
    } else if (table === 'ordersPending') {
      this.ordersPendingCurrentResizeIndex = index;
      this.ordersPendingPressed = true;
      this.ordersPendingStartX = event.pageX;
      this.ordersPendingStartWidth = event.target.clientWidth;
    } else if (table === 'ordersDraft') {
      this.ordersDraftCurrentResizeIndex = index;
      this.ordersDraftPressed = true;
      this.ordersDraftStartX = event.pageX;
      this.ordersDraftStartWidth = event.target.clientWidth;
    }

    event.preventDefault();
    this.mouseMove(index, table);
  }

  private checkResizing(event, index, table) {
    const cellData = this.getCellData(index, table);

    if (table === 'ordersDispatch') {
      if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.ordersDispatch.length - 1)) {
        this.ordersDispatchIsResizingRight = true;
      } else {
        this.ordersDispatchIsResizingRight = false;
      }
    } else if (table === 'ordersPending') {
      if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.ordersPending.length - 1)) {
        this.ordersPendingIsResizingRight = true;
      } else {
        this.ordersPendingIsResizingRight = false;
      }
    } else if (table === 'ordersDraft') {
      if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.ordersDraft.length - 1)) {
        this.ordersDraftIsResizingRight = true;
      } else {
        this.ordersDraftIsResizingRight = false;
      }
    }
  }

  private getCellData(index: number, table: string) {
    let headerRow;

    if (table === 'ordersDispatch') {
      headerRow = this.ordersDispatchTable.nativeElement.children[0];
    } else if (table === 'ordersPending') {
      headerRow = this.ordersPendingTable.nativeElement.children[0];
    } else if (table === 'ordersDraft') {
      headerRow = this.ordersDraftTable.nativeElement.children[0];
    }

    const cell = headerRow.children[index];

    return cell.getBoundingClientRect();
  }

  mouseMove(index: number, table: string) {

    this.resizableMousemove = this.renderer.listen('document', 'mousemove', (event) => {

      switch (table) {
        case 'ordersDispatch':
          if (this.ordersDispatchPressed && event.buttons) {
            const dx = (this.ordersDispatchIsResizingRight) ? (event.pageX - this.ordersDispatchStartX) : (-event.pageX + this.ordersDispatchStartX);
            const width = this.ordersDispatchStartWidth + dx;
            if (this.ordersDispatchCurrentResizeIndex === index && width > 50) {
              this.setColumnWidthChanges(index, width, table);
            }
          }
          break;
        case 'ordersPending':
          if (this.ordersPendingPressed && event.buttons) {
            const dx = (this.ordersPendingIsResizingRight) ? (event.pageX - this.ordersPendingStartX) : (-event.pageX + this.ordersPendingStartX);
            const width = this.ordersPendingStartWidth + dx;
            if (this.ordersPendingCurrentResizeIndex === index && width > 50) {
              this.setColumnWidthChanges(index, width, table);
            }
          }
          break;
        case 'ordersDraft':
          if (this.ordersDraftPressed && event.buttons) {
            const dx = (this.ordersDraftIsResizingRight) ? (event.pageX - this.ordersDraftStartX) : (-event.pageX + this.ordersDraftStartX);
            const width = this.ordersDraftStartWidth + dx;
            if (this.ordersDraftCurrentResizeIndex === index && width > 50) {
              this.setColumnWidthChanges(index, width, table);
            }
          }
          break;
      }

    });

    if (table === 'ordersDispatch') {
      this.ordersDispatchTimeout ? clearTimeout(this.ordersDispatchTimeout) : null;
    } else if (table === 'ordersPending') {
      this.ordersPendingTimeout ? clearTimeout(this.ordersPendingTimeout) : null;
    } else if (table === 'ordersDraft') {
      this.ordersDraftTimeout ? clearTimeout(this.ordersDraftTimeout) : null;
    }

    this.resizableMouseup = this.renderer.listen('document', 'mouseup', (event) => {
      switch (table) {
        case 'ordersDispatch':
          if (this.ordersDispatchPressed) {
            this.ordersDispatchPressed = false;
            this.ordersDispatchCurrentResizeIndex = -1;
            this.resizableMousemove();
            this.resizableMouseup();

            let updatedOrdersDispatch: any = localStorage.getItem('updatedOrdersDispatch');

            if (updatedOrdersDispatch) {

              this.ordersDispatchTimeout = setTimeout(() => {

                updatedOrdersDispatch = JSON.parse(updatedOrdersDispatch);

                updatedOrdersDispatch.forEach(element => {
                  element.width = element.width / element.scale;
                });

                let request = {
                  column: 'orders_dispatch',
                  value: JSON.stringify(updatedOrdersDispatch),
                  user_id: this.sharedService.getUserDetails().userId,
                  noloading: true
                }
                this.sharedService.updateUiSettings('users', request).subscribe(response => {
                  localStorage.removeItem('updatedOrdersDispatch');
                  localStorage.setItem('orders_dispatch', JSON.stringify(updatedOrdersDispatch));
                }, error => {
                  // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
                })
              }, 2000);

            }

          }
          break;
        case 'ordersPending':
          if (this.ordersPendingPressed) {
            this.ordersPendingPressed = false;
            this.ordersPendingCurrentResizeIndex = -1;
            this.resizableMousemove();
            this.resizableMouseup();

            let updatedOrdersPending: any = localStorage.getItem('updatedOrdersPending');

            if (updatedOrdersPending) {

              this.ordersPendingTimeout = setTimeout(() => {

                updatedOrdersPending = JSON.parse(updatedOrdersPending);

                updatedOrdersPending.forEach(element => {
                  element.width = element.width / element.scale;
                });

                let request = {
                  column: 'orders_pending',
                  value: JSON.stringify(updatedOrdersPending),
                  user_id: this.sharedService.getUserDetails().userId,
                  noloading: true
                }
                this.sharedService.updateUiSettings('users', request).subscribe(response => {
                  localStorage.removeItem('updatedOrdersPending');
                  localStorage.setItem('orders_pending', JSON.stringify(updatedOrdersPending));
                }, error => {
                  // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
                })
              }, 2000);

            }

          }
          break;
        case 'ordersDraft':
          if (this.ordersDraftPressed) {
            this.ordersDraftPressed = false;
            this.ordersDraftCurrentResizeIndex = -1;
            this.resizableMousemove();
            this.resizableMouseup();

            let updatedOrdersDraft: any = localStorage.getItem('updatedOrdersDraft');

            if (updatedOrdersDraft) {

              this.ordersDraftTimeout = setTimeout(() => {

                updatedOrdersDraft = JSON.parse(updatedOrdersDraft);

                updatedOrdersDraft.forEach(element => {
                  element.width = element.width / element.scale;
                });

                let request = {
                  column: 'orders_draft',
                  value: JSON.stringify(updatedOrdersDraft),
                  user_id: this.sharedService.getUserDetails().userId,
                  noloading: true
                }
                this.sharedService.updateUiSettings('users', request).subscribe(response => {
                  localStorage.removeItem('updatedOrdersDraft');
                  localStorage.setItem('orders_draft', JSON.stringify(updatedOrdersDraft));
                }, error => {
                  // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
                })
              }, 2000);

            }

          }
          break;
      }

    });
  }

  setColumnWidthChanges(index: number, width: number, table: string) {
    let orgWidth;
    let dx;
    let modifiedUiSettings;

    switch (table) {
      case 'ordersDispatch':
        orgWidth = this.ordersDispatch[index].width;
        dx = width - orgWidth;
        if (dx !== 0) {
          const j = (this.ordersDispatchIsResizingRight) ? index + 1 : index - 1;
          const newWidth = this.ordersDispatch[j].width - dx;
          if (newWidth > 5) {
            this.ordersDispatch[index].width = width;
            this.setColumnWidth(this.ordersDispatch[index], table);
            this.ordersDispatch[j].width = newWidth;
            this.setColumnWidth(this.ordersDispatch[j], table);

            modifiedUiSettings = this.ordersDispatch.slice();

            modifiedUiSettings.forEach(temp => {
              temp.scale = this.ordersDispatchScale;
            });

            localStorage.setItem('updatedOrdersDispatch', JSON.stringify(modifiedUiSettings));
          }
        }
        break;
      case 'ordersPending':
        orgWidth = this.ordersPending[index].width;
        dx = width - orgWidth;
        if (dx !== 0) {
          const j = (this.ordersPendingIsResizingRight) ? index + 1 : index - 1;
          const newWidth = this.ordersPending[j].width - dx;
          if (newWidth > 5) {
            this.ordersPending[index].width = width;
            this.setColumnWidth(this.ordersPending[index], table);
            this.ordersPending[j].width = newWidth;
            this.setColumnWidth(this.ordersPending[j], table);

            let modifiedUiSettings = this.ordersPending.slice();

            modifiedUiSettings.forEach(temp => {
              temp.scale = this.ordersPendingScale;
            });

            localStorage.setItem('updatedOrdersPending', JSON.stringify(modifiedUiSettings));
          }
        }
        break;
      case 'ordersDraft':
        orgWidth = this.ordersDraft[index].width;
        dx = width - orgWidth;
        if (dx !== 0) {
          const j = (this.ordersDraftIsResizingRight) ? index + 1 : index - 1;
          const newWidth = this.ordersDraft[j].width - dx;
          if (newWidth > 5) {
            this.ordersDraft[index].width = width;
            this.setColumnWidth(this.ordersDraft[index], table);
            this.ordersDraft[j].width = newWidth;
            this.setColumnWidth(this.ordersDraft[j], table);

            modifiedUiSettings = this.ordersDraft.slice();

            modifiedUiSettings.forEach(temp => {
              temp.scale = this.ordersDraftScale;
            });

            localStorage.setItem('updatedOrdersDraft', JSON.stringify(modifiedUiSettings));
          }
        }
        break;
    }
  }

  /*   Fliter Dispatch Table through Date */
  onChangeDate(table: string) {
    this.initialPage(table);
    this.getOrderDataFromService(table);
  }

  /* clearing the date filter */
  onClickClear(table: string) {
    table === 'dispatch' ? this.dispatchFilterDate = null : this.pendingFilterDate = null;
    this.initialPage(table);
    setTimeout(() => {
      this.getOrderDataFromService(table);
    });
  }

  onClickFilterColumn(table: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "filterData": table === 'dispatch' ? this.orderInDispatchFilterData
        : (table === 'draft' ? this.draftOrderFilterData : this.orderInPendingFilterData),
      "filterColumns": ordersFilterColumns,
      "key": table === 'dispatch' ? "ordersDispatchData"
        : (table === 'pending' ? "ordersPendingData" : "ordersDraftData")
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.orderInDispatchFilterData = table === 'dispatch' ? result : this.orderInDispatchFilterData;
        this.draftOrderFilterData = table === 'draft' ? result : this.draftOrderFilterData;
        this.orderInPendingFilterData = table === 'pending' ? result : this.orderInPendingFilterData;
        this.dispatchFilterEvent = table === 'dispatch' ? true : this.dispatchFilterEvent;
        this.draftOrderFilterEvent = table === 'draft' ? true : this.draftOrderFilterEvent;
        this.pendingFilterEvent = table === 'pending' ? true : this.pendingFilterEvent;
        this.initialPage(table);
        this.getOrderDataFromService(table)
      }
    });
  };

  onClickColumnFilterReset(table: string) {
    this.orderInDispatchFilterData = table === 'dispatch' ? undefined : this.orderInDispatchFilterData;
    this.draftOrderFilterData = table === 'draft' ? undefined : this.draftOrderFilterData;
    this.orderInPendingFilterData = table === 'pending' ? undefined : this.orderInPendingFilterData;
    this.dispatchFilterEvent = table === 'dispatch' ? false : this.dispatchFilterEvent;
    this.draftOrderFilterEvent = table === 'draft' ? false : this.draftOrderFilterEvent;
    this.pendingFilterEvent = table === 'pending' ? false : this.pendingFilterEvent;
    this.initialPage(table);
    this.getOrderDataFromService(table)
  }

  onClickAddColumn(tableView: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      tableView: tableView,
      displayColumns: tableView === 'orders_draft' ? invoiceColumns : ordersColumns
    }

    const dialogRef = this.dialog.open(ManageColumnsComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        switch (tableView) {
          case 'orders_dispatch':
            this.setDisplayedColumns('ordersDispatch');

            setTimeout(() => {
              this.setTableResize(this.ordersDispatchTable.nativeElement.clientWidth, 'ordersDispatch');
            }, 100);

            localStorage.removeItem('updatedOrdersDispatch');
            break;
          case 'orders_pending':
            this.setDisplayedColumns('ordersPending');

            setTimeout(() => {
              this.setTableResize(this.ordersPendingTable.nativeElement.clientWidth, 'ordersPending');
            }, 100);

            localStorage.removeItem('updatedOrdersPending');
            break;
          case 'orders_draft':
            this.setDisplayedColumns('ordersDraft');

            setTimeout(() => {
              this.setTableResize(this.ordersDraftTable.nativeElement.clientWidth, 'ordersDraft');
            }, 100);

            localStorage.removeItem('updatedOrdersDraft');
            break;
        }
      }
    })
  }

  onClickMoveOrders(event: boolean) {
    let request = {
      isDispatch: event,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    if (event) {
      request['id'] = this.selectedOrdersInPending.join(',');
      request['columnFilter'] = this.pendingFilterEvent;
      request['filterData'] = this.orderInPendingFilterData;
      request['unSelectedId'] = this.unSelectedOrdersInPending;
      request['isAllSelected'] = this.isAllOrdersInPendingSelected;
      request['table'] = 'pending';
    } else {
      request['id'] = this.selectedOrdersInDispatch.join(',');
      request['columnFilter'] = this.dispatchFilterEvent;
      request['filterData'] = this.orderInDispatchFilterData;
      request['unSelectedId'] = this.unSelectedOrdersInDispatch;
      request['isAllSelected'] = this.isAllOrdersInDispatchSelected;
      request['table'] = 'dispatch';
    }

    let valid = true;

    if (event) {
      let isNotNull = [];

      isNotNull = this.ordersInPendingDatasource.data.map(temp => {
        return this.selectedOrdersInPending.some(elem => elem === temp.order_container_chassis_id) ?
          temp.est_pickup_from_time !== null && temp.est_pickup_to_time !== null &&
          temp.est_delivery_from_time !== null && temp.est_delivery_to_time !== null : null;
      })

      isNotNull = isNotNull.length !== 0 ? isNotNull.filter(temp => temp !== null) : [];

      valid = isNotNull.length !== 0 ? !isNotNull.includes(false) : true;
    }

    if (valid) {
      this.orderService.updateIsDispatch(this.componentName, request).subscribe(
        response => {
          if (response.code === 200) {
            this.socketService.sendChangesInDriverFromClient('order');
            this.getOrderDataFromService('dispatch');
            this.getOrderDataFromService('pending');
            if (request['table'] === 'dispatch') {
              this.ordersInDispatchSelection.clear();
              this.isDispatchDataSelected = false;
              this.isAllOrdersInDispatchSelected = false;
              this.selectedOrdersInDispatch = [];
              this.unSelectedOrdersInDispatch = [];
            } else {
              this.ordersInPendingSelection.clear();
              this.isPendingDataSelected = false;
              this.isAllOrdersInPendingSelected = false;
              this.selectedOrdersInPending = [];
              this.unSelectedOrdersInPending = [];
            }
          } else {
            this.sharedService.openSnackbar(response.error, 4000, 'warning-msg');
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        });
    } else {
      this.sharedService.openSnackbar(this.selectedOrdersInPending.length === 1 ?
        "Selected order do not have Pickup/Delivery Date & Time set" :
        "One or few orders selected do not have Pickup/Delivery Date & Time set", 4000, 'warning-msg')
    }

  }

  onChangeSortDirection(event, table) {
    switch (table) {
      case 'ordersInDispatch':
        this.dispatchSortDirection = event.direction;
        this.dispatchSortColumn = (event.active === 'pickup_date' || event.active === 'pickup_time') ?
          'pu_time' : ((event.active === 'delivery_date' || event.active === 'delivery_time') ? 'dl_time' : event.active);
        this.getOrderDataFromService('dispatch')
        break;
      case 'ordersInPending':
        this.pendingSortDirection = event.direction;
        this.pendingSortColumn = (event.active === 'pickup_date' || event.active === 'pickup_time') ?
          'pu_time' : ((event.active === 'delivery_date' || event.active === 'delivery_time') ? 'dl_time' : event.active);
        this.getOrderDataFromService('pending')
        break;
      case 'draftOrders':
        this.draftOrderSortDirection = event.direction
        this.draftOrderSortColumn = (event.active === 'order_pickup_date' || event.active === 'order_pickup_time') ?
          'est_pickup_from_time' : ((event.active === 'order_delivery_date' || event.active === 'oder_delivery_time') ?
            'est_delivery_from_time' : event.active);
        this.getOrderDataFromService('draft')
        break;
    }
  }

  onClickToCopyToClipboard() {
    let textToCopy;

    if (this.containerToCopy.length !== 0) {
      textToCopy = this.containerToCopy.join(`\n`)
      this.containerToCopy = [];
      this.ordersInPendingDatasource.data.forEach(temp => temp.isContainerSelected = false);
    } else {
      textToCopy = this.ordersInPendingDatasource.data.map(temp => temp.formated_container)
        .filter(temp => temp !== "").join(`\n`)
    }

    this.clipboardService
      .copy(textToCopy)
      .then(
        (value: string): void => {

          this.sharedService.openSnackbar('Copied to clipboard', 1500, 'success-msg');

        }
      )
      .catch(
        (error: Error): void => {

          this.sharedService.openSnackbar('Problem in copying', 1500, 'warning-msg');

        }
      );
  }

  onCheckboxChange(event: MatCheckboxChange, containerNumber: string, index: number) {
    event.checked ?
      this.containerToCopy.push(containerNumber) :
      this.containerToCopy = this.containerToCopy.filter(temp => temp !== containerNumber);
    this.ordersInPendingDatasource.data[index].isContainerSelected = event.checked;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllOrdersInDispatchDataSelected() {
    const numSelected = this.selectedOrdersInDispatch.length;
    const numRows = this.ordersInDispatchDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear ordersInDispatchSelection. */
  ordersInDispatchMasterToggle(event: MatCheckboxChange) {
    this.selectedOrdersInDispatch = event.checked ?
      this.ordersInDispatchDatasource.data.map(temp => temp.order_container_chassis_id) : [];

    this.ordersInDispatchDatasource.data.forEach(row => row.is_selected = event.checked);

    event.checked ?
      this.ordersInDispatchDatasource.data
        .forEach(row => this.ordersInDispatchSelection.select(row))
      : this.ordersInDispatchSelection.clear();

    this.isDispatchDataSelected = this.isAllOrdersInDispatchDataSelected();

    this.isAllOrdersInDispatchSelected = this.isAllOrdersInDispatchDataSelected();

    this.unSelectedOrdersInDispatch = [];
  }

  ordersInDispatchToggle(selectedRow: OrderData, event: MatCheckboxChange) {
    event.checked ?
      this.selectedOrdersInDispatch.push(selectedRow.order_container_chassis_id) :
      this.selectedOrdersInDispatch = this.selectedOrdersInDispatch
        .filter(temp => temp !== selectedRow.order_container_chassis_id);

    event.checked ?
      this.unSelectedOrdersInDispatch = this.unSelectedOrdersInDispatch
        .filter(temp => temp !== selectedRow.order_container_chassis_id) :
      this.unSelectedOrdersInDispatch.push(selectedRow.order_container_chassis_id);

    let dataSelection = [];

    this.ordersInDispatchDatasource.data.forEach(row => {
      dataSelection.push(this.ordersInDispatchSelection.isSelected(row));
    });

    this.isDispatchDataSelected = event.checked ? event.checked : dataSelection.includes(true);
  }

  onClickOrderNumber(selectedRow: OrderData) {
    this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.route });
  }

  onClickEditOrder(selectedRow: OrderData) {
    this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'edit'], { relativeTo: this.route });
  }

  onDeleteOrder(selectedRow: OrderData, table: string) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Order',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.confirmAndDeleteOrder(selectedRow, table);
      }
    })
  };

  onClickCopyOrder(selectedRow: OrderData) {
    this.orderService.isOrderCopied = true;

    this.orderService.copiedOrder = selectedRow;

    this.router.navigate(['order-management', 'create']);
  }

  confirmAndDeleteOrder(selectedRow: OrderData, table: string) {

    let confirmDialogRef = this.sharedService.openConfirmation({
      action: 'confirmWithPassword',
      name: 'Order',
      cancelLabel: 'No',
      confirmLabel: 'Yes',
      confirmColor: 'green'
    })

    confirmDialogRef.subscribe(result => {
      if (result) {

        let request = {
          order_id: selectedRow.order_id,
          orderContainerId: selectedRow.order_container_chassis_id,
          email: this.sharedService.getUserDetails().email,
          password: result,
          key: table === 'dispatch' ?
            'ordersDispatchData' :
            (table === 'pending' ? 'ordersPendingData' : 'ordersDraftData')
        }

        this.orderService.deleteOrder(this.componentName, request).subscribe((response: any) => {
          if (response.code === 200) {
            this.sharedService.openSnackbar('Order successfully deleted', 2500, 'success-msg');
            this.getOrderDataFromService(table);
          } else if (response.code === 400) {
            this.sharedService.openSnackbar(response.error, 2500, 'warning-msg');
          }
        }, error => {
          if (error.status === 401) {
            this.sharedService.openSnackbar('Invalid Password', 2500, 'failure-msg');
          } else {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          }
        });
      }
    })

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllOrdersInPendingDataSelected() {
    const numSelected = this.selectedOrdersInPending.length;
    const numRows = this.ordersInPendingDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear ordersInDispatchSelection. */
  ordersInPendingMasterToggle(event: MatCheckboxChange) {
    this.selectedOrdersInPending = event.checked ?
      this.ordersInPendingDatasource.data.map(temp => temp.order_container_chassis_id) : [];

    this.ordersInPendingDatasource.data.forEach(row => row.is_selected = event.checked);

    event.checked ?
      this.ordersInPendingDatasource.data
        .forEach(row => this.ordersInPendingSelection.select(row))
      : this.ordersInPendingSelection.clear();

    this.isPendingDataSelected = this.isAllOrdersInPendingDataSelected();

    this.isAllOrdersInPendingSelected = this.isAllOrdersInPendingDataSelected();

    this.unSelectedOrdersInPending = [];
  }

  ordersInPendingToggle(selectedRow: OrderData, event: MatCheckboxChange) {
    event.checked ?
      this.selectedOrdersInPending.push(selectedRow.order_container_chassis_id) :
      this.selectedOrdersInPending = this.selectedOrdersInPending
        .filter(temp => temp !== selectedRow.order_container_chassis_id);

    event.checked ?
      this.unSelectedOrdersInPending = this.unSelectedOrdersInPending
        .filter(temp => temp !== selectedRow.order_container_chassis_id) :
      this.unSelectedOrdersInPending.push(selectedRow.order_container_chassis_id);

    let dataSelection = [];

    this.ordersInPendingDatasource.data.forEach(row => {
      dataSelection.push(this.ordersInPendingSelection.isSelected(row));
    });

    this.isPendingDataSelected = event.checked ? event.checked : dataSelection.includes(true);
  }

  ordersDraftToggle(selectedRow: OrderData, event: MatCheckboxChange) {
    event.checked ?
      this.selectedOrdersDraft.push(selectedRow.order_container_chassis_id) :
      this.selectedOrdersDraft = this.selectedOrdersDraft
        .filter(temp => temp !== selectedRow.order_container_chassis_id);
  }

  onClickDraftOrder(selectedRow: OrderData) {
    this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'draft'], { relativeTo: this.route });
  }

  onChangeOrdersPage(event: PageEvent, table: string) {
    switch (table) {
      case 'ordersInDispatch':
        this.ordersInDispatchPagination(event);
        this.getOrderDataFromService('dispatch')
        break;
      case 'ordersInPending':
        this.ordersInPendingPagination(event);
        this.getOrderDataFromService('pending')
        break;
      case 'draftOrders':
        this.draftOrderPagination(event);
        this.getOrderDataFromService('draft')
        break;
    }
  }

  ordersInDispatchPagination(event: PageEvent) {
    this.dispatchOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.dispatchLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);
  }

  ordersInPendingPagination(event: PageEvent) {
    this.pendingOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.pendingLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);
  }

  draftOrderPagination(event: PageEvent) {
    this.draftOrderOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.draftOrderLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);
  }

  generateRequest(table: string) {
    let request: OrderGetRequest = {
      offset: table === 'dispatch' ? this.dispatchOffset : (table === 'pending' ? this.pendingOffset : this.draftOrderOffset),
      limit: table === 'dispatch' ? this.dispatchLimit : (table === 'pending' ? this.pendingLimit : this.draftOrderLimit),
      columnFilter: table === 'dispatch' ? this.dispatchFilterEvent : (table === 'pending' ? this.pendingFilterEvent : this.draftOrderFilterEvent),
      filterData: table === 'dispatch' ? this.orderInDispatchFilterData : (table === 'pending' ? this.orderInPendingFilterData : this.draftOrderFilterData),
      column: table === 'dispatch' ? this.dispatchSortColumn : (table === 'pending' ? this.pendingSortColumn : this.draftOrderSortColumn),
      direction: table === 'dispatch' ? this.dispatchSortDirection : (table === 'pending' ? this.pendingSortDirection : this.draftOrderSortDirection),
      table,
      filterDate: table === 'dispatch' ? (this.dispatchFilterDate !== null ?
        this.dispatchFilterDate.toDateString() : null) :
        (this.pendingFilterDate !== null ? this.pendingFilterDate.toDateString() : null),
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }

  initialPage(table: string) {
    if (table === 'dispatch') {
      this.ordersInDispatchPaginator.pageIndex = 0;
      this.ordersInDispatchPaginator.pageSize = this.ordersInDispatchPaginator.pageSize ? this.ordersInDispatchPaginator.pageSize : 10;
      this.dispatchOffset = 1;
      this.dispatchLimit = this.ordersInDispatchPaginator.pageSize ? this.ordersInDispatchPaginator.pageSize : 10;
    } else if (table === 'pending') {
      this.ordersInPendingPaginator.pageIndex = 0;
      this.ordersInPendingPaginator.pageSize = this.ordersInPendingPaginator.pageSize ? this.ordersInPendingPaginator.pageSize : 10;
      this.pendingOffset = 1;
      this.pendingLimit = this.ordersInPendingPaginator.pageSize ? this.ordersInPendingPaginator.pageSize : 10;
    } else {
      this.draftOrderPaginator.pageIndex = 0;
      this.draftOrderPaginator.pageSize = this.draftOrderPaginator.pageSize ? this.draftOrderPaginator.pageSize : 10;
      this.draftOrderOffset = 1;
      this.draftOrderLimit = this.draftOrderPaginator.pageSize ? this.draftOrderPaginator.pageSize : 10;
    }
  }

  generateToolTip() {
    return `
    <table width="250px">
      <tr>
        <th align="left" colspan="3">Rail Tracing</th>
      </tr>
      <tr>
        <th align="left">Rail</th>
        <th align="left">Last Traced</th>
      </tr>
      ${this.railTraceHistory.map(temp => {
      return `<tr>
        <td align="left">${temp.railway}</td>
        <td align="left">${moment(temp.lastUpdatedOn).format('M/D/YY HH:mm')}</td>
        </tr>`
    })}
    </table>`
  }
}





