import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SharedService } from 'app/shared/service/shared.service';
import { OrderData } from 'app/shared/models/order.model';

@Injectable({
  providedIn: 'root'
})

export class OrderService {
  HttpUploadOptions = {
    headers: new HttpHeaders({})
  }

  private baseURL = environment.baseURL;

  public isOrderCopied: boolean = false;

  public copiedOrder: OrderData;

  httpOptions = {};
  constructor(private http: HttpClient,
    private sharedService: SharedService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_token': this.sharedService.getAuthToken()
      })
    };
  }

  updateIsDispatch(actionComponentName: String, newOrderData: any) {
    const url = `${this.baseURL + actionComponentName}/updateIsDispatch`;
    return this.http.put<any>(url, newOrderData, this.httpOptions)
  }

  /** DELETE: delete the order from the server */
  deleteOrder(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/deleteOrder`;
    return this.http.post(url, request, this.httpOptions)
  }

  getRailTraceHistory(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getRailTraceHistory`;
    return this.http.get(url, this.httpOptions)
  }
}