import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './order.component';
import { LegComponent } from './leg/leg.component';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { SequenceResolver } from 'app/shared/resolver/sequence.resolver';
import { EditOrderComponent } from 'app/shared/modules/order-component/edit-order/edit-order.component';
import { DriverinfoComponent } from 'app/shared/modules/driver-info-module/driverinfo.component';

const OrderRoutes: Routes = [
  {
    path: '',
    component: OrderComponent
  },
  {
    path: 'create',
    component: EditOrderComponent,
    resolve: {
      'sequence': SequenceResolver
    }
  },
  {
    path: ':orderNumber/:sequence/edit',
    component: EditOrderComponent,
    resolve: {
      'editOrder': OrderLegResolver
    }
  },
  {
    path: ':orderNumber/:sequence/draft',
    component: EditOrderComponent,
    resolve: {
      'editOrder': OrderLegResolver
    }
  },
  {
    path: ':orderNumber/:sequence/leg',
    component: LegComponent,
    resolve: {
      'orderLeg': OrderLegResolver
    }
  },
  {
    path: ':orderNumber/:sequence/leg/driver-info',
    component: DriverinfoComponent,
    resolve: {
      'driverData': DriverInfoResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(OrderRoutes)],
  exports: [RouterModule]
})

export class OrderRoutingModule { }
