import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { TooltipModule } from 'ng2-tooltip-directive';

import { OrderComponent } from './order.component';
import { LegComponent } from './leg/leg.component';
import { OrderRoutingModule } from './order-routing.module';
import { loadingInterceptor } from '../shared/interceptor/loading.interceptor';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { SequenceResolver } from 'app/shared/resolver/sequence.resolver';
import { OrderSharedModule } from 'app/shared/modules/order-component/order-component.module';
import { DriverInfoModule } from 'app/shared/modules/driver-info-module/driver-info-module.module';
import { CustomerSharedModule } from 'app/shared/modules/customer-shared/customer-shared.module';

@NgModule({
  imports: [
    OrderSharedModule,
    HttpClientModule,
    DriverInfoModule,
    CustomerSharedModule,
    TooltipModule,
    OrderRoutingModule
  ],
  declarations: [
    OrderComponent,
    LegComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    },
    OrderLegResolver,
    DriverInfoResolver,
    SequenceResolver
  ]
})

export class OrderComponentsModule { }
