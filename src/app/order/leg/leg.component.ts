import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';

import { MatTableDataSource, MatDialogConfig, MatDialog, MatSort, MatPaginator } from '@angular/material';

import * as moment from 'moment';

import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { LegData } from 'app/shared/models/leg.model';
import { OrderData } from 'app/shared/models/order.model';
import { loginData } from 'app/session/session.model';
import { AccessoriesData } from 'app/shared/models/accessories.model';
import { DriverDetailsData } from 'app/driver/DriverData';
import { Subscription } from 'rxjs';
import { EditLegComponent } from 'app/shared/modules/order-component/edit-leg/edit-leg.component';
import { UploadDocsComponent } from 'app/shared/modules/order-component/upload-docs/upload-docs.component';
import { AssignDriverComponent } from 'app/shared/modules/order-component/assigndriver/assigndriver.component';
import { PrintLoadSheetComponent } from 'app/shared/modules/order-component/print-load-sheet/print-load-sheet.component';
import { EditAccessoriesComponent } from 'app/shared/modules/accessories-shared/edit-accessories.component';

@Component({
  selector: 'app-leg',
  templateUrl: './leg.component.html',
  styleUrls: ['./leg.component.scss']
})
export class LegComponent implements OnInit {

  public dropdownComponentName: String = "configurations";

  public legTypes: { label: string; value: number }[];

  public uploadComponentName: string = "uploaddocumentlinks";

  public legComponentName: String = "legs";

  public orderComponentName: String = "orders";

  public generalAccessoriesComponent: String = "accessorial_charges";

  public accessoriesNames: String[];

  public accessoriesCharges: any;

  public transactionAccessoriesComponent = "order_accessorial_charges";

  public selectedOrder: OrderData;

  public isDispatch: boolean = true;

  public uploadedDocumentTemplate: string;

  public isDocumentUploaded: boolean = false;

  public uploadedDocumentList: string[];

  public isStreetTurn: boolean = false;

  public isHub: boolean = false;

  public legDatasource: MatTableDataSource<LegData>;

  @ViewChild('legSort') legSort: MatSort;

  public statuses: { label: string; value: number }[] = [
    { label: "Open", value: 1 },
    { label: "Assigned", value: 2 },
    { label: "Picked Up", value: 3 },
    { label: "Delivered", value: 4 }
  ];

  public editEnableColor: string = '#ff9800';

  public editDisableColor: string = '#e6bc86';

  public deleteEnableColor: string = '#f44336';

  public deleteDisableColor: string = '#ffafa9';

  public legColumns: string[] = ['leg_number', 'leg_type', 'driver_name', 'deadline', 'leg_status',
    'pu_name', 'pickup_time', 'dl_name', 'delivery_time', 'est_miles', 'dmg', 'container_number', 'chassis_number', 'action'];

  @ViewChild('legsPaginator') legsPaginator: MatPaginator;

  public loadedLegDatasource: MatTableDataSource<any>;

  public loadedLegColumns: string[] = ['leg_number', 'leg_type', 'pu_name', 'dl_name',
    'est_miles', 'rate', 'action'];

  public chargesInlineInput: { miles: any, rate: any } = { miles: 0, rate: 0 };

  public selectedLegId: number = 0;

  public accessoriesDatasource: MatTableDataSource<AccessoriesData>;

  @ViewChild('accessoriesSort') accessoriesSort: MatSort;

  public loggedInUser: loginData;

  public accessoriesColumns: string[] = ['accessories_name', 'accessories_interval', 'accessories_value',
    'description', 'rate', 'action'];

  @ViewChild('accessoriesPaginator') accessoriesPaginator: MatPaginator;

  public legCount: number = 0;

  public legInjectData: any;

  public legDuplicate: LegData[];

  public streetTurnData: LegData;

  public selectedHubLeg: LegData;

  public dialogConfig = new MatDialogConfig();

  public showMobileView: boolean = false;

  public socketSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private router: Router,
    private socketService: SocketService,
    private sharedService: SharedService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.legDatasource = new MatTableDataSource();

    this.loadedLegDatasource = new MatTableDataSource();

    this.accessoriesDatasource = new MatTableDataSource();

    this.loggedInUser = this.sharedService.getUserDetails();

    this.loggedInUser.role === 'Dispatch' ? this.loadedLegColumns.pop() : null;

    this.route.data.subscribe((data: Data) => {
      this.selectedOrder = data['orderLeg'].length !== 0 ? data['orderLeg'][0] : this.router.navigate(['/error/404']);
      if (this.selectedOrder) {
        this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
          moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
        this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
          moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

        this.loadedLegDatasource.data = [{
          leg_number: 1,
          leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
          pu_name: this.selectedOrder.pu_name,
          pu_loc: this.selectedOrder.pu_loc,
          dl_name: this.selectedOrder.dl_name,
          dl_loc: this.selectedOrder.dl_loc,
          est_miles: this.selectedOrder.miles,
          rate: this.selectedOrder.rate,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          pu_street: this.selectedOrder.pu_street,
          pu_city: this.selectedOrder.pu_city,
          pu_state: this.selectedOrder.pu_state,
          pu_postal_code: this.selectedOrder.pu_postal_code,
          dl_street: this.selectedOrder.dl_street,
          dl_city: this.selectedOrder.dl_city,
          dl_state: this.selectedOrder.dl_state,
          dl_postal_code: this.selectedOrder.dl_postal_code
        }];
      }
    })

    this.getLegAccessoriesDataFromService();

    this.getUploadedDocumentListFromService();

    this.getAccessorialDropdownNamesFromService();

    this.getAccessorialPrefillChargesFromService();

    this.getLegTypesDataFromService();

    this.dialogConfig.disableClose = true;

    this.dialogConfig.autoFocus = true;

    this.showMobileView = window.innerWidth < 400 ? true : false;
  }

  getLegAccessoriesDataFromService(): void {
    this.sharedService.getLegAccessories(this.legComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.legDatasource.data = response.data.legs;
        this.legDatasource.paginator = this.legsPaginator;
        this.accessoriesDatasource.data = response.data.charges;
        this.accessoriesDatasource.paginator = this.accessoriesPaginator;
        this.isStreetTurn = false;
        this.streetTurnData = undefined;
        this.isHub = false;
        this.legCount = this.legDatasource.data.length;
        this.legDuplicate = [... this.legDatasource.data];
        this.legDuplicate.push(this.generateLegData());
        this.legInjectData = {
          dl_loc: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_loc : null,
          dl_name: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_name : null,
          leg_number: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].leg_number + 1 : 1,
          order_id: this.selectedOrder.order_id,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          container_number: this.selectedOrder.container_number,
          chassis_number: this.selectedOrder.chassis_number,
          hazmat_req: this.selectedOrder.hazmat_req,
          username: `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
          format_address: this.selectedOrder.format_address,
          customerId: this.selectedOrder.customer_id,
          orderTypeId: this.selectedOrder.order_type_id,
          order_type: this.selectedOrder.order_type,
          isDispatch: this.selectedOrder.is_dispatch,
          is_brokered: this.selectedOrder.is_brokered,
          carrier_id: this.selectedOrder.carrier_id,
          carrier_name: this.selectedOrder.carrier_name
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getUploadedDocumentListFromService(): void {
    this.sharedService.getOrderUploadedDocumentList(this.uploadComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.uploadedDocumentList = response.data;
        this.isDocumentUploaded = response.data.length !== 0;
        let checkFileList = response.data.map(temp => temp.split('/')[2].split('.')[0]);
        this.uploadedDocumentTemplate = this.generateTemplateForTooltip(checkFileList);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialDropdownNamesFromService(): void {
    this.sharedService.getAccessoriesNameList(this.generalAccessoriesComponent)
      .subscribe(accessoriesnamelist => {
        this.accessoriesNames = accessoriesnamelist;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialPrefillChargesFromService(): void {
    this.sharedService.getAccessoriesChargeList(this.generalAccessoriesComponent, this.selectedOrder.customer_id)
      .subscribe(accessoriesChargesList => {
        this.accessoriesCharges = accessoriesChargesList;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getLegTypesDataFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'leg')
      .subscribe(response => {
        this.legTypes = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getSelectedOrder() {
    this.sharedService.getSelectedOrder(this.orderComponentName,
      this.selectedOrder.order_number, this.selectedOrder.container_sequence).subscribe((response: OrderData[]) => {
        this.selectedOrder = response.length !== 0 ? response[0] : null;
        if (this.selectedOrder) {
          this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
            moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
          this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
            moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

          this.loadedLegDatasource.data = [{
            leg_number: 1,
            leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
            pu_name: this.selectedOrder.pu_name,
            pu_loc: this.selectedOrder.pu_loc,
            dl_name: this.selectedOrder.dl_name,
            dl_loc: this.selectedOrder.dl_loc,
            est_miles: this.selectedOrder.miles,
            rate: this.selectedOrder.rate,
            order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
            pu_street: this.selectedOrder.pu_street,
            pu_city: this.selectedOrder.pu_city,
            pu_state: this.selectedOrder.pu_state,
            pu_postal_code: this.selectedOrder.pu_postal_code,
            dl_street: this.selectedOrder.dl_street,
            dl_city: this.selectedOrder.dl_city,
            dl_state: this.selectedOrder.dl_state,
            dl_postal_code: this.selectedOrder.dl_postal_code
          }];
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onClickEditOrder() {
    this.router.navigate(['/order-management/', this.selectedOrder.order_number, this.selectedOrder.container_sequence,
      'edit'], { relativeTo: this.route, skipLocationChange: true });
  }

  onClickAddLeg() {
    this.dialogConfig.data = {
      "modeSelect": false,
      "legTypes": this.legTypes,
      "commonData": this.legInjectData,
      "legDuplicate": this.legDuplicate,
      "isLeg": true
    };
    let dialogRef = this.dialog.open(EditLegComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.postLeg(this.legComponentName, result).subscribe(
          response => {
            if (response.code === 200) {
              this.getLegAccessoriesDataFromService();
            } else {
              this.sharedService.openSnackbar("Order Closed. Cannot Add New leg", 2500, "warning-msg");
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  };

  onClickViewOrUpload() {
    this.dialogConfig.data = {
      "containerLevelId": this.selectedOrder.order_container_chassis_id,
      "uploadedDocumentList": this.uploadedDocumentList,
      "fileName": `orders/${this.selectedOrder.order_number}-${this.selectedOrder.container_sequence}/`,
      "isManualInvoice": false
    };
    let dialogRef = this.dialog.open(UploadDocsComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; tempPreviewFile: any, isDeleted: boolean, cancel: boolean }) => {
      if (result) {
        if (!result.isDeleted && !result.cancel) {
          let fields = {
            id: this.selectedOrder.order_container_chassis_id,
            order_number: this.selectedOrder.order_number,
            sequence: this.selectedOrder.container_sequence,
            userId: this.loggedInUser.userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadOrderfile(this.uploadComponentName, result.fileFormData).subscribe(response => {

            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement;

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getUploadedDocumentListFromService();

              this.socketSubscription.unsubscribe();
            });

          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else if (result.isDeleted && result.cancel) {
          this.getUploadedDocumentListFromService();
        }
      }
    });
  };

  onClickDriverName(selectedRow: LegData) {
    this.router.navigate(['driver-info'], { queryParams: { driverId: selectedRow.driver_id }, relativeTo: this.route });
  }

  onClickAssignDriver(selectedRow: LegData, index: number) {
    this.dialogConfig.data = {
      "driverId": index === 0 ? null : this.legDatasource.data[index - 1].driver_id,
      "isHazmat": this.selectedOrder.hazmat_req,
      "pickupLocation": selectedRow.pu_loc,
      "pickupLocationName": selectedRow.pu_name
    };
    if (index === 0) {
      this.openPopupToSelectDriver(this.dialogConfig, selectedRow)
    } else if (index !== 0 && this.legDatasource.data[index - 1].driver_id !== null) {
      let dialogRef = this.sharedService.openConfirmation({
        action: "sameDriver",
        name: this.legDatasource.data[index - 1].driver_name,
        cancelLabel: "No",
        confirmColor: "#4d97f3",
        confirmLabel: "Yes"
      });
      dialogRef.subscribe(result => {
        if (result) {
          let request = {
            "order_id": selectedRow.order_id,
            "order_container_chassis_id": selectedRow.order_container_chassis_id,
            "leg_id": selectedRow.leg_id,
            "driver_id": this.legDatasource.data[index - 1].driver_id,
            "sparseTitle": "driver"
          }
          this.sharedService.sparseUpdateLegData(this.legComponentName, request).subscribe(response => {
            if (response.code === 200) {
              this.socketService.sendChangesInDriverFromClient('both');
              this.socketService.sendNotification({
                role: 'Dispatch',
                msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is Assigned`,
                hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
                icon: 'event_note',
                styleClass: 'mat-deep-purple'
              })
              this.getLegAccessoriesDataFromService();
              this.getSelectedOrder();
            } else if (response.code === 103) {
              this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else {
          this.openPopupToSelectDriver(this.dialogConfig, selectedRow)
        }
      });
    } else if (index !== 0 && this.legDatasource.data[index - 1].driver_id === null) {
      this.sharedService.openSnackbar("Please Assign Driver From Top Bottom", 2000, "warning-msg");
    }
  }

  openPopupToSelectDriver(dialogConfig: MatDialogConfig, selectedRow: LegData) {
    let dialogRef = this.dialog.open(AssignDriverComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: DriverDetailsData) => {
      if (result) {
        let request = {
          "order_id": selectedRow.order_id,
          "order_container_chassis_id": selectedRow.order_container_chassis_id,
          "leg_id": selectedRow.leg_id,
          "driver_id": result.driver_id,
          "sparseTitle": "driver"
        }
        this.sharedService.sparseUpdateLegData(this.legComponentName, request).subscribe(response => {
          if (response.code === 200) {
            this.socketService.sendNotification({
              isDriver: true,
              driverUserId: result.user_id,
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} has been assigned to you`,
              hyperlink: `/driver-management/driver-detail/driver-info?driverId=${result.driver_id}`,
              icon: 'info',
              styleClass: 'mat-text-accent'
            });
            this.socketService.sendChangesInDriverFromClient('both');
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is Assigned`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
            this.getLegAccessoriesDataFromService();
            this.getSelectedOrder();
          } else if (response.code === 103) {
            this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        });
      }
    });
  }

  onClickChangeStatus(selectedRow: LegData, status: string, index: number) {
    let request = {
      "leg_id": selectedRow.leg_id,
      "order_id": selectedRow.order_id,
      "order_container_chassis_id": selectedRow.order_container_chassis_id,
      "leg_number": selectedRow.leg_number,
      "driver_id": selectedRow.driver_id,
      "previousStatus": selectedRow.leg_status,
      "currentStatus": status,
      "lastLeg": this.legDatasource.data.length,
      "pu_loc": selectedRow.pu_loc,
      "pu_name": selectedRow.pu_name,
      "is_brokered": selectedRow.is_brokered,
      "carrier_id": selectedRow.carrier_id
    }

    if (selectedRow.driver_name || selectedRow.is_brokered) {
      if (selectedRow.leg_status === "Open" && status === "Assigned") {
        request['isTopBottom'] = true;
        this.topToBottomServiceCall(selectedRow, request);
      } else if (selectedRow.leg_status === "Assigned" && status === "Open") {
        request['isTopBottom'] = false;
        this.bottomToTopServiceCall(selectedRow, request, "Driver");
      }
    } else {
      if (selectedRow.pu_loc && selectedRow.dl_loc
        && selectedRow.pu_name && selectedRow.dl_name) {
        this.onClickAssignDriver(selectedRow, index);
      } else {
        this.sharedService.openSnackbar("Please Complete the Pickup and Delivery Address to Continue", 3000, "warning-msg")
      }
    }
  }

  topToBottomServiceCall(selectedRow: LegData, request) {
    this.sharedService.updateLegStatusTopBottom(this.legComponentName, request).subscribe(response => {
      if (response.code === 200) {
        this.socketService.sendChangesInDriverFromClient('both');
        this.socketService.sendNotification({
          role: 'Dispatch',
          msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is ${response.endStatus}`,
          hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        })
        this.getLegAccessoriesDataFromService();
        this.getSelectedOrder();
      } else if (response.code === 103) {
        this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
      }
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
    })
  }

  bottomToTopServiceCall(selectedRow: LegData, request, reset) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "bottomTop",
      previousName: request.previousStatus,
      name: request.currentStatus,
      reset,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.updateLegStatusBottomTop(this.legComponentName, request).subscribe(response => {
          if (response.code === 200) {
            this.socketService.sendChangesInDriverFromClient('both');
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is ${response.endStatus}`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
            this.getLegAccessoriesDataFromService();
            this.getSelectedOrder();
          } else {
            this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
        })
      }
    })
  }

  onClickSortLeg(selectedRow: LegData, isUp: boolean, index: number) {
    if (this.legDatasource.data[index].leg_status === selectedRow.leg_status &&
      this.legDatasource.data[index].driver_id === selectedRow.driver_id) {
      let request = {
        "leg_number": selectedRow.leg_number,
        "order_id": selectedRow.order_id,
        "order_container_chassis_id": selectedRow.order_container_chassis_id,
        isUp
      }
      this.sharedService.rearrangeLegNumber(this.legComponentName, request).subscribe(response => {
        this.getLegAccessoriesDataFromService();
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      })
    } else {
      this.sharedService.openSnackbar("status & driver is not same", 2500, "warning-msg")
    }
  }

  onClickEditLeg(selectedRow: LegData) {
    this.dialogConfig.data = {
      "selectedValue": selectedRow,
      "modeSelect": true,
      "legTypes": this.legTypes,
      "commonData": this.legInjectData,
      "isLeg": true
    };
    let dialogRef = this.dialog.open(EditLegComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.sharedService.editLeg(this.legComponentName, result).subscribe(
          responseData => {
            this.getLegAccessoriesDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteLeg(selectedRow: LegData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Leg',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          order_id: selectedRow.order_id,
          containerLevelId: selectedRow.order_container_chassis_id,
          leg_number: selectedRow.leg_number,
          leg_id: selectedRow.leg_id,
          isDispatch: this.selectedOrder.is_dispatch
        }
        this.sharedService.deleteLeg(this.legComponentName, request).subscribe(
          response => {
            if (response.code === 200) {
              this.getLegAccessoriesDataFromService();
            } else {
              this.sharedService.openSnackbar("Order Closed. Cannot Add New leg", 2500, "warning-msg");
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  };

  onClickPrintLoadsheet(selectedRow: LegData) {
    if (selectedRow.driver_id !== null) {
      this.dialogConfig.data = {
        "selectedleg": selectedRow,
        "selectedorder": this.selectedOrder,
        "senderAddress": selectedRow.email
      }
      this.dialog.open(PrintLoadSheetComponent, this.dialogConfig)
    } else {
      this.sharedService.openSnackbar("Please Assign Driver", 2000, "warning-msg");
    }
  }

  onClickEditMilesAndRate(selectedRow: any) {
    this.selectedLegId = selectedRow.order_container_chassis_id;
    this.chargesInlineInput.miles = selectedRow.est_miles;
    this.chargesInlineInput.rate = selectedRow.rate;
  }

  onClickSaveMilesAndRate(selectedRow: any) {
    if (/^[0-9]*$/.test(this.chargesInlineInput.miles) && /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/.test(this.chargesInlineInput.rate)) {
      let request = {
        order_container_chassis_id: selectedRow.order_container_chassis_id,
        miles: this.chargesInlineInput.miles,
        rate: this.chargesInlineInput.rate,
        order_id: this.selectedOrder.order_id,
        customer_id: this.selectedOrder.customer_id
      }
      this.sharedService.updateOrdersMiles(this.orderComponentName, request).subscribe(response => {
        this.selectedLegId = 0;
        this.getSelectedOrder();
        this.getLegAccessoriesDataFromService();
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
    } else {
      this.sharedService.openSnackbar("miles and rate must be number", 2500, "warning-msg");
    }
  }

  onClickAddAccessoriesCharge() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': false,
      'title': "New Accessorial Charge",
      'orderFlag': true
    }
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.postAccessories(this.transactionAccessoriesComponent, result, result.order_container_chassis_id).subscribe(
          response => {
            this.accessoriesDatasource.data.unshift(response);
            this.accessoriesDatasource.paginator = this.accessoriesPaginator;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  };

  onClickEditAccessoriesCharge(selectedRow: AccessoriesData) {
    this.dialogConfig.data = {
      'selectedValue': selectedRow,
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': true,
      'title': "Accessorial Charge",
      'orderFlag': true
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.editAccessories(this.transactionAccessoriesComponent, result).subscribe(
          response => {
            this.accessoriesDatasource.data = this.accessoriesDatasource.data
              .filter(temp => temp.acc_charge_id !== response.acc_charge_id);
            this.accessoriesDatasource.data.unshift(response);
            this.accessoriesDatasource.paginator = this.accessoriesPaginator;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteAccessorialCharge(accessoryChargeId) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Accessorial Charge',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteAccessories(this.transactionAccessoriesComponent, accessoryChargeId).subscribe(
          response => {
            this.accessoriesDatasource.data = this.accessoriesDatasource.data.filter(temp => temp.acc_charge_id !== accessoryChargeId);
            this.accessoriesDatasource.paginator = this.accessoriesPaginator;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  };

  generateTemplateForTooltip(checkFileList) {
    let wrong = `❌`;
    let correct = `✔️`;
    return `
    <table>
      <tr>
        <th align="center" colspan="2">Uploaded Documents</th>
      </tr>
      <tr>
      <td align="left">Prenote</td>
      <td align="right">${checkFileList.includes('prenote') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Proof of Delivery</td>
      <td align="right">${checkFileList.includes('proof_of_delivery') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Outgate</td>
      <td align="right">${checkFileList.includes('outgate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Ingate</td>
      <td align="right">${checkFileList.includes('ingate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Bill of Lading</td>
      <td align="right">${checkFileList.includes('bill_of_landing') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Miscellaneous</td>
      <td align="right">${checkFileList.includes('miscellaneous') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Hazmat</td>
      <td align="right">${checkFileList.includes('hazmat') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Chassis</td>
      <td align="right">${checkFileList.includes('chassis') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Interchange</td>
      <td align="right">${checkFileList.includes('interchange') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Scale Tickets</td>
      <td align="right">${checkFileList.includes('scale_tickets') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Incidents</td>
      <td align="right">${checkFileList.includes('incidents') ? correct : wrong}</td>
      </tr>
    </table>`
  }

  generateLegData(): LegData {
    return {
      chassis_number: this.selectedOrder.chassis_number,
      container_number: this.selectedOrder.container_number,
      pu_loc: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_loc : null,
      pu_name: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_name : null,
      dl_loc: null,
      dl_name: null,
      leg_number: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].leg_number + 1 : 1,
      pu_time: null,
      dl_time: null,
      driver_id: null,
      leg_type_id: null,
      driver_name: null,
      leg_status: 'Open'
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.showMobileView = event.target.innerWidth < 400 ? true : false;
  }

}
