import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { loginRequest, loginResponse } from './session.model';

@Injectable()

export class SessionService {

  private baseURL = environment.baseURL;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  setNewPassword(componentName: String, newPassword: string, access_token: string) {
    let validatehttpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        access_token,
        newPassword
      })
    }
    let body = new HttpParams();
    body = body.set('newPassword', newPassword);
    let url = `${this.baseURL}${componentName}/reset-password?access_token=${access_token}`
    return this.httpClient.post<any>(url, body, validatehttpOptions)
  }

  loginUser(componentName: String, request: loginRequest) {
    let url = `${this.baseURL}${componentName}/login`
    return this.httpClient.post<loginResponse>(url, request, this.httpOptions)
  }

  resetPassword(componentName: String, request: { email: string }) {
    let url = `${this.baseURL}${componentName}/reset`
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

}
