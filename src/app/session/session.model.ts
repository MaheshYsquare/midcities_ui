export interface loginRequest {
    email: string;
    password: string
}

export interface loginResponse {
    code: number,
    data: loginData,
    error: string
}

export interface loginData {
    id: string,
    ttl: number,
    created: Date,
    userId: number,
    emailVerified: boolean,
    first_name: string,
    is_signed_up: any,
    last_name: string,
    role: string,
    role_permissions: any,
    user_status: any,
    permissions: any,
    notification_toggle: any;
    table_view?: any;
    driverId?: number
    email: any;
}