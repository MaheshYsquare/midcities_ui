import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { ActivatedRoute } from '@angular/router';

import { SessionService } from '../session.service';
import { matchingPasswords } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {

  public setNewPasswordForm: FormGroup; // variable for the reactive form

  public passwordShowHide = true; // variable for the password show or hide options

  public confirmPasswordShowHide = true; // variable for the confirm password show or hide options

  public resetToken: any; // variable to store retrieved token from query params

  public resetSuccess: boolean = false; // flag once reset email triggerd

  public errorMessage: string; // to store error string

  /* injected services and routing strategy handler */
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public sessionService: SessionService) { }

  /* angular life cycle hooks for initial call */
  ngOnInit() {
    /*  retrieve token from query param and store it in variable */
    this.resetToken = this.route.snapshot.queryParams['access_token'];

    /* initiated the reactive form with input declared in it with validation condition */
    this.setNewPasswordForm = this.formBuilder.group({
      newPassword: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^\s]{8,}$/)]),
      confirmPassword: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^\s]{8,}$/)])
    }, { validator: matchingPasswords('newPassword', 'confirmPassword') });
  }

  /* this function will trigger,
 check for the validation and make api call to trigger reset email */
  onSubmit() {
    if (this.setNewPasswordForm.valid) {
      this.sessionService.setNewPassword("users", this.setNewPasswordForm.get("newPassword").value, this.resetToken).subscribe(
        response => {
          this.resetSuccess = true;
          this.setNewPasswordForm.disable();
        }, error => {
          this.errorMessage = 'Unauthorized User'
        }
      );
    }
  }
}
