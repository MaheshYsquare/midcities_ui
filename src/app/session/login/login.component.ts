import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { SessionService } from '../session.service';
import { loginRequest } from '../session.model';
import { MenuService } from 'app/shared/service/menu-items.service.';

@Component({
  selector: 'ms-login-session',
  templateUrl: './login-component.html',
  styleUrls: ['./login-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup; // variable for the reactive form

  public passwordShowHide: boolean = true; // variable for the password show or hide options

  public componentName: String = "users"; // api route component name to call api regarding login

  public errorMessage: string; //variable to show the error message

  /* injected services and routing strategy handler */
  constructor(private router: Router,
    private sessionService: SessionService,
    private menuService: MenuService) { }

  /* angular life cycle hooks for initial call */
  ngOnInit() {
    /* initiated the reactive form with input declared in it with validation condition */
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/)]),
      password: new FormControl('', Validators.required)
    });
  }

  /* on click login function will trigger,
   check for the validation and make api call to login to the app  */
  onClickLogin() {
    if (this.loginForm.valid) {
      let request: loginRequest = {
        email: this.loginForm.get('email').value.trim(),
        password: this.loginForm.get('password').value
      }
      this.sessionService.loginUser(this.componentName, request).subscribe(response => {
        localStorage.setItem('userDetails', JSON.stringify(response.data));
        this.menuService.setMenuItem();
        localStorage.setItem('login', 'true');
        localStorage.setItem('notificationToggle', response.data.notification_toggle);
        localStorage.setItem('table_view', response.data.table_view)
        this.router.navigate(['/dashboard']);
      }, error => {
        this.errorMessage = error.error.code === 403 ? error.error.error : 'Invalid Email Id or Password';
      })
    }
  }

}
