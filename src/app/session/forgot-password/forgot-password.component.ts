import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { SessionService } from '../session.service';

@Component({
  selector: 'ms-forgot-password',
  templateUrl: './forgot-password-component.html',
  styleUrls: ['./forgot-password-component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class ForgotPasswordComponent implements OnInit {

  public resetForm: FormGroup; // variable for the reactive form

  public isMailTriggered: boolean = false; // variable for the email triggered or not

  /* injected services and routing strategy handler */
  constructor(private sessionService: SessionService) { }

  /* angular life cycle hooks for initial call */
  ngOnInit() {
    /* initiated the ractive form with input declared in it with validation condition */
    this.resetForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/)])
    })
  }

  /* to call the reset password api */
  onSubmit() {
    if (this.resetForm.valid) {
      this.sessionService.resetPassword("users", this.resetForm.value).subscribe(response => {
        this.isMailTriggered = true;
      }, error => {
        this.resetForm.get('email').setErrors({ emailNotFound: true })
      });
    }
  }


}



