import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginGuard } from '../shared/guard/login.guard';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewComponent } from './new/new.component';
import { SessionGuard } from '../shared/guard/session.guard';
import { SetPasswordGuard } from 'app/shared/guard/set-password.guard';

const SessionRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, 
  {
    path: '',
    children: [
      {
        path: 'login',
        canActivate: [LoginGuard],
        component: LoginComponent
      },
      {
        path: 'forgot-password',
        canActivate: [SessionGuard],
        component: ForgotPasswordComponent
      },
      {
        path: 'new',
        canActivate: [SetPasswordGuard],
        component: NewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(SessionRoutes)],
  exports: [RouterModule]
})

export class SessionRoutingModule { }