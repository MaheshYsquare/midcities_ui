import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { error } from 'app/shared/locales/error.translate';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
	selector: 'ms-error-404',
	templateUrl: './error-404-component.html',
	encapsulation: ViewEncapsulation.None
})
export class ErrorOneComponent implements OnInit {

	public error = error;

	constructor(private router: Router, public sharedService: SharedService) { }

	ngOnInit() { }

	onClick() {
		this.router.navigate(['/dashboard'])
	}
}


