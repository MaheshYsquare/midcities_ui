import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material';
import {
  MatCardModule,
  MatInputModule,
  MatRadioModule,
  MatButtonModule,
  MatProgressBarModule,
  MatToolbarModule
} from '@angular/material';

import { ErrorOneComponent } from './404/error-404.component';
import { ErrorRoutingModule } from './error-routing.module';


@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    MatProgressBarModule,
    MatToolbarModule,
    ErrorRoutingModule
  ],
  declarations: [
    ErrorOneComponent
  ]
})

export class ErrorModule { }
