import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorOneComponent } from './404/error-404.component';
import { ErrorGuard } from 'app/shared/guard/error.guard';

const ErrorRoutes: Routes = [
  {
    path: '',
    redirectTo: '404',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: '404',
        component: ErrorOneComponent,
        canActivate: [ErrorGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(ErrorRoutes)],
  exports: [RouterModule]
})

export class ErrorRoutingModule { }
