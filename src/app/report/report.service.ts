import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { BehaviorSubject, Observable } from "rxjs";

import { environment } from "environments/environment";
import { SharedService } from "app/shared/service/shared.service";

@Injectable()
export class ReportService {

    private baseURL = environment.baseURL;

    private tokenId: string;

    public httpOptions = {};

    private requestStateBehaviour = new BehaviorSubject<any>(null);

    constructor(private httpClient: HttpClient, private sharedService: SharedService) {
        this.tokenId = this.sharedService.getAuthToken();
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'access_token': this.tokenId
            })
        };
    }

    /** Allows subscription to the behavior subject as an observable */
    getState(): Observable<any> {
        return this.requestStateBehaviour.asObservable();
    }

    /**
     * Allows updating the current state of the behavior subject
     * @param request a object representing the current state
     */
    setState(request: any): void {
        this.requestStateBehaviour.next(request);
    }

    generateReport(actionComponentName: String, data: any) {
        const url = `${this.baseURL + actionComponentName}/generateReport`;
        return this.httpClient.post(url, data, this.httpOptions)
    }

    getCompletedOrder(actionComponentName: String, date_from: any, date_to: any) {
        const url = `${this.baseURL + actionComponentName}/getCompletedOrder?date_from=${date_from}&date_to=${date_to}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getDriverManifestReport(actionComponentName: String, driverId: number, date_from: any, date_to: any) {
        const url = `${this.baseURL + actionComponentName}/getDriverManifestReport?driverId=${driverId}&date_from=${date_from}&date_to=${date_to}&timezone=${Intl.DateTimeFormat().resolvedOptions().timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getDriverList(actionComponentName: String) {
        const url = `${this.baseURL + actionComponentName}/getDriverList`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getIncomeGeneratedReport(actionComponentName: String, date_from: any, date_to: any) {
        const url = `${this.baseURL + actionComponentName}/getIncomeGeneratedReport?date_from=${date_from}&date_to=${date_to}&timezone=${Intl.DateTimeFormat().resolvedOptions().timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getChassisReconcileReport(actionComponentName: String, chassis: any, date_from: any, date_to: any) {
        chassis = encodeURIComponent(chassis);

        date_from = encodeURIComponent(date_from);

        date_to = encodeURIComponent(date_to);

        let timeZone = encodeURIComponent(Intl.DateTimeFormat().resolvedOptions().timeZone);

        const url = `${this.baseURL + actionComponentName}/getChassisReconcileReport?chassis=${chassis}&date_from=${date_from}&date_to=${date_to}&timezone=${timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getRevenueGeneratedReport(actionComponentName: String, date_from: any, date_to: any) {
        date_from = encodeURIComponent(date_from);

        date_to = encodeURIComponent(date_to);

        let timeZone = encodeURIComponent(Intl.DateTimeFormat().resolvedOptions().timeZone);

        const url = `${this.baseURL + actionComponentName}/getRevenueGeneratedReport?date_from=${date_from}&date_to=${date_to}&timezone=${timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getCompletedMovesReport(actionComponentName: String, customerId: any, date_from: any, date_to: any) {
        customerId = encodeURIComponent(customerId);

        date_from = encodeURIComponent(date_from);

        date_to = encodeURIComponent(date_to);

        let timeZone = encodeURIComponent(Intl.DateTimeFormat().resolvedOptions().timeZone);

        const url = `${this.baseURL + actionComponentName}/getCompletedMovesReport?customerId=${customerId}&date_from=${date_from}&date_to=${date_to}&timezone=${timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getMilesReport(actionComponentName: String, driverTypeId: any, date_from: any, date_to: any) {
        driverTypeId = encodeURIComponent(driverTypeId);

        date_from = encodeURIComponent(date_from);

        date_to = encodeURIComponent(date_to);

        let timeZone = encodeURIComponent(Intl.DateTimeFormat().resolvedOptions().timeZone);

        const url = `${this.baseURL + actionComponentName}/getMilesReport?driverTypeId=${driverTypeId}&date_from=${date_from}&date_to=${date_to}&timezone=${timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getPastDueReport(actionComponentName: String, customerId: any) {
        customerId = encodeURIComponent(customerId);

        let timeZone = encodeURIComponent(Intl.DateTimeFormat().resolvedOptions().timeZone);

        const url = `${this.baseURL + actionComponentName}/getPastDueReport?customerId=${customerId}&timezone=${timeZone}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getAgingSummaryReport(actionComponentName: String) {
        const url = `${this.baseURL + actionComponentName}/getAgingSummaryReport`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getAgingDetailReport(actionComponentName: String, customerId: any) {
        const url = `${this.baseURL + actionComponentName}/getAgingDetailReport?customerId=${customerId}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    getReportNotes(componentName: string, id: any, type: any) {
        const url = `${this.baseURL + componentName}/getReportNotes?id=${id}&type=${type}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }

    updateReportNotes(componentName: string, request: any) {
        const url = `${this.baseURL + componentName}/updateReportNotes`;
        return this.httpClient.put<any>(url, request, this.httpOptions)
    }

    sendEmail(actionComponentName: String, emailData: any) {
        const url = `${this.baseURL + actionComponentName}/sendEmail`;
        return this.httpClient.post<any>(url, emailData, this.httpOptions)
    }

    fetchInvoiceDetail(actionComponentName: String, id: any) {
        const url = `${this.baseURL + actionComponentName}/fetchInvoiceDetail?id=${id}`;
        return this.httpClient.get<any>(url, this.httpOptions)
    }
}