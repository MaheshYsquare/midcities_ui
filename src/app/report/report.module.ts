import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { OwlDateTimeModule, OwlNativeDateTimeModule, DateTimeAdapter, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

import { loadingInterceptor } from 'app/shared/interceptor/loading.interceptor';
import { ReportComponent } from './report.component';
import { ReportRoutingModule } from './report-routing.module';
import { MaterialModule } from 'app/shared/demo.module';
import { ReceipientComponent } from './receipient/receipient.component';
import { MY_CUSTOM_FORMATS } from 'app/shared/models/shared.model';
import { ReportService } from './report.service';
import { ReportNotesComponent } from './report-notes/report-notes.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReportRoutingModule
  ],
  declarations: [
    ReportComponent,
    ReceipientComponent,
    ReportNotesComponent
  ],
  entryComponents: [
    ReceipientComponent,
    ReportNotesComponent
  ],
  providers: [
    ReportService,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    }
  ]
})
export class ReportModule { }
