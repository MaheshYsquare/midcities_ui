import { Component, Inject, OnInit } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { loginData } from 'app/session/session.model';
import { SharedService } from 'app/shared/service/shared.service';
import { ReportService } from '../report.service';

@Component({
  selector: 'app-report-notes',
  templateUrl: './report-notes.component.html',
  styleUrls: ['./report-notes.component.scss']
})
export class ReportNotesComponent implements OnInit {

  public componentName: string = "noteshistories";

  public reportComponentName: string = "reports";

  public user: loginData;

  public selectedNotesId: number = 0;

  public notesValue: string;

  public isEdit: boolean = false;

  constructor(private dialogRef: MatDialogRef<ReportNotesComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService,
    private reportService: ReportService) { }

  ngOnInit() {
    this.user = this.sharedService.getUserDetails();
  }

  getNotesFromService() {
    this.reportService.getReportNotes(this.reportComponentName, this.injectedData.request_id, this.injectedData.type)
      .subscribe(response => {
        this.injectedData.notes = response.data.length ? response.data[0].notes : [];
      })
  }


  onClickSaveSingleNotes() {
    let request = {
      type: this.injectedData.type,
      id: this.injectedData.id,
      notes: this.notesValue
    }
    this.reportService.updateReportNotes(this.reportComponentName, request).subscribe(response => {
      this.isEdit = false;
      this.injectedData.notes[0].notes = this.notesValue; 
    })
  }

  onClickAddNotes() {
    let notesObject = {
      created_by: this.user.first_name + " " + this.user.last_name,
      notes: null,
      notes_description: "invoice_notes",
      order_container_chassis_id: this.injectedData.id,
      notes_id: null
    }

    this.notesValue = null;

    this.injectedData.notes.push(notesObject);
  }

  onSaveNotes(selectedNote: any) {
    if (this.notesValue && this.notesValue.trim()) {

      let request = {
        'notes': this.notesValue,
        'notes_description': selectedNote.notes_description,
        'created_by': selectedNote.created_by,
        'order_container_chassis_id': this.injectedData.id,
        'user_id': this.user.userId
      }
      this.sharedService.postNotes(this.componentName, request).subscribe(response => {
        this.getNotesFromService()
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      })

    } else {
      this.sharedService.openSnackbar("Please enter notes to proceed", 2500, 'warning-msg');
    }
  }

  onCancelNotes(i) {
    this.injectedData.notes.splice(i, 1);
  }

  onSaveEditNotes(selectedNote: any) {
    if (this.notesValue && this.notesValue.trim()) {
      let request = {
        'notes': this.notesValue,
        'notes_description': selectedNote.notes_description,
        'lastupdated_by': this.user.first_name + ' ' + this.user.last_name,
        'notes_id': selectedNote.notes_id,
        'order_container_chassis_id': this.injectedData.id
      }
      this.sharedService.editNotes(this.componentName, request).subscribe(response => {
        this.getNotesFromService();
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      })
    }

  }

  onDeleteNotes(notesId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Note',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteNotes(this.componentName, notesId, this.injectedData.id).subscribe(response => {
          this.getNotesFromService()
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    })
  }

  onClose() {
    this.dialogRef.close();
  }

}
