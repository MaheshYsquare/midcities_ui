import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';

import { MatTableDataSource, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';

import { Subscription } from 'rxjs';

import * as moment from 'moment';
import { orderBy } from 'lodash';

import { ReportService } from './report.service';
import { ReceipientComponent } from './receipient/receipient.component';
import { SharedService } from 'app/shared/service/shared.service';
import { reportingColumnFields } from './report.model';
import { ReportNotesComponent } from './report-notes/report-notes.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnDestroy {

  public reportComponentName: string = 'reports';

  public reportForm: FormGroup;

  public reports: { label: string; value: string }[] = [
    { label: "Aging Report", value: "agingReport" },
    { label: "Chassis Invoicing", value: "chassisInvoice" },
    { label: "Completed Orders", value: "completedOrder" },
    { label: "Driver Manifest", value: "driverManifest" },
    { label: "Income Generated", value: "incomeGenerated" },
    { label: "Miles", value: "miles" },
    { label: "Moves Completed For Customer", value: "completedMoves" },
    { label: "Past Due Report for Customer", value: "pastDue" },
    { label: "Revenue Generated", value: "revenueGenerated" }
  ]

  public reportType: string;

  public drivers: { driver_name: string; driver_id: number }[];

  public customers: any[];

  public driverTypes: { label: string; value: number; }[];

  public rangeMaxTime: Date = new Date();

  public hideOrShow: { customer: boolean; driver: boolean; chassis: boolean; driverType: boolean; date: boolean } = {
    customer: false,
    driver: false,
    driverType: false,
    date: true,
    chassis: false
  };

  public reportData: any;

  public reportingDatasource: MatTableDataSource<any>;

  public cloneReportingData: any;

  public reportColumnFields: any[] = [];

  public currentIndex: any = 0;

  public reportingColumns: string[] = [];

  @ViewChild(MatPaginator) reportingPaginator: MatPaginator;

  public subscription: Subscription;

  public init: boolean = true;

  public revenueCard: { total_amount: any, total_invoice: any } = { total_amount: 0, total_invoice: 0 };

  public agingSummaryFooter: any = {};

  public reportTitle: string = "Aging Summary";

  constructor(private reportService: ReportService,
    private dialog: MatDialog,
    private sharedService: SharedService,
    private router: Router) { }

  ngOnInit() {
    this.subscription = this.reportService.getState().subscribe(state => {

      if (state && this.init) {

        this.reportForm = new FormGroup({
          reports: new FormControl(state.reports),
          date: new FormControl(state.date),
          driver_id: new FormControl(state.driver_id),
          customer_id: new FormControl(state.customer_id),
          chassis_number: new FormControl(state.chassis_number),
          driver_type_id: new FormControl(state.driver_type_id),
          doc_format: new FormControl(state.doc_format),
          customer_quickbooks_id: new FormControl(state.customer_quickbooks_id)
        });

        this.reportTitle = state.reports === 'agingReport' && state.customer_quickbooks_id ? 'Aging Detail' : 'Aging Summary';

        this.onReportSelectionChange({ value: state.reports }, true);

        this.init = false;
      } else if (this.init) {

        this.reportForm = new FormGroup({
          reports: new FormControl(),
          date: new FormControl(),
          driver_id: new FormControl(),
          customer_id: new FormControl(),
          chassis_number: new FormControl(),
          driver_type_id: new FormControl(),
          doc_format: new FormControl('pdf'),
          customer_quickbooks_id: new FormControl()
        })

        this.init = false;
      }
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onReportSelectionChange(event, isStateCall) {

    this.reportType = event.value;

    setTimeout(() => {
      this.hideOrShow = {
        customer: this.reportType === 'completedMoves' || this.reportType === 'pastDue',
        driver: this.reportType === 'driverManifest',
        driverType: this.reportType === 'miles',
        date: this.reportType !== 'pastDue' && this.reportType !== 'agingReport',
        chassis: this.reportType === 'chassisInvoice'
      };
    });

    if (this.reportType === 'driverManifest') {

      this.reportService.getDriverList(this.reportComponentName).subscribe(response => {
        this.drivers = response;
        isStateCall ? this.onSubmit(true) : null;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      })

    } else if (this.reportType === 'completedMoves' || this.reportType === 'pastDue') {

      this.sharedService.searchCustomerAutosuggest('customers', "all", 'dropdown').subscribe(response => {
        this.customers = response;
        isStateCall ? this.onSubmit(true) : null;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      })

    } else if (this.reportType === 'miles') {

      this.sharedService.getAllDropdown('configurations', "driver")
        .subscribe(response => {
          this.driverTypes = response.driver;
          isStateCall ? this.onSubmit(true) : null;
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
        })

    } else if (this.reportType === 'chassisInvoice') {

      if (!isStateCall) {
        this.rangeMaxTime = new Date();

        let date = new Date();

        date = new Date(date.setDate(date.getDate() - 90));

        this.reportForm.get('date').setValue([date, new Date()]);

      } else {
        this.onSubmit(true);
      }

    } else if (isStateCall) {

      this.onSubmit(true);

    }
  }

  onSubmit(isStateCall?, isReset?) {

    if (this.reportForm.valid) {

      isReset ? this.reportTitle = "Aging Summary" : null;

      !isStateCall ?
        this.reportService.setState(this.reportForm.value) : null;

      this.reportingDatasource = new MatTableDataSource();

      this.reportColumnFields = this.reportType === 'agingReport' ? reportingColumnFields[this.reportType]["Aging Summary"] :
        reportingColumnFields[this.reportType];

      this.reportingColumns = this.reportColumnFields.map(temp => temp.field);

      switch (this.reportType) {

        case 'completedOrder':
          this.reportService.getCompletedOrder(this.reportComponentName,
            formatDate(this.reportForm.value.date[0], 'y-MM-d', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-d', 'en')).subscribe(response => {

              let reportResponse = [];

              response.forEach(element => {
                element.act_delivery_time = new Date(element.act_delivery_time).toLocaleDateString()
                reportResponse.push(element)
              });

              this.onSetDataToTable(response, 'Completed Orders', false);

            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'driverManifest':

          this.reportService.getDriverManifestReport(this.reportComponentName, this.reportForm.value.driver_id,
            formatDate(this.reportForm.value.date[0], 'y-MM-d', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-d', 'en')).subscribe(response => {
              let data = [];

              response.driverRates.forEach(temp => {
                temp.legs.forEach(element => {
                  element.pu_time = element.pu_time ? moment(element.pu_time).format('MM/DD/YYYY HH:mm') : null;
                  element.dl_time = element.dl_time ? moment(element.dl_time).format('MM/DD/YYYY HH:mm') : null;
                  element.is_approved = element.is_paid ? 'Yes' : 'No';
                  data.push(element);
                });
              });

              this.onSetDataToTable(data, null, true);

              let userName = this.sharedService.getUserDetails().first_name + ' ' + this.sharedService.getUserDetails().last_name;

              this.reportData = {
                ...response,
                loggedInUser: userName,
                print_date: moment(new Date()).format('M/D/YY'),
                print_time: moment(new Date()).format('HH:mm'),
                legs: data
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'incomeGenerated':

          this.reportService.getIncomeGeneratedReport(this.reportComponentName,
            formatDate(this.reportForm.value.date[0], 'y-MM-d', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-d', 'en')).subscribe(response => {

              this.onSetDataToTable(response, 'Income Generated', false);
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'chassisInvoice':

          this.reportService.getChassisReconcileReport(this.reportComponentName,
            this.reportForm.value.chassis_number,
            formatDate(this.reportForm.value.date[0], 'y-MM-dd', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-dd', 'en')).subscribe(response => {

              this.onSetDataToTable(response.data, 'Chassis Invoicing', false);
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'revenueGenerated':

          this.reportService.getRevenueGeneratedReport(this.reportComponentName,
            formatDate(this.reportForm.value.date[0], 'y-MM-dd', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-dd', 'en')).subscribe(response => {

              this.revenueCard = {
                total_amount: response.data.total_amount,
                total_invoice: response.data.total_invoice
              }

              this.onSetDataToTable(response.data, 'Revenue Generated', false);
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'completedMoves':

          this.reportService.getCompletedMovesReport(this.reportComponentName,
            this.reportForm.value.customer_id,
            formatDate(this.reportForm.value.date[0], 'y-MM-dd', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-dd', 'en')).subscribe(response => {

              this.onSetDataToTable(response.data, 'Moves Completed For Customer', false);
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'miles':

          this.reportService.getMilesReport(this.reportComponentName,
            this.reportForm.value.driver_type_id.toString(),
            formatDate(this.reportForm.value.date[0], 'y-MM-dd', 'en'),
            formatDate(this.reportForm.value.date[1], 'y-MM-dd', 'en')).subscribe(response => {

              this.onSetDataToTable(response.data, 'Miles', false);
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'pastDue':

          this.reportService.getPastDueReport(this.reportComponentName,
            this.reportForm.value.customer_id).subscribe(response => {

              if (response.code === 103) {
                this.sharedService.quickbooksConnection(this.onSubmit.bind(this, false));
                return;
              }

              this.onSetDataToTable(response.data.list, 'Past Due Report For Customer', true);

              this.reportData = {
                report_name: 'Past Due',
                date: moment(new Date()).format('MM/DD/YYYY'),
                data: response.data
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
            })

          break;
        case 'agingReport':

          if (this.reportTitle === 'Aging Summary') {
            this.getAgingSummaryReport(isStateCall)
          } else {
            let req = {
              customer_quickbooks_id: this.reportForm.get('customer_quickbooks_id').value
            }
            this.getAgingDetailReport(req, "Customer", isStateCall);
          }

          break;
        default:
          break;
      }
    }
  }

  getAgingSummaryReport(isStateCall) {
    this.reportService.getAgingSummaryReport(this.reportComponentName).subscribe(response => {

      if (response.code === 103) {
        this.sharedService.quickbooksConnection(this.onSubmit.bind(this, false));
        return;
      }

      this.reportTitle = "Aging Summary";

      this.reportForm.get('customer_quickbooks_id').setValue(null);

      !isStateCall ?
        this.reportService.setState(this.reportForm.value) : null;

      this.agingSummaryFooter = response.data.pop();

      this.onSetDataToTable(response.data, 'Aging Summary', true);

      this.reportData = {
        report_name: 'Aging Summary',
        date: moment(new Date()).format('MM/DD/YYYY'),
        data: response.data,
        agingFooter: this.agingSummaryFooter
      }
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg'))
  }

  onSetDataToTable(data, report_name, isManifest?) {
    this.reportingDatasource.data = report_name === 'Revenue Generated' ? data.list : data;

    this.cloneReportingData = [...this.reportingDatasource.data];

    setTimeout(() => {
      this.reportingDatasource.paginator = this.reportingPaginator;
    }, 200);

    if (!isManifest) {

      this.reportData = {
        report_name,
        date_from: formatDate(this.reportForm.value.date[0], 'y-MM-dd', 'en'),
        date_to: formatDate(this.reportForm.value.date[1], 'y-MM-dd', 'en'),
        data
      }

    }

  }

  /* client side sorting using lodash */
  onChangeSortDirection(event: any) {

    this.reportingDatasource.data = orderBy(this.reportingDatasource.data,
      event.active, event.direction);

    if (event.direction === "") {
      this.reportingDatasource.data = this.cloneReportingData;
    }
  }

  onClickHyperLink(selectedRow, field) {

    if (field === 'order_sequence') {
      this.router.navigate(['dispatch', selectedRow.order_number, selectedRow.container_sequence, 'leg']);
      return
    }

    if (field === "Customer") {
      this.getAgingDetailReport(selectedRow, field, false);
      return
    }

    if (field === "Invoice #") {
      this.reportService.fetchInvoiceDetail("invoices", selectedRow.invoice_quickbooks_id)
        .subscribe(response => {

          if (response.data.length) {

            let invoice = response.data[0];

            if (invoice.invoice_type === 'manual') {
              this.router.navigate(['invoicing', 'invoice-management', 'manual-invoice', selectedRow.invoice_number]);
            } else {
              this.router.navigate(['invoicing', 'invoice-management', invoice.order_number, invoice.container_sequence, 'leg']);
            }

            return;

          }

          this.sharedService.openSnackbar("Invoice Not Found", 2500, "warning-msg")

        }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg"))
    }

  }

  getAgingDetailReport(selectedRow, field, isStateCall) {
    this.reportService.getAgingDetailReport(this.reportComponentName, selectedRow.customer_quickbooks_id)
      .subscribe(response => {

        if (response.code === 103) {
          this.sharedService.quickbooksConnection(this.onClickHyperLink.bind(this, selectedRow, field));
          return;
        }

        this.reportTitle = "Aging Detail";

        this.reportForm.get('customer_quickbooks_id').setValue(selectedRow.customer_quickbooks_id);

        !isStateCall ?
          this.reportService.setState(this.reportForm.value) : null;

        this.reportColumnFields = reportingColumnFields[this.reportType]["Aging Detail"];

        this.reportingColumns = this.reportColumnFields.map(temp => temp.field);

        this.agingSummaryFooter = response.data.pop();

        this.onSetDataToTable(response.data, 'Aging Detail', true);

        this.reportData = {
          report_name: 'Aging Detail',
          date: moment(new Date()).format('MM/DD/YYYY'),
          data: response.data,
          agingFooter: this.agingSummaryFooter
        }

      }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg"));
  }

  onClickOpenNotes(selectedRow) {
    let id;
    let type;
    let title;

    if (this.reportTitle === "Aging Summary") {
      id = selectedRow.customer_quickbooks_id;
      type = 'customer';
      title = selectedRow.Customer;
    } else {
      id = selectedRow.invoice_quickbooks_id;
      type = 'invoice';
      title = selectedRow["Invoice #"];
    }

    this.reportService.getReportNotes(this.reportComponentName, id, type).subscribe(response => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        title,
        type,
        request_id: id,
        id: response.data.length ? response.data[0].id : null,
        isMultipleNotes: response.isInvoice,
        notes: response.isInvoice ? response.data[0].notes : response.data
      };
      this.dialog.open(ReportNotesComponent, dialogConfig);
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg"));

  }

  isGroup(index, item): boolean {
    return item.isgrouped;
  }

  onChangePage(event) {
    this.currentIndex = (event.pageIndex * event.pageSize);
  }

  onClickEmail() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = {}
    let dialogRef = this.dialog.open(ReceipientComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.reportData['format'] = this.reportForm.value.doc_format;

        this.reportData['reportTitle'] = this.reportTitle;

        let request = {
          reportType: this.reportType,
          subject: this.reportType,
          senderAddress: result,
          reportData: this.reportData,
          fileName: `${this.reportType}.${this.reportForm.value.doc_format}`
        }
        this.reportService.sendEmail(this.reportComponentName, request).subscribe(response => {
          this.sharedService.openSnackbar(response, 2000, 'success-msg');
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
        })
      }
    })
  }

  onClickView() {

    this.reportData['format'] = this.reportForm.value.doc_format;

    this.reportData['reportTitle'] = this.reportTitle;

    let request = {
      reportType: this.reportType,
      reportData: this.reportData
    }

    this.reportService.generateReport(this.reportComponentName, request).subscribe((response: any) => {
      // let blob = this.converBase64toBlob(response.$data, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
      let blob = this.converBase64toBlob(response.$data, this.reportForm.value.doc_format === 'pdf' ?
        'application/pdf' : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      let fileURL = URL.createObjectURL(blob);
      window.open(fileURL);
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
    })
  }

  converBase64toBlob(content, contentType) {
    contentType = contentType || '';
    let sliceSize = 512;
    let byteCharacters = window.atob(content); //method which converts base64 to binary
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);
      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    let blob = new Blob(byteArrays, {
      type: contentType
    }); //statement which creates the blob
    return blob;
  }
}
