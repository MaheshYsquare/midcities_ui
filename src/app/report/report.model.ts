export const reportingColumnFields = {
    completedOrder: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_number',
            label: 'Order #',
            link: 'normal'
        },
        {
            field: 'order_status',
            label: 'Order Status',
            link: 'normal'
        },
        {
            field: 'pu_loc',
            label: 'Pickup',
            link: 'normal'
        },
        {
            field: 'dl_loc',
            label: 'Delivery',
            link: 'normal'
        },
        {
            field: 'act_delivery_time',
            label: 'Completed On',
            link: 'normal'
        }
    ],
    driverManifest: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_sequence',
            label: 'Order #',
            link: 'normal'
        },
        {
            field: 'pu_name',
            label: 'Pickup',
            link: 'pickupTooltip'
        },
        {
            field: 'pu_time',
            label: 'PU Date/Time',
            link: 'normal'
        },
        {
            field: 'dl_name',
            label: 'Delivery',
            link: 'deliveryTooltip'
        },
        {
            field: 'dl_time',
            label: 'DL Date/Time',
            link: 'normal'
        },
        {
            field: 'container_number',
            label: 'Container #',
            link: 'normal'
        },
        {
            field: 'leg_type',
            label: 'Type',
            link: 'normal'
        },
        {
            field: 'accessories_names',
            label: 'Accessorial Charges',
            link: 'normal'
        },
        {
            field: 'accessories_rate',
            label: 'Accessorial Amount',
            link: 'currency'
        },
        {
            field: 'driver_rate',
            label: 'Amount',
            link: 'currency'
        },
        {
            field: 'is_approved',
            label: 'Approved',
            link: 'normal'
        }
    ],
    incomeGenerated: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'business_name',
            label: 'Customer',
            link: 'normal'
        },
        {
            field: 'paid',
            label: 'Paid',
            link: 'normal'
        },
        {
            field: 'lastPayDate',
            label: 'Last Pay Date',
            link: 'normal'
        }
    ],
    chassisInvoice: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_sequence',
            label: 'Order #',
            link: 'hyperlink'
        },
        {
            field: 'leg_number',
            label: 'Leg #',
            link: 'normal'
        },
        {
            field: 'pu_name',
            label: 'Pickup',
            link: 'pickupTooltip'
        },
        {
            field: 'pu_time',
            label: 'PU Date/Time',
            link: 'normal'
        },
        {
            field: 'dl_name',
            label: 'Delivery',
            link: 'deliveryTooltip'
        },
        {
            field: 'dl_time',
            label: 'DL Date/Time',
            link: 'normal'
        },
        {
            field: 'chassis_number',
            label: 'Chassis #',
            link: 'normal'
        },
        {
            field: 'leg_type',
            label: 'Type',
            link: 'normal'
        }
    ],
    revenueGenerated: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_sequence',
            label: 'Order #',
            link: 'normal'
        },
        {
            field: 'invoice_number',
            label: 'Invoice #',
            link: 'normal'
        },
        {
            field: 'total_amount',
            label: 'Amount',
            link: 'currency'
        },
        {
            field: 'created_on',
            label: 'Raised Date',
            link: 'normal'
        }
    ],
    completedMoves: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_sequence',
            label: 'Order #',
            link: 'normal'
        },
        {
            field: 'container_number',
            label: 'Container #',
            link: 'normal'
        },
        {
            field: 'chassis_number',
            label: 'Chassis #',
            link: 'normal'
        },
        {
            field: 'customer_reference',
            label: 'Customer Ref #',
            link: 'normal'
        },
        {
            field: 'pickup_date',
            label: 'PU Date',
            link: 'normal'
        },
        {
            field: 'pickup_time',
            label: 'PU Time',
            link: 'normal'
        },
        {
            field: 'delivery_date',
            label: 'DL Date',
            link: 'normal'
        },

        {
            field: 'delivery_time',
            label: 'DL Time',
            link: 'normal'
        }
    ],
    miles: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'order_sequence',
            label: 'Order #',
            link: 'normal'
        },
        {
            field: 'container_number',
            label: 'Container #',
            link: 'normal'
        },
        {
            field: 'leg_type',
            label: 'Leg Type',
            link: 'normal'
        },
        {
            field: 'pu_name',
            label: 'Leg PU',
            link: 'pickupTooltip'
        },
        {
            field: 'pu_state',
            label: 'Pickup State',
            link: 'normal'
        },
        {
            field: 'pickup_date',
            label: 'PU Date',
            link: 'normal'
        },
        {
            field: 'dl_name',
            label: 'Leg DL',
            link: 'deliveryTooltip'
        },
        {
            field: 'dl_state',
            label: 'Delivery State',
            link: 'normal'
        },
        {
            field: 'delivery_date',
            label: 'DL Date',
            link: 'normal'
        },
        {
            field: 'est_miles',
            label: 'Miles',
            link: 'normal'
        },
        {
            field: 'driver_type',
            label: 'Driver Type',
            link: 'normal'
        },
        {
            field: 'driver_name',
            label: 'Driver Name',
            link: 'normal'
        }
    ],
    pastDue: [
        {
            field: 's_no',
            label: 'S.No',
            link: 'serial'
        },
        {
            field: 'invoice_number',
            label: 'Invoice #',
            link: 'normal'
        },
        {
            field: 'date',
            label: 'Date',
            link: 'normal'
        },
        {
            field: 'customer_reference',
            label: 'Reference #',
            link: 'normal'
        },
        {
            field: 'container_number',
            label: 'Container #',
            link: 'normal'
        },
        {
            field: 'total_amount',
            label: 'Charge',
            link: 'currency'
        },
        {
            field: 'paid',
            label: 'Credit',
            link: 'currency'
        },
        {
            field: 'outstanding',
            label: 'Outstanding',
            link: 'currency'
        }
    ],
    agingReport: {
        "Aging Summary": [
            {
                field: 'Customer',
                label: 'Customer',
                link: 'hyperlink',
                footer: 'Customer',
                footerLink: 'normal'
            },
            {
                field: 'Current',
                label: 'Current',
                link: 'normal',
                footer: 'Current',
                footerLink: 'currency'
            },
            {
                field: 'one_thirty',
                label: '1 - 30',
                link: 'normal',
                footer: '1 - 30',
                footerLink: 'currency'
            },
            {
                field: 'thirty_sixty',
                label: '31 - 60',
                link: 'normal',
                footer: '31 - 60',
                footerLink: 'currency'
            },
            {
                field: 'sixty_ninety',
                label: '61 - 90',
                link: 'normal',
                footer: '61 - 90',
                footerLink: 'currency'
            },
            {
                field: 'ninety_over',
                label: '91 and over',
                link: 'normal',
                footer: '91 and over',
                footerLink: 'currency'
            },
            {
                field: 'Total',
                label: 'Total',
                link: 'currency',
                footer: 'Total',
                footerLink: 'currency'
            },
            {
                field: 'action',
                label: 'Action',
                link: 'icon',
                footer: 'action',
                footerLink: 'icon'
            }
        ],
        "Aging Detail": [
            {
                field: 'Date',
                label: 'Date',
                link: 'normal',
                footer: 'Date',
                footerLink: 'normal'
            },
            {
                field: 'Invoice #',
                label: 'Invoice #',
                link: 'hyperlink',
                footer: 'Invoice #',
                footerLink: 'normal'
            },
            {
                field: 'Customer',
                label: 'Customer',
                link: 'normal',
                footer: 'Customer',
                footerLink: 'normal'
            },
            {
                field: 'Due Date',
                label: 'Due Date',
                link: 'normal',
                footer: 'Due Date',
                footerLink: 'normal'
            },
            {
                field: 'Amount',
                label: 'Amount',
                link: 'normal',
                footer: 'Amount',
                footerLink: 'currency'
            },
            {
                field: 'open_balance',
                label: 'Open Balance',
                link: 'normal',
                footer: 'Open Balance',
                footerLink: 'currency'
            },
            {
                field: 'action',
                label: 'Action',
                link: 'icon',
                footer: 'action',
                footerLink: 'icon'
            }
        ]
    }
}
