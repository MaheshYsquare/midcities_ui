import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-receipient',
  templateUrl: './receipient.component.html',
  styleUrls: ['./receipient.component.scss']
})
export class ReceipientComponent implements OnInit {

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  public emailId: Array<string> = [];

  public emailValidation: Array<boolean> = [];

  public errorMessage: string;

  constructor(public dialogRef: MatDialogRef<ReceipientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.emailValidation = []

    this.emailId.forEach(temp => {
      this.emailValidation.push(/^[a-zA-Z0-9._%+-]+@[a-z.-]+\.[a-zA-Z]{2,}$/.test(temp))
    })
    
    if (this.emailId.length !== 0 && !this.emailValidation.includes(false)) {
      this.dialogRef.close(this.emailId);
    } else {
      this.errorMessage = 'Enter a Valid Email Id'
    }
  }

  onAddEmailId(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.emailId.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.errorMessage = undefined
  }

  onRemoveEmailId(email): void {
    const index = this.emailId.indexOf(email);

    if (index >= 0) {
      this.emailId.splice(index, 1);
    }

    this.errorMessage = undefined
  }



}
