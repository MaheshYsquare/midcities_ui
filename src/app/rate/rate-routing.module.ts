import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralRouteRateComponent } from './general-route-rate/general-route-rate.component';
import { AccessorialChargesComponent } from './accessorial-charges/accessorial-charges.component';
import { DriverRatesComponent } from './driver-rates/driver-rates.component';
import { SubMenuGuard } from 'app/shared/guard/sub-menu.guard';

const RateRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'general-route-rate',
        canActivate: [SubMenuGuard],
        component: GeneralRouteRateComponent
      }, {
        path: 'accessorial-charges',
        canActivate: [SubMenuGuard],
        component: AccessorialChargesComponent
      }, {
        path: 'driver-rates',
        canActivate: [SubMenuGuard],
        component: DriverRatesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(RateRoutes)],
  exports: [RouterModule]
})

export class RateRoutingModule { }
