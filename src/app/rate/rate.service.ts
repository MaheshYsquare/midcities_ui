import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SharedService } from 'app/shared/service/shared.service';
import { RouteResponse } from '../shared/models/routes.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { GeneralAccessoriesResponse, Access } from './accessorial-charges/accessorial-charges.model';
import { Rates, GeneralDriverRatesResponse } from './driver-rates/driver-rates.model';

@Injectable({
  providedIn: 'root'
})

export class RateService {
  private baseURL = environment.baseURL;
  httpOptions = {};

  constructor(private http: HttpClient,
    private sharedService: SharedService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_token': this.sharedService.getAuthToken()
      })
    };
  }

  /** GET Access from the server */
  getAccessList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getGeneralAccessoriesRates`;
    return this.http.post<GeneralAccessoriesResponse>(url, request, this.httpOptions)
  }

  /** POST: add a new hero to the server */
  openinfo(actionComponentName: String, newAccess: Access) {
    const url = `${this.baseURL + actionComponentName}/postGeneralAccessoriesRates`;
    return this.http.post<any>(url, newAccess, this.httpOptions)
  }

  /** Put: edit a new hero to the server */
  editAccessinfo(actionComponentName: String, newAccess: Access) {
    const url = `${this.baseURL + actionComponentName}/updateGeneralAccessoriesRates`;
    return this.http.put<any>(url, newAccess, this.httpOptions)
  }

  /** DELETE: delete the Access from the server */
  deleteAccess(actionComponentName: String, accessorialId: number) {
    const url = `${this.baseURL + actionComponentName}/deleteGeneralAccessoriesRates?acc_charge_id=${accessorialId}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  /** GET Driver from the server */
  getDriverList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getGeneralDriverRates`;
    return this.http.post<GeneralDriverRatesResponse>(url, request, this.httpOptions)
  }

  /** POST: add a new  Driverrate to the server */
  createrates(actionComponentName: String, newRates: Rates) {
    const url = `${this.baseURL + actionComponentName}/postGeneralDriverRates`;
    return this.http.post<any>(url, newRates, this.httpOptions)
  }

  /** Put: edit a Driver rates to the server */
  editDriverinfo(actionComponentName: String, newRatesData: Rates) {
    const url = `${this.baseURL + actionComponentName}/updateGeneralDriverRates`;
    return this.http.put<any>(url, newRatesData, this.httpOptions)
  }

  /** DELETE: delete the Access from the server */
  deleteDriver(actionComponentName: String, ratesLeg: String) {
    let id: String = ratesLeg;
    const url = `${this.baseURL + actionComponentName}/deleteGeneralDriverRates?driver_rate_id=${id}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  /** GET Routes rate from the server */
  getRoutesList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getGeneralRouteRateDetails`;
    return this.http.post<RouteResponse>(url, request, this.httpOptions)
  }

  //bulk update of general route rate
  GeneralRateBulkUpdate(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateBulkRate`;
    return this.http.put<any>(url, newCustomerData, this.httpOptions)
  }

  /** DELETE: delete the Access from the server */
  deleteRoutes(actionComponentName: String, id: number) {
    const url = `${this.baseURL + actionComponentName}/deleteGeneralRouteRate?route_rate_id=${id}`;
    return this.http.delete<any>(url, this.httpOptions)
  }
}