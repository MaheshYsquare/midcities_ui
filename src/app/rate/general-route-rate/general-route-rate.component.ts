import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource, PageEvent, MatCheckboxChange, Sort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { RateService } from '../rate.service';
import { EditRouteRateComponent } from 'app/shared/modules/route-rate-shared/edit-route-rate/edit-route-rate.component';
import { BulkUpdateComponent } from 'app/shared/modules/route-rate-shared/bulk-update/bulk-update.component';
import { SharedService } from 'app/shared/service/shared.service';
import { RoutesData, GeneralRouteRateColumnFilter } from '../../shared/models/routes.model';
import { GetRequest } from 'app/shared/models/shared.model';

@Component({
  selector: 'app-general-route-rate',
  templateUrl: './general-route-rate.component.html',
  styleUrls: ['./general-route-rate.component.scss']
})
export class GeneralRouteRateComponent implements OnInit {

  public componentName: String = "route_rates";

  public dropdownComponentName: String = "configurations";

  public locationTypes: { label: string; value: number }[];

  public columnFilter: boolean = false;

  public columnFilterValue: GeneralRouteRateColumnFilter = { pu_name: '', dl_name: '', order_type: '', distance: '', rate: '', format_address: '' };

  public orderTypes: { label: string; value: number }[];

  public formatTypes: string[] = ["P2P", "C2C", "P2C", "Z2Z", "S2S", "P2S", "S2P", "C2P"];

  public isBulkUpdateEnabled: boolean = false;

  public routeDatasource: MatTableDataSource<RoutesData>;

  public routeSelection = new SelectionModel<RoutesData>(true, []);

  public routeFilterHeaders: string[] = ['dummy', 'pickup', 'delivery', 'formatType', 'orderType', 'mile', 'amount', 'icon'];

  public routeColumns: string[] = ['checkbox', 'pu_name', 'dl_name', 'format_address', 'order_type', 'distance', 'rate', 'action'];

  @ViewChild(MatPaginator) routePaginator: MatPaginator;

  @ViewChild(MatSort) routeSort: MatSort;

  public routeLength: number = 0;

  public sortColumn: string;

  public sortDirection: string;

  public offset: number = 1;

  public limit: number = 10;

  public selectedRouteList: number[] = [];

  public unSelectedRouteList: number[] = [];

  public isAllRouteSelected: boolean = false;

  constructor(public dialog: MatDialog,
    private rateService: RateService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.routeDatasource = new MatTableDataSource();

    this.getRoutesDataFromService();

    this.getAllDropdownFromService();
  }

  getRoutesDataFromService(): void {
    let request = this.generateRequest();

    this.rateService.getRoutesList(this.componentName, request)
      .subscribe(response => {
        this.routeDatasource.data = response.data.routeData;
        this.routeLength = response.data.routePaginationLength;
        this.selectedRouteList = this.isAllRouteSelected ?
          this.routeDatasource.data.map(temp => temp.route_rate_id) : this.selectedRouteList;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg');
      })
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown('configurations', "orders,location")
      .subscribe(response => {
        this.orderTypes = response.orders;
        this.locationTypes = response.location;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getRoutesDataFromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getRoutesDataFromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getRoutesDataFromService();
  }

  onBulkUpdate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "title": "General Route Rates Bulk Update"
    };
    let dialogRef = this.dialog.open(BulkUpdateComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        var inputData = {
          "is_increase": result.operation == "Increase" ? 1 : 0,
          "increase_type": result.mode,
          "amount": result.value,
          "customer_id": 0,
          "route_rate": this.selectedRouteList.join(','),
          "isAllRouteSelected": this.isAllRouteSelected,
          columnFilter: this.columnFilter,
          columnFilterValue: this.columnFilterValue,
          unSelectedId: this.unSelectedRouteList
        }
        this.rateService.GeneralRateBulkUpdate(this.componentName, inputData).subscribe(
          responseData => {
            this.routeSelection.clear();
            this.isBulkUpdateEnabled = false;
            this.routeDatasource.data.forEach(row => row.is_selected = false);
            this.selectedRouteList = [];
            this.unSelectedRouteList = [];
            this.isAllRouteSelected = false;
            this.getRoutesDataFromService()
            this.sharedService.openSnackbar('General Route Rate BulkUpdated successfully', 2000, 'success-msg');
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg');
          })
      }
    });
  };

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selectedRouteList.length;
    const numRows = this.routeDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(event: MatCheckboxChange) {
    this.selectedRouteList = event.checked ?
      this.routeDatasource.data.map(temp => temp.route_rate_id) : [];

    this.routeDatasource.data.forEach(row => row.is_selected = event.checked);

    event.checked ?
      this.routeDatasource.data.forEach(row => this.routeSelection.select(row)) : this.routeSelection.clear();

    this.isBulkUpdateEnabled = this.isAllSelected();

    this.isAllRouteSelected = this.isAllSelected();

    this.unSelectedRouteList = [];
  }

  onChangeCheckbox(event: MatCheckboxChange, selectedRow: RoutesData) {

    event.checked ?
      this.selectedRouteList.push(selectedRow.route_rate_id) :
      this.selectedRouteList = this.selectedRouteList.filter(temp => temp !== selectedRow.route_rate_id);

    event.checked ?
      this.unSelectedRouteList = this.unSelectedRouteList.filter(temp => temp !== selectedRow.route_rate_id) :
      this.unSelectedRouteList.push(selectedRow.route_rate_id);

    let dataSelection = [];

    this.routeDatasource.data.forEach(row => {
      dataSelection.push(this.routeSelection.isSelected(row))
    });

    this.isBulkUpdateEnabled = event.checked ? event.checked : dataSelection.includes(true);
  }

  onAddGeneralRouteRate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "orderTypes": this.orderTypes,
      "locationTypes": this.locationTypes,
      "title": "Route Rate",
      "customerId": 0,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditRouteRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage();
        this.getRoutesDataFromService();
        this.sharedService.openSnackbar('General Route Rate created successfully', 2000, 'success-msg');
      }
    });
  }

  onEditGeneralRouteRate(selectedRow: RoutesData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "selectedValue": selectedRow,
      "orderTypes": this.orderTypes,
      "locationTypes": this.locationTypes,
      "title": "Route Rate",
      "customerId": 0,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditRouteRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getRoutesDataFromService();
        this.sharedService.openSnackbar('General Route Rate updated successfully', 2000, 'success-msg');
      }
    });
  }

  onDeleteGeneralRouteRate(routeRateId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Route Rate',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.rateService.deleteRoutes(this.componentName, routeRateId).subscribe(
          responseData => {
            this.getRoutesDataFromService();
            this.sharedService.openSnackbar('General Route Rate deleted successfully', 2000, 'success-msg');
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg');
          });
      }
    })
  };

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getRoutesDataFromService();
  }

  /* api service for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getRoutesDataFromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.routePaginator.pageIndex = 0;
    this.routePaginator.pageSize = this.routePaginator.pageSize ? this.routePaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.routePaginator.pageSize ? this.routePaginator.pageSize : 10;
  }
}
