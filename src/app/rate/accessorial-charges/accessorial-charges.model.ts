export interface Access {
    acc_charge_id: number;
    customer_id: number;
    accessories_name: string;
    accessories_interval: string;
    accessories_value: number;
    description: string;
    rate: string;
}

export class GeneralAccessoriesResponse {
    code: number
    error?: string
    data: {
        generalAccessoriesData: Access[]
        generalAccessoriesPaginationLength: number
    };
    disableCharges?: string[];
}

export interface GeneralAccessoriesColumnFilter {
    accessories_name: string;
    accessories_interval: string;
    accessories_value: string;
    description: string;
    rate: string;
}