import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource, PageEvent, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { RateService } from '../rate.service';
import { GeneralAccessoriesResponse, GeneralAccessoriesColumnFilter, Access } from './accessorial-charges.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { SharedService } from 'app/shared/service/shared.service';
import { EditAccessoriesComponent } from 'app/shared/modules/accessories-shared/edit-accessories.component';

@Component({
  selector: 'app-accessorial-charges',
  templateUrl: './accessorial-charges.component.html',
  styleUrls: ['./accessorial-charges.component.scss']
})
export class AccessorialChargesComponent implements OnInit {

  componentName: String = "accessorial_charges";

  displayedColumns = ['accessories_name', 'accessories_interval', 'accessories_value', 'description', 'rate', 'action'];

  public columnFilter: boolean = false;

  public columnFilterValue: GeneralAccessoriesColumnFilter = { accessories_name: '', accessories_interval: '', accessories_value: '', description: '', rate: '' };

  @ViewChild('f') filterForm: NgForm;

  public generalAccessoriesFilterHeaders: string[] = ['AccessoriesName', 'AccessoriesInterval', 'AccessoriesValue', 'Description', 'Rate', 'icon'];

  generalAccessoriesDatasource: MatTableDataSource<Access>;

  @ViewChild(MatPaginator) generalAccessoriesPaginator: MatPaginator;

  @ViewChild(MatSort) generalAccessoriesSort: MatSort;

  public accessLength: any;

  public sortColumn: any;

  public sortDirection: any;

  public pageIndex: any = 0;

  public pageSize: any = 10;

  public offset: number = 1;

  public limit: number = 10;

  public disableCharges: string[];

  ngOnInit() {
    this.getaccessDatafromService();
  }

  constructor(private router: Router,
    public dialog: MatDialog,
    private rateservice: RateService,
    public sharedService: SharedService) {
    const ACCESS_DATA: Access[] = [];
    this.generalAccessoriesDatasource = new MatTableDataSource(ACCESS_DATA);
  }

  getaccessDatafromService(): void {
    let request = this.generateRequest();

    this.rateservice.getAccessList(this.componentName, request)
      .subscribe((response: GeneralAccessoriesResponse) => {
        this.generalAccessoriesDatasource.data = response.data.generalAccessoriesData;
        this.disableCharges = response.disableCharges;
        this.accessLength = response.data.generalAccessoriesPaginationLength;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      });
  }

  /* api serivice for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getaccessDatafromService()
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getaccessDatafromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getaccessDatafromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getaccessDatafromService();
  }

  onAddAccessoryRate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'accessorialData': this.generalAccessoriesDatasource.data,
      'accessorialCharges': localStorage.getItem("fixed_acc_charges") ? JSON.parse(localStorage.getItem("fixed_acc_charges")) : [],
      'modeSelect': false,
      'title': "New Accessorial Charge",
      'customerId': 0,
      disableCharges: this.disableCharges
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.rateservice.openinfo(this.componentName, result).subscribe(
          response => {
            if (response.code === 200) {
              this.initialPage();
              this.getaccessDatafromService();
              this.sharedService.openSnackbar('Accessories Rates Created Successfully', 500, 'success-msg')
            } else if (response.code === 422) {
              this.sharedService.openSnackbar('Accessories Name Already Exists', 2500, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });
  };

  onEditAccessoryRate(selectedRow: Access) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      'accessorialData': this.generalAccessoriesDatasource.data,
      'accessorialCharges': JSON.parse(localStorage.getItem("fixed_acc_charges")),
      'modeSelect': true,
      'title': "Accessorial Charge",
      'customerId': 0,
      disableCharges: this.disableCharges
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.rateservice.editAccessinfo(this.componentName, result).subscribe(
          response => {
            if (response.code === 200) {
              this.getaccessDatafromService();
              this.sharedService.openSnackbar('Accessories Rate Updated Successfully', 500, 'success-msg')
            } else if (response.code === 422) {
              this.sharedService.openSnackbar('Accessories Name Already Exists', 2500, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });
  }

  onDeleteAccessoryRate(accessorialChargeId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Accessorial Charge',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.rateservice.deleteAccess(this.componentName, accessorialChargeId).subscribe(
          responseData => {
            this.getaccessDatafromService();
            this.sharedService.openSnackbar('Accessories Rate Deleted successfully', 500, 'success-msg')
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    })
  };

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getaccessDatafromService()
        break;
      case 15:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getaccessDatafromService()
        break;
      case 20:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getaccessDatafromService()
        break;
    }
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.generalAccessoriesPaginator.pageIndex = 0;
    this.generalAccessoriesPaginator.pageSize = this.generalAccessoriesPaginator.pageSize ? this.generalAccessoriesPaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.generalAccessoriesPaginator.pageSize ? this.generalAccessoriesPaginator.pageSize : 10;
  }
}
