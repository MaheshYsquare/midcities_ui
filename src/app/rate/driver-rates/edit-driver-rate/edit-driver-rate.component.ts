import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatRadioChange } from '@angular/material';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { Rates } from '../driver-rates.model';
import { RateService } from 'app/rate/rate.service';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-edit-driver-rate',
  templateUrl: './edit-driver-rate.component.html',
  styleUrls: ['./edit-driver-rate.component.scss']
})
export class EditDriverRateComponent implements OnInit {

  public driverRateForm: FormGroup;

  public data: Rates;

  public editMode: boolean;

  public legTypes: { label: string; value: number }[];

  public driverTypes: { label: string; value: number }[];

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public componentName: string = 'general_driver_rates';

  public isRestriction: boolean = false;

  constructor(public dialogRef: MatDialogRef<EditDriverRateComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private rateService: RateService,
    private sharedService: SharedService) {
    if (data) {
      this.editMode = data.modeSelect;
      this.driverTypes = data.driverTypes;
      this.legTypes = data.legTypes;
      this.data = data.selectedValue;
    }
  }

  ngOnInit() {
    this.initForm()
  }

  private initForm() {
    let driver_rate_id = null;
    let leg_type_id = null;
    let driver_type_id = null;
    let rates_per_mile = new FormArray([
      this.mileRateArray(1, 0, false, null)
    ])

    if (this.editMode) {
      driver_rate_id = this.data.driver_rate_id;
      leg_type_id = this.data.leg_type_id;
      driver_type_id = this.data.driver_type_id;
      rates_per_mile = new FormArray([]);
      this.data.rates_per_mile.forEach(temp => {
        rates_per_mile.push(
          this.mileRateArray(temp.fixed_rate, temp.range_from,
            temp.fixed_rate === 1 ? true : false, temp.range_to, temp.rate)
        );
      })
      this.isRestriction = true;
    }

    this.driverRateForm = new FormGroup({
      'driver_rate_id': new FormControl(driver_rate_id),
      'leg_type_id': new FormControl(leg_type_id, Validators.required),
      'driver_type_id': new FormControl(driver_type_id, Validators.required),
      'rates_per_mile': rates_per_mile
    })
  }

  mileRateArray(fixed_rate?: number, range_from?: number, isDisabled?: boolean, range_to?: number, rate?: number) {
    return new FormGroup({
      'fixed_rate': new FormControl({
        value: fixed_rate,
        disabled: this.editMode ? isDisabled : false
      }, Validators.required),

      'range_from': new FormControl(range_from, [Validators.required,
      Validators.pattern(/^[0-9]{0,9}(\.[0-9]{0,1})?%?$/)]),

      'range_to': new FormControl({
        value: range_to,
        disabled: this.editMode ? fixed_rate === 0 : false
      }, Validators.pattern(/^[0-9]{0,9}(\.[0-9]{0,1})?%?$/)),

      'rate': new FormControl(rate, [Validators.required,
      Validators.pattern(/^[0-9]{0,9}(\.[0-9]{0,2})?%?$/)]),

      'rate_label': new FormControl(fixed_rate === 1 ? 'Rate' : 'Cpm')
    })
  }

  getMileRate() {
    return (<FormArray>this.driverRateForm.get('rates_per_mile')).controls;
  }

  onClickAddMileRate() {
    if (!this.isRestriction) {
      let range_to = (<FormArray>this.driverRateForm.get('rates_per_mile'))
        .controls[this.driverRateForm.get('rates_per_mile').value.length - 1].get('range_to').value;

      (<FormArray>this.driverRateForm.get('rates_per_mile'))
        .push(this.mileRateArray(1, ((range_to * 10) + (0.1 * 10)) / 10, false, null));
    } else {
      this.sharedService.openSnackbar("Change Last Radio Button To FR to add another row", 3000, 'warning-msg');
    }
  }

  onChangeRadioButton(event: MatRadioChange, index: number) {
    let ratesPerMile = (<FormArray>this.driverRateForm.get('rates_per_mile'));

    if (!ratesPerMile.controls[index + 1]) {

      if (event.value === 0) {

        ratesPerMile.controls[index].get('rate_label').setValue('Cpm');

        ratesPerMile.controls[index].get('range_to').setValue(null);

        ratesPerMile.controls[index].get('range_to').disable();

        this.isRestriction = true;

        ratesPerMile.controls.forEach((element, j) => {
          if (index !== j) {
            element.get('fixed_rate').disable();
          }
        });
      } else {

        ratesPerMile.controls[index].get('rate_label').setValue('Rate');

        ratesPerMile.controls[index].get('range_to').enable();

        this.isRestriction = false;

        ratesPerMile.controls.forEach((element, j) => {
          if (index !== j) {
            element.get('fixed_rate').enable();
          }
        });
      }

    } else {
      ratesPerMile.controls[index].get('fixed_rate').setValue(1);

      ratesPerMile.controls[index].get('rate_label').setValue('Rate');

      this.sharedService.openSnackbar("Only last Row can be cpm", 2500, 'warning-msg');
    }

  }

  onChangeRangeTo(event: number, index: number) {
    if (this.driverRateForm.get('rates_per_mile').value.length > 1) {
      (<FormArray>this.driverRateForm.get('rates_per_mile')).controls[index + 1] ?
        (<FormArray>this.driverRateForm.get('rates_per_mile')).controls[index + 1].get('range_from')
          .setValue(((+event * 10) + (0.1 * 10)) / 10) : null;
    }
  }

  onClickRemoveMileRate(index) {
    if (!this.isRestriction) {
      setTimeout(() => {
        (<FormArray>this.driverRateForm.get('rates_per_mile')).removeAt(index);
      });
    } else {
      this.sharedService.openSnackbar("Change Radio Button To FR to delete this row", 3000, 'warning-msg');
    }
  }

  onSubmit() {
    if (this.driverRateForm.valid) {

      let ratesPerMile = this.driverRateForm.getRawValue().rates_per_mile

      let isValid = ratesPerMile[ratesPerMile.length - 1].range_to === null &&
        ratesPerMile[ratesPerMile.length - 1].fixed_rate === 0 ? true : false;

      let message = isValid ? null : "Please Change Last Radio Button To 'CPM'";

      ratesPerMile.forEach(element => {
        if (element.range_to !== null && (+element.range_to < +element.range_from)) {
          isValid = false;
          message = isValid ? null : "Range from must be less than Range To";
        }
      });

      if (this.editMode) {

        if (isValid) {
          this.rateService.editDriverinfo(this.componentName, this.driverRateForm.getRawValue()).subscribe(
            response => {
              if (response.code === 200) {
                this.dialogRef.close("success");
              } else if (response.code === 409) {
                this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
            });
        } else {
          this.sharedService.openSnackbar(message, 3000, 'warning-msg');
        }

      } else {

        if (isValid) {
          this.rateService.createrates(this.componentName, this.driverRateForm.getRawValue()).subscribe(
            response => {
              if (response.code === 200) {
                this.dialogRef.close("success");
              } else if (response.code === 409) {
                this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
            });
        } else {
          this.sharedService.openSnackbar(message, 2500, 'warning-msg');
        }

      }

    }

  }
}
