import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { RateService } from '../rate.service';
import { EditDriverRateComponent } from './edit-driver-rate/edit-driver-rate.component';
import { SharedService } from 'app/shared/service/shared.service';
import { Rates, GeneralDriverRatesResponse, GeneralDriverRatesColumnFilter } from './driver-rates.model';
import { GetRequest } from 'app/shared/models/shared.model';

@Component({
  selector: 'app-driver-rates',
  templateUrl: './driver-rates.component.html',
  styleUrls: ['./driver-rates.component.scss']
})
export class DriverRatesComponent implements OnInit {

  componentName: String = "general_driver_rates";

  dropdownComponentName: String = "configurations";

  displayedColumns = ['serialNo', 'leg_type', 'driver_type', 'action'];

  driverTypes: { label: string; value: number }[];

  legTypes: { label: string; value: number }[];

  public columnFilter: boolean = false;

  public columnFilterValue: GeneralDriverRatesColumnFilter = { leg_type: '', driver_type: '' };

  @ViewChild('f') filterForm: NgForm;

  public generalDriverRatesFilterHeaders: string[] = ['dummy', 'legType', 'driverType', 'icon'];

  generalDriverRatesDatasource: MatTableDataSource<Rates>;

  @ViewChild(MatPaginator) generalDriverRatesPaginator: MatPaginator;

  @ViewChild(MatSort) generalDriverRatesSort: MatSort;

  public driverRatesLength: any;

  public sortColumn: any;

  public sortDirection: any;

  public pageIndex: any = 0;

  public pageSize: any = 10;

  public offset: number = 1;

  public limit: number = 10;

  public currentPageIndex: number = 0;

  ngOnInit() {
    this.getratesDatafromService();
    this.getAllDropdownFromService();
  }

  constructor(private router: Router,
    public dialog: MatDialog,
    private rateservice: RateService,
    private sharedService: SharedService) {
    const Driver_DATA: Rates[] = [];
    this.generalDriverRatesDatasource = new MatTableDataSource(Driver_DATA);
  }

  getratesDatafromService(): void {
    let request = this.generateRequest();

    this.rateservice.getDriverList(this.componentName, request)
      .subscribe((response: GeneralDriverRatesResponse) => {
        this.generalDriverRatesDatasource.data = response.data.generalDriverRatesData;
        this.driverRatesLength = response.data.generalDriverRatesPaginationLength
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      });
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown('configurations', "leg,driver")
      .subscribe(response => {
        this.legTypes = response.leg;
        this.driverTypes = response.driver;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onAddDriverRate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "driverTypes": this.driverTypes,
      "legTypes": this.legTypes,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditDriverRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.initialPage();
        this.getratesDatafromService();
        this.sharedService.openSnackbar('Driver Rate Created Successfully', 2500, 'success-msg');
      }
    });
  }

  /* api serivice for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getratesDatafromService()
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getratesDatafromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getratesDatafromService();
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getratesDatafromService();
  }

  onClickSerialNo(selectedRow: Rates, serialNo: number) {
    selectedRow.serialNo = serialNo;

    this.sharedService.openInfo({
      view: "list&table_driverRates",
      name: "General Driver Rates",
      listNames: ["ID", "Driver Type", "Leg Type"],
      columnNames: ["serialNo", "driver_type", "leg_type"],
      secondaryName: "Rates Per Miles",
      secondaryColumnNames: ["S.No", "Range", "Rate"],
      selectedArray: selectedRow.rates_per_mile,
      selectedData: selectedRow
    })

  };

  onEditDriverRate(selectedRow: Rates) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      "driverTypes": this.driverTypes,
      "legTypes": this.legTypes,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditDriverRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getratesDatafromService();
        this.sharedService.openSnackbar('Driver Rate Updated Successfully', 500, 'success-msg');
      }
    });
  }

  onDeleteDriverRate(driverrateId: String) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Driver Rate',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.rateservice.deleteDriver(this.componentName, driverrateId).subscribe(
          responseData => {
            this.getratesDatafromService();
            this.sharedService.openSnackbar('Driver Rate Deleted Successfully', 500, 'success-msg');
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
          });
      }
    })
  };

  onChangePage(event) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getratesDatafromService()
        break;
      case 15:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getratesDatafromService()
        break;
      case 20:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getratesDatafromService()
        break;
    }
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.generalDriverRatesPaginator.pageIndex = 0;
    this.generalDriverRatesPaginator.pageSize = this.generalDriverRatesPaginator.pageSize ? this.generalDriverRatesPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.generalDriverRatesPaginator.pageSize ? this.generalDriverRatesPaginator.pageSize : 10;
  }
}
