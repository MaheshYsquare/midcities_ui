export interface Rates {
    serialNo?: number;
    driver_rate_id?: number;
    leg_type?: string;
    leg_type_id?: number;
    driver_type?: string;
    driver_type_id?: number;
    rates_per_mile: RatesPerMiles[];
}

export class GeneralDriverRatesResponse {
    code: number
    error?: string
    data: {
        generalDriverRatesData: Rates[]
        generalDriverRatesPaginationLength: number
    }
}

export interface RatesPerMiles {
    driver_rate_per_mile_id?: number;
    fixed_rate?: number;
    range_from: number;
    range_to: number;
    rate: number;
}

export interface GeneralDriverRatesColumnFilter {
    leg_type: string;
    driver_type: string;
}
