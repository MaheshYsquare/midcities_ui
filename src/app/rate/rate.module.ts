import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { loadingInterceptor } from '../shared/interceptor/loading.interceptor';
import { RateRoutingModule } from './rate-routing.module';
import { MaterialModule } from '../shared/demo.module';
import { GeneralRouteRateComponent } from './general-route-rate/general-route-rate.component';
import { AccessorialChargesComponent } from './accessorial-charges/accessorial-charges.component';
import { DriverRatesComponent } from './driver-rates/driver-rates.component';
import { EditDriverRateComponent } from './driver-rates/edit-driver-rate/edit-driver-rate.component';
import { RouteRateSharedModule } from 'app/shared/modules/route-rate-shared/route-rate-shared.module';
import { LocationSharedModule } from 'app/shared/modules/location-shared/location-shared.module';
import { AccessoriesSharedModule } from 'app/shared/modules/accessories-shared/accessories-shared.module';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    MaterialModule,
    LocationSharedModule,
    RouteRateSharedModule,
    AccessoriesSharedModule,
    RateRoutingModule
  ],
  declarations: [
    AccessorialChargesComponent,
    GeneralRouteRateComponent,
    DriverRatesComponent,
    EditDriverRateComponent
  ],
  entryComponents: [
    EditDriverRateComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    }
  ]
})

export class RateComponentsModule { }
