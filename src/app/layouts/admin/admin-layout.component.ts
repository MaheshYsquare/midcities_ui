import { Component, OnInit, OnDestroy, ViewChild, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { UserIdleService } from 'angular-user-idle';

import { AdminService } from '../../admin/admin.service';
import { SharedService } from 'app/shared/service/shared.service';
import { MatSnackBarConfig, MatSnackBar, MatSidenav } from '@angular/material';
import { SocketService } from 'app/shared/service/socket.service';
import { loginData } from 'app/session/session.model';
import { environment } from 'environments/environment';
import { MenuService } from 'app/shared/service/menu-items.service.';

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss'],
  providers: [SocketService]
})
export class AdminLayoutComponent implements OnInit, OnDestroy {

  private _router: Subscription;
  componentName: String = "users"

  today: number = Date.now();
  url: string;
  dark: boolean;
  boxed: boolean;
  collapseSidebar: boolean;
  compactSidebar: boolean;
  sidebarBg: boolean = true;
  currentLang = 'en';
  layoutDir = 'ltr';
  userName: string = "";
  menuLayout: any = 'vertical-menu';
  selectedSidebarImage: any = 'bg-1';
  selectedSidebarColor: any = 'sidebar-default';
  selectedHeaderColor: any = 'header-default';
  collapsedClass: any = 'side-panel-opened';
  menu: any;

  public userDetails: loginData;

  public role: string;

  public userId: number;

  public menuObj: any;

  public notificationSubscription: Subscription;

  public socketSubscription: Subscription;

  public notification: number = 0;

  public notificationData: any;

  public notificationToggle: boolean = true;

  public pushNotify: boolean = false;

  @ViewChild('sidemenu') sidemenu;
  // public config: PerfectScrollbarConfigInterface = {};

  public version: string = environment.version;

  constructor(private router: Router,
    public menuService: MenuService,
    private socketService: SocketService,
    private adminservice: AdminService,
    private userIdle: UserIdleService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.userDetails = this.sharedService.getUserDetails();

    this.notificationSubscription = this.socketService.listenServerThroughSocket('notification')
      .subscribe(response => {
        this.pushNotify = true;
        this.getNotification();
      });

    this.socketSubscription = this.socketService.listenServerThroughSocket('addSocket').subscribe(res => {
      if (this.userDetails) {
        this.socketService.sendLoginDataFromClient()
      }
    });

    this.getStateList();

    this.notificationToggle = localStorage.getItem('notificationToggle') == '1' ? true : false;

    this.userId = this.userDetails.userId;

    this.role = this.userDetails.role;

    this.getNotification();

    this.getUsersUiSettings();

    this.userName = this.userDetails.first_name + " " + this.userDetails.last_name;

    const elemSidebar = <HTMLElement>document.querySelector('.sidebar-container ');

    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac() && !this.compactSidebar && this.layoutDir != 'rtl') {
      // const ps = new PerfectScrollbar(elemSidebar, {
      //   wheelSpeed: 2,
      //   suppressScrollX: true
      // });
    }

    this._router = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      this.url = event.url;
      if (this.isOver()) {
        this.sidemenu.close();
      }

      if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac() && !this.compactSidebar && this.layoutDir != 'rtl') {
        // Ps.update(elemContent);
      }

    });

    // Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe((number) => {
      // console.log(number)
      //this.redirectToLogin();
    });

    this.checkTokenExpired()

    this.userIdle.onTimeout().subscribe(() => {
      this.redirectToLogin();
    });

    this.menu = this.menuService.getAllMenu();

    this.userIdle.ping$.subscribe(() => {
      this.checkTokenExpired();
    });
  }

  onChangeToggle(event) {
    let request = {
      column: 'notification_toggle',
      value: event.checked ? 1 : 0,
      user_id: this.userId
    }

    this.sharedService.sparseUpdateUser(this.componentName, request)
      .subscribe(response => {
        localStorage.setItem('notificationToggle', event.checked ? '1' : '0');
        this.notificationToggle = event.checked;
      })
  }

  getStateList() {
    this.sharedService.getStatesAndChargesConfig().subscribe((response: any) => {
      Object.keys(response).forEach(key => {
        localStorage.setItem(key, JSON.stringify(response[key]));
      })
    })
  }

  getUsersUiSettings() {
    this.sharedService.getUsersUiSettings('users', this.userId).subscribe((response: any) => {

      let permissions = this.userDetails ? this.userDetails.permissions.split(',') : [];

      if (permissions.includes('Order Management')) {

        localStorage.setItem('orders_dispatch',
          response.data.orders_dispatch ?
            response.data.orders_dispatch : '[]');

        localStorage.setItem('orders_pending',
          response.data.orders_pending ?
            response.data.orders_pending : '[]');

        localStorage.setItem('orders_draft',
          response.data.orders_draft ?
            response.data.orders_draft : '[]');
      }

      if (permissions.includes('Dispatch')) {
        localStorage.setItem('dispatch_view',
          response.data.dispatch_view ?
            response.data.dispatch_view : '[]');
      }

      if (permissions.includes('Invoicing')) {

        localStorage.setItem('invoice_view',
          response.data.invoice_view ?
            response.data.invoice_view : '[]');

        localStorage.setItem('invoice_to_generated_view',
          response.data.invoice_to_generated_view ?
            response.data.invoice_to_generated_view : '[]');

      }

    })
  }

  getNotification() {
    this.sharedService.getUserNotification('notifications', this.userId).subscribe((response: any) => {
      this.notification = response.length !== 0 ? response.filter(temp => temp.is_viewed == false).length : 0;
      this.notificationData = response;
      if (this.notificationToggle && this.pushNotify) {
        let snackbarConfig = new MatSnackBarConfig();
        snackbarConfig.horizontalPosition = 'right';
        snackbarConfig.duration = 3000;
        snackbarConfig.panelClass = ['snackbar-transparent'];
        this.snackBar.open(`You have ${this.notification} new notification`, null, snackbarConfig);
        this.pushNotify = false;
      }
    })
  }

  updateIsViewed() {
    if (this.notification !== 0) {
      this.sharedService.updateUserNotification('notifications', this.userId).subscribe(response => {
        this.getNotification();
      })
    }
  }

  redirectToLogin() {
    this.adminservice.userLogOut(this.componentName).subscribe(response => {
      this.socketService.sendLogoutDataFromClient();
      localStorage.clear();
      window.location.reload();
    }, error => {
      localStorage.clear();
      window.location.reload();
    })
  }

  redirectToreset() {
    this.router.navigate(['/admin-features/change-password'])
  }

  checkTokenExpired() {
    this.sharedService.checkTokenExpired().subscribe(response => {
      response ? null : this.redirectToLogin();
    }, error => {
      if (error.status === 401) {
        this.redirectToLogin()
      }
    })
  }

  @HostListener('click', ['$event'])
  onClick(e: any) {
    const elemSidebar = <HTMLElement>document.querySelector('.sidebar-container ');
    setTimeout(() => {
      if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac() && !this.compactSidebar && this.layoutDir != 'rtl') {
        // const ps = new PerfectScrollbar(elemSidebar, {
        //   wheelSpeed: 2,
        //   suppressScrollX: true
        // });
      }
    }, 350);
  }

  ngOnDestroy() {
    this._router.unsubscribe();

    this.socketSubscription.unsubscribe();

    this.notificationSubscription.unsubscribe();

    this.userIdle.stopWatching();
  }

  isOver(): boolean {
    if (this.url === '/apps/messages' || this.url === '/apps/calendar' || this.url === '/apps/media' || this.url === '/maps/leaflet') {
      return true;
    } else {
      return window.matchMedia(`(max-width: 1100px)`).matches;
    }
  }

  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }

  menuMouseOver(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && this.collapseSidebar) {
      this.sidemenu.mode = 'over';
    }
  }

  menuMouseOut(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && this.collapseSidebar) {
      this.sidemenu.mode = 'side';
    }
  }

  menuToggleFunc() {
    this.sidemenu.toggle();

    if (!this.isOver()) {
      this.sharedService.setSideNavToggleState(true);
    }

    if (this.collapsedClass == 'side-panel-opened') {
      this.collapsedClass = 'side-panel-closed';
    }
    else {
      this.collapsedClass = 'side-panel-opened';
    }
  }

  changeMenuLayout(value) {
    // console.log(value)
    if (value) {
      this.menuLayout = 'top-menu';
    }
    else {
      this.menuLayout = 'vertical-menu';
    }
  }

  onClickNotification(selectedNotification, sideNav: MatSidenav) {
    if (selectedNotification.hyperlink) {

      if (selectedNotification.hyperlink.includes("?")) {
        let hyperlinkArray = selectedNotification.hyperlink.split("?");

        let queryParams = {};

        queryParams[hyperlinkArray[1].split("=")[0]] = hyperlinkArray[1].split("=")[1];

        this.router.navigate([hyperlinkArray[0]], { queryParams })

      } else {
        this.router.navigate([selectedNotification.hyperlink]);
      }

      sideNav.toggle();
    }
  }

  onSelectSidebarImage(selectedClass, event) {
    this.selectedSidebarImage = selectedClass;
  }

  onSelectedSidebarColor(selectedClass) {
    this.selectedSidebarColor = selectedClass;
  }

  onSelectedHeaderColor(selectedClass) {
    this.selectedHeaderColor = selectedClass;
  }

  isBgActive(value) {
    if (value == this.selectedSidebarImage) {
      return true;
    }
    else {
      return false;
    }
  }

  isSidebarActive(value) {
    if (value == this.selectedSidebarColor) {
      return true;
    }
    else {
      return false;
    }
  }

  isHeaderActive(value) {
    if (value == this.selectedHeaderColor) {
      return true;
    }
    else {
      return false;
    }
  }
}
