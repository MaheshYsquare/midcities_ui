import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoiceComponent } from './invoice.component';
import { InvoiceManagementComponent } from './invoice-management/invoice-management.component';
import { InvoiceToGeneratedComponent } from './invoice-to-generated/invoice-to-generated.component';
import { ManualInvoiceResolver } from 'app/shared/resolver/manual-invoice.resolver';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { ViewInvoiceResolver } from 'app/shared/resolver/view-invoice.resolver';
import { ChassisInvoiceManagementComponent } from './chassis-invoice-management/chassis-invoice-management.component';
import { SequenceResolver } from 'app/shared/resolver/sequence.resolver';
import { CustomerInfoResolver } from 'app/shared/resolver/customer-info.resolver';
import { ManualInvoiceComponent } from 'app/shared/modules/customer-info-module/manual-invoice/manual-invoice.component';
import { PreviewInvoiceComponent } from 'app/shared/modules/customer-info-module/manual-invoice/preview-invoice/preview-invoice.component';
import { CustomerinfoComponent } from 'app/shared/modules/customer-info-module/customerinfo.component';
import { LegComponent } from 'app/shared/modules/customer-info-module/leg/leg.component';
import { EditOrderComponent } from 'app/shared/modules/order-component/edit-order/edit-order.component';
import { ViewInvoiceComponent } from 'app/shared/modules/customer-info-module/leg/view-invoice/view-invoice.component';

const InvoiceRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: InvoiceComponent
      },
      {
        path: 'chassis-invoice',
        component: ChassisInvoiceManagementComponent
      },
      {
        path: 'manual-invoice',
        children: [
          {
            path: '',
            component: ManualInvoiceComponent,
            resolve: {
              'sequence': SequenceResolver
            }
          },
          {
            path: ':invoiceNumber/preview-invoice',
            component: PreviewInvoiceComponent,
            resolve: {
              'invoiceData': ManualInvoiceResolver
            }
          }
        ]
      },
      {
        path: 'invoice-management',
        children: [
          {
            path: '',
            component: InvoiceManagementComponent
          },
          {
            path: 'customer-info',
            component: CustomerinfoComponent,
            resolve: {
              'customerData': CustomerInfoResolver
            }
          },
          {
            path: ':orderNumber/:sequence/leg',
            component: LegComponent,
            resolve: {
              'orderLeg': OrderLegResolver
            }
          },
          {
            path: ':orderNumber/:sequence/leg/edit',
            component: EditOrderComponent,
            resolve: {
              'editOrder': OrderLegResolver
            }
          },
          {
            path: ':orderNumber/:sequence/leg/view-invoice',
            component: ViewInvoiceComponent,
            resolve: {
              'orderLeg': OrderLegResolver
            }
          },
          {
            path: 'manual-invoice',
            children: [
              {
                path: ':invoiceNumber',
                component: ManualInvoiceComponent,
                resolve: {
                  'invoiceData': ManualInvoiceResolver
                }
              },
              {
                path: ':invoiceNumber/preview-invoice',
                component: PreviewInvoiceComponent,
                resolve: {
                  'invoiceData': ManualInvoiceResolver
                }
              }
            ]
          }
        ]
      },
      {
        path: ':orderNumber',
        children: [
          {
            path: '',
            component: InvoiceToGeneratedComponent
          },
          {
            path: 'multiple-invoice',
            component: ViewInvoiceComponent,
            resolve: {
              'orderData': ViewInvoiceResolver
            }
          },
          {
            path: ':sequence/leg',
            component: LegComponent,
            resolve: {
              'orderLeg': OrderLegResolver
            }
          },
          {
            path: ':sequence/leg/view-invoice',
            component: ViewInvoiceComponent,
            resolve: {
              'orderLeg': OrderLegResolver
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(InvoiceRoutes)],
  exports: [RouterModule]
})

export class InvoiceRoutingModule { }