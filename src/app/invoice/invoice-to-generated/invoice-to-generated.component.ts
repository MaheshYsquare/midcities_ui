import { Component, OnInit, ViewChild, OnDestroy, Renderer2, ElementRef, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator, PageEvent, MatDialogConfig, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { InvoiceService } from '../invoice.service';
import { SocketService } from 'app/shared/service/socket.service';
import { SharedService } from 'app/shared/service/shared.service';
import { OrderGetRequest, OrderData } from 'app/shared/models/order.model';
import { loginData } from 'app/session/session.model';
import { Subscription } from 'rxjs';
import { UiSettings, invoiceColumns } from 'app/shared/models/display-column.model';
import { ManageColumnsComponent } from 'app/shared/modules/order-component/manage-columns/manage-columns.component';

@Component({
  selector: 'app-invoice-to-generated',
  templateUrl: './invoice-to-generated.component.html',
  styleUrls: ['./invoice-to-generated.component.scss']
})
export class InvoiceToGeneratedComponent implements OnInit, OnDestroy {

  public orderComponentName: string = 'orders';

  public invoiceComponentName: string = 'invoices';

  public user: loginData;

  public orderNumber: number;

  public generateInvoice: boolean = true;

  public multipleInvoiceToBeGeneratedData: any = [];

  public invoiceDatasource: MatTableDataSource<OrderData>;

  public invoiceSelection = new SelectionModel<OrderData>(true, []);

  @ViewChild('invoiceSort') invoiceSort: MatSort;

  @ViewChild('invoiceTable', { read: ElementRef }) private invoiceTable: ElementRef;

  public invoiceColumns: string[] = [];

  public invoiceRowColumns: UiSettings[] = [];

  public invoiceLength: number = 0;

  @ViewChild('invoicePaginator') invoicePaginator: MatPaginator;

  public offset: number = 1;

  public limit: number = 10;

  public sortColumn: string;

  public sortDirection: string;

  /* resize Column Variables */
  public pressed = false;

  public currentResizeIndex: number;

  public startX: number;

  public startWidth: number;

  public isResizingRight: boolean;

  public scale: number;

  public sideNavToggleSubscription: Subscription;

  public resizableMousemove: () => void;

  public resizableMouseup: () => void;

  public timeout: any;

  public columnsLabel = {
    order_sequence: "Order #",
    leg_status: "Leg Status",
    leg_number: "Leg #",
    driver_name: "Driver",
    container_size: "Size",
    order_type: "Type",
    container_number: "Container #",
    chassis_number: "Chassis #",
    miles: "Miles",
    order_container_number: "Container #",
    order_chassis_number: "Chassis #",
    order_miles: "Miles",
    leg_type: "Leg Type",
    hazmat_req: "Haz",
    triaxle: "Triaxle",
    order_flag: "Order Flag",
    tags: "Tags",
    pickup_date: "SCH PU",
    pickup_time: "PU Time",
    pu_name: "Pickup",
    dl_name: "Delivery",
    delivery_date: "SCH DL",
    delivery_time: "DL Time",
    order_pickup_date: "SCH PU",
    order_pickup_time: "PU Time",
    order_pu_name: "Pickup",
    order_dl_name: "Delivery",
    order_delivery_date: "SCH DL",
    order_delivery_time: "DL Time",
    order_status: "Order Status",
    container_type: "Container Type",
    p_zip: "PU Zip",
    d_zip: "DL Zip",
    business_name: "Customer",
    rail_cut: "Rail Cut",
    pdm: "PDM",
    dmg: "LFD",
    lfd_date: 'LFD',
    chs: "CHS",
    pu_ref: "PU REF",
    ll: "L/L",
    chs_days: "CHS Days",
    action: "Action",
    customer_reference: "Customer #",
    booking_number: "Booking #",
    container_name: "Container Owner",
    chassis_name: "Chassis Name",
    eta: "ETA"
  }

  public isLoading: boolean = true;

  constructor(public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private invoiceService: InvoiceService,
    private sharedService: SharedService,
    private socketService: SocketService,
    private renderer: Renderer2) { }

  ngOnInit() {
    this.invoiceDatasource = new MatTableDataSource();

    this.user = this.sharedService.getUserDetails();

    this.sideNavToggleSubscription = this.sharedService.getSideNavToggleState().subscribe(isToggled => {
      if (isToggled && !this.isLoading) {
        setTimeout(() => {
          this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
        }, 450);
      }
    });

    this.setDisplayedColumns();

    this.route.params.subscribe((param: Params) => {
      this.orderNumber = +param['orderNumber'];

      this.getInvoiceToBeGenerated();
    });
  }

  ngOnDestroy() {
    this.sideNavToggleSubscription.unsubscribe();
  }

  setTableResize(tableWidth: number) {
    let totWidth = 0;
    this.invoiceRowColumns.forEach((column) => {
      totWidth += column.width;
    });
    this.scale = (tableWidth) / totWidth;
    this.invoiceRowColumns.forEach((column) => {
      column.width *= this.scale;
      this.setColumnWidth(column);
    });
  }

  setDisplayedColumns() {
    this.invoiceColumns = [];

    this.invoiceRowColumns = localStorage.getItem('invoice_to_generated_view') ? JSON.parse(localStorage.getItem('invoice_to_generated_view')) : [];

    if (this.user.role !== 'Admin' && this.user.role !== 'Invoice') {
      this.invoiceRowColumns = this.invoiceRowColumns.filter(temp => temp.field !== 'action');
    }

    this.invoiceRowColumns.forEach((column, index) => {
      column.index = index;
      this.invoiceColumns[index] = column.field;
    });
  }

  setColumnWidth(column: any) {
    const columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field}`));
    columnEls ? columnEls.forEach((el: HTMLDivElement) => {
      el.style.flex = 'none';
      el.style.width = column.width + 'px';
    }) : null;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
  }

  getInvoiceToBeGenerated() {
    let request = this.generateRequest()
    this.invoiceService.getInvoiceList(this.orderComponentName, request).subscribe(response => {
      this.invoiceDatasource.data = response.data.orderData;
      this.invoiceLength = response.data.orderPaginationLength;
      setTimeout(() => {
        this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
      }, 100);
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    })
  }

  onResizeColumn(event: any, index: number) {
    this.checkResizing(event, index);
    this.currentResizeIndex = index;
    this.pressed = true;
    this.startX = event.pageX;
    this.startWidth = event.target.clientWidth;
    event.preventDefault();
    this.mouseMove(index);
  }

  private checkResizing(event, index) {
    const cellData = this.getCellData(index);
    if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.invoiceRowColumns.length - 1)) {
      this.isResizingRight = true;
    } else {
      this.isResizingRight = false;
    }
  }

  private getCellData(index: number) {
    const headerRow = this.invoiceTable.nativeElement.children[0];
    const cell = headerRow.children[index];
    return cell.getBoundingClientRect();
  }

  mouseMove(index: number) {

    this.resizableMousemove = this.renderer.listen('document', 'mousemove', (event) => {
      if (this.pressed && event.buttons) {
        const dx = (this.isResizingRight) ? (event.pageX - this.startX) : (-event.pageX + this.startX);
        const width = this.startWidth + dx;
        if (this.currentResizeIndex === index && width > 50) {
          this.setColumnWidthChanges(index, width);
        }
      }
    });

    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.resizableMouseup = this.renderer.listen('document', 'mouseup', (event) => {
      if (this.pressed) {
        this.pressed = false;
        this.currentResizeIndex = -1;
        this.resizableMousemove();
        this.resizableMouseup();

        let updateInvoiceToGeneratedView: any = localStorage.getItem('updatedInvoiceToGeneratedView');

        if (updateInvoiceToGeneratedView) {

          this.timeout = setTimeout(() => {

            updateInvoiceToGeneratedView = JSON.parse(updateInvoiceToGeneratedView);

            updateInvoiceToGeneratedView.forEach(element => {
              element.width = element.width / element.scale;
            });

            let request = {
              column: 'invoice_to_generated_view',
              value: JSON.stringify(updateInvoiceToGeneratedView),
              user_id: this.sharedService.getUserDetails().userId,
              noloading: true
            }
            this.sharedService.updateUiSettings('users', request).subscribe(response => {
              localStorage.removeItem('updatedInvoiceToGeneratedView');
              localStorage.setItem('invoice_to_generated_view', JSON.stringify(updateInvoiceToGeneratedView));
            }, error => {
              // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
            })
          }, 2000);

        }

      }
    });
  }

  setColumnWidthChanges(index: number, width: number) {
    const orgWidth = this.invoiceRowColumns[index].width;
    const dx = width - orgWidth;
    if (dx !== 0) {
      const j = (this.isResizingRight) ? index + 1 : index - 1;
      const newWidth = this.invoiceRowColumns[j].width - dx;
      if (newWidth > 5) {
        this.invoiceRowColumns[index].width = width;
        this.setColumnWidth(this.invoiceRowColumns[index]);
        this.invoiceRowColumns[j].width = newWidth;
        this.setColumnWidth(this.invoiceRowColumns[j]);

        let modifiedUiSettings = this.invoiceRowColumns.slice();

        modifiedUiSettings.forEach(temp => {
          temp.scale = this.scale;
        });

        localStorage.setItem('updatedInvoiceToGeneratedView', JSON.stringify(modifiedUiSettings));
      }
    }
  }

  onClickGenerateInvoice() {
    let containerToBeInvoiced = this.multipleInvoiceToBeGeneratedData.filter(temp => temp !== null)
    let input = {
      containerToBeInvoiced,
      customer_quickbooks_id: containerToBeInvoiced[0].customer_quickbooks_id
    }
    let queryParams = containerToBeInvoiced.map(temp => temp.order_container_chassis_id).join(',');
    this.invoiceService.postMultipleAutoInvoice(this.invoiceComponentName, input).subscribe(response => {
      if (response.code === 200) {
        this.socketService.sendNotification({
          isMultiple: true,
          multipleRole: ['Admin', 'Invoice'],
          msg: `Invoice for ${this.orderNumber} has been generated`,
          hyperlink: `/invoicing/${this.orderNumber}/multiple-invoice?orderChassis=${queryParams}`,
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        })
        setTimeout(() => {
          this.router.navigate(['multiple-invoice'], { relativeTo: this.route, queryParams: { orderChassis: queryParams } })
        }, 600)
      } else if (response.code == 103) {
        this.sharedService.quickbooksConnection(this.onClickGenerateInvoice.bind(this));
      } else if (response.code == 422) {
        this.sharedService.openSnackbar(response.error, 3000, "warning-msg");
      }

    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
    })
  }

  onClickAddColumn() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      tableView: 'invoice_to_generated_view',
      displayColumns: invoiceColumns
    }

    const dialogRef = this.dialog.open(ManageColumnsComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setDisplayedColumns();

        setTimeout(() => {
          this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
        }, 100);

        localStorage.removeItem('updatedInvoiceView');
      }
    })
  }

  onSortDirectionChange(event) {
    this.sortColumn = (event.active === 'order_pickup_date' || event.active === 'order_pickup_time') ?
      'est_pickup_from_time' : ((event.active === 'order_delivery_date' || event.active === 'oder_delivery_time') ?
        'est_delivery_from_time' : event.active);
    this.sortDirection = event.direction;
    this.getInvoiceToBeGenerated()
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllInvoiceSelected() {
    const numSelected = this.invoiceSelection.selected.length;
    const numRows = this.invoiceDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  invoiceMasterSelect(event) {
    this.isAllInvoiceSelected() ?
      this.invoiceDatasource.data.forEach(row => row.is_selected = false) :
      this.invoiceDatasource.data.forEach(row => row.is_selected = true);
    this.isAllInvoiceSelected() ?
      this.invoiceSelection.clear() :
      this.invoiceDatasource.data.forEach(row => this.invoiceSelection.select(row));
    event.checked ?
      this.generateInvoice = false :
      this.generateInvoice = true;
    event.checked ?
      this.invoiceDatasource.data.forEach((row, index) => row.is_invoice_generated === 0 ? this.multipleInvoiceToBeGeneratedData[index] = this.formatInputToGenerate(row) : []) :
      this.multipleInvoiceToBeGeneratedData = [];
    event.checked ?
      this.generateInvoice = !this.invoiceDatasource.data.map(temp => temp.is_invoice_generated).includes(0) : true;
  }

  onSelectRow(event, data, index) {
    let dataSelection = [];
    this.invoiceDatasource.data.forEach(row => dataSelection.push(this.invoiceSelection.isSelected(row)))
    event.checked ?
      this.generateInvoice = !event.checked :
      dataSelection.includes(true) ? this.generateInvoice = false : this.generateInvoice = true;
    event.checked ?
      this.multipleInvoiceToBeGeneratedData[index] = this.formatInputToGenerate(data) :
      this.multipleInvoiceToBeGeneratedData[index] = null;
  }

  onClickOrderNumber(selectedRow: OrderData) {
    this.router.navigate([selectedRow.container_sequence, 'leg'], { relativeTo: this.route });
  }

  onClickMoveToDispatch(selectedRow: OrderData) {
    if (!selectedRow.is_invoice_generated) {
      let dialogRef = this.sharedService.openConfirmation({
        action: 'moveOrderToDispatch',
        cancelLabel: 'No',
        confirmColor: 'rgb(24, 102, 219)',
        confirmLabel: 'Yes'
      });

      dialogRef.subscribe(result => {
        if (result) {
          this.sharedService.moveOrderBackToDispatch(this.orderComponentName,
            { flag: 'invoice_to_generated', id: selectedRow.order_container_chassis_id }).subscribe(response => {
              this.router.navigate(['invoicing'], { replaceUrl: true });
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 3500, 'failure-msg');
            })
        }
      })
    } else {
      this.sharedService.openSnackbar("Invoice Generated For this order", 3500, "warning-msg");
    }
  }

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getInvoiceToBeGenerated()
        break;
      case 20:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getInvoiceToBeGenerated()
        break;
      case 30:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getInvoiceToBeGenerated()
        break;
    }
  }

  formatInputToGenerate(row) {
    var input = {
      "customer_id": row.customer_id,
      'customer_quickbooks_id': row.customer_quickbooks_id,
      "created_by": this.user.first_name + " " + this.user.last_name,
      "order_container_chassis_id": row.order_container_chassis_id,
      "order_id": row.order_id
    }

    return input
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: OrderGetRequest = {
      offset: this.offset,
      limit: this.limit,
      columnFilter: false,
      filterData: null,
      column: this.sortColumn,
      direction: this.sortDirection,
      isUnique: false,
      orderNumber: this.orderNumber,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }
}
