import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { MatTableDataSource, MatPaginator, MatDialogConfig, MatDialog, Sort } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';
import { InvoiceManagementData, InvoiceManagementResponse, InvoiceManagementRequest } from 'app/shared/models/invoice.model';
import { InvoiceMailComponent } from 'app/shared/modules/customer-info-module/leg/view-invoice/invoice-mail/invoice-mail.component';
import { invoiceMgmtFilterColumns } from 'app/shared/models/display-column.model';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';
import { OrderGetRequest } from 'app/shared/models/order.model';

@Component({
  selector: 'app-invoice-management',
  templateUrl: './invoice-management.component.html',
  styleUrls: ['./invoice-management.component.scss']
})
export class InvoiceManagementComponent implements OnInit {

  public unpaidInvoiceDatasource: MatTableDataSource<InvoiceManagementData>;

  public paidInvoiceDatasource: MatTableDataSource<InvoiceManagementData>;

  public unpaidInvoiceDataHeader = ['order_number', 'order_type',
    'quickbook_status', 'business_name', 'customer_reference', 'invoice_number',
    'date', 'total_amount', 'paid', 'outstanding', 'write_off', 'date_of_payment',
    'booking_number', 'container_number', 'action'];

  public paidInvoiceDataHeader = ['order_number', 'order_type',
    'quickbook_status', 'business_name', 'customer_reference', 'invoice_number',
    'date', 'total_amount', 'paid', 'outstanding', 'write_off', 'payment_type',
    'payment_reference', 'date_of_payment', 'booking_number', 'container_number'];

  public invoiceComponentName: string = 'invoices';

  public invoiceManagementData: any;

  public manageBilling: boolean = true;

  public manageBillingData: any = [];

  public unpaidFilterData: any;

  public paidFilterData: any;

  public paidOffset: number = 1;

  public paidLimit: number = 5;

  public unpaidOffset: number = 1;

  public unpaidLimit: number = 5;

  public initialQuickbookSync: boolean = false;

  public paidPageLength: number = 0;

  public unpaidPageLength: number = 0;

  public paidDirection: string;

  public paidColumn: string;

  public unpaidDirection: string;

  public unpaidColumn: string;

  @ViewChild('unpaidInvoicePaginator') unpaidInvoicePaginator: MatPaginator;

  @ViewChild('paidInvoicePaginator') paidInvoicePaginator: MatPaginator;

  public unpaidColumnFilter: boolean = false;

  public unpaidColumnFilterValue: OrderGetRequest['filterData'];

  public paidColumnFilter: boolean = false;

  public paidColumnFilterValue: OrderGetRequest['filterData'];

  @ViewChild('paidFilterForm') paidFilterForm: NgForm;

  constructor(private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService) { }

  ngOnInit() {

    this.unpaidInvoiceDatasource = new MatTableDataSource();

    this.paidInvoiceDatasource = new MatTableDataSource();

    this.getInvoiceManagementDataFromService();
  }

  getInvoiceManagementDataFromService(table?: string) {
    let request = this.generateRequest(table);

    this.sharedService.getInvoiceManagementData(this.invoiceComponentName, request).subscribe((response: InvoiceManagementResponse) => {
      if (response.code == 200) {
        this.unpaidInvoiceDatasource.data = response.data.unpaidInvoices.invoiceData;
        this.unpaidPageLength = response.data.unpaidInvoices.invoicePaginationLength;
        this.paidInvoiceDatasource.data = response.data.paidInvoices.invoiceData;
        this.paidPageLength = response.data.paidInvoices.invoicePaginationLength;
        this.initialQuickbookSync = false;

      } else if (response.code == 103) {
        this.initialQuickbookSync = false;
        localStorage.setItem('quickbookSync', 'false');
        this.sharedService.quickbooksConnection(this.getInvoiceManagementDataFromService.bind(this));
      }
    }, error => { })
  }

  onClickFilterColumns(table: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "filterData": table === 'unpaid' ? this.unpaidColumnFilterValue : this.paidColumnFilterValue,
      "filterColumns": invoiceMgmtFilterColumns,
      isInvoiceMgmt: true,
      "key": "invoice_mgmt"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.unpaidColumnFilterValue = table === 'unpaid' ? result : this.unpaidColumnFilterValue;
        this.paidColumnFilterValue = table === 'paid' ? result : this.paidColumnFilterValue;
        this.paidColumnFilter = table === 'paid' ? true : this.paidColumnFilter;
        this.unpaidColumnFilter = table === 'unpaid' ? true : this.unpaidColumnFilter;
        this.initialPage(table);
        this.getInvoiceManagementDataFromService(table);
      }
    });
  };



  onClickReset(table: string) {
    this.paidColumnFilter = table === 'paid' ? false : this.paidColumnFilter;
    this.unpaidColumnFilter = table === 'unpaid' ? false : this.unpaidColumnFilter;
    this.unpaidColumnFilterValue = table === 'unpaid' ? undefined : this.unpaidColumnFilterValue;
    this.paidColumnFilterValue = table === 'paid' ? undefined : this.paidColumnFilterValue;
    this.initialPage(table);
    this.getInvoiceManagementDataFromService(table);
  }

  onClickQuicbooksSync() {
    this.initialQuickbookSync = true;
    this.paidInvoicePaginator.pageIndex = 0;
    this.paidInvoicePaginator.pageSize = this.paidInvoicePaginator.pageSize ? this.paidInvoicePaginator.pageSize : 5;
    this.unpaidInvoicePaginator.pageIndex = 0;
    this.unpaidInvoicePaginator.pageSize = this.unpaidInvoicePaginator.pageSize ? this.unpaidInvoicePaginator.pageSize : 5;
    localStorage.setItem('quickbookSync', 'true');
    this.getInvoiceManagementDataFromService();
  }

  onClickReconnectQuickbooks() {
    localStorage.setItem('quickbookSync', 'false');
    this.sharedService.reconnectQuickBooksConnection(this.invoiceComponentName).subscribe(response => {
      let parameters = "location=1,width=800,height=650";
      parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;
      let win = window.open(response, 'connectPopup', parameters);
      let pollOAuth = window.setInterval(() => {
        try {
          if (win.closed) {
            window.clearInterval(pollOAuth);
          }
          if (win.document.URL.indexOf("code") != -1) {
            window.clearInterval(pollOAuth);
            win.close();
            localStorage.setItem('quickbookSync', 'true');
            this.onClickQuicbooksSync();
            this.sharedService.openSnackbar("Successfully Connected With Quickbooks", 2000, "success-msg");
          }
        } catch (e) { }
      }, 100);
    })
  }

  onChangeSortDirection(event: Sort, table: string) {
    if (table === 'paid') {
      this.paidColumn = event.active;
      this.paidDirection = event.direction ? event.direction : null;
    } else {
      this.unpaidColumn = event.active;
      this.unpaidDirection = event.direction ? event.direction : null;
    }

    this.getInvoiceManagementDataFromService(table);
  }

  onClickCustomerName(selectedRow) {
    this.router.navigate(['customer-info'], { relativeTo: this.route, queryParams: { customerId: selectedRow.customer_id } })
  }

  onClickInvoiceNumber(selectedRow: InvoiceManagementData) {
    if (selectedRow.invoice_type === 'auto') {
      this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.route });
    } else {
      this.router.navigate(['manual-invoice', selectedRow.invoice_number], { relativeTo: this.route })
    }
  }

  onChangeUnpaidInvoices(event) {
    this.unpaidOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.unpaidLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getInvoiceManagementDataFromService('unpaid');
  }

  onChangePaidInvoices(event) {
    this.paidOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.paidLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getInvoiceManagementDataFromService('paid')
  }


  /* to set variable for initial page configuration */
  initialPage(table: string) {
    if (table == 'unpaid') {
      this.unpaidInvoicePaginator.pageIndex = 0;
      this.unpaidInvoicePaginator.pageSize = this.unpaidInvoicePaginator.pageSize ?
        this.unpaidInvoicePaginator.pageSize : 5;
      this.unpaidOffset = 1;
      this.unpaidLimit = this.unpaidInvoicePaginator.pageSize ?
        this.unpaidInvoicePaginator.pageSize : 5;
    } else {
      this.paidInvoicePaginator.pageIndex = 0;
      this.paidInvoicePaginator.pageSize = this.paidInvoicePaginator.pageSize ?
        this.paidInvoicePaginator.pageSize : 5;
      this.paidOffset = 1;
      this.paidLimit = this.paidInvoicePaginator.pageSize ?
        this.paidInvoicePaginator.pageSize : 5;
    }
  }

  generateRequest(table: string) {

    let request: InvoiceManagementRequest = {
      isQuickbooks: localStorage.getItem('quickbookSync') && localStorage.getItem('quickbookSync') == 'true',
      isQuickbooksInitial: this.initialQuickbookSync,
      isUniqueByCustomer: false,
      paidOffset: this.paidOffset,
      paidLimit: this.paidLimit,
      unpaidOffset: this.unpaidOffset,
      unpaidLimit: this.unpaidLimit,
      paidDirection: this.paidDirection,
      paidColumn: this.paidColumn,
      unpaidDirection: this.unpaidDirection,
      unpaidColumn: this.unpaidColumn,
      paidColumnFilter: this.paidColumnFilter,
      unpaidColumnFilter: this.unpaidColumnFilter,
      paidColumnFilterValue: this.paidColumnFilterValue,
      unpaidColumnFilterValue: this.unpaidColumnFilterValue,
      table,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    return request
  }

  onClickSendEmail(selectedRow: InvoiceManagementData) {
    let dialog = this.sharedService.openConfirmation({
      action: "mailConfirmation",
      name: null,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialog.subscribe(result => {
      if (result) {
        const emailDialogConfig = new MatDialogConfig();
        emailDialogConfig.data = {
          to: selectedRow.email,
          prefferedDocuments: selectedRow.preferred_documents ?
            selectedRow.preferred_documents.split(',') : [],
          subject: `Mid Cities Invoice #${selectedRow.invoice_number}`
        };
        const emailDialogRef = this.dialog.open(InvoiceMailComponent, emailDialogConfig);
        emailDialogRef.afterClosed().subscribe(result => {
          if (result) {
            result.id = selectedRow.invoice_type === 'auto' ?
              selectedRow.order_container_chassis_id : selectedRow.manual_invoice_id;

            result.invoice_type = selectedRow.invoice_type;
            result.multiple = false;
            result.isNeedToFetchLeg = true;
            result.isNeedToFetch = true;
            result.invoiceGenerationData = this.invoiceMailRequestGeneration(selectedRow);
            result.loggedInUser = this.sharedService.getUserDetails().userId;

            if (!result.mergeDocs) {
              this.sharedService.emailInvoiceWithAttachment(this.invoiceComponentName, result)
                .subscribe(response => {
                  if (response.code === 200) {
                    this.sharedService.openSnackbar(response.data, 2000, "success-msg");
                  } else {
                    this.sharedService.openSnackbar("Mail Not Sent", 2000, "failure-msg");
                  }
                }, error => {
                  this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
                })
            } else {
              this.sharedService.sendEmailWithMergeDocs(this.invoiceComponentName, result)
                .subscribe(response => {
                  this.sharedService.openSnackbar(response.data, 2000, response.code === 103 ? "warning-msg" : "success-msg");
                }, error => {
                  this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
                })
            }

          }
        })
      }
    })
  }

  invoiceMailRequestGeneration(selectedRow: InvoiceManagementData) {
    return {
      invoice_number: selectedRow.invoice_number,
      order_number: selectedRow.order_sequence,
      date: selectedRow.date ? new Date(selectedRow.date).toLocaleDateString() : null,
      customerName: selectedRow.business_name,
      customerAddress: selectedRow.b_address,
      customerPhone: selectedRow.business_phone,
      customerEmail: selectedRow.email,
      shippingName: selectedRow.shipperName,
      shippingAddress: selectedRow.shipperAddress,
      shippingPhone: selectedRow.shipperPhone,
      consigneeName: selectedRow.consigneeName,
      consigneeAddress: selectedRow.consigneeAddress,
      consigneePhone: selectedRow.consigneePhone,
      customer_reference: selectedRow.customer_reference,
      booking_number: selectedRow.booking_number,
      delivery_date: selectedRow.delivery_date ? new Date(selectedRow.delivery_date).toLocaleDateString() : null,
      container_number: selectedRow.container_number,
      pu_street: selectedRow.pu_street,
      pu_city: selectedRow.pu_city,
      pu_state: selectedRow.pu_state,
      pu_postal_code: selectedRow.pu_postal_code,
      dl_street: selectedRow.dl_street,
      dl_city: selectedRow.dl_city,
      dl_state: selectedRow.dl_state,
      dl_postal_code: selectedRow.dl_postal_code,
      b_street: selectedRow.b_street,
      b_city: selectedRow.b_city,
      b_state: selectedRow.b_state,
      b_postal_code: selectedRow.b_postal_code,
      leg_mgmt: '',
      accessoriesCharge: '',
      random_notes: selectedRow.invoice_type === 'auto' ?
        `I.C.C. regulations require payment within seven days. 
      PLEASE REMIT TO:  MIDCITIES MOTOR FREIGHT, INC. 
      PO BOX 4025 
      St. Joseph, MO 64504 - 0025` : selectedRow.random_notes,
      due_date: selectedRow.due_date ? new Date(selectedRow.due_date).toLocaleDateString() : null,
      total_amount: selectedRow.total_amount
    }
  }


}
