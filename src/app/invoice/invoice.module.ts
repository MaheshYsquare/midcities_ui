import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { InvoiceComponent } from './invoice.component';
import { loadingInterceptor } from 'app/shared/interceptor/loading.interceptor';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { InvoiceManagementComponent } from './invoice-management/invoice-management.component';
import { InvoiceToGeneratedComponent } from './invoice-to-generated/invoice-to-generated.component';
import { ManualInvoiceResolver } from 'app/shared/resolver/manual-invoice.resolver';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { ViewInvoiceResolver } from 'app/shared/resolver/view-invoice.resolver';
import { ChassisInvoiceManagementComponent } from './chassis-invoice-management/chassis-invoice-management.component';
import { SequenceResolver } from 'app/shared/resolver/sequence.resolver';
import { CustomerInfoResolver } from 'app/shared/resolver/customer-info.resolver';
import { DataMapComponent } from './chassis-invoice-management/data-map/data-map.component';
import { CustomerInfoModule } from 'app/shared/modules/customer-info-module/customer-info.module';
import { CustomerSharedModule } from 'app/shared/modules/customer-shared/customer-shared.module';

@NgModule({
  imports: [
    CustomerInfoModule,
    CustomerSharedModule,
    InvoiceRoutingModule
  ],
  declarations: [
    InvoiceComponent,
    InvoiceManagementComponent,
    InvoiceToGeneratedComponent,
    ChassisInvoiceManagementComponent,
    DataMapComponent
  ],
  entryComponents: [
    DataMapComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    },
    ManualInvoiceResolver,
    SequenceResolver,
    OrderLegResolver,
    ViewInvoiceResolver,
    CustomerInfoResolver
  ]
})

export class InvoiceComponentsModule { }