import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialogConfig, MatDialog } from '@angular/material';

import { InvoiceService } from '../invoice.service';
import { SharedService } from 'app/shared/service/shared.service';
import { ChassisInvoiceReconcilationData, ChassisInvoiceData } from 'app/shared/models/invoice.model';
import { DataMapComponent } from './data-map/data-map.component';

@Component({
  selector: 'app-chassis-invoice-management',
  templateUrl: './chassis-invoice-management.component.html',
  styleUrls: ['./chassis-invoice-management.component.scss']
})
export class ChassisInvoiceManagementComponent implements OnInit {

  public componentName: string = "chassis_invoices";

  public chassisReconcilationDatasource: MatTableDataSource<ChassisInvoiceReconcilationData>;

  public chassisReconcilationColumns: string[] = ['s_no', 'chassis_number', 'match', 'days_billed', 'days_dispatched', 'days_variance'];

  public formData: FormData;

  public currentIndex: number = 0;

  @ViewChild('chassisReconcilationPaginator') chassisReconcilationPaginator: MatPaginator;

  constructor(private invoiceService: InvoiceService,
    private sharedService: SharedService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.chassisReconcilationDatasource = new MatTableDataSource();
  }

  onChange(event) {
    let files = event.target.files;

    if (files.length) { //file selected

      let fileExtension = files[0].name.split('.')[files[0].name.split('.').length - 1]; // split the file extension from name for validation

      if (fileExtension === 'xlsx' || fileExtension === 'csv') { // only accepts csv or xlsx

        this.formData = new FormData(); // used to wrap the mulipart file data to pass to backend

        this.formData.append("files", files[0]);

        this.invoiceService.validateDocument(this.componentName, this.formData).subscribe((response: any) => { //to validate the column

          if (response.code === 103 || response.code === 409) { //any error will show the warnings
            return this.sharedService.openSnackbar(response.error, 2500,
              response.code === 409 ? 'failure-msg' : 'warning-msg');
          }

          let notMappedData = response.data.filter(temp => !temp.isSelected); // check whether the field is mapped completely or not

          if (notMappedData.length) { //any one field not mapped will open manual map field popup
            return this.mapColumnAndReconcile(response.data, this.formData);
          }

          let resultObject = {};

          response.data.forEach(element => {
            resultObject[element.label] = element.compareValue;
          });

          this.formData.append("fields", JSON.stringify(resultObject));

          this.reconcileDocumentFromService(this.formData);

        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        });

      } else {
        this.sharedService.openSnackbar('File Format Must be either xlsx or csv', 2000, 'warning-msg')
      }
    }
  }


  mapColumnAndReconcile(data: any, formData: FormData) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    let dialogRef = this.dialog.open(DataMapComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        formData.append("fields", JSON.stringify(result));

        this.reconcileDocumentFromService(formData);
      }
    })
  }


  reconcileDocumentFromService(formData: FormData) {

    this.invoiceService.reconcileDocument(this.componentName, formData).subscribe((response: ChassisInvoiceData) => {

      if (response.code === 103 || response.code === 409) {
        return this.sharedService.openSnackbar(response.error, 2500,
          response.code === 409 ? 'failure-msg' : 'warning-msg');
      }

      this.chassisReconcilationDatasource.data = response.data;

      this.chassisReconcilationDatasource.paginator = this.chassisReconcilationPaginator;

    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
    })

  }

  onChangePage(event) {
    this.currentIndex = (event.pageIndex * event.pageSize);
  }
}
