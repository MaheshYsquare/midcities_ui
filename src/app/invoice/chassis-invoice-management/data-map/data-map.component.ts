import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-data-map',
  templateUrl: './data-map.component.html',
  styleUrls: ['./data-map.component.scss']
})
export class DataMapComponent implements OnInit {

  public mapForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<DataMapComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    this.mapForm = new FormGroup({
      'mapFormArray': new FormArray([])
    });

    this.injectedData.forEach(element => {
      (<FormArray>this.mapForm.get('mapFormArray')).push(this.formControls(element));
    });
  }

  formControls(element: any) {
    return new FormGroup({
      'label': new FormControl(element.label),
      'actualColumn': new FormControl(element.compareValue, Validators.required),
      'columns': new FormControl(element.selectArray)
    })
  }

  getControls() {
    return (<FormArray>this.mapForm.get('mapFormArray')).controls;
  }

  onClickCancel() {
    this.dialogRef.close();
  }

  onClickSave() {
    if (this.mapForm.valid) {

      let resultObject = {};

      this.mapForm.get("mapFormArray").value.forEach(element => {
        resultObject[element.label] = element.actualColumn;
      });

      this.dialogRef.close(resultObject);

    }
  }

}
