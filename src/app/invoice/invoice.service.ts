import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SharedService } from 'app/shared/service/shared.service';
import { OrderResponse, OrderGetRequest } from 'app/shared/models/order.model';

@Injectable({
  providedIn: 'root'
})

export class InvoiceService {

  public accessoriesCharge: any;

  HttpUploadOptions = {
    headers: new HttpHeaders({
      'access_token': this.sharedService.getAuthToken()
    })
  }

  private baseURL = environment.baseURL;
  httpOptions = {};

  constructor(private http: HttpClient,
    private sharedService: SharedService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_token': this.sharedService.getAuthToken()
      })
    };
  }
  
  /** GET Invoice from the server */
  getInvoiceList(actionComponentName: String, request: OrderGetRequest) {
    const url = `${this.baseURL + actionComponentName}/getInvoiceData`;
    return this.http.post<OrderResponse>(url, request, this.httpOptions)
  }

  postMultipleAutoInvoice(actionComponentName: String, multipleAutoInvoice: any) {
    const url = `${this.baseURL + actionComponentName}/postMultipleAutoInvoice`;
    return this.http.post<any>(url, multipleAutoInvoice, this.httpOptions)
  }

  validateDocument(actionComponentName: string, formData: FormData) {
    const url = `${this.baseURL + actionComponentName}/validateDocument`;
    return this.http.post<any>(url, formData, this.HttpUploadOptions)
  }

  reconcileDocument(actionComponentName: string, formData: FormData) {
    const url = `${this.baseURL + actionComponentName}/reconcileDocument`;
    return this.http.post<any>(url, formData, this.HttpUploadOptions)
  }
}
