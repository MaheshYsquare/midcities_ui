import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, Renderer2 } from '@angular/core';
import { MatTableDataSource, MatDialogConfig, MatDialog, MatSort, PageEvent, MatPaginator, Sort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { InvoiceService } from './invoice.service';
import { OrderGetRequest, OrderData } from 'app/shared/models/order.model';
import { SharedService } from 'app/shared/service/shared.service';
import { DropdownData } from 'app/shared/models/dropdown.model';
import { Subscription } from 'rxjs';
import { UiSettings, invoiceColumns, invoiceFilterColumns } from 'app/shared/models/display-column.model';
import { loginData } from 'app/session/session.model';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';
import { ManageColumnsComponent } from 'app/shared/modules/order-component/manage-columns/manage-columns.component';


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit, OnDestroy {

  public componentName: String = "orders";

  public dropdownComponentName: String = "configurations";

  public columnFilter: boolean = false;

  public filterData: OrderGetRequest['filterData'];

  public invoiceDatasource: MatTableDataSource<OrderData>;

  @ViewChild(MatSort) invoiceSort: MatSort;

  @ViewChild('invoiceTable', { read: ElementRef }) private invoiceTable: ElementRef;

  public invoiceColumns: string[] = [];

  public invoiceRowColumns: UiSettings[] = [];

  public invoiceLength: number = 0;

  @ViewChild(MatPaginator) invoicePaginator: MatPaginator;

  public sortColumn: string;

  public sortDirection: string;

  public offset: number = 1;

  public limit: number = 10;

  /* resize Column Variables */
  public pressed = false;

  public currentResizeIndex: number;

  public startX: number;

  public startWidth: number;

  public isResizingRight: boolean;

  public scale: number;

  public sideNavToggleSubscription: Subscription;

  public resizableMousemove: () => void;

  public resizableMouseup: () => void;

  public timeout: any;

  public user: loginData;

  public columnsLabel = {
    order_sequence: "Order #",
    leg_status: "Leg Status",
    leg_number: "Leg #",
    driver_name: "Driver",
    container_size: "Size",
    order_type: "Type",
    container_number: "Container #",
    chassis_number: "Chassis #",
    miles: "Miles",
    order_container_number: "Container #",
    order_chassis_number: "Chassis #",
    order_miles: "Miles",
    leg_type: "Leg Type",
    hazmat_req: "Haz",
    triaxle: "Triaxle",
    order_flag: "Order Flag",
    tags: "Tags",
    pickup_date: "SCH PU",
    pickup_time: "PU Time",
    pu_name: "Pickup",
    dl_name: "Delivery",
    delivery_date: "SCH DL",
    delivery_time: "DL Time",
    order_pickup_date: "SCH PU",
    order_pickup_time: "PU Time",
    order_pu_name: "Pickup",
    order_dl_name: "Delivery",
    order_delivery_date: "SCH DL",
    order_delivery_time: "DL Time",
    order_status: "Order Status",
    container_type: "Container Type",
    p_zip: "PU Zip",
    d_zip: "DL Zip",
    business_name: "Customer",
    rail_cut: "Rail Cut",
    pdm: "PDM",
    dmg: "LFD",
    lfd_date: 'LFD',
    chs: "CHS",
    pu_ref: "PU REF",
    ll: "L/L",
    chs_days: "CHS Days",
    action: "Action",
    customer_reference: "Customer #",
    booking_number: "Booking #",
    container_name: "Container Owner",
    chassis_name: "Chassis Name",
    eta: "ETA"
  }

  public isLoading: boolean = true;

  constructor(public dialog: MatDialog,
    private invoiceService: InvoiceService,
    private router: Router,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    private renderer: Renderer2) { }

  ngOnInit() {
    this.invoiceDatasource = new MatTableDataSource();

    this.user = this.sharedService.getUserDetails();

    this.sideNavToggleSubscription = this.sharedService.getSideNavToggleState().subscribe(isToggled => {
      if (isToggled && !this.isLoading) {
        setTimeout(() => {
          this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
        }, 450);
      }
    });

    this.setDisplayedColumns();

    this.getInvoiceDataFromService();
  }

  ngOnDestroy() {
    this.sideNavToggleSubscription.unsubscribe();
  }

  setTableResize(tableWidth: number) {
    let totWidth = 0;
    this.invoiceRowColumns.forEach((column) => {
      totWidth += column.width;
    });
    this.scale = (tableWidth) / totWidth;
    this.invoiceRowColumns.forEach((column) => {
      column.width *= this.scale;
      this.setColumnWidth(column);
    });
  }

  setDisplayedColumns() {
    this.invoiceColumns = [];

    this.invoiceRowColumns = localStorage.getItem('invoice_view') ? JSON.parse(localStorage.getItem('invoice_view')) : [];

    if (this.user.role !== 'Admin' && this.user.role !== 'Invoice') {
      this.invoiceRowColumns = this.invoiceRowColumns.filter(temp => temp.field !== 'action');
    }

    this.invoiceRowColumns.forEach((column, index) => {
      column.index = index;
      this.invoiceColumns[index] = column.field;
    });
  }

  setColumnWidth(column: any) {
    const columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field}`));
    columnEls ? columnEls.forEach((el: HTMLDivElement) => {
      el.style.flex = 'none';
      el.style.width = column.width + 'px';
    }) : null;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
  }

  /** 
   *  fetch invoice data from backend 
  **/
  getInvoiceDataFromService(): void {
    let request = this.generateRequest()
    this.invoiceService.getInvoiceList(this.componentName, request)
      .subscribe(response => {
        this.invoiceDatasource.data = response.data.orderData;
        this.invoiceLength = response.data.orderPaginationLength;
        setTimeout(() => {
          this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
        }, 100);
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  onResizeColumn(event: any, index: number) {
    this.checkResizing(event, index);
    this.currentResizeIndex = index;
    this.pressed = true;
    this.startX = event.pageX;
    this.startWidth = event.target.clientWidth;
    event.preventDefault();
    this.mouseMove(index);
  }

  private checkResizing(event, index) {
    const cellData = this.getCellData(index);
    if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.invoiceRowColumns.length - 1)) {
      this.isResizingRight = true;
    } else {
      this.isResizingRight = false;
    }
  }

  private getCellData(index: number) {
    const headerRow = this.invoiceTable.nativeElement.children[0];
    const cell = headerRow.children[index];
    return cell.getBoundingClientRect();
  }

  mouseMove(index: number) {

    this.resizableMousemove = this.renderer.listen('document', 'mousemove', (event) => {
      if (this.pressed && event.buttons) {
        const dx = (this.isResizingRight) ? (event.pageX - this.startX) : (-event.pageX + this.startX);
        const width = this.startWidth + dx;
        if (this.currentResizeIndex === index && width > 50) {
          this.setColumnWidthChanges(index, width);
        }
      }
    });

    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.resizableMouseup = this.renderer.listen('document', 'mouseup', (event) => {
      if (this.pressed) {
        this.pressed = false;
        this.currentResizeIndex = -1;
        this.resizableMousemove();
        this.resizableMouseup();

        let updateInvoiceView: any = localStorage.getItem('updatedInvoiceView');

        if (updateInvoiceView) {

          this.timeout = setTimeout(() => {

            updateInvoiceView = JSON.parse(updateInvoiceView);

            updateInvoiceView.forEach(element => {
              element.width = element.width / element.scale;
            });

            let request = {
              column: 'invoice_view',
              value: JSON.stringify(updateInvoiceView),
              user_id: this.sharedService.getUserDetails().userId,
              noloading: true
            }
            this.sharedService.updateUiSettings('users', request).subscribe(response => {
              localStorage.removeItem('updatedInvoiceView');
              localStorage.setItem('invoice_view', JSON.stringify(updateInvoiceView));
            }, error => {
              // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
            })
          }, 2000);

        }

      }
    });
  }

  setColumnWidthChanges(index: number, width: number) {
    const orgWidth = this.invoiceRowColumns[index].width;
    const dx = width - orgWidth;
    if (dx !== 0) {
      const j = (this.isResizingRight) ? index + 1 : index - 1;
      const newWidth = this.invoiceRowColumns[j].width - dx;
      if (newWidth > 5) {
        this.invoiceRowColumns[index].width = width;
        this.setColumnWidth(this.invoiceRowColumns[index]);
        this.invoiceRowColumns[j].width = newWidth;
        this.setColumnWidth(this.invoiceRowColumns[j]);

        let modifiedUiSettings = this.invoiceRowColumns.slice();

        modifiedUiSettings.forEach(temp => {
          temp.scale = this.scale;
        });

        localStorage.setItem('updatedInvoiceView', JSON.stringify(modifiedUiSettings));
      }
    }
  }

  /** 
   *  open filter component and filter invoice data in backend 
  **/
  onClickFilterColumns() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "filterData": this.filterData,
      "filterColumns": invoiceFilterColumns,
      "key": "invoiceData"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.filterData = result;
        this.columnFilter = true;
        this.initialPage();
        this.getInvoiceDataFromService();
      }
    });
  };

  onClickReset() {
    this.columnFilter = false;
    this.filterData = undefined;
    this.initialPage();
    this.getInvoiceDataFromService()
  }

  onClickAddColumn() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      tableView: 'invoice_view',
      displayColumns: invoiceColumns
    }

    const dialogRef = this.dialog.open(ManageColumnsComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setDisplayedColumns();

        setTimeout(() => {
          this.setTableResize(this.invoiceTable.nativeElement.clientWidth);
        }, 100);

        localStorage.removeItem('updatedInvoiceView');
      }
    })
  }

  onSortDirectionChange(event: Sort) {
    this.sortColumn = (event.active === 'order_pickup_date' || event.active === 'order_pickup_time') ?
      'est_pickup_from_time' : ((event.active === 'order_delivery_date' || event.active === 'oder_delivery_time') ?
        'est_delivery_from_time' : event.active);
    this.sortDirection = event.direction;
    this.getInvoiceDataFromService()
  }

  onClickOrderNumber(selectedRow: OrderData) {
    if (selectedRow.sequenceCount > 1) {
      this.router.navigate([selectedRow.order_number], { relativeTo: this.route });
    } else {
      this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.route });
    }
  }

  onClickMoveToDispatch(selectedRow: OrderData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'moveOrderToDispatch',
      cancelLabel: 'No',
      confirmColor: 'rgb(24, 102, 219)',
      confirmLabel: 'Yes'
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.moveOrderBackToDispatch(this.componentName,
          { flag: 'invoice', id: selectedRow.order_id }).subscribe(response => {
            this.getInvoiceDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 3500, 'failure-msg');
          })
      }
    })
  }

  onClickMoveToArchive(selectedRow: OrderData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'moveOrderToArchive',
      cancelLabel: 'No',
      confirmColor: 'rgb(24, 102, 219)',
      confirmLabel: 'Yes'
    });

    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          order_status: 'No Invoice',
          order_id: selectedRow.order_id,
          order_container_chassis_id: selectedRow.order_container_chassis_id
        }
        this.sharedService.makeOrderAsNoInvoice(this.componentName, request).subscribe(response => {
          this.getInvoiceDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 3500, 'failure-msg');
        })
      }
    })
  }

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getInvoiceDataFromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: OrderGetRequest = {
      offset: this.offset,
      limit: this.limit,
      columnFilter: this.columnFilter,
      filterData: this.filterData,
      column: this.sortColumn,
      direction: this.sortDirection,
      isUnique: true,
      orderNumber: null,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.invoicePaginator.pageIndex = 0;
    this.invoicePaginator.pageSize = this.invoicePaginator.pageSize ? this.invoicePaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.invoicePaginator.pageSize ? this.invoicePaginator.pageSize : 10;
  }
}
