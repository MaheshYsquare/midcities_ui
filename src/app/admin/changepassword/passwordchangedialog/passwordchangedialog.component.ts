import { Component, OnInit } from '@angular/core';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-passwordchangedialog',
  templateUrl: './passwordchangedialog.component.html',
  styleUrls: ['./passwordchangedialog.component.scss']
})
export class PasswordchangedialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PasswordchangedialogComponent>) { }

  ngOnInit() { }

}
