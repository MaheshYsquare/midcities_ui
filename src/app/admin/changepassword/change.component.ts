import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { AdminService } from '../admin.service'
import { PasswordchangedialogComponent } from './passwordchangedialog/passwordchangedialog.component';
import { matchingPasswords, feildsMatched } from 'app/shared/utils/app-validators';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss']
})

export class ChangeComponent implements OnInit {

  public changePasswordForm: FormGroup;

  public oldPasswordShowHide: boolean = true;

  public newPasswordShowHide: boolean = true;

  public confirmPasswordShowHide: boolean = true;

  public patternErrorMsg: string = 'Your password must contain at least one uppercase, one lowercase, one number and 8 characters long without empty space';

  constructor(public dialog: MatDialog,
    private formBuilder: FormBuilder,
    public authenticateService: AdminService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: new FormControl(null, Validators.required),
      newPassword: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^\s]{8,}$/)]),
      confirmPassword: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^\s]{8,}$/)])
    }, { validator: [matchingPasswords('newPassword', 'confirmPassword'), feildsMatched('oldPassword', 'newPassword')] });
  }

  onChangePassword(): void {
    if (this.changePasswordForm.valid) {
      this.authenticateService.ChangePassword("users", this.changePasswordForm.get("newPassword").value, this.changePasswordForm.get("oldPassword").value).subscribe(
        response => {
          this.changePasswordForm.disable();
          const dialogConfig = new MatDialogConfig();
          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {};
          let dialogRef = this.dialog.open(PasswordchangedialogComponent, dialogConfig);
          dialogRef.afterClosed().subscribe(result => {
            localStorage.clear()
            window.location.reload()
          })
        }, error => {
          if (error && error.error.error.code && error.error.error.code.includes('INVALID_PASSWORD')) {
            this.sharedService
              .openSnackbar('Incorrect existing password, If you have forgotten your existing password.Please logout and select forgot password to continue',
                8000, 'failure-msg')
          }
        });
    }
  }
}
