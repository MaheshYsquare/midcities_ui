export interface EquipmentData {
    serialNo: number;
    equipment_id: number;
    equipment_type?: string;
    equipment_type_id?: number;
    equipment_name: string;
    make: string;
    model: string;
    year: string;
    vin: string;
    license: string;
    license_state: string;
    license_expiry: string;
    equipment_location?: string;
    equipment_location_id?: number;
    remarks: string;
    vehicle_id?: any;
    truck_no?: any;
    equipment_location_history: {
        equipment_location: string;
        equipment_location_id: any;
        equipment_location_history_id: any;
        update_date: any;
        updated_by: any;
    }[];
}

export interface equipmentColumnFilter {
    equipment_type: string;
    equipment_name: string;
    make: string;
    model: string;
    year: string;
    vin: string;
    license: string;
    license_state: string;
    license_expiry: string;
    equipment_location: string;
    remarks: string;
    truck_no: any;
}

export class EquipmentResponse {
    code: number
    error?: string
    data: {
        equipmentData: EquipmentData[]
        equipmentPaginationLength: number
    }
    disableIds?: any;
}