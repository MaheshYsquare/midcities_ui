import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MatDialogRef, MatSelectChange, MAT_DIALOG_DATA } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';
import { EquipmentData } from '../equipment.model';
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-edit-equipment',
  templateUrl: './edit-equipment.component.html',
  styleUrls: ['./edit-equipment.component.scss']
})
export class EditEquipmentComponent implements OnInit {

  public editMode: boolean;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public equipmentForm: FormGroup;

  public data: EquipmentData;

  public listOfYear = [];

  public userDetails: any;

  public userName: string;

  public equipmentTypes: { label: string; value: number }[];

  public equipmentLocations: { label: string; value: number }[];

  public equipmentType: string;

  public truckNumbers: any[];

  constructor(public dialogRef: MatDialogRef<EditEquipmentComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) {
    if (injectedData) {
      this.editMode = injectedData.modeSelect;
      this.data = injectedData.selectedValue;
      this.equipmentTypes = injectedData.equipmentTypes;
      this.equipmentLocations = injectedData.equipmentLocations;
    }
  }

  ngOnInit() {

    let currentYear = new Date().getFullYear();

    for (let year = 1990; year <= currentYear; year++) {
      this.listOfYear.push(year);
    }

    this.initForm();

    this.userDetails = this.sharedService.getUserDetails();

    this.userName = this.userDetails.first_name + " " + this.userDetails.last_name;

    if (this.editMode) {
      (<any>Object).values(this.equipmentForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }
  }

  private initForm() {

    let equipment_type = null;
    let equipment_name = null;
    let make = null;
    let model = null;
    let year = null;
    let vin = null;
    let license = null;
    let license_state = null;
    let equipment_location_id = null;
    let license_expiry = null;
    let remarks = null;
    let truck_no = null;
    let vehicle_id = null;

    if (this.editMode) {
      equipment_type = this.data.equipment_type_id;
      equipment_name = this.data.equipment_name;
      make = this.data.make;
      model = this.data.model;
      year = Number(this.data.year);
      vin = this.data.vin;
      license = this.data.license;
      license_state = this.data.license_state;
      equipment_location_id = this.data.equipment_location_id;
      license_expiry = this.data.license_expiry;
      remarks = this.data.remarks;
      truck_no = this.data.truck_no;
      vehicle_id = this.data.vehicle_id;

      this.equipmentType = this.data.equipment_type;

      if (this.data.equipment_type === 'Truck') {
        this.getVehiclesListFromService();
      }
    }

    this.equipmentForm = new FormGroup({
      'equipment_type_id': new FormControl(equipment_type, Validators.required),
      'equipment_name': new FormControl(equipment_name, Validators.required),
      'make': new FormControl(make, Validators.required),
      'model': new FormControl(model, Validators.required),
      'year': new FormControl(year, Validators.required),
      'vin': new FormControl(vin, Validators.required),
      'license': new FormControl(license, Validators.required),
      'license_state': new FormControl(license_state, [Validators.required, stateValidator]),
      'equipment_location_id': new FormControl(equipment_location_id, Validators.required),
      'license_expiry': new FormControl(license_expiry, Validators.required),
      'remarks': new FormControl(remarks, Validators.required),
      'truck_no': new FormControl(truck_no),
      'vehicle_id': new FormControl(vehicle_id)
    });
  }

  onSelectEquipmentType(event: any) {
    let equipment = this.equipmentTypes.find(temp => temp.value === event.value);

    this.equipmentType = equipment ? equipment.label : null;

    if (this.equipmentType === 'Truck') {
      this.equipmentForm.get('truck_no').setValidators(Validators.required);
      this.equipmentForm.get('vehicle_id').setValidators(Validators.required);
      this.getVehiclesListFromService();
    } else {
      this.equipmentForm.get('truck_no').setValidators(null);
      this.equipmentForm.get('vehicle_id').setValidators(null);
      this.equipmentForm.patchValue({
        'make': null,
        'model': null,
        'year': null,
        'vin': null,
        'license': null,
        'license_state': null,
        'vehicle_id': null
      })
    }
  }

  /* service call for the vehicle's number */
  getVehiclesListFromService() {
    this.sharedService.getVehiclesList('keeptruckins')
      .subscribe(response => {
        this.truckNumbers = response.data;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onSelectTruckNumber(event: MatSelectChange) {
    let vehicle = this.truckNumbers.find(temp => temp.number === event.value);

    if (vehicle) {
      this.equipmentForm.patchValue({
        'make': vehicle.make,
        'model': vehicle.model,
        'year': +vehicle.year,
        'vin': vehicle.vin,
        'license': vehicle.license_plate_number,
        'license_state': vehicle.license_plate_state,
        'vehicle_id': vehicle.id
      })
    }
  }

  onSubmit() {
    if (this.equipmentForm.valid) {

      let request = this.equipmentForm.getRawValue();

      request['userName'] = this.userName;

      request['equipment_id'] = this.editMode ? this.data.equipment_id : null;

      this.dialogRef.close(request);
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
