import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialogConfig, MatDialog, PageEvent, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { EditEquipmentComponent } from './edit-equipment/edit-equipment.component';
import { AdminService } from '../admin.service';
import { UpdateLocationComponent } from './update-location/update-location.component';
import { SharedService } from 'app/shared/service/shared.service';
import { EquipmentResponse, equipmentColumnFilter, EquipmentData } from './equipment.model';
import { GetRequest } from 'app/shared/models/shared.model';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.scss']
})
export class EquipmentComponent implements OnInit {

  displayedColumns = ['serialNo', 'equipment_type', 'truck_no', 'equipment_name', 'make', 'model',
    'year', 'vin', 'license', 'license_state', 'license_expiry', 'equipment_location', 'remarks', 'action'];

  public equipmentFilterHeaders: string[] = ['dummy', 'EquipmentType', 'truckNumber', 'EquipmentName',
    'Make', 'Model', 'Year', 'Vin', 'License', 'LicenseState', 'LicenseExpiry', 'EquipmentLocation', 'Remarks', 'icon'];

  public columnFilter: boolean = false;

  public columnFilterValue: equipmentColumnFilter = {
    equipment_type: '',
    truck_no: '',
    equipment_name: '',
    make: '',
    model: '',
    year: '',
    vin: '',
    license: '',
    license_state: '',
    license_expiry: '',
    equipment_location: '',
    remarks: ''
  };

  @ViewChild('f') filterForm: NgForm;

  componentName: String = "equipments";

  public equipmentTypes: { label: string; value: number }[];

  public equipmentLocations: { label: string; value: number }[];

  equipmentDatasource: MatTableDataSource<EquipmentData>;

  @ViewChild(MatPaginator) equipmentPaginator: MatPaginator;

  @ViewChild(MatSort) equipmentSort: MatSort;

  public equipmentsLength: any;

  currentPageIndex: any = 0;

  public pageIndex: any = 0;

  public pageSize: any = 10;

  public sortColumn: any;

  public sortDirection: any;

  public offset: number = 1;

  public limit: number = 10;

  public disableIds: any[];

  constructor(private router: Router,
    public dialog: MatDialog,
    private adminService: AdminService,
    private sharedService: SharedService) {
    this.equipmentDatasource = new MatTableDataSource();
  }

  ngOnInit() {
    this.getEquipmentsDataFromService();

    this.getAllDropdownFromService();
  }

  getEquipmentsDataFromService(): void {
    let request = this.generateRequest();

    this.adminService.getEquipmentList(this.componentName, request)
      .subscribe((response: EquipmentResponse) => {
        this.equipmentDatasource.data = response.data.equipmentData;
        this.equipmentsLength = response.data.equipmentPaginationLength;
        this.disableIds = response.disableIds;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      });
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown('configurations', "equipmentLocations,equipmentTypes")
      .subscribe(response => {
        this.equipmentTypes = response.equipmentTypes;
        this.equipmentLocations = response.equipmentLocations;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getEquipmentsDataFromService()
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getEquipmentsDataFromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getEquipmentsDataFromService()
  }

  /* api serivice for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getEquipmentsDataFromService()
  }

  onAddEquipment() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'equipmentTypes': this.equipmentTypes,
      'equipmentLocations': this.equipmentLocations,
      "modeSelect": false,
      "disableIds": this.disableIds
    };
    let dialogRef = this.dialog.open(EditEquipmentComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.adminService.createEquipment(this.componentName, result).subscribe(
          responseData => {
            this.initialPage();
            this.getEquipmentsDataFromService();
            this.sharedService.openSnackbar('Equipment Created Successfully', 500, 'success-msg');
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });
  };

  onEditEquipment(selectedRow: EquipmentData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "selectedValue": selectedRow,
      'equipmentTypes': this.equipmentTypes,
      'equipmentLocations': this.equipmentLocations,
      "modeSelect": true,
      "disableIds": this.disableIds
    };
    let dialogRef = this.dialog.open(EditEquipmentComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.adminService.editEquipment(this.componentName, result).subscribe(
          (response: any) => {
            if (response.code === 200) {
              this.getEquipmentsDataFromService();
              this.sharedService.openSnackbar('Equipment Updated Successfully', 500, 'success-msg');
            } else if (response.code === 103) {
              this.sharedService.openSnackbar(response.error, 2500, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });
  }

  onDeleteEquipment(equipmentId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Equipment',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result != undefined) {
        this.adminService.deleteEquipment(this.componentName, equipmentId).subscribe(
          (response: any) => {
            if (response.code === 200) {
              this.getEquipmentsDataFromService();
              this.sharedService.openSnackbar('Equipment Deleted Successfully', 500, 'success-msg');
            } else if (response.code === 103) {
              this.sharedService.openSnackbar(response.error, 2500, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    })
  };

  onEditEquipmentLocation(selectedRow: EquipmentData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'equipmentLocations': this.equipmentLocations,
    };
    let dialogRef = this.dialog.open(UpdateLocationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let request = {
          'equipment_location_id': result.equipment_location_id,
          'userName': result.userName,
          'date': result.date,
          'equipmentId': selectedRow.equipment_id
        }
        this.adminService.editEquipmentLocation(this.componentName, request).subscribe(
          responseData => {
            this.getEquipmentsDataFromService();
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg');
          })
      }
    });
  }

  onClickSerialNo(selectedRow: EquipmentData, serialNo: number) {
    selectedRow.serialNo = serialNo;

    this.sharedService.openInfo({
      view: "list&table_equipment",
      name: "Equipment",
      listNames: ['ID', 'Equipment Type', 'Truck #', 'Equipment Name', 'Make', 'Model', 'Year', 'VIN', 'License #', 'License State', 'License Expiry', 'Location', 'Remarks'],
      columnNames: this.displayedColumns,
      secondaryName: "Location History",
      secondaryColumnNames: ['ID', 'Location To', 'Updated By', 'Update Date'],
      selectedArray: selectedRow.equipment_location_history,
      selectedData: selectedRow
    })
  };

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getEquipmentsDataFromService()
        break;
      case 20:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getEquipmentsDataFromService()
        break;
      case 30:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getEquipmentsDataFromService()
        break;
    }
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.equipmentPaginator.pageIndex = 0;
    this.equipmentPaginator.pageSize = this.equipmentPaginator.pageSize ? this.equipmentPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.equipmentPaginator.pageSize ? this.equipmentPaginator.pageSize : 10;
  }

}
