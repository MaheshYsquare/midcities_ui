import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-update-location',
  templateUrl: './update-location.component.html',
  styleUrls: ['./update-location.component.scss']
})
export class UpdateLocationComponent implements OnInit {

  public date: any;

  public userDetails: any;

  public userName: string;

  @ViewChild('f') updateLocationForm: NgForm;

  public equipmentLocations: { label: string; value: number }[];

  constructor(public dialogRef: MatDialogRef<UpdateLocationComponent>,
    private sharedService: SharedService,
    @Inject(MAT_DIALOG_DATA) data) {
    this.equipmentLocations = data.equipmentLocations;
  }

  ngOnInit() {
    this.date = new Date();
    this.userDetails = this.sharedService.getUserDetails();
    this.userName = this.userDetails.first_name + " " + this.userDetails.last_name;
  }

  onSubmit() {
    if (this.updateLocationForm.valid) {
      var editData = {
        'equipment_location_id': this.updateLocationForm.value.toLocation,
        'userName': this.userName,
        'date': this.date
      }
      this.dialogRef.close(editData)
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
