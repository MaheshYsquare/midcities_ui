export interface CarrierData {
    carrier_id?: number;
    carrier_name: string;
    mc_no: string;
    email: string;
    phone_no: string;
    address: string;
    location_name: string;
    carrier_documents: string[];
    carrier_rate?: any;
}