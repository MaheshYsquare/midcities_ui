import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';

import { CarrierData } from './carrier.model';
import { SharedService } from 'app/shared/service/shared.service';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-carrier-management',
  templateUrl: './carrier-management.component.html',
  styleUrls: ['./carrier-management.component.scss']
})
export class CarrierManagementComponent implements OnInit {

  public componentName: string = 'carriers';

  public carrierDatasource: MatTableDataSource<CarrierData>;

  @ViewChild(MatSort) carrierSort: MatSort;

  public currentPageIndex: number = 0;

  public carrierColumns: string[] = ['carrier_name', 'mc_no',
    'email', 'phone_no', 'location_name', 'action'];

  @ViewChild(MatPaginator) carrierPaginator: MatPaginator;

  constructor(private adminService: AdminService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.carrierDatasource = new MatTableDataSource();

    this.getCarrierFromService();
  }

  getCarrierFromService() {
    this.sharedService.getCarriersList(this.componentName).subscribe(response => {
      this.carrierDatasource.data = response;
      this.carrierDatasource.sort = this.carrierSort;
      this.carrierDatasource.paginator = this.carrierPaginator;
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg'));
  }

  onClickViewDocs(selectedRow: CarrierData) {

    let displayData: any = { ...selectedRow };

    displayData.documents = selectedRow.carrier_documents.map(temp => {
      return {
        label: temp.split('/')[temp.split('/').length - 1].split('.')[0],
        value: temp,
        clickFn: this.getFileService.bind(this, temp)
      }
    })

    this.sharedService.openInfo({
      view: "viewDocs",
      name: "Carrier Documents",
      documents: displayData.documents
    })
  };

  getFileService(documentName: string) {
    this.sharedService.getOrderFile('utils', { fileName: documentName }).subscribe(response => {
      let utf = new Uint8Array(response.buffer.data);
      let file = new Blob([utf], { type: response.ContentType });
      let fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
    })
  }

  onClickDelete(id: number) {

    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Carrier',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {

        this.adminService.deleteCarrier(this.componentName, id).subscribe(response => {
          this.getCarrierFromService();
          this.sharedService.openSnackbar('Carrier Deleted Successfully', 3000, 'success-msg');
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg');
        })

      }
    })
  }
}
