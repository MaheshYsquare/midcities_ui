import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { OwlDateTimeModule, OwlNativeDateTimeModule, DateTimeAdapter, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

import { UserComponent } from './user/user.component';
import { ContainerComponent } from './container/container.component';
import { CustomerComponent } from './customer/customer.component';
import { LocationComponent } from './location/location.component';
import { AdminService } from './admin.service';
import { loadingInterceptor } from '../shared/interceptor/loading.interceptor';
import { ChassisComponent } from './chassis/chassis.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { ChangeComponent } from './changepassword/change.component';
import { PasswordchangedialogComponent } from './changepassword/passwordchangedialog/passwordchangedialog.component';
import { AdminRoutingModule } from './admin-routing.module';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { EditContainerComponent } from './container/edit-container/edit-container.component';
import { EditChassisComponent } from './chassis/edit-chassis/edit-chassis.component';
import { MatTreeModule } from '@angular/material/tree';
import { EditEquipmentComponent } from './equipment/edit-equipment/edit-equipment.component';
import { UpdateLocationComponent } from './equipment/update-location/update-location.component';
import { ConfigurationComponent, EditConfirmationComponent } from './configuration/configuration.component';
import { ManualInvoiceResolver } from 'app/shared/resolver/manual-invoice.resolver';
import { MY_CUSTOM_FORMATS } from 'app/shared/models/shared.model';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { ArchivedComponent } from './archived/archived.component';
import { CustomerInfoResolver } from 'app/shared/resolver/customer-info.resolver';
import { QuickbookCustomerComponent } from './customer/quickbook-customer/quickbook-customer.component';
import { CustomerInfoModule } from 'app/shared/modules/customer-info-module/customer-info.module';
import { CustomerSharedModule } from 'app/shared/modules/customer-shared/customer-shared.module';
import { LocationSharedModule } from 'app/shared/modules/location-shared/location-shared.module';
import { CarrierManagementComponent } from './carrier-management/carrier-management.component';
import { AutocompleteModule } from 'app/shared/utils/autocomplete/autocomplete.module'

@NgModule({
  imports: [
    CustomerInfoModule,
    MatTreeModule,
    CustomerSharedModule,
    LocationSharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AutocompleteModule,
    AdminRoutingModule
  ],
  declarations: [
    UserComponent,
    EditUserComponent,
    ContainerComponent,
    CustomerComponent,
    LocationComponent,
    ChassisComponent,
    EquipmentComponent,
    ChangeComponent,
    PasswordchangedialogComponent,
    EditContainerComponent,
    EditChassisComponent,
    EditEquipmentComponent,
    UpdateLocationComponent,
    ConfigurationComponent,
    EditConfirmationComponent,
    ArchivedComponent,
    QuickbookCustomerComponent,
    CarrierManagementComponent
  ],
  entryComponents: [
    EditUserComponent,
    EditContainerComponent,
    EditChassisComponent,
    EditEquipmentComponent,
    ChangeComponent,
    PasswordchangedialogComponent,
    UpdateLocationComponent,
    EditConfirmationComponent,
    QuickbookCustomerComponent
  ],
  providers: [
    AdminService,
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    },
    ManualInvoiceResolver,
    OrderLegResolver,
    CustomerInfoResolver
  ]
})

export class AdminComponentsModule { }
