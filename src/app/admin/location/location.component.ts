import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  MatTableDataSource,
  MatDialog,
  MatDialogConfig,
  MatPaginator,
  MatSort,
  PageEvent,
  Sort
} from '@angular/material';

import * as _ from 'lodash';
import * as moment from 'moment';

import { AdminService } from '../admin.service';
import { LocationResponse, LocationData, LocationColumnFilter } from '../../shared/models/location.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { SharedService } from 'app/shared/service/shared.service';
import { EditLocationComponent } from 'app/shared/modules/location-shared/edit-location.component';


@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  public componentName: String = "locations"; // api component name to fetch location

  public dropdownComponentName: String = "configurations"; // api component name to fetch location types

  public locationTypes: { label: string; value: number; }[]; // to store location types

  public searchFilter: boolean = false; // flag to check whether filter enabled or not

  public searchFilterValue: string = ""; // store value for the search     

  public columnFilter: boolean = false;

  public columnFilterValue: LocationColumnFilter = {
    location_type: '', location_name: '',
    address: '', phone_number: '', email: '', receiving_hours: ''
  };

  @ViewChild('f') filterForm: NgForm;

  public locationFilterHeaders: string[] = ['dummy', 'locationType', 'locationName',
    'location', 'phoneNumber', 'emailFilter', 'receivingHours', 'icon'];

  /* table source,sort,pagination,etc */
  public locationDatasource: MatTableDataSource<LocationData>;

  public locationColumns = ['s_no', 'location_type', 'location_name', 'address', 'phone_number', 'email',
    'receiving_hours', 'action'];

  @ViewChild(MatSort) locationSort: MatSort;

  @ViewChild(MatPaginator) locationPaginator: MatPaginator;

  public currentPageIndex: number = 0;

  public locationLength: number = 0;

  public sortColumn: string;

  public sortDirection: string;

  public offset: number = 1;

  public limit: number = 10;

  /* injecting service,dialog,snackbar */
  constructor(public dialog: MatDialog,
    private adminService: AdminService,
    public sharedService: SharedService) { }

  /* angular life cycle hooks for initial */
  ngOnInit() {
    this.locationDatasource = new MatTableDataSource();

    this.getLocationsDatafromService();

    this.getlocationTypesDatafromService();
  }

  /* initial service call */
  getLocationsDatafromService(): void {
    let request = this.generateRequest()
    this.sharedService.getLocationManagementList(this.componentName, request)
      .subscribe((response: LocationResponse) => {
        this.locationDatasource.data = response.data.locationData;
        this.locationLength = response.data.locationPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  getlocationTypesDatafromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'location')
      .subscribe(response => {
        this.locationTypes = response;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  onClickColumnFilter() {

    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);

    if (values.filter(temp => temp !== "" && temp !== null).length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getLocationsDatafromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getLocationsDatafromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length - 1 &&
      this.columnFilterValue['receiving_hours'] === null) {
      this.initialPage();
      this.columnFilter = false;
      this.getLocationsDatafromService();
    }

  }

  onChangeOfReceivingHours(event) {
    if (event.value !== null) {
      this.initialPage();
      this.columnFilter = true;
      this.getLocationsDatafromService();
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getLocationsDatafromService()
  }

  /* open a dialog and call api service call */
  onAddLocation() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "locationTypes": this.locationTypes,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditLocationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage();
        this.getLocationsDatafromService();
        this.sharedService.openSnackbar('Location Created Successfully', 500, 'success-msg')
      }
    });
  };

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilter = true;
      this.initialPage();
      this.getLocationsDatafromService();
    } else {
      this.searchFilter = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getLocationsDatafromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilter = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getLocationsDatafromService();
  }

  /* api serivice for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getLocationsDatafromService()
  }

  /* to view certain data in list view */
  onClickSerialNo(selectedRow: LocationData, serialNo: number) {

    let displayData: any = { ...selectedRow };

    displayData.s_no = serialNo;

    displayData.receiving_hours = displayData.receiving_hours_from !== null && displayData.receiving_hours_to !== null ?
      `${moment(displayData.receiving_hours_from).format('HH:mm')} - ${moment(displayData.receiving_hours_to).format('HH:mm')}`
      : null;

    this.sharedService.openInfo({
      view: "list",
      name: "Location",
      listNames: ['ID', 'Location Type', 'Location Name', 'Address', 'City', 'State', 'Zip Code',
        'Phone Number', 'Email', 'Receiving Hours'],
      columnNames: ['s_no', 'location_type', 'location_name', 'address',
        'city', 'state', 'postal_code', 'phone_number', 'email', 'receiving_hours'],
      selectedData: displayData
    })
  };

  /* open a dialog and to call api service call */
  onEditLocation(selectedRow: LocationData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "selectedValue": selectedRow,
      "locationTypes": this.locationTypes,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditLocationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getLocationsDatafromService()
        this.sharedService.openSnackbar('Location Updated Successfully', 500, 'success-msg')
      }
    });
  }

  /* open a dialog and after dialog closed make api service call */
  onDeleteLocation(locationId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Location',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.adminService.deleteLocation(this.componentName, locationId).subscribe(
          responseData => {
            this.getLocationsDatafromService()
            this.sharedService.openSnackbar('Location Deleted Successfully', 500, 'success-msg')
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
          });
      }
    })
  };

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getLocationsDatafromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: this.searchFilter,
      searchFilterValue: this.searchFilterValue.toLowerCase().replace(/[^a-zA-Z0-9]+/ig, ""),
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.locationPaginator.pageIndex = 0;
    this.locationPaginator.pageSize = this.locationPaginator.pageSize ? this.locationPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.locationPaginator.pageSize ? this.locationPaginator.pageSize : 10;
  }
}
