import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserComponent } from './user/user.component';
import { CustomerComponent } from './customer/customer.component';
import { ContainerComponent } from './container/container.component';
import { ChassisComponent } from './chassis/chassis.component';
import { LocationComponent } from './location/location.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { ChangeComponent } from './changepassword/change.component';
import { SubMenuGuard } from 'app/shared/guard/sub-menu.guard';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ManualInvoiceResolver } from 'app/shared/resolver/manual-invoice.resolver';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { ArchivedComponent } from './archived/archived.component';
import { CustomerInfoResolver } from 'app/shared/resolver/customer-info.resolver';
import { CustomerinfoComponent } from 'app/shared/modules/customer-info-module/customerinfo.component';
import { LegComponent } from 'app/shared/modules/customer-info-module/leg/leg.component';
import { EditOrderComponent } from 'app/shared/modules/order-component/edit-order/edit-order.component';
import { ViewInvoiceComponent } from 'app/shared/modules/customer-info-module/leg/view-invoice/view-invoice.component';
import { ManualInvoiceComponent } from 'app/shared/modules/customer-info-module/manual-invoice/manual-invoice.component';
import { PreviewInvoiceComponent } from 'app/shared/modules/customer-info-module/manual-invoice/preview-invoice/preview-invoice.component';
import { CarrierManagementComponent } from './carrier-management/carrier-management.component';

const AdminRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'user-management',
                canActivate: [SubMenuGuard],
                component: UserComponent
            },
            {
                path: 'customer-management',
                canActivate: [SubMenuGuard],
                children: [
                    {
                        path: '',
                        component: CustomerComponent
                    },
                    {
                        path: 'customer-info',
                        component: CustomerinfoComponent,
                        resolve: {
                            'customerData': CustomerInfoResolver
                        }
                    },
                    {
                        path: ':orderNumber/:sequence/leg',
                        component: LegComponent,
                        resolve: {
                            'orderLeg': OrderLegResolver
                        }
                    },
                    {
                        path: ':orderNumber/:sequence/leg/edit',
                        component: EditOrderComponent,
                        resolve: {
                            'editOrder': OrderLegResolver
                        }
                    },
                    {
                        path: ':orderNumber/:sequence/leg/view-invoice',
                        component: ViewInvoiceComponent,
                        resolve: {
                            'orderLeg': OrderLegResolver
                        }
                    },
                    {
                        path: 'manual-invoice/:invoiceNumber',
                        component: ManualInvoiceComponent,
                        resolve: {
                            'invoiceData': ManualInvoiceResolver
                        }
                    },
                    {
                        path: 'manual-invoice/:invoiceNumber/preview-invoice',
                        component: PreviewInvoiceComponent,
                        resolve: {
                            'invoiceData': ManualInvoiceResolver
                        }
                    },
                ]
            },
            {
                path: 'location-management',
                canActivate: [SubMenuGuard],
                component: LocationComponent
            },
            {
                path: 'chassis-management',
                canActivate: [SubMenuGuard],
                component: ChassisComponent
            },
            {
                path: 'container-owner-management',
                canActivate: [SubMenuGuard],
                component: ContainerComponent
            },
            {
                path: 'equipment-management',
                canActivate: [SubMenuGuard],
                component: EquipmentComponent
            },
            {
                path: 'carrier-management',
                canActivate: [SubMenuGuard],
                component: CarrierManagementComponent
            },
            {
                path: 'menu-configuration',
                canActivate: [SubMenuGuard],
                component: ConfigurationComponent
            },
            {
                path: 'archived-orders',
                canActivate: [SubMenuGuard],
                children: [
                    {
                        path: '',
                        component: ArchivedComponent
                    },
                    {
                        path: ':orderNumber/:sequence/leg',
                        component: LegComponent,
                        resolve: {
                            'orderLeg': OrderLegResolver
                        }
                    },
                    {
                        path: 'manual-invoice/:invoiceNumber',
                        component: ManualInvoiceComponent,
                        resolve: {
                            'invoiceData': ManualInvoiceResolver
                        }
                    }
                ]
            },
            {
                path: 'change-password',
                component: ChangeComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(AdminRoutes)],
    exports: [RouterModule]
})

export class AdminRoutingModule { }