export class UserPostRequest {
    offset: number;
    limit: number;
    userId: number;
    searchFilter: boolean;
    searchFilterValue: string;
    column: string;
    direction: string;
}

export class UserResponse {
    code: number
    error?: string
    data: {
        userData: UserData[]
        userPaginationLength: number
    }
}

export class UserPostResponse {
    code: number
    error?: string
    data: UserData
}

export class UserData {
    serialNo?: number;
    user_id?: number;
    first_name: string;
    last_name: string;
    email: string;
    role: string;
    permissions: string;
    username: string;
    emailVerified: boolean;
    realm: string;
    driver_type: string;
    driver_type_id: number;
    user_status: string;
    role_permissions: any;
    is_signed_up: any;
}

export class PermissionsNode {
    children: PermissionsNode[];
    item: string;
}

export class PermissionsFlatNode {
    item: string;
    level: number;
    expandable: boolean;
    parent?: string;
}

export const TREE_DATA = {
    "Admin Features": [
        "User Management",
        "Customer Management",
        "Location Management",
        "Chassis Management",
        "Container Owner Management",
        "Equipment Management",
        "Carrier Management",
        "Menu Configuration",
        "Archived Orders"
    ],
    "Rate Management": [
        "General Route Rate",
        "Accessorial Charges",
        "Driver Rates"
    ],
    "Order Management": null,
    "Dispatch": null,
    "Invoicing": null,
    "Driver Management": [
        "Driver Details",
        "Driver Pay"
    ],
    "Advanced Search": null,
    "Reporting": null
}

