import { Injectable } from "@angular/core";

import { BehaviorSubject, Observable, of } from "rxjs";

import { PermissionsFlatNode, PermissionsNode, TREE_DATA } from "../user.model";


@Injectable()
export class CheckListService {
    dataChange: BehaviorSubject<PermissionsNode[]> = new BehaviorSubject<PermissionsNode[]>([]);

    get data(): PermissionsNode[] { return this.dataChange.value; };

    private nestedNodeMap: Map<PermissionsNode, PermissionsFlatNode> = new Map<PermissionsNode, PermissionsFlatNode>();

    private flatNodeMap: Map<PermissionsFlatNode, PermissionsNode> = new Map<PermissionsFlatNode, PermissionsNode>();

    private parent: string;

    constructor() {
        this.initialize();
    }

    initialize() {
        // Build the tree nodes from Json object. The result is a list of `PermissionsNode` with nested
        //     file node as children.
        const data = this.buildFileTree(TREE_DATA, 0);

        // Notify the change.
        this.dataChange.next(data);
    }

    /**
     * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
     * The return value is the list of `PermissionsNode`.
     */
    buildFileTree(value: any, level: number) {
        let data: any[] = [];
        for (let k in value) {
            let v = value[k];
            let node = new PermissionsNode();
            node.item = `${k}`;
            if (v === null || v === undefined) {
                // no action
            } else if (typeof v === 'object') {
                node.children = this.buildFileTree(v, level + 1);
            } else {
                node.item = v;
            }
            data.push(node);
        }
        return data;
    }

    /** Allows subscription to the behavior subject as an observable */
    getPermissions(): Observable<PermissionsNode[]> {
        return this.dataChange.asObservable();
    }

    transformer = (node: PermissionsNode, level: number) => {
        let flatNode = this.nestedNodeMap.has(node) && this.nestedNodeMap.get(node)!.item === node.item
            ? this.nestedNodeMap.get(node)!
            : new PermissionsFlatNode();
        flatNode.item = node.item;
        flatNode.level = level;
        flatNode.expandable = !!node.children;
        this.parent = !!node.children ? node.item : this.parent;
        flatNode.parent = level === 1 ? this.parent : null;
        this.flatNodeMap.set(flatNode, node);
        this.nestedNodeMap.set(node, flatNode);
        return flatNode;
    }

    getLevel = (node: PermissionsFlatNode) => { return node.level; };

    isExpandable = (node: PermissionsFlatNode) => { return node.expandable; };

    getChildren = (node: PermissionsNode): Observable<PermissionsNode[]> => {
        return of(node.children);
    }
}