import { FlatTreeControl } from "@angular/cdk/tree";
import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { MatDialogRef, MatSelectChange, MatTreeFlatDataSource, MatTreeFlattener, MAT_DIALOG_DATA } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { Subscription } from "rxjs";

import * as _ from 'lodash';

import { CheckListService } from "./checklist.service";
import { PermissionsFlatNode, PermissionsNode } from "../user.model";
import { dispatchView, invoiceGeneratedView, invoiceView, ordersDraftView, ordersView } from "./ui-settings.model";


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  providers: [CheckListService]
})
export class EditUserComponent implements OnInit, OnDestroy {

  public subscription: Subscription

  public userForm: FormGroup;

  public isDriverTypeRequired: boolean;

  public permissionsPlaceholder: string = "Select Permissions";

  public dataSource: MatTreeFlatDataSource<PermissionsNode, PermissionsFlatNode>;

  public treeControl: FlatTreeControl<PermissionsFlatNode>;

  public treeFlattener: MatTreeFlattener<PermissionsNode, PermissionsFlatNode>;

  public checklistSelection = new SelectionModel<PermissionsFlatNode>(true /* multiple */);

  public addButtonColor = '#2196f3';

  public editButtonColor = 'rgb(45, 185, 26)';

  constructor(private dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private checkListService: CheckListService) { }

  ngOnInit() {
    this.treeFlattener = new MatTreeFlattener(this.checkListService.transformer,
      this.checkListService.getLevel,
      this.checkListService.isExpandable,
      this.checkListService.getChildren);

    this.treeControl = new FlatTreeControl<PermissionsFlatNode>(this.checkListService.getLevel,
      this.checkListService.isExpandable);

    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    this.subscription = this.checkListService.getPermissions().subscribe(data => {
      this.dataSource.data = data;
    });

    this.initForm();
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
  }

  private initForm() {
    let first_name;
    let last_name;
    let email;
    let role;
    let driver_type_id;
    let permissions;
    let user_status = 'inactive';

    if (this.injectedData.editMode) {
      let previousData = this.injectedData.selectedValue;

      first_name = previousData.first_name;
      last_name = previousData.last_name;
      email = previousData.email;
      role = previousData.role;

      driver_type_id = previousData.role === "Driver" ?
        previousData.driver_type_id : null;

      permissions = previousData.permissions;
      user_status = previousData.is_signed_up ? previousData.user_status : 'inactive';

      setTimeout(() => this.isDriverTypeRequired = previousData.role === "Driver", 200);

      let rolePermission = JSON.parse(previousData.role_permissions);

      this.toSelectPermissions(rolePermission);

    }

    this.userForm = new FormGroup({
      first_name: new FormControl(first_name, [Validators.required,
      Validators.minLength(2),
      Validators.maxLength(15),
      Validators.pattern(/^[a-zA-Z]+(\s[a-zA-Z]+)?$/)]),

      last_name: new FormControl(last_name, [Validators.required,
      Validators.minLength(2),
      Validators.maxLength(15),
      Validators.pattern(/^[a-zA-Z]+(\s[a-zA-Z]+)?$/)]),

      email: new FormControl(
        { value: email, disabled: this.injectedData.editMode },
        [
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)
        ]),

      role: new FormControl(role, Validators.required),
      driver_type_id: new FormControl(driver_type_id),
      permissions: new FormControl(permissions, Validators.required),

      user_status: new FormControl({
        value: user_status,
        disabled: this.injectedData.editMode &&
          !this.injectedData.selectedValue.is_signed_up
      })
    })
  }

  /**
   * toSelectPermissions
   * to select permission on editmode
   */
  public toSelectPermissions(permissions) {

    let dataNodes = this.treeControl.dataNodes.map(temp => temp.item);

    Object.keys(permissions).forEach(key => {

      if (typeof permissions[key] === 'object' &&
        permissions[key].length) {

        switch (key) {

          case "Admin Features":
            this.toggleSelectionViaParentOrNode(permissions[key], dataNodes, 9, 0);
            break;

          case "Rate Management":
            this.toggleSelectionViaParentOrNode(permissions[key], dataNodes, 3, 10);
            break;

          case "Driver Management":
            this.toggleSelectionViaParentOrNode(permissions[key], dataNodes, 2, 17);
            break;

          default:
            break;
        }

      } else if (typeof permissions[key] === 'boolean' &&
        permissions[key]) {

        let index = this.treeControl.dataNodes.findIndex(i => i.item === key);

        this.permissionSelectionToggle(this.treeControl.dataNodes[index]);

      }

    });

  }

  /**
   * toggleSelectionViaParentOrNode
   * function to check whether to toggle checklist via parent or node
   */
  public toggleSelectionViaParentOrNode(permissions: string[], dataNodes: string[], actualLength: number, parentIndex: number) {
    if (permissions.length === actualLength) {

      this.permissionSelectionToggle(this.treeControl.dataNodes[parentIndex]);

      this.treeControl.expand(this.treeControl.dataNodes[parentIndex]);

    } else {

      permissions.forEach(permission => {
        let index = dataNodes.findIndex(node => node === permission);

        this.permissionSelectionToggle(this.treeControl.dataNodes[index]);
      })

    }
  }

  /**
   * onSubmit
   * triggerd when the form get submitted
   */
  public onSubmit() {
    if (this.userForm.valid) {
      let request = this.generateRequest();

      // console.log(request);
      this.dialogRef.close(request);
    }
  }

  /**
   * onRoleSelectionChange
   * triggerd when role field get selected
   */
  public onRoleSelectionChange(event: MatSelectChange) {

    setTimeout(() => this.isDriverTypeRequired = event.value === "Driver", 200);

    let permission = [];

    this.checklistSelection.clear();

    // to toggle check list based on role
    this.treeControl.dataNodes.forEach(temp => {

      switch (event.value) {
        case 'Admin':
          this.checklistSelection.toggle(temp);
          this.treeControl.isExpandable(temp) ? this.treeControl.expand(temp) : null;
          break;

        case 'Dispatch':
          permission = ['Location Management',
            'Chassis Management',
            'Container Owner Management',
            'Equipment Management',
            'Carrier Management',
            'Dispatch'];
          break;

        case 'Driver':
          permission = ['Driver Management'];
          break;

        case 'Invoice':
          permission = ['General Route Rate', 'Accessorial Charges', 'Invoicing'];
          break;

        case 'Order Entry':
          permission = ['Customer Management',
            'Location Management',
            'Chassis Management',
            'Container Owner Management',
            'Equipment Management',
            'Carrier Management',
            'Rate Management',
            'Order Management',
            'Dispatch'];
          break;

        case 'Driver Pay':
          permission = ['Driver Rates', 'Driver Management', 'Invoicing'];
          break;

        default:
          break;
      }

      permission.length && permission.includes(temp.item) ?
        this.permissionSelectionToggle(temp) : null;

    });

    this.setPermissionsValueAndPlaceholder();
  }

  /** Toggle the permission selection. Select/deselect all the descendants node */
  permissionSelectionToggle(node: PermissionsFlatNode): void {

    this.checklistSelection.toggle(node);

    const descendants = this.treeControl.getDescendants(node);

    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants)

    this.setPermissionsValueAndPlaceholder();

  }

  /**
   * setPermissionsValueAndPlacholder
   */
  public setPermissionsValueAndPlaceholder() {
    if (this.checklistSelection.selected.length) {

      let node = this.checklistSelection.selected
        .map(elem => elem.item).join(',');

      this.userForm ? this.userForm.get('permissions').setValue(node) : null;

      this.permissionsPlaceholder = "Click To View/Edit Selected Permissions";

      return;
    }

    this.userForm ? this.userForm.get('permissions').setValue(null) : null;

    this.permissionsPlaceholder = "Select Permissions";
  }

  hasChild = (_: number, _nodeData: PermissionsFlatNode) => _nodeData.expandable;

  /** Whether all the descendants of the node are selected */
  descendantsAllSelected(node: PermissionsFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    return descendants.every(child => this.checklistSelection.isSelected(child));
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: PermissionsFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /**
   * onCancel
   * To Close the dialog
   */
  public onCancel() {
    this.dialogRef.close();
  }

  /**
   * generateRequest
   * format the form data for the request
   */
  public generateRequest() {
    let request = { ...this.userForm.getRawValue() };

    let permissions = [];

    let adminFeatures = ['User Management', 'Customer Management',
      'Location Management', 'Chassis Management',
      'Container Owner Management', 'Equipment Management', 'Carrier Management',
      'Menu Configuration', 'Archived Orders'];

    let rateMgmt = ['General Route Rate', 'Accessorial Charges', 'Driver Rates'];

    let driverMgmt = ['Driver Details', 'Driver Pay'];

    let rolePermissions = {
      "Admin Features": adminFeatures.filter(temp => (this.filterArray("Admin Features").indexOf(temp) > -1)),
      "Rate Management": rateMgmt.filter(temp => (this.filterArray("Rate Management").indexOf(temp) > -1)),
      "Order Management": this.isPermissionAvailable("Order Management"),
      "Dispatch": this.isPermissionAvailable("Dispatch"),
      "Invoicing": this.isPermissionAvailable("Invoicing"),
      "Driver Management": driverMgmt.filter(temp => (this.filterArray("Driver Management").indexOf(temp) > -1)),
      "Advanced Search": this.isPermissionAvailable("Advanced Search"),
      "Reporting": this.isPermissionAvailable("Reporting")
    }

    request['driver_type_id'] = request.role === 'Driver' ? request['driver_type_id'] : 4;

    request['role_permissions'] = JSON.stringify(rolePermissions);

    Object.keys(rolePermissions).forEach(key => {

      if (typeof rolePermissions[key] === 'object') {
        rolePermissions[key].length ? permissions.push(key) : null;
      } else {
        rolePermissions[key] ? permissions.push(key) : null;
      }

    });

    request['permissions'] = permissions.join(',');

    if (this.injectedData.editMode) {
      request['user_id'] = this.injectedData.selectedValue.user_id;

    } else {
      request['username'] = request.email;
      request['emailVerified'] = false;
      request['orders_dispatch'] = JSON.stringify(ordersView);
      request['orders_pending'] = JSON.stringify(ordersView);
      request['orders_draft'] = JSON.stringify(ordersDraftView);
      request['dispatch_view'] = JSON.stringify(dispatchView);
      request['invoice_view'] = JSON.stringify(invoiceView);
      request['invoice_to_generated_view'] = JSON.stringify(invoiceGeneratedView);
    }


    return request;
  }

  /**
   * filterArray
   * to filter array for role permissions
   * returns sub tree array
   */
  public filterArray(titleName: string): string[] {
    let subTree = this.checklistSelection.selected
      .filter(temp => temp.level === 1);

    return subTree
      .filter(temp => temp.parent === titleName)
      .map(elem => elem.item);

  }

  /**
   * isPermissionAvailable
   * to check the whethe the permission is available or not
   * return boolean
   */
  public isPermissionAvailable(titleName): boolean {
    let mainTree = this.checklistSelection.selected
      .filter(temp => temp.level === 0);

    return mainTree
      .map(temp => temp.item)
      .includes(titleName);
  }


}
