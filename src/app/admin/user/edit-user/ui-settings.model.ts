export const dispatchView = [
  {
    "field": "serialNo",
    "width": 38.69957081545064
  },
  {
    "field": "order_sequence",
    "width": 72
  },
  {
    "field": "leg_status",
    "width": 89
  },
  {
    "field": "business_name",
    "width": 88.75536480686695
  },
  {
    "field": "pu_name",
    "width": 100.75536480686695
  },
  {
    "field": "pickup_date",
    "width": 86
  },
  {
    "field": "pickup_time",
    "width": 79.5107296137339
  },
  {
    "field": "dl_name",
    "width": 88.75536480686695
  },
  {
    "field": "delivery_date",
    "width": 88.75536480686695
  },
  {
    "field": "delivery_time",
    "width": 84.5107296137339
  },
  {
    "field": "order_type",
    "width": 89.6908210956243
  },
  {
    "field": "action",
    "width": 48.336557059961315
  }
]

export const ordersView = [
  {
    "field": "select",
    "width": 38.69957081545064
  },
  {
    "field": "order_sequence",
    "width": 72
  },
  {
    "field": "order_status",
    "width": 89
  },
  {
    "field": "business_name",
    "width": 88.75536480686695
  },
  {
    "field": "pu_name",
    "width": 100.75536480686695
  },
  {
    "field": "pickup_date",
    "width": 86
  },
  {
    "field": "pickup_time",
    "width": 79.5107296137339
  },
  {
    "field": "dl_name",
    "width": 88.75536480686695
  },
  {
    "field": "delivery_date",
    "width": 88.75536480686695
  },
  {
    "field": "delivery_time",
    "width": 84.5107296137339
  },
  {
    "field": "order_type",
    "width": 89.6908210956243
  },
  {
    "field": "action",
    "width": 85
  }
]

export const ordersDraftView = [
  {
    "field": "order_sequence",
    "width": 72
  },
  {
    "field": "business_name",
    "width": 88.75536480686695
  },
  {
    "field": "order_pu_name",
    "width": 100.75536480686695
  },
  {
    "field": "order_pickup_date",
    "width": 86
  },
  {
    "field": "order_pickup_time",
    "width": 79.5107296137339
  },
  {
    "field": "order_dl_name",
    "width": 88.75536480686695
  },
  {
    "field": "order_delivery_date",
    "width": 88.75536480686695
  },
  {
    "field": "order_delivery_time",
    "width": 84.5107296137339
  },
  {
    "field": "order_type",
    "width": 89.6908210956243
  },
  {
    "field": "action",
    "width": 48.336557059961315
  }
]

export const invoiceView = [
  {
    "field": "order_sequence",
    "width": 126.36802584511149
  },
  {
    "field": "order_status",
    "width": 125.16313274388023
  },
  {
    "field": "hazmat_req",
    "width": 60.374685138539036
  },
  {
    "field": "triaxle",
    "width": 96.4008317740704
  },
  {
    "field": "order_type",
    "width": 92.0969773299748
  },
  {
    "field": "business_name",
    "width": 183.17065491183877
  },
  {
    "field": "order_pu_name",
    "width": 178.0541561712846
  },
  {
    "field": "order_dl_name",
    "width": 158.61146095717882
  },
  {
    "field": "order_container_number",
    "width": 167.62911015499398
  },
  {
    "field": "order_chassis_number",
    "width": 142.2386649874055
  },
  {
    "field": "customer_reference",
    "width": 110.79617869929253
  },
  {
    "field": "booking_number",
    "width": 119.62823715544738
  },
  {
    "field": "action",
    "width": 64.46788413098236
  }
]

export const invoiceGeneratedView = [
  {
    "field": "select",
    "width": 49.19961322344366
  },
  {
    "field": "order_sequence",
    "width": 107.65285620259618
  },
  {
    "field": "order_status",
    "width": 127
  },
  {
    "field": "hazmat_req",
    "width": 76.40316691927586
  },
  {
    "field": "triaxle",
    "width": 82.24190250971776
  },
  {
    "field": "order_type",
    "width": 78.685341040624
  },
  {
    "field": "business_name",
    "width": 134
  },
  {
    "field": "order_pu_name",
    "width": 190.685341040624
  },
  {
    "field": "order_dl_name",
    "width": 168
  },
  {
    "field": "order_container_number",
    "width": 171.73785861428019
  },
  {
    "field": "order_chassis_number",
    "width": 128
  },
  {
    "field": "customer_reference",
    "width": 121
  },
  {
    "field": "booking_number",
    "width": 108
  },
  {
    "field": "action",
    "width": 62.393920449438326
  }
]
