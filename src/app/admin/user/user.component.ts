import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource, Sort, PageEvent } from '@angular/material';
import { Router } from '@angular/router';

import { UserData, UserResponse } from './user.model'
import { AdminService } from '../admin.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { loginData } from 'app/session/session.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public userDetails: loginData; // logged in user details

  public roleComponentName: String = "roles"; // roles api component name

  public driverComponentName: String = "drivers"; // driver api component name

  public userComponentName: String = "users"; // user api component name

  public dropdownComponentName: string = "configurations"; // driver type api component name

  public driverTypes: { label: string, value: number }[]; // to store driver types from api

  public roles: String[]; // to store roles from api

  public searchFilter: boolean = false; // flag to check whether filter enabled or not

  public searchFilterValue: string = ""; // store value for search

  /* user table variables to store datasource,headers,pagination,sorting and catching */
  public userDatasource: MatTableDataSource<UserData>;

  public userHeaderColumns = ['first_name', 'last_name', 'email', 'role', 'role_permissions', 'user_status', 'action'];

  @ViewChild(MatSort) userSort: MatSort;

  public currentPageIndex: any = 0;

  public usersLength: number = 0;

  @ViewChild(MatPaginator) userPaginator: MatPaginator;

  /* variable to manage data from redis cache api */
  public sortColumn: any;

  public sortDirection: any;

  public offset: any = 1;

  public limit: any = 10;

  constructor(private router: Router,
    public dialog: MatDialog,
    private adminService: AdminService,
    private socketService: SocketService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.userDetails = this.sharedService.getUserDetails();

    this.userDatasource = new MatTableDataSource();

    this.getUserDataFromService();

    this.getRoleTypesDataFromService();

    this.getDriverTypesDataFromService();
  }

  getUserDataFromService(): void {
    let request = this.generateRequest()
    this.adminService.getUserList(this.userComponentName, request)
      .subscribe((response: UserResponse) => {
        this.userDatasource.data = response.data.userData;
        this.usersLength = response.data.userPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  getRoleTypesDataFromService(): void {
    this.adminService.getRoleTypesList(this.roleComponentName)
      .subscribe(response => {
        this.roles = response.map(temp => temp.name)
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
      });
  }

  getDriverTypesDataFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'driver')
      .subscribe(response => {
        this.driverTypes = response;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
      });
  }

  onAddUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "role_types": this.roles,
      "driverTypes": this.driverTypes,
      "editMode": false
    };
    let dialogRef = this.dialog.open(EditUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.adminService.postUserData(this.userComponentName, result)
          .subscribe(response => {
            this.socketService.sendChangesInDriverFromClient('driver')
            this.initialPage();
            this.getUserDataFromService();
            this.socketService.sendNotification({
              isNewUserCreated: true,
              newUserId: response.data.user_id,
              role: 'Admin',
              msg: `User ${response.data.first_name} ${response.data.last_name} was created by ${this.userDetails.first_name} ${this.userDetails.last_name}`,
              hyperlink: null,
              icon: 'person_add',
              styleClass: 'mat-text-primary'
            });
            this.sharedService.openSnackbar('User Created Successfully', 2000, 'success-msg')
          },
            error => {
              if (error.error.error.details.messages && error.error.error.details.messages.email[0] == "Email already exists") {
                this.sharedService.openSnackbar(error.error.error.details.messages.email[0], 2000, 'failure-msg')
              } else {
                this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
              }
            });
      }
    });
  };

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilter = true;
      this.initialPage();
      this.getUserDataFromService();
    } else {
      this.searchFilter = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getUserDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilter = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getUserDataFromService();
  }

  /* api serivice for sort */
  onChangeSortDirection(event: Sort) {
    this.sortColumn = event.active == 'role_permissions' ? 'permissions' : event.active;
    this.sortDirection = event.direction;
    this.getUserDataFromService();
  }

  onClickSerialNo(selectedRow: UserData, serialNo: number) {

    let displayData: any = { ...selectedRow };

    displayData.serialNo = serialNo;

    displayData.permissionsView = typeof displayData.role_permissions === 'string' ?
      JSON.parse(displayData.role_permissions) : displayData.role_permissions;

    let listNames = [];
    let columnNames = [];

    Object.keys(displayData.permissionsView).forEach(key => {

      if (typeof displayData.permissionsView[key] === 'object') {

        if (displayData.permissionsView[key].length) {

          listNames.push(key);

          columnNames.push(key);

          displayData[key] = displayData.permissionsView[key].join(', ').trim();

        }

      } else {

        if (displayData.permissionsView[key]) {

          listNames.push(key);

          columnNames.push(key);

          displayData[key] = ' ';

        }

      }

    })

    let multipleListData = {
      "A": {
        listNames: ['ID', 'First Name', 'Last Name', 'Email', 'Role', 'Status'],
        columnNames: ['serialNo', 'first_name', 'last_name', 'email', 'role', 'user_status'],
        selectedData: displayData
      },
      "Permissions": {
        listNames,
        columnNames,
        selectedData: displayData
      }
    }

    this.sharedService.openInfo({
      view: "multipleList",
      name: "User",
      multipleListData
    })
  };

  onClickPermission(selectedRow: UserData) {

    let displayData: any = { ...selectedRow };

    displayData.permissionsView = typeof displayData.role_permissions === 'string' ?
      JSON.parse(displayData.role_permissions) : displayData.role_permissions;

    let listNames = [];

    let columnNames = [];

    Object.keys(displayData.permissionsView).forEach(key => {

      if (typeof displayData.permissionsView[key] === 'object') {

        if (displayData.permissionsView[key].length) {

          listNames.push(key);

          columnNames.push(key);

          displayData[key] = displayData.permissionsView[key].join(', ').trim();

        }

      } else {

        if (displayData.permissionsView[key]) {

          listNames.push(key);

          columnNames.push(key);

          displayData[key] = ' ';

        }

      }

    })

    let multipleListData = {
      "Permissions": {
        listNames,
        columnNames,
        selectedData: displayData
      }
    }

    this.sharedService.openInfo({
      view: "multipleList",
      name: "Permissions",
      multipleListData
    })
  }

  onRefreshUser(selectedRow: UserData) {
    let dialog = this.sharedService.openConfirmation({
      action: "verification",
      name: selectedRow.first_name + " " + selectedRow.last_name,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    });
    dialog.subscribe(result => {
      if (result) {
        selectedRow.emailVerified = false;
        this.adminService.postRefreshUser(this.userComponentName, selectedRow).subscribe(response => {
          this.sharedService.openSnackbar('Verification mail sent successfully', 3000, 'success-msg')
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    })
  }

  onEditUser(selectedRow: UserData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      "role_types": this.roles,
      "driverTypes": this.driverTypes,
      "editMode": true
    };
    let dialogRef = this.dialog.open(EditUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        result.previousRole = selectedRow.role
        this.adminService.updateUserData(this.userComponentName, result).subscribe(
          (response: any) => {
            if (response.code == 200) {
              this.socketService.sendChangesInDriverFromClient('driver');
              this.getUserDataFromService();
              this.sharedService.openSnackbar('User Value Updated Successfully', 2000, 'success-msg');
              if (selectedRow.role === 'Driver' && response.data.role === 'Driver' &&
                selectedRow.user_status === 'active' && response.data.user_status === 'inactive') {

                this.socketService.sendNotification({
                  role: 'Admin',
                  msg: `Driver ${selectedRow.first_name} ${selectedRow.last_name} has been deactivated`,
                  hyperlink: null,
                  icon: 'people',
                  styleClass: 'mat-text-warn'
                });

              } else if (selectedRow.role === 'Driver' && response.data.role === 'Driver' &&
                selectedRow.user_status === 'inactive' && response.data.user_status === 'active') {

                this.socketService.sendNotification({
                  role: 'Admin',
                  msg: `Driver ${selectedRow.first_name} ${selectedRow.last_name} has been activated`,
                  hyperlink: null,
                  icon: 'people',
                  styleClass: 'mat-text-primary'
                });

              }
            } else if (response.code == 400) {
              this.sharedService.openSnackbar(`Not Able Change from Driver Role to ${result.role} Role`, 2000, 'warning-msg');
            } else if (response.code === 409) {
              this.sharedService.openSnackbar("Driver is Assigned. Cannot deactivate", 2500, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
          });
      }
    });
  };

  onDeleteUser(selectedRow: UserData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'User',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.adminService.deleteUser(this.userComponentName, selectedRow.user_id, selectedRow.role).subscribe(
          (response: any) => {
            if (response.code == 200) {
              this.socketService.sendChangesInDriverFromClient('driver')
              this.getUserDataFromService();
              this.sharedService.openSnackbar('User Deleted Successfully', 2000, 'success-msg')
            } else if (response.code == 400) {
              this.sharedService.openSnackbar('Driver is Assigned. Cannot delete', 2000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
          });
      }
    })
  }

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getUserDataFromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request = {
      offset: this.offset,
      limit: this.limit,
      userId: this.userDetails.userId,
      searchFilter: this.searchFilter,
      searchFilterValue: this.searchFilterValue.toLowerCase().replace(/[^a-zA-Z0-9]+/ig, ""),
      column: this.sortColumn,
      direction: this.sortDirection
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.userPaginator.pageIndex = 0;
    this.userPaginator.pageSize = this.userPaginator.pageSize ? this.userPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.userPaginator.pageSize ? this.userPaginator.pageSize : 10;
  }
}

