import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from '../../environments/environment'
import { SharedService } from 'app/shared/service/shared.service';
import { LocationData } from '../shared/models/location.model';
import { UserPostRequest, UserResponse, UserData, UserPostResponse } from './user/user.model';
import { CustomerResponse } from '../shared/models/customer.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { ChassisResponse, ChassisData } from './chassis/chassis.model';
import { ContainerResponse, ContainerData } from './container/container.model';
import { EquipmentResponse, EquipmentData } from './equipment/equipment.model';

@Injectable({
  providedIn: 'root'
})

export class AdminService {
  private baseURL = environment.baseURL;
  public httpOptions = {};

  constructor(private http: HttpClient,
    private sharedService: SharedService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_token': this.sharedService.getAuthToken()
      })
    };
  }

  ChangePassword(actionComponentName: String, newpassword: string, oldpassword: string) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded')
    headers.append('accesstoken', this.sharedService.getAuthToken())
    headers.append('newPassword', newpassword)
    headers.append('oldPassword', oldpassword)
    let validatehttpOptions = {
      headers: headers
    }
    let body = new HttpParams();
    body = body.set('oldPassword', oldpassword)
    body = body.append('newPassword', newpassword)
    // console.log(body)
    return this.http.post<any>(this.baseURL + actionComponentName + "/change-password?access_token=" + this.sharedService.getAuthToken(), body, validatehttpOptions)
  }

  /** USER CRUD & OTHER SERVICE CALL FUNCTIONS
   * postUserData & updateUserData: @body UserData is required to post and update the user data
   * getUserList: function used to fetch user list
   * deleteUser: @param user_id is required to delete user from service
   * dropdown data for role
   */
  postUserData(actionComponentName: String, request: UserData) {
    const url = `${this.baseURL + actionComponentName}`;
    return this.http.post<UserPostResponse>(url, request, this.httpOptions)
  }

  getUserList(actionComponentName: String, request: UserPostRequest) {
    const url = `${this.baseURL + actionComponentName}/getUserDetails`;
    return this.http.post<UserResponse>(url, request, this.httpOptions)
  }

  updateUserData(actionComponentName: String, request: UserData) {
    const url = `${this.baseURL + actionComponentName}/updateUserDetails`;
    return this.http.put<UserData>(url, request, this.httpOptions)
  }

  deleteUser(actionComponentName: String, userID: number, role: string) {
    const url = `${this.baseURL + actionComponentName}/deleteUserDetails?user_id=${userID}&role=${role}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  getRoleTypesList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getRoleType`;
    return this.http.get<any>(url, this.httpOptions)
  }

  getDriverTypeId(actionComponentName: String, driverType) {
    const url = `${this.baseURL + actionComponentName}/getDriverTypeId?driverType=${driverType}`;
    return this.http.get(url, this.httpOptions)
  }

  postRefreshUser(actionComponentName: String, request: UserData) {
    const url = `${this.baseURL + actionComponentName}/resendVerificationEmail`;
    return this.http.post<UserData>(url, request, this.httpOptions)
  }

  /** GET ContainerList from the server */
  getContainerList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getContainerManagementData`;
    return this.http.post<ContainerResponse>(url, request, this.httpOptions)
  }

  /** GET chassis types from the server */
  getChassisTypesList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getChassisName`;
    return this.http.get<String[]>(url, this.httpOptions)
  }

  /** POST: add a new container to the server */
  postContainerData(actionComponentName: String, newContainerData: any) {
    const url = `${this.baseURL + actionComponentName}/postContainerManagement`;
    return this.http.post<any>(url, newContainerData, this.httpOptions)
  }

  /** DELETE: delete the container from the server */
  deleteContainer(actionComponentName: String, containerId: number) {
    const url = `${this.baseURL + actionComponentName}/deleteContainerManagement?container_id=${containerId}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  /** Put: edit a container to the server */
  updateContainerData(actionComponentName: String, newContainerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateContainerManagement`;
    return this.http.put(url, newContainerData, this.httpOptions)
  }

  /** GET Customers from the server */
  getCustomerList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getCustomerDetails`;
    return this.http.post<CustomerResponse>(url, request, this.httpOptions)
  }

  /** POST: add a new location to the server */
  postLocationData(actionComponentName: String, newLocationData: LocationData) {
    const url = `${this.baseURL + actionComponentName}/postLocationManagement`;
    return this.http.post<LocationData>(url, newLocationData, this.httpOptions)
  }

  /** Put: edit a Location to the server */
  updateLocationData(actionComponentName: String, newLocationData: LocationData) {
    const url = `${this.baseURL + actionComponentName}/updateLocationManagement`;
    return this.http.put<LocationData>(url, newLocationData, this.httpOptions)
  }

  /** DELETE: delete the locations from the server */
  deleteLocation(actionComponentName: String, locationId: number) {
    const url = `${this.baseURL + actionComponentName}/deleteLocationManagement?location_id=${locationId}`;
    return this.http.delete<String>(url, this.httpOptions)
  }


  /** GET Chassis Data from the server */
  getChassisList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getChassisManagementData`;
    return this.http.post<ChassisResponse>(url, request, this.httpOptions)
  }

  /** POST: add a new chassis to the server */
  postChassisData(actionComponentName: String, newChassisData: ChassisData) {
    const url = `${this.baseURL + actionComponentName}/postChassisManagement`;
    return this.http.post<ChassisData>(url, newChassisData, this.httpOptions)
  }

  /** Put: edit a customer to the server */
  updateChassisData(actionComponentName: String, newChassisData: ChassisData) {
    const url = `${this.baseURL + actionComponentName}/updateChassisManagement`;
    return this.http.put<ChassisData>(url, newChassisData, this.httpOptions)
  }

  /** DELETE: delete the container from the server */
  deleteChassis(actionComponentName: String, chassisId: number) {
    const url = `${this.baseURL + actionComponentName}/deleteChassisManagement?chassis_id=${chassisId}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  /** Put: edit a customer to the server */
  customerRateBulkUpdate(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateBulkRate`;
    return this.http.put<any>(url, newCustomerData, this.httpOptions)
  }

  /** POST: add a new chassis to the server */
  userLogOut(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/logout`;
    var data = {};
    return this.http.post<any>(url, data, this.httpOptions)
  }

  /** GET Equipments from the server */
  getEquipmentList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getEquipmentManagement`;
    return this.http.post<EquipmentResponse>(url, request, this.httpOptions)
  }

  /** POST: add a new equipment to the server */
  createEquipment(actionComponentName: String, newEquipmentData: EquipmentData) {
    const url = `${this.baseURL + actionComponentName}/postEquipmentManagement`;
    return this.http.post<EquipmentData>(url, newEquipmentData, this.httpOptions)
  }

  /** Put: edit a equipment to the server */
  editEquipment(actionComponentName: String, newEquipmentData: any) {
    const url = `${this.baseURL + actionComponentName}/updateEquipmentManagement`;
    return this.http.put<any>(url, newEquipmentData, this.httpOptions)
  }

  /** DELETE: delete the equipment from the server */
  deleteEquipment(actionComponentName: String, equipmentId: number) {
    const url = `${this.baseURL + actionComponentName}/deleteEquipmentManagement?equipment_id=${equipmentId}`;
    return this.http.delete<any>(url, this.httpOptions)
  }

  /** Put: edit a equipment location to the server */
  editEquipmentLocation(actionComponentName: String, newEquipmentData: any) {
    const url = `${this.baseURL + actionComponentName}/updateEquipmentLocation`;
    return this.http.put<any>(url, newEquipmentData, this.httpOptions)
  }

  /** GET Equipment Location from the server */
  getEquipmentLocation(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getEquipmentLocation`;
    return this.http.get<any>(url, this.httpOptions)
  }

  /** GET selected dropdown types from the server */
  getSelectedDropdownType(actionComponentName: String, selectedType: String) {
    const url = `${this.baseURL + actionComponentName}/getSelectedDropdownTypes?selectedType=${selectedType}`;
    return this.http.get<any>(url, this.httpOptions)
  }

  /* POST: add a new configuration to the server */
  addConfiguration(actionComponentName: String, newFieldData: any) {
    const url = `${this.baseURL + actionComponentName}/postNewConfiguration`;
    return this.http.post<any>(url, newFieldData, this.httpOptions)
  }

  /* Put: edit a configuration to the server */
  editConfiguration(actionComponentName: String, newFieldData: any) {
    const url = `${this.baseURL + actionComponentName}/updateConfiguration`;
    return this.http.put<any>(url, newFieldData, this.httpOptions)
  }

  /* DELETE: delete the configuration from the server */
  deleteConfiguration(actionComponentName: String, selectedDropdown: String, id) {
    const url = `${this.baseURL + actionComponentName}/deleteConfiguration?selectedDropdown=${selectedDropdown}&id=${id}`;
    return this.http.delete<String>(url, this.httpOptions)
  }

  /* POST: add a new configuration to the server */
  postOrderFlag(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/postOrderFlag`;
    return this.http.post<any>(url, request, this.httpOptions)
  }

  /* Put: edit a configuration to the server */
  updateOrderFlag(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateOrderFlag`;
    return this.http.put<any>(url, request, this.httpOptions)
  }

  /* DELETE: delete the configuration from the server */
  deleteOrderFlag(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/deleteOrderFlag`;
    return this.http.post<any>(url, request, this.httpOptions)
  }

  /* DELETE: delete the configuration from the server */
  getArchivedData(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/getArchivedData`;
    return this.http.post<any>(url, request, this.httpOptions)
  }

  customerQuickbookSync(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/customerQuickbookSync`;
    return this.http.post<any>(url, request, this.httpOptions)
  }

  deleteCarrier(actionComponentName: String, id: number) {
    const url = `${this.baseURL + actionComponentName}/deleteCarrier?id=${id}`;
    return this.http.delete<any>(url, this.httpOptions)
  }
}
