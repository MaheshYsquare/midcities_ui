import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';

import { MatCheckboxChange, MatDialogRef, MatPaginator, MatTableDataSource, MAT_DIALOG_DATA, PageEvent, Sort } from '@angular/material';

import { CustomerQuickbooks } from 'app/shared/models/customer.model';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-quickbook-customer',
  templateUrl: './quickbook-customer.component.html',
  styleUrls: ['./quickbook-customer.component.scss']
})
export class QuickbookCustomerComponent implements OnInit {

  public searchFilterEvent: boolean = false;

  public searchFilterValue: string = "";

  public quickbooksCustomerDatasource: MatTableDataSource<CustomerQuickbooks>;

  public customerSelection = new SelectionModel<CustomerQuickbooks>(true, []);

  public selectedCustomers: any[] = [];

  public unSelectedCustomers: any[] = [];

  public isCustomerSelected: boolean;

  public quickbooksCustomerColumns: string[] = ['select', 'DisplayName', 'Active'];

  public pageLength: number = 0;

  @ViewChild('paginator') paginator: MatPaginator;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  public offset: number = 1;

  public limit: number = 5;

  public sortColumn: string;

  public sortDirection: string;

  public checkSelectedCustomer: any[] = [];

  public checkUnselectedCustomer: any[] = [];

  constructor(private dialogRef: MatDialogRef<QuickbookCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.quickbooksCustomerDatasource = new MatTableDataSource();

    this.getQuickbooksCustomerFromService();
  }

  getQuickbooksCustomerFromService(): void {
    let request = this.generateRequest()
    this.sharedService.quickbooksCustomer('customers', request)
      .subscribe(response => {
        this.quickbooksCustomerDatasource.data = response.data.customerData;
        this.pageLength = response.data.customerPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilterEvent = true;
      this.initialPage();
      this.getQuickbooksCustomerFromService();
    } else {
      this.searchFilterEvent = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getQuickbooksCustomerFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilterEvent = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getQuickbooksCustomerFromService();
  }

  /* api service for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getQuickbooksCustomerFromService();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllCustomerSelected() {
    const numSelected = this.selectedCustomers.length;
    const numRows = this.quickbooksCustomerDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  customerSelect(event: MatCheckboxChange) {
    this.selectedCustomers = event.checked ?
      this.quickbooksCustomerDatasource.data : [];

    this.checkSelectedCustomer = event.checked ?
      this.quickbooksCustomerDatasource.data.map(temp => temp.Id) : []

    event.checked ?
      this.quickbooksCustomerDatasource.data.forEach(row => this.customerSelection.select(row))
      : this.customerSelection.clear();

    this.isCustomerSelected = this.isAllCustomerSelected();

    this.unSelectedCustomers = [];

    this.checkUnselectedCustomer = [];
  }

  onSelectRow(selectedRow: CustomerQuickbooks, event: MatCheckboxChange, index) {

    event.checked ?
      this.selectedCustomers.push(selectedRow) :
      this.selectedCustomers = this.selectedCustomers.filter(temp => temp.Id !== selectedRow.Id);

    event.checked ?
      this.unSelectedCustomers = this.unSelectedCustomers.filter(temp => temp.Id !== selectedRow.Id) :
      this.unSelectedCustomers.push(selectedRow);

    event.checked ?
      this.checkSelectedCustomer.push(selectedRow.Id) :
      this.checkSelectedCustomer = this.checkSelectedCustomer.filter(temp => temp !== selectedRow.Id);

    event.checked ?
      this.checkUnselectedCustomer = this.checkUnselectedCustomer.filter(temp => temp !== selectedRow.Id) :
      this.checkUnselectedCustomer.push(selectedRow.Id);

    let dataSelection = [];

    this.quickbooksCustomerDatasource.data.forEach(row => {
      dataSelection.push(this.customerSelection.isSelected(row))
    });
  }

  onChangePage(event: PageEvent) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getQuickbooksCustomerFromService();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.selectedCustomers && this.selectedCustomers.length) {
      this.dialogRef.close(this.selectedCustomers);
    }
  }

  generateRequest() {
    let request: any = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: this.searchFilterEvent,
      searchFilterValue: this.searchFilterValue,
      column: this.sortColumn,
      direction: this.sortDirection
    }
    return request
  }

  initialPage() {
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = this.paginator.pageSize ? this.paginator.pageSize : 5;
    this.offset = 1;
    this.limit = this.paginator.pageSize ? this.paginator.pageSize : 5;
  }

}
