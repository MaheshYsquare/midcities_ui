import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialogConfig, MatDialog, MatPaginator, MatSort, Sort, PageEvent } from '@angular/material';
import { Router } from '@angular/router';

import { AdminService } from '../admin.service';
import { CustomerData, CustomerColumnFilter } from '../../shared/models/customer.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { NgForm } from '@angular/forms';
import { SharedService } from 'app/shared/service/shared.service';
import { QuickbookCustomerComponent } from './quickbook-customer/quickbook-customer.component';
import { EditCustomerComponent } from 'app/shared/modules/customer-shared/edit-customer.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  animations: []
})
export class CustomerComponent implements OnInit {

  public componentName: String = "customers";

  public dropdownComponentName: string = "configurations";

  public locationTypes: { label: string; value: number; }[];

  public columnFilter: boolean = false;

  @ViewChild('f') filterForm: NgForm;

  public columnFilterValue: CustomerColumnFilter = { business_name: '', point_of_contact: '', business_phone: '', email: '', is_active: '' };

  public searchFilter: boolean = false;

  public searchFilterValue: string = "";

  public customerDatasource: MatTableDataSource<CustomerData>;

  public currentPageIndex: number = 0;

  public customerFilterHeaders: string[] = ['dummy', 'businessName', 'pointOfContact', 'businessPhone', 'businessMail', 'Status', 'icon'];

  public customerColumns: string[] = ['serialNo', 'business_name', 'point_of_contact', 'business_phone', 'email', 'is_active', 'action'];

  @ViewChild(MatSort) customerSort: MatSort;

  @ViewChild(MatPaginator) customerPaginator: MatPaginator;

  public customersLength: number = 0;

  public sortColumn: string;

  public sortDirection: string;

  public offset: number = 1;

  public limit: number = 10;

  public pointOfContact = [];

  public pointOfContactValue = [];

  public emailOfContact = [];

  public emailOfContactValue = [];

  constructor(public dialog: MatDialog,
    private adminService: AdminService,
    private sharedService: SharedService,
    private router: Router) { }

  ngOnInit() {
    this.customerDatasource = new MatTableDataSource();

    this.getCustomerDataFromService();

    this.getDropdownFromService();
  }

  getCustomerDataFromService(): void {
    
    let arrayInside = []
    let pointOfContactarrayInside = [];
    let emailOfContactarrayInside = []

    let request = this.generateRequest();
    this.adminService.getCustomerList(this.componentName, request).subscribe(response => {
      this.customerDatasource.data = response.data.customerData;
      this.customersLength = response.data.customerPaginationLength;
      // this.customerDatasource.data = responseDup.data.customerData;
      // this.customersLength = responseDup.data.customerPaginationLength;

      // this.pointOfContact = [];
      // this.customerDatasource.data.forEach((x,y)=>{
      //   this.pointOfContact.push(x.point_of_contact)
      // })
      // this.pointOfContact.forEach((x,y)=>{
      //   arrayInside = []
      //    if(x){
      //     x.split(',').forEach(z=>{
      //         arrayInside.push(z)
      //     })
      //    }
      //       pointOfContactarrayInside.push(arrayInside);
      // });
      // this.pointOfContact = [];
      // pointOfContactarrayInside.forEach((x,y)=>{
      //   x = x.filter(a => a)
      //   this.pointOfContact.push(x)
      // })

      this.pointOfContact = [];
      this.customerDatasource.data.forEach((x,y)=>{
        this.pointOfContact.push(x.contact_details)
      })
      this.pointOfContact.forEach((x,y)=>{
        let array = [];
        let arrayEmail = [];
        let arrayWoE = [];
        if(x.length > 0){
          x.forEach((w,z)=>{
            if(w.point_of_contact != null ){
              array.push(w.point_of_contact);
            } else {
              array.push(w.point_of_contact = '');
            }

            if(w.email != null){
              arrayEmail.push(w.email);
            } else {
              arrayEmail.push(w.email = '');
            }
           })
        } else {
          array.push(x.point_of_contact ='')
          arrayEmail.push(x.email = '');
        }
         
        pointOfContactarrayInside[y]=array;
        emailOfContactarrayInside[y]=arrayEmail;
        this.pointOfContactValue[y]= array.filter(a => a)
        this.emailOfContactValue[y] = arrayEmail.filter(a => a);

      });
      this.pointOfContact = [];
      this.pointOfContact = pointOfContactarrayInside;
      this.emailOfContact = emailOfContactarrayInside;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    });
  }

  getDropdownFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'location')
      .subscribe(response => {
        this.locationTypes = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 800, 'failure-msg');
      });
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getCustomerDataFromService()
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getCustomerDataFromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getCustomerDataFromService()
  }

  onAddCustomer() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": false,
      "locationTypes": this.locationTypes
    };
    let dialogRef = this.dialog.open(EditCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage();
        this.getCustomerDataFromService();
        this.sharedService.openSnackbar('Customer Created Successfully', 500, 'success-msg')
      }
    });
  };

  onClickQuicbooksSync() {
    let request = this.generateRequest();
    this.adminService.customerQuickbookSync(this.componentName, request).subscribe(response => {
      this.customerDatasource.data = response.data.customerData;
      this.customersLength = response.data.customerPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    });
  }

  onClickImportCustomer() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {};
    let dialogRef = this.dialog.open(QuickbookCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.importQuickbooksCustomer(this.componentName, { customersData: result })
          .subscribe(response => {
            this.initialPage();
            this.getCustomerDataFromService();
            this.sharedService.openSnackbar('Customer Imported Successfully', 500, 'success-msg')
          }, error => {
            this.sharedService.openSnackbar("SOmething Went Wrong", 2500, 'failure-msg');
          })
      }
    });
  }

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilter = true;
      this.initialPage();
      this.getCustomerDataFromService();
    } else {
      this.searchFilter = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getCustomerDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilter = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getCustomerDataFromService();
  }

  /* api serivice for sort */
  onChangeSortDirection(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getCustomerDataFromService();
  }

  onClickSerialNo(selectedRow: CustomerData, serialNo: number) {

    let displayData: any = { ...selectedRow };

    displayData.serialNo = serialNo;

    displayData.status = displayData.is_active ? 'Active' : 'In Active';

    displayData.emailLists = displayData.email ? displayData.email.split(',').join(',\n') : null;

    this.sharedService.openInfo({
      view: "list",
      name: "Customer",
      listNames: ['ID', 'Business Name', 'Phone Number', 'Email', 'Business Address', 'Zip Code', 'City', 'State', 'Fax', 'Status'],
      columnNames: ['serialNo', 'business_name', 'business_phone', 'emailLists', 'b_address', 'b_postal_code', 'b_city', 'b_state', 'fax', 'status'],
      selectedData: displayData
    })
  };

  onClickCustomerName(selectedRow: CustomerData) {
    this.router.navigate(
      ['admin-features', 'customer-management', 'customer-info'],
      {
        queryParams: {
          customerId: selectedRow.customer_id
        }
      });
  }

  onClickOpenEmailLists(selectedRow: CustomerData) {

    let email = [];
    let emailString =''
    selectedRow.contact_details.forEach(x=>{
      if(x.email){
        email.push(x.email);
      }
    });
    selectedRow.email = email.join(',');
    console.log(emailString)

    this.sharedService.openInfo({
      view: "onlyList",
      name: "Email List",
      selectedData: selectedRow.email ? selectedRow.email.split(',') : [],
      isHover: false
    })
  }

  onClickOpenPonitOfCotactLists(selectedRow: CustomerData) {

    let pointOfContact = [];
    selectedRow.contact_details.forEach(x=>{
      if(x.point_of_contact){
        pointOfContact.push(x.point_of_contact);
      }
    });
    selectedRow.point_of_contact = pointOfContact.join(',');

    this.sharedService.openInfo({
      view: "onlyList",
      name: "Point of Contact List",
      selectedData: selectedRow.point_of_contact ? selectedRow.point_of_contact.split(',') : [],
      isHover: false
    })
  }

  onEditCustomer(selectedRow: CustomerData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": true,
      "selectedValue": selectedRow,
      "locationTypes": this.locationTypes
    };
    let dialogRef = this.dialog.open(EditCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getCustomerDataFromService();
        this.sharedService.openSnackbar('Customer Updated Successfully', 500, 'success-msg')
      }
    });
  }

  onDeactivateCustomer(selectedRow: CustomerData, isActive: boolean) {
    if (isActive) {
      this.makeInactiveOrActiveService(selectedRow, isActive)
    } else {
      let dialogRef = this.sharedService.openConfirmation({
        action: 'inactive',
        name: selectedRow.business_name,
        cancelLabel: 'No',
        confirmLabel: 'Yes',
        confirmColor: 'red'
      })

      dialogRef.subscribe(result => {
        if (result) {
          this.makeInactiveOrActiveService(selectedRow, isActive)
        }
      })
    }
  };

  makeInactiveOrActiveService(selectedRow: CustomerData, isActive: boolean) {
    let request = {
      is_active: isActive,
      customer_quickbooks_id: selectedRow.customer_quickbooks_id,
      customer_quickbooks_sync: selectedRow.customer_quickbooks_sync,
      customer_id: selectedRow.customer_id
    }
    this.sharedService.makeActiveOrInactive(this.componentName, request).subscribe(
      (response: any) => {
        if (response.code == 200) {
          this.getCustomerDataFromService()
          this.sharedService.openSnackbar(`Customer ${isActive ? 'Activated' : 'Deactivated'} Successfully`, 2000, 'success-msg')
        } else if (response.code == 422) {
          this.sharedService.openSnackbar("Customer Having Open Invoice", 2000, "warning-msg");
        } else if (response.code == 103) {
          this.sharedService.quickbooksConnection(this.makeInactiveOrActiveService.bind(this, selectedRow, isActive));
        }
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }
  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getCustomerDataFromService()
        break;
      case 20:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getCustomerDataFromService()
        break;
      case 30:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getCustomerDataFromService()
        break;
    }
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: this.searchFilter,
      searchFilterValue: this.searchFilterValue.toLowerCase().replace(/[^a-zA-Z0-9@]+/ig, ""),
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.customerPaginator.pageIndex = 0;
    this.customerPaginator.pageSize = this.customerPaginator.pageSize ? this.customerPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.customerPaginator.pageSize ? this.customerPaginator.pageSize : 10;
  }
}