import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MatTableDataSource, MatSort, MatPaginator, PageEvent, Sort, MatDialogConfig, MatDialog } from '@angular/material';

import { loginData } from 'app/session/session.model';
import { archivedFilterColumns } from 'app/shared/models/display-column.model';
import { OrderData } from 'app/shared/models/order.model';
import { SharedService } from 'app/shared/service/shared.service';
import { AdminService } from '../admin.service';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';

@Component({
  selector: 'app-archived',
  templateUrl: './archived.component.html',
  styleUrls: ['./archived.component.scss']
})
export class ArchivedComponent implements OnInit {

  public userDetails: loginData;

  public archivedComponentName: string = 'archives';

  public archivedOrdersDatasource: MatTableDataSource<any>;

  @ViewChild(MatSort) archivedOrdersSort: MatSort;

  public isAdminInvoice: boolean;

  public archivedOrdersColumns: string[] = ['order_sequence', 'order_status', 'order_type', 'business_name',
    'pu_name', 'dl_name', 'hazmat_req', 'triaxle', 'container_number', 'chassis_number', 'customer_reference',
    'booking_number', 'paid', 'write_off', 'date_of_payment', 'action'];

  @ViewChild(MatPaginator) archivedOrdersPaginator: MatPaginator;

  public archivedOrdersLength: number = 0;

  public offset: number = 1;

  public limit: number = 10;

  public sortColumn: string;

  public sortDirection: string;

  public columnFilter: boolean = false;

  public filterData: any;

  constructor(private sharedService: SharedService,
    private adminService: AdminService,
    private router: Router,
    private routes: ActivatedRoute,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.userDetails = this.sharedService.getUserDetails();

    this.archivedOrdersDatasource = new MatTableDataSource();

    this.isAdminInvoice = (this.userDetails.role === 'Admin' || this.userDetails.role === 'Invoice');

    this.getArchivedOrdersFromService();
  }

  getArchivedOrdersFromService() {
    let request = this.generateRequest();
    this.adminService.getArchivedData(this.archivedComponentName, request).subscribe(response => {
      this.archivedOrdersDatasource.data = response.data.archivedOrder;
      this.archivedOrdersLength = response.data.archivedOrderPaginationLength;
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
    })
  }

  /** 
   *  open filter component and filter archived data in backend 
  **/
  onClickFilterColumns() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "filterData": this.filterData,
      "filterColumns": archivedFilterColumns,
      "isArchived": true,
      "key": "archived_orders"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.filterData = result;
        this.columnFilter = true;
        this.initialPage();
        this.getArchivedOrdersFromService();
      }
    });
  };

  onClickReset() {
    this.columnFilter = false;
    this.filterData = undefined;
    this.initialPage();
    this.getArchivedOrdersFromService();
  }

  onChangeSortDirection(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getArchivedOrdersFromService()
  }

  onClickOrderNumber(selectedRow) {
    if (!selectedRow.invoice_type || selectedRow.invoice_type === "auto") {
      this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.routes });
    } else {
      this.router.navigate(["manual-invoice", selectedRow.invoice_number], { relativeTo: this.routes });
    }
  }

  onClickMoveBackToInvoice(selectedRow: OrderData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'moveOrderToInvoice',
      cancelLabel: 'No',
      confirmColor: 'rgb(24, 102, 219)',
      confirmLabel: 'Yes'
    });

    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          order_status: 'Delivered',
          order_id: selectedRow.order_id,
          order_container_chassis_id: selectedRow.order_container_chassis_id
        }
        this.sharedService.makeOrderAsNoInvoice('orders', request).subscribe(response => {
          this.getArchivedOrdersFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 3500, 'failure-msg');
        })
      }
    })
  }

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getArchivedOrdersFromService();

  }

  /* to generate format for the post request */
  generateRequest() {
    let request: any = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      filterData: this.filterData,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }

  initialPage() {
    this.archivedOrdersPaginator.pageIndex = 0;
    this.archivedOrdersPaginator.pageSize = this.archivedOrdersPaginator.pageSize ?
      this.archivedOrdersPaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.archivedOrdersPaginator.pageSize ?
      this.archivedOrdersPaginator.pageSize : 10;
  }

}
