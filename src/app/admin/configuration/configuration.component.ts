import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  MatTableDataSource,
  MatPaginator,
  MatDialogConfig,
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material';

import { AdminService } from '../admin.service';
import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  displayedColumns: any;

  dataSource: MatTableDataSource<any>;

  currentIndex: any = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild('f') configurationForm: NgForm;

  componentName: String = "configurations";

  public userDetails: any;

  public userName: string;

  public selectedDropdown: string;

  public editMode: boolean = false;

  public selectedRow: any;

  public setFieldName: string;

  public pageSize: any = 10;

  public lastUpdated: any;

  public updatedBy: any;

  public color: any;

  public id: number;

  public confirmationComponentName: string;

  public previousRangeFrom: number;

  public previousRangeTo: number;

  constructor(private adminService: AdminService,
    private sharedService: SharedService,
    private socketService: SocketService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();

    this.userDetails = this.sharedService.getUserDetails();

    this.userName = this.userDetails.first_name + " " + this.userDetails.last_name;
  }

  onChangePage(event) {
    this.currentIndex = (event.pageIndex * event.pageSize);
    this.pageSize = event.pageSize;
  }

  onChange(event): void {
    if (event.value === 'Order Types' || event.value === 'Leg Types' || event.value === 'Tags') {
      this.displayedColumns = ['serialNo', 'dropdown', 'field_name', 'color', 'last_updated', 'updated_by', 'action'];
    } else if (event.value === 'Order Flags') {
      this.displayedColumns = ['serialNo', 'dropdown', 'field_name', 'miles_range', 'last_updated', 'updated_by', 'action'];
    } else {
      this.displayedColumns = ['serialNo', 'dropdown', 'field_name', 'last_updated', 'updated_by', 'action'];
    }

    this.selectedDropdown = event.value;

    this.getDropdownDatafromService(this.selectedDropdown);

    this.editMode = false;

    switch (this.selectedDropdown) {
      case 'Location Types':
        this.confirmationComponentName = 'Location Type';
        break;
      case 'Container Types':
        this.confirmationComponentName = 'Container Type';
        break;
      case 'Order Types':
        this.confirmationComponentName = 'Order Type';
        break;
      case 'Leg Types':
        this.confirmationComponentName = 'Leg Type';
        break;
      case 'Driver Types':
        this.confirmationComponentName = 'Driver Type';
        break;
      case 'Equipment Types':
        this.confirmationComponentName = 'Equipment Type';
        break;
      case 'Equipment Locations':
        this.confirmationComponentName = 'Equipment Location';
        break;
      case 'Tags':
        this.confirmationComponentName = 'Tag';
        break;
      case 'Order Flags':
        this.confirmationComponentName = 'Order Flag';
        break;
      case 'Attendence Category':
        this.confirmationComponentName = 'Attendence Category';
        break;
    }
  }

  onSubmit() {
    let request;
    let rangeFlag = false;
    let nameFlag = false;

    if (this.configurationForm.valid) {

      if (this.editMode) {

        if (this.selectedDropdown === 'Order Flags') {
          request = {
            'order_flag': this.configurationForm.value.field_name,
            'range_from': this.configurationForm.value.range_from,
            'range_to': this.configurationForm.value.range_to === '' ? null : this.configurationForm.value.range_to,
            'user_name': this.userName,
            'order_flag_id': this.id,
            'previousRangeFrom': this.previousRangeFrom,
            'previousRangeTo': this.previousRangeTo
          }
          this.dataSource.data.filter(temp => temp.order_flag_id !== this.id).forEach(temp => {
            if ((temp.range_to === null && this.configurationForm.value.range_to === null) ||
              (this.configurationForm.value.range_to === null && temp.range_to >= this.configurationForm.value.range_from) ||
              (temp.range_to === null && temp.range_from <= this.configurationForm.value.range_to) ||
              (temp.range_from <= this.configurationForm.value.range_to && temp.range_to >= this.configurationForm.value.range_from)) {
              rangeFlag = rangeFlag ? true : true;
            }
            (temp.order_flag === this.configurationForm.value.field_name) ?
              (nameFlag = nameFlag ? true : true) : null;
          });
          if (this.configurationForm.value.range_to !== null && this.configurationForm.value.range_from >= this.configurationForm.value.range_to) {
            this.sharedService.openSnackbar('Range from should be lesser than range to and both the values should not be the same', 3000, 'warning-msg');
          } else if (rangeFlag) {
            this.sharedService.openSnackbar('the given range collides with existing ranges', 3000, 'warning-msg');
          } else if (nameFlag) {
            this.sharedService.openSnackbar('order flag already exist', 3000, 'warning-msg');
          } else {
            this.orderFlagDialogwithService(request);
          }
        } else {
          request = {
            'dropdown': this.selectedDropdown,
            'fieldName': (this.selectedDropdown === 'Order Types' ||
              this.selectedDropdown === 'Leg Types' ||
              this.selectedDropdown === 'Tags' && this.setFieldName === 'BROKERED') ?
              this.setFieldName : this.configurationForm.value.field_name,
            'color': this.configurationForm.value.color,
            'user_name': this.userName,
            'id': this.id
          }
          this.generalDialogwithService(request);
        }
      } else {
        if (this.selectedDropdown === 'Order Flags') {
          request = {
            'order_flag': this.configurationForm.value.field_name,
            'range_from': this.configurationForm.value.range_from,
            'range_to': this.configurationForm.value.range_to === '' ? null : this.configurationForm.value.range_to,
            'user_name': this.userName
          }
          this.dataSource.data.forEach(temp => {
            if ((temp.range_to === null && this.configurationForm.value.range_to === null) ||
              (this.configurationForm.value.range_to === null && temp.range_to >= this.configurationForm.value.range_from) ||
              (temp.range_to === null && temp.range_from <= this.configurationForm.value.range_to) ||
              (temp.range_from <= this.configurationForm.value.range_to && temp.range_to >= this.configurationForm.value.range_from)) {
              rangeFlag = rangeFlag ? true : true;
            }
            (temp.order_flag === this.configurationForm.value.field_name) ?
              (nameFlag = nameFlag ? true : true) : null;
          });
          if (this.configurationForm.value.range_to !== null && this.configurationForm.value.range_from >= this.configurationForm.value.range_to) {
            this.sharedService.openSnackbar('Range from should be lesser than range to and both the values should not be the same', 3000, 'warning-msg');
          } else if (rangeFlag) {
            this.sharedService.openSnackbar('the given range collides with existing ranges', 3000, 'warning-msg');
          } else if (nameFlag) {
            this.sharedService.openSnackbar('order flag already exist', 3000, 'warning-msg');
          } else {
            this.orderFlagDialogwithService(request);
          }
        } else if (this.selectedDropdown !== 'Order Types' && this.selectedDropdown !== 'Leg Types') {
          request = {
            'dropdown': this.selectedDropdown,
            'field_name': this.configurationForm.value.field_name,
            'color': this.configurationForm.value.color,
            'user_name': this.userName
          }
          this.generalDialogwithService(request);
        }
      }
    }
  }

  generalDialogwithService(request) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'oldValue': this.setFieldName,
      'newValue': this.configurationForm.value.field_name,
      'type': this.editMode ? this.confirmationComponentName : this.selectedDropdown,
      'editMode': this.editMode
    };
    const dialogRef = this.dialog.open(EditConfirmationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (this.editMode) {
          this.adminService.editConfiguration(this.componentName, request).subscribe(response => {
            if (response.code == 200) {
              this.socketService.sendNotification({
                role: 'Admin',
                msg: this.setFieldName === this.configurationForm.value.field_name ? `Menu Configuration ${this.selectedDropdown} has been updated on ${this.setFieldName}` :
                  `Menu Configuration ${this.selectedDropdown} has been updated from ${this.setFieldName} to ${this.configurationForm.value.field_name}`,
                hyperlink: null,
                icon: 'info',
                styleClass: 'mat-text-accent'
              })
              this.getDropdownDatafromService(this.selectedDropdown);
              this.editMode = false;
            } else {
              this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
          });
        } else {
          this.adminService.addConfiguration(this.componentName, request).subscribe(
            response => {
              if (response.code == 200) {
                this.getDropdownDatafromService(this.selectedDropdown)
              } else {
                this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
              }
            }, error => {
              this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
            });
        }
      }
    })
  }

  orderFlagDialogwithService(request) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'oldValue': this.setFieldName,
      'newValue': this.configurationForm.value.field_name,
      'type': this.editMode ? this.confirmationComponentName : this.selectedDropdown,
      'editMode': this.editMode
    };
    const dialogRef = this.dialog.open(EditConfirmationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (this.editMode) {
          this.adminService.updateOrderFlag(this.componentName, request).subscribe(response => {
            if (response.code == 200) {
              this.socketService.sendNotification({
                role: 'Admin',
                msg: this.setFieldName === this.configurationForm.value.field_name ? `Menu Configuration ${this.selectedDropdown} has been updated on ${this.setFieldName}` :
                  `Menu Configuration ${this.selectedDropdown} has been updated from ${this.setFieldName} to ${this.configurationForm.value.field_name}`,
                hyperlink: null,
                icon: 'info',
                styleClass: 'mat-text-accent'
              })
              this.getDropdownDatafromService(this.selectedDropdown);
              this.editMode = false;
            } else if (response.code == 402) {
              this.sharedService.openSnackbar(response.error, 3000, 'warning-msg');
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg');
          });
        } else {
          this.adminService.postOrderFlag(this.componentName, request).subscribe(
            response => {
              if (response.code == 200) {
                this.getDropdownDatafromService(this.selectedDropdown)
              } else if (response.code == 402) {
                this.sharedService.openSnackbar(response.error, 3000, 'warning-msg');
              }
            }, error => {
              this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg');
            });
        }
      }
    })
  }

  getDropdownDatafromService(selectedType): void {
    this.adminService.getSelectedDropdownType(this.componentName, selectedType).subscribe(response => {
      if (response.code == 200) {
        this.dataSource.data = response.data;
        this.currentIndex = (0 * this.pageSize);
        this.paginator.pageIndex = 0;
        this.dataSource.paginator = this.paginator;
        if (this.selectedDropdown === 'Order Types' || this.selectedDropdown === 'Leg Types') {
          this.configurationForm.controls.field_name.reset();
          this.configurationForm.controls.color.reset();
        } else if (this.selectedDropdown === 'Order Flags') {
          this.configurationForm.controls.field_name.reset();
          this.configurationForm.controls.range_from.reset();
          this.configurationForm.controls.range_to.reset();
        } else {
          this.configurationForm.controls.field_name.reset();
        }
      }
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
    });
  }

  onEditConfiguration(selectedRow: any) {
    this.selectedRow = selectedRow;

    this.editMode = true;

    this.onSetField(selectedRow);

    this.configurationForm.form.patchValue({
      'field_name': this.setFieldName,
      'color': selectedRow.color,
      'range_from': selectedRow.range_from,
      'range_to': selectedRow.range_to
    })
  }

  onDeleteConfiguration(selectedRow: any) {
    this.onSetField(selectedRow);

    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: this.confirmationComponentName,
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        if (this.selectedDropdown === 'Order Flags') {
          let request = {
            range_from: selectedRow.range_from,
            range_to: selectedRow.range_to,
            order_flag_id: selectedRow.order_flag_id
          }
          this.adminService.deleteOrderFlag(this.componentName, request).subscribe((response: any) => {
            if (response.code == 200) {
              this.editMode = false;
              this.getDropdownDatafromService(this.selectedDropdown)
            } else if (response.code == 400 && response.error == 'type is used') {
              this.sharedService.openSnackbar('This field is being used in the application. Please edit/delete it there and try again.', 3000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
          })
        } else {
          this.adminService.deleteConfiguration(this.componentName, this.selectedDropdown, this.id).subscribe((response: any) => {
            if (response.code == 200) {
              this.editMode = false;
              this.getDropdownDatafromService(this.selectedDropdown)
            } else if (response.code == 400 && response.error == 'type is used') {
              this.sharedService.openSnackbar('This field is being used in the application. Please edit/delete it there and try again.', 3000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
          })
        }
      }
    })
  }

  onClickSerialNo(selectedRow, serialNo) {
    this.onSetField(selectedRow);

    let displayData;

    let listNames: string[] = [];

    if ((this.selectedDropdown == 'Leg Types' || this.selectedDropdown == 'Order Types' ||
      this.selectedDropdown === 'Tags')) {
      displayData = {
        'serialNo': serialNo,
        'dropdown': this.selectedDropdown,
        'field_name': this.setFieldName,
        'color': this.color,
        'last_updated': this.lastUpdated,
        'updated_by': this.updatedBy
      }

      listNames = ['ID', 'Dropdown', 'Field Name', 'Color', 'Last Updated', 'Updated By'];
    } else if (this.selectedDropdown == 'Order Flags') {
      displayData = {
        'serialNo': serialNo,
        'dropdown': this.selectedDropdown,
        'field_name': this.setFieldName,
        'miles_range': selectedRow.range_to ? `${selectedRow.range_from} - ${selectedRow.range_to}`
          : `${selectedRow.range_from}+`,
        'last_updated': this.lastUpdated,
        'updated_by': this.updatedBy
      }

      listNames = ['ID', 'Dropdown', 'Field Name', 'Miles Range', 'Last Updated', 'Updated By'];

    } else {

      displayData = {
        'serialNo': serialNo,
        'dropdown': this.selectedDropdown,
        'field_name': this.setFieldName,
        'last_updated': this.lastUpdated,
        'updated_by': this.updatedBy
      }

      listNames = ['ID', 'Dropdown', 'Field Name', 'Last Updated', 'Updated By'];

    }

    this.sharedService.openInfo({
      view: "list",
      name: "Menu Configuration",
      listNames,
      columnNames: this.displayedColumns,
      selectedData: displayData
    })
  }

  onSetField(selectedRow: any) {
    this.lastUpdated = selectedRow.lastupdated_on !== null ? new Date(selectedRow.lastupdated_on).toLocaleDateString() : '--'
    this.updatedBy = selectedRow.lastupdated_by !== null ? selectedRow.lastupdated_by : '--'
    this.color = selectedRow.color
    switch (this.selectedDropdown) {
      case 'Location Types':
        this.setFieldName = selectedRow.location_type
        this.id = selectedRow.location_type_id
        break;
      case 'Container Types':
        this.setFieldName = selectedRow.container_type
        this.id = selectedRow.container_type_id
        break;
      case 'Order Types':
        this.setFieldName = selectedRow.order_type
        this.id = selectedRow.order_type_id
        break;
      case 'Leg Types':
        this.setFieldName = selectedRow.leg_type
        this.id = selectedRow.leg_type_id
        break;
      case 'Driver Types':
        this.setFieldName = selectedRow.driver_type
        this.id = selectedRow.driver_type_id
        break;
      case 'Equipment Types':
        this.setFieldName = selectedRow.equipment_type
        this.id = selectedRow.equipment_type_id
        break;
      case 'Equipment Locations':
        this.setFieldName = selectedRow.equipment_location
        this.id = selectedRow.equipment_location_id
        break;
      case 'Tags':
        this.setFieldName = selectedRow.tag_title
        this.id = selectedRow.tag_id
        break;
      case 'Order Flags':
        this.setFieldName = selectedRow.order_flag
        this.id = selectedRow.order_flag_id;
        this.previousRangeFrom = selectedRow.range_from;
        this.previousRangeTo = selectedRow.range_to;
        break;
      case 'Attendence Category':
        this.setFieldName = selectedRow.title;
        this.id = selectedRow.driver_attendence_category_id;
      default:
        break;
    }
  }

}

@Component({
  selector: 'app-edit-confirmation',
  template: `<div class="add-dailog position" cdkDrag cdkDragRootElement=".cdk-overlay-pane" cdkDragBoundary="body">
  
  <div class="drag-drop-handle" cdkDragHandle>
    <svg width="24px" fill="currentColor" viewBox="0 0 24 24">
      <path
        d="M10 9h4V6h3l-5-5-5 5h3v3zm-1 1H6V7l-5 5 5 5v-3h3v-4zm14 2l-5-5v3h-3v4h3v3l5-5zm-9 3h-4v3H7l5 5 5-5h-3v-3z">
      </path>
      <path d="M0 0h24v24H0z" fill="none"></path>
    </svg>
  </div>

  <mat-card-title fxLayout="row" fxLayoutAlign="start center">
  {{ data.editMode ? 'Edit' : 'Create' }} Confirmation
  </mat-card-title>

  <mat-card-content>
    <h5 *ngIf="data.editMode && data.type !== 'Order Flag'">Are you sure want to Edit {{ data.type }} from {{ data.oldValue }} to {{ data.newValue }}?</h5>
    <h5 *ngIf="data.editMode && data.type === 'Order Flag'">Are you sure want to Edit {{ data.type }} details ?</h5>
    <h5 *ngIf="!data.editMode">Are you sure want to Create {{ data.newValue }} in {{ data.type }}?</h5>
  </mat-card-content>
  <button mat-button type="button" id="mr-1" class="mat-raised-button table-btn-rt mr-1"
  [ngStyle]="{'background-color': data.editMode ? editButtonColor : addButtonColor}"
    (click)="dialogRef.close('yes')">{{ data.editMode ? 'Save' : 'Create' }}</button>
  <button mat-button type="button" id="mr-1" class="mat-raised-button table-btn-rt mr-1" style="background-color:grey;"
    (click)="dialogRef.close()">Cancel</button>
</div>`
})
export class EditConfirmationComponent {
  public addButtonColor = '#2196f3';

  public editButtonColor = 'rgb(45, 185, 26)';

  constructor(public dialogRef: MatDialogRef<EditConfirmationComponent>, @Inject(MAT_DIALOG_DATA) public data) { }
}


