import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig, Sort, PageEvent } from '@angular/material';
import { NgForm } from '@angular/forms';

import { AdminService } from '../admin.service';
import { EditContainerComponent } from './edit-container/edit-container.component';
import { containerColumnFilter, ContainerResponse, ContainerData } from './container.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})

export class ContainerComponent implements OnInit {

  displayedColumns = ['container_id', 'container_name', 'location_name', 'hire_location', 'email', 'phone_number', 'preferred_chassis_rental', 'action'];

  public containerFilterHeaders: string[] = ['dummy', 'containerName', 'pickupAddress', 'hireDehireAddress', 'mail', 'phoneNumber', 'preferredChassisRental', 'icon'];

  public columnFilter: boolean = false;

  public columnFilterValue: containerColumnFilter = { container_name: '', location_name: '', hire_location: '', email: '', phone_number: '', preferred_chassis_rental: '' };

  @ViewChild('f') filterForm: NgForm;

  componentName: String = "containers";

  dropdownComponentName: String = "configurations";

  typeComponentName: String = "chassises";

  locationTypes: { label: string, value: number }[];

  containers: ContainerData[];

  chassises: String[];

  containerDatasource: MatTableDataSource<ContainerData>;

  @ViewChild(MatPaginator) containerPaginator: MatPaginator;

  @ViewChild(MatSort) containersort: MatSort;

  currentPageIndex: any = 0;

  public containerLength: any;

  public sortColumn: any;

  public sortDirection: any;

  public pageIndex: any = 0;

  public pageSize: any = 10;

  public offset: number = 1;

  public limit: number = 10;

  ngOnInit() {
    this.getcontainerDatafromService();

    this.getDropdownFromService();

    this.getChassisTypesDatafromService();
  }

  constructor(public dialog: MatDialog,
    private adminservice: AdminService,
    public sharedService: SharedService) {
    this.containerDatasource = new MatTableDataSource();
  }

  getcontainerDatafromService(): void {
    let request = this.generateRequest();

    this.adminservice.getContainerList(this.componentName, request)
      .subscribe((response: ContainerResponse) => {
        this.containerDatasource.data = response.data.containerData;
        this.containerLength = response.data.containerPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  getDropdownFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'location')
      .subscribe(response => {
        this.locationTypes = response;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg');
      });
  }

  getChassisTypesDatafromService(): void {
    this.adminservice.getChassisTypesList(this.typeComponentName)
      .subscribe(chassistypeslist => {
        this.chassises = chassistypeslist;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getcontainerDatafromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getcontainerDatafromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getcontainerDatafromService();
  }

  /* api serivice for sort */
  onSort(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getcontainerDatafromService();
  }

  onAddContainer() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "locationTypes": this.locationTypes,
      "chassisTypes": this.chassises,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditContainerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage();
        this.getcontainerDatafromService();
        this.sharedService.openSnackbar('Container Created Successfully"', 500, 'success-msg')
      }
    });
  };

  onEditContainer(selectedRow: ContainerData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      'locationTypes': this.locationTypes,
      "chassisTypes": this.chassises,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditContainerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getcontainerDatafromService();
        this.sharedService.openSnackbar('Container Updated Successfully', 500, 'success-msg')
      }
    });
  }

  onDeleteContainer(containerId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Container',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.adminservice.deleteContainer(this.componentName, containerId).subscribe(
          (response: any) => {
            if (response.code == 200) {
              this.getcontainerDatafromService();
              this.sharedService.openSnackbar('Container Deleted Successfully', 500, 'success-msg')
            } else if (response.code == 409) {
              this.sharedService.openSnackbar('Container is being used in the order. Please edit/delete it there and try again', 3000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
          });
      }
    })
  };

  openPcr(selectedRow: ContainerData) {
    this.sharedService.openInfo({
      view: "onlyList",
      name: "Preferred Chassis Rental",
      selectedData: selectedRow.preferred_chassis_rental,
      isHover: false
    })
  }

  onClickSerialNo(selectedRow: ContainerData, serialNo: number) {
    let rowSelected: any = { ...selectedRow };

    rowSelected.serialNo = serialNo;

    rowSelected.view_hire = rowSelected.hire_location ?
      rowSelected.hire_location.map(temp => temp.hireAddress).join(',\n') : null;

    rowSelected.view_preffered_chassis = rowSelected.preferred_chassis_rental ?
      rowSelected.preferred_chassis_rental.join(',\n') : null;

    rowSelected.emailLists = rowSelected.email ? rowSelected.email.split(',').join(',\n') : null;

    this.sharedService.openInfo({
      view: "list",
      name: "Container",
      listNames: ['ID', 'Container Name', 'Pickup Address', 'Email', 'Phone Number', 'Preferred Chassis Rental', 'Empty Pickup/Return Location'],
      columnNames: ['serialNo', 'container_name', 'address', 'emailLists', 'phone_number', 'view_preffered_chassis', 'view_hire'],
      selectedData: rowSelected
    })
  };

  openHireDehireLocation(selectedRow: ContainerData) {
    this.sharedService.openInfo({
      view: "onlyList",
      name: "Empty Pickup/Return Locations",
      selectedData: selectedRow.hire_location,
      isHover: true
    })
  }

  onClickOpenEmailLists(selectedRow: ContainerData) {
    this.sharedService.openInfo({
      view: "onlyList",
      name: "Email List",
      selectedData: selectedRow.email ? selectedRow.email.split(',') : [],
      isHover: false
    })
  }

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getcontainerDatafromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.containerPaginator.pageIndex = 0;
    this.containerPaginator.pageSize = this.containerPaginator.pageSize ? this.containerPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.containerPaginator.pageSize ? this.containerPaginator.pageSize : 10;
  }
}