export interface ContainerData {
    serialNo?: number;
    container_id: number;
    container_name: string;
    address: string;
    street: string;
    city: string;
    postal_code: string;
    state: string;
    email: string;
    phone_number: string;
    preferred_chassis_rental: string;
    location_name: string;
    hire_location?: HireLocation[];
    view_hire?: any;
}

export interface HireLocation {
    hireAddress: string;
    hire_name: string;
    isDefault: boolean;
}

export interface containerColumnFilter {
    container_name: string;
    location_name: string;
    hire_location: string;
    email: string;
    phone_number: string;
    preferred_chassis_rental: string;
}

export class ContainerResponse {
    code: number
    error?: string
    data: {
        containerData: ContainerData[]
        containerPaginationLength: number
    }
}