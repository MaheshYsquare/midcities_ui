import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ContainerData } from '../container.model';
import { SharedService } from 'app/shared/service/shared.service';
import { AdminService } from 'app/admin/admin.service';
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-edit-container',
  templateUrl: './edit-container.component.html',
  styleUrls: ['./edit-container.component.scss']
})
export class EditContainerComponent implements OnInit {

  public editMode: boolean;

  public chassisTypes: any[];

  public locationTypes: { label: string; value: number }[];

  public data: ContainerData;

  public stateList: any;

  public containerForm: FormGroup;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  constructor(public dialogRef: MatDialogRef<EditContainerComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private adminService: AdminService,
    private sharedService: SharedService) {
    if (data) {
      this.editMode = data.modeSelect;
      this.chassisTypes = data.chassisTypes;
      this.data = data.selectedValue;
      this.locationTypes = data.locationTypes;
    }
  }

  ngOnInit() {
    this.initForm();

    if (this.editMode) {
      (<any>Object).values(this.containerForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }
  }

  private initForm() {
    let container_name = null;
    let pu_name = null;
    let address = null;
    let street = null;
    let city = null;
    let postal_code = null;
    let state = null;
    let phone_number = null;
    let preferred_chassis_rental = null;
    let emailLists = new FormArray([this.emailLists(null)]);
    let hire_dehire_loc = new FormArray([this.hireDehireArray(null, null, false)]);

    if (this.editMode) {
      container_name = this.data.container_name;
      address = this.data.address;
      street = this.data.street;
      city = this.data.city;
      postal_code = this.data.postal_code;
      state = this.data.state;
      phone_number = this.data.phone_number;
      pu_name = this.data.location_name;
      preferred_chassis_rental = this.data.preferred_chassis_rental;

      emailLists = new FormArray([]);

      this.data.email ? this.data.email.split(',').forEach(temp => {
        emailLists.push(this.emailLists(temp));
      }) : emailLists.push(this.emailLists(null));

      hire_dehire_loc = new FormArray([]);

      this.data.hire_location ? this.data.hire_location.forEach((temp, i) => {
        hire_dehire_loc.push(this.hireDehireArray(temp.hireAddress, temp.hire_name,
          temp.isDefault ? true : false));
      }) : hire_dehire_loc.push(this.hireDehireArray(null, null, false));
    }

    this.containerForm = new FormGroup({
      'containerName': new FormControl(container_name, Validators.required),
      'location_name': new FormControl(pu_name, Validators.required),
      'address': new FormControl(address, Validators.required),
      'street': new FormControl(street),
      'city': new FormControl(city),
      'postalCode': new FormControl(postal_code, Validators.pattern('[0-9]{5}')),
      'state': new FormControl(state, stateValidator),
      'phoneNumber': new FormControl(phone_number, [Validators.required, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)]),
      'preferredChassisRental': new FormControl(preferred_chassis_rental, Validators.required),
      'hireDehireLocation': hire_dehire_loc,
      'emailLists': emailLists,
    })
  }

  emailLists(email) {
    return new FormGroup({
      'email': new FormControl(email, [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)])
    })
  }

  getControls() {
    return (<FormArray>this.containerForm.get('emailLists')).controls;
  }

  onClickAddEmailList(index: number) {
    let emailArray = (<FormArray>this.containerForm.get('emailLists'));

    emailArray.controls[index].get('email').valid ?
      emailArray.push(this.emailLists(null)) : null;
  }

  onClickDeleteEmailList(index: number) {
    (<FormArray>this.containerForm.get('emailLists')).removeAt(index);
  }

  hireDehireArray(address?: string, hire_name?: string, isDefault?: boolean, isDisabled?: boolean) {
    return new FormGroup({
      'hireAddress': new FormControl(address, Validators.required),
      'hire_name': new FormControl(hire_name, Validators.required),
      'isDefault': new FormControl({ value: isDefault, disabled: isDisabled })
    })
  }

  getHireDehire() {
    return (<FormArray>this.containerForm.get('hireDehireLocation')).controls;
  }

  onChangeIsDefault(isDefaultChecked: boolean, index: number) {
    let hireDehireLocation = (<FormArray>this.containerForm.get('hireDehireLocation'));

    hireDehireLocation.controls.forEach((element, i) => {
      if (isDefaultChecked) {
        if (i !== index) {
          element.get('isDefault').setValue(false);
        }
      }
    });
  }

  onAddHireDehire() {
    (<FormArray>this.containerForm.get('hireDehireLocation')).push(this.hireDehireArray(null, null, false));
  }

  onDeleteHireDehire(index) {
    (<FormArray>this.containerForm.get('hireDehireLocation')).removeAt(index);
  }

  onSubmit() {
    if (this.containerForm.valid) {
      let hire_location = [];

      this.containerForm.getRawValue().hireDehireLocation.forEach(element => {
        element.hire_name = element.hire_name ? element.hire_name.toUpperCase() : element.hire_name;
        hire_location.push(element);
      })

      if (this.editMode) {
        let editOutput = {
          'container_id': this.data.container_id,
          'container_name': this.containerForm.get('containerName').value,
          'address': this.containerForm.get('address').value,
          'street': this.containerForm.get('street').value,
          'city': this.containerForm.get('city').value,
          'postal_code': this.containerForm.get('postalCode').value,
          'state': this.containerForm.get('state').value,
          'email': this.containerForm.get('emailLists').value ?
            this.containerForm.get('emailLists').value.map(temp => temp.email).join(',') : null,
          'phone_number': this.containerForm.get('phoneNumber').value,
          'preferred_chassis_rental': this.containerForm.get('preferredChassisRental').value.toString(),
          'location_name': this.containerForm.get('location_name').value,
          'hire_location': JSON.stringify(hire_location)
        }
        this.adminService.updateContainerData('containers', editOutput).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.containerForm.get('containerName').reset(this.data.container_name);
            this.sharedService.openSnackbar('Container Name Already Exists', 3000, 'warning-msg');
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
        })
      } else {
        let addOutput = {
          'container_name': this.containerForm.get('containerName').value,
          'address': this.containerForm.get('address').value,
          'street': this.containerForm.get('street').value,
          'city': this.containerForm.get('city').value,
          'postal_code': this.containerForm.get('postalCode').value,
          'state': this.containerForm.get('state').value,
          'email': this.containerForm.get('emailLists').value ?
            this.containerForm.get('emailLists').value.map(temp => temp.email).join(',') : null,
          'phone_number': this.containerForm.get('phoneNumber').value,
          'preferred_chassis_rental': this.containerForm.get('preferredChassisRental').value.toString(),
          'location_name': this.containerForm.get('location_name').value,
          'hire_location': JSON.stringify(hire_location)
        }
        this.adminService.postContainerData('containers', addOutput).subscribe(response => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.containerForm.get('containerName').reset()
            this.sharedService.openSnackbar('Container Name Already Exists', 3000, 'warning-msg')
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
        })
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }
}
