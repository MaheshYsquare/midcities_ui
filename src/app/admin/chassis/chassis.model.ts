export interface ChassisData {
    serialNo?: number;
    chassis_id?: number;
    chassis_name: string;
    address: string;
    street: string;
    city: string;
    postal_code: string;
    state: string;
    email: string;
    phone_number: string;
    location_name: string;
}

export interface ChassisColumnFilter {
    chassis_name: string,
    location_name: string,
    street: string;
    city: string;
    state: string;
    postal_code: string;
    email: string;
    phone_number: string;
}

export class ChassisResponse {
    code: number
    error?: string
    data: {
        chassisData: ChassisData[]
        chassisPaginationLength: number
    }
}