import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  MatDialog,
  MatDialogConfig,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  PageEvent,
  Sort
} from '@angular/material';

import { AdminService } from '../admin.service';
import { EditChassisComponent } from './edit-chassis/edit-chassis.component';
import { ChassisColumnFilter, ChassisResponse, ChassisData } from './chassis.model';
import { GetRequest } from 'app/shared/models/shared.model';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-chassis',
  templateUrl: './chassis.component.html',
  styleUrls: ['./chassis.component.scss']
})
export class ChassisComponent implements OnInit {

  public componentName: String = "chassises";

  public dropdownComponentName: String = "configurations";

  public locationTypes: { label: string; value: number }[];

  public chassisDatasource: MatTableDataSource<ChassisData>;

  @ViewChild(MatSort) chassisSort: MatSort;

  public sortColumn: string;

  public sortDirection: string;

  public columnFilter: boolean = false;

  public columnFilterValue: ChassisColumnFilter = { chassis_name: '', location_name: '', street: '', city: '', state: '', postal_code: '', email: '', phone_number: '' };

  public chassisFilterColumns: string[] = ['dummy', 'chassisName', 'location', 'locationStreet', 'locationCity', 'locationState', 'locationZip', 'chassisMail', 'phoneNumber', 'icon'];

  public chassisColumns = ['chassis_id', 'chassis_name', 'location_name', 'street', 'city', 'state', 'postal_code', 'email', 'phone_number', 'action'];

  @ViewChild(MatPaginator) chassisPaginator: MatPaginator;

  public currentPageIndex: number = 0;

  public chassisPageLength: number;

  public offset: number = 1;

  public limit: number = 10;

  constructor(public dialog: MatDialog,
    private adminService: AdminService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.chassisDatasource = new MatTableDataSource();

    this.getChassisDataFromService();

    this.getDropdownFromService();
  }

  getChassisDataFromService(): void {
    let request = this.generateRequest();
    this.adminService.getChassisList(this.componentName, request).subscribe((response: ChassisResponse) => {
      this.chassisDatasource.data = response.data.chassisData;
      this.chassisPageLength = response.data.chassisPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    });
  }

  getDropdownFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'location')
      .subscribe(response => {
        this.locationTypes = response;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg');
      });
  }

  onClickAddChassis() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "locationTypes": this.locationTypes,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditChassisComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage();
        this.getChassisDataFromService();
        this.sharedService.openSnackbar('Chassis Created Successfully', 500, 'success-msg')
      }
    });
  }

  onClickColumnFilter() {
    let values = Object.keys(this.columnFilterValue).map(temp => this.columnFilterValue[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialPage();
      this.columnFilter = true;
      this.getChassisDataFromService()
    } else if (values.filter(temp => temp === "").length === Object.keys(this.columnFilterValue).length) {
      this.initialPage();
      this.columnFilter = false;
      this.getChassisDataFromService()
    }
  }

  onClickResetColumnFilter() {
    Object.keys(this.columnFilterValue).forEach(temp => this.columnFilterValue[temp] = '');
    this.initialPage();
    this.columnFilter = false;
    this.getChassisDataFromService()
  }

  /* api serivice for sort */
  onChangeSortDirection(event: Sort) {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getChassisDataFromService()
  }

  onClickSerialNo(selectedRow: ChassisData, serialNo: number) {
    selectedRow.serialNo = serialNo;

    this.sharedService.openInfo({
      view: "list",
      name: "Chassis Company",
      listNames: ['ID', 'Chassis Company', 'Address', 'City', 'State', 'Zip Code', 'Email', 'Phone Number'],
      columnNames: ['serialNo', 'chassis_name', 'address', 'city', 'state', 'postal_code', 'email', 'phone_number'],
      selectedData: selectedRow
    })
  };

  onClickEditChassis(selectedRow: ChassisData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      'locationTypes': this.locationTypes,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditChassisComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getChassisDataFromService();
        this.sharedService.openSnackbar('Chassis Information Updated successfully', 500, 'success-msg')
      }
    });
  }

  onClickDeleteChassis(chassisId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Chassis',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.adminService.deleteChassis(this.componentName, chassisId).subscribe(
          (response: any) => {
            if (response.code == 200) {
              this.getChassisDataFromService();
              this.sharedService.openSnackbar('Chassis Deleted Successfully', 500, 'success-msg')
            } else if (response.code == 422) {
              this.sharedService.openSnackbar('Chassis is being used in the order. Please edit/delete it there and try again', 3000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
          });
      }
    })
  };

  /* api service call based on page offset and limit */
  onChangePage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getChassisDataFromService();
  }

  /* to generate format for the post request */
  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: this.columnFilter,
      columnFilterValue: this.columnFilterValue
    }
    return request
  }

  /* to set variable for initial page configuration */
  initialPage() {
    this.chassisPaginator.pageIndex = 0;
    this.chassisPaginator.pageSize = this.chassisPaginator.pageSize ? this.chassisPaginator.pageSize : 10;
    this.currentPageIndex = 0;
    this.offset = 1;
    this.limit = this.chassisPaginator.pageSize ? this.chassisPaginator.pageSize : 10;
  }
}