import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AdminService } from 'app/admin/admin.service';
import { ChassisData } from '../chassis.model';
import { SharedService } from 'app/shared/service/shared.service';
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-edit-chassis',
  templateUrl: './edit-chassis.component.html',
  styleUrls: ['./edit-chassis.component.scss']
})
export class EditChassisComponent implements OnInit {

  public editMode: boolean;

  public locationTypes: { label: string; value: number }[];

  public selectedChassis: ChassisData;

  public stateList: any;

  public chassisForm: FormGroup;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  constructor(public dialogRef: MatDialogRef<EditChassisComponent>,
    @Inject(MAT_DIALOG_DATA) injectedData,
    private adminService: AdminService,
    private sharedService: SharedService) {
    if (injectedData) {
      this.editMode = injectedData.modeSelect;
      this.selectedChassis = injectedData.selectedValue;
      this.locationTypes = injectedData.locationTypes;
    }
  }

  ngOnInit() {
    this.initForm()

    if (this.editMode) {
      (<any>Object).values(this.chassisForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }
  }

  private initForm() {
    let chassis_name = null;
    let pu_name = null;
    let address = null;
    let street = null;
    let city = null;
    let postal_code = null;
    let state = null;
    let email = null;
    let phone_number = null;

    if (this.editMode) {
      chassis_name = this.selectedChassis.chassis_name
      address = this.selectedChassis.address
      street = this.selectedChassis.street
      city = this.selectedChassis.city
      postal_code = this.selectedChassis.postal_code
      state = this.selectedChassis.state
      email = this.selectedChassis.email
      phone_number = this.selectedChassis.phone_number
      pu_name = this.selectedChassis.location_name;
    }

    this.chassisForm = new FormGroup({
      'chassisName': new FormControl(chassis_name, Validators.required),
      'pu_name': new FormControl(pu_name, Validators.required),
      'address': new FormControl(address, [Validators.required]),
      'street': new FormControl(street),
      'city': new FormControl(city),
      'postalCode': new FormControl(postal_code, [Validators.pattern('[0-9]{5}')]),
      'state': new FormControl(state, stateValidator),
      'email': new FormControl(email, [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)]),
      'phoneNumber': new FormControl(phone_number, [Validators.required, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)])
    });
  }

  onSubmit() {
    if (this.chassisForm.valid) {
      if (this.editMode) {
        let editData = {
          'chassis_id': this.selectedChassis.chassis_id,
          'chassis_name': this.chassisForm.get('chassisName').value,
          'address': this.chassisForm.get('address').value,
          'street': this.chassisForm.get('street').value,
          'city': this.chassisForm.get('city').value,
          'postal_code': this.chassisForm.get('postalCode').value,
          'state': this.chassisForm.get('state').value,
          'email': this.chassisForm.get('email').value,
          'phone_number': this.chassisForm.get('phoneNumber').value,
          'location_name': this.chassisForm.get('pu_name').value
        }
        this.adminService.updateChassisData('chassises', editData).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.chassisForm.get('chassisName').reset(this.selectedChassis.chassis_name)
            this.sharedService.openSnackbar('Chassis Name Already Exists', 3000, 'warning-msg')
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
        })
      } else {
        let addData = {
          'chassis_name': this.chassisForm.get('chassisName').value,
          'address': this.chassisForm.get('address').value,
          'street': this.chassisForm.get('street').value,
          'city': this.chassisForm.get('city').value,
          'postal_code': this.chassisForm.get('postalCode').value,
          'state': this.chassisForm.get('state').value,
          'email': this.chassisForm.get('email').value,
          'phone_number': this.chassisForm.get('phoneNumber').value,
          'location_name': this.chassisForm.get('pu_name').value
        }
        this.adminService.postChassisData('chassises', addData).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.chassisForm.get('chassisName').reset()
            this.sharedService.openSnackbar('Chassis Name Already Exists', 3000, 'warning-msg')
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2000, 'failure-msg')
        })
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
