import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';

import { DashboardService } from './dashboard.service';
import { Dashboard } from './dashboard.model'
import { SharedService } from 'app/shared/service/shared.service';
import { loginData } from 'app/session/session.model';
import { DriverDetailsPostRequest, DriverDetailsResponse, DriverDetailsData } from 'app/driver/DriverData';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public componentName: string = 'dashboards'; // to select component of api

  public isDriver: boolean = false; //flag to check whether the logged in user role is admin or driver

  public isAdmin: boolean = false; //flag to check whether the logged in user role is admin

  public userDetails: loginData; // logged in user details

  public dashboardMetrics: Dashboard.DashboardMetrics; // to store dashboard metrics object

  public dashboardDriverMetrics: Dashboard.DashboardDriverMetrics; // to store dashboard metrics object

  /* order progess table variables to store datasource,header,etc.. */
  public orderProgressDataSource: MatTableDataSource<Dashboard.OrderProgress>;

  public orderProgressDate: Date = new Date();

  public orderProgressHeaderColumns = ['order_number', 'delivery_date', 'driver', 'progress'];

  @ViewChild('orderProgressPaginator') orderProgressPaginator: MatPaginator;

  /* doughnut chart variable and settings */
  public pieChartData: number[] = [];

  public globalChartOptions: any = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    }
  };

  public doughnutOptions: any = Object.assign({
    elements: {
      arc: {
        borderWidth: 0
      }
    }
  }, this.globalChartOptions);

  public doughnutChartColors: any[] = [{
    backgroundColor: ['#673ab7', '#f44336', '#009688 ', '#2196f3']
  }];

  public pieChartLabels: string[] = ['Unpaid', 'Paid', 'Partial'];

  public pieChartType = 'pie';

  /* driver board table variables to store datasource,header,etc.. */
  public offset: number = 1;

  public limit: number = 3;

  public driverDataSource: MatTableDataSource<DriverDetailsData>;

  public driverBoardDate: Date = new Date();

  public driverHeaderColumns = ['s.no', 'driver_name', 'current_order', 'truck_number',
    'driver_status', 'business_name', 'hazmat_req', 'pu_time', 'pu_loc', 'dl_time', 'dl_loc'];

  public currentPageIndex: number = 0;

  @ViewChild('driverBoardPaginator') driverBoardPaginator: MatPaginator;

  public pageLength: number = 0;

  public loadTypeDate: Date = new Date();

  public loadTypeMetrics: Dashboard.LoadTypeResponse['data']; //to store load type metrics

  /* injected services in constructor */
  constructor(private dashboardService: DashboardService,
    private sharedService: SharedService) { }

  /* Angular life cycle hooks to Initialize the component */
  ngOnInit() {

    this.userDetails = this.sharedService.getUserDetails() //get the userdetails from localstorage of browser

    if (this.userDetails.role === 'Driver') {
      this.dashboardDriverMetrics = { activeLegs: 0, completedLegs: 0, unpaidLegs: 0 };

      this.getDashboardDriverMetricsFromService();
    } else {
      this.dashboardMetrics = { ordersInProgress: 0, ordersReady: 0, ordersThisWeek: 0 };

      this.getDashboardMetricsFromService();
    }

    if (this.userDetails)

      if (this.userDetails.role == 'Driver') {
        this.isDriver = true;

        this.driverDataSource = new MatTableDataSource();

        this.driverHeaderColumns = ['s.no', 'driver_name', 'order_sequence', 'truck_number',
          'leg_status', 'business_name', 'hazmat_req', 'pu_time', 'pu_loc', 'dl_time', 'dl_loc'];

        this.getDriverOnlyBoardDataFromService();
      }

    if (this.userDetails.role == 'Admin') {
      this.isAdmin = true;

      this.driverDataSource = new MatTableDataSource();

      this.orderProgressDataSource = new MatTableDataSource();

      this.getInvoicesMetricsFromService();

      this.getOrderProgressFromService(this.orderProgressDate);

      this.getLoadTypeMetricsFromService();

      setTimeout(() => {
        this.getDriverBoardDataFromService();
      }, 300);
    }

  }

  /* Get Services for this component */
  getDashboardMetricsFromService() {
    this.dashboardService.getDashboardMetrics(this.componentName).subscribe(response => {
      this.dashboardMetrics.ordersThisWeek = response.data.ordersThisWeek;
      this.dashboardMetrics.ordersInProgress = response.data.ordersInProgress;
      this.dashboardMetrics.ordersReady = response.data.ordersReady;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    })
  }

  /* Get Services for this component */
  getDashboardDriverMetricsFromService() {
    this.dashboardService.getDashboardDriverMetrics(this.componentName, this.userDetails.driverId)
      .subscribe(response => {
        this.dashboardDriverMetrics = { ...response.data };

      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      })
  }

  getOrderProgressFromService(date) {
    this.dashboardService.getOrderProgress(this.componentName, date).subscribe(response => {
      this.orderProgressDataSource.data = response.data;
      this.orderProgressDataSource.paginator = this.orderProgressPaginator;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    })
  }

  getInvoicesMetricsFromService() {
    this.dashboardService.getInvoicesMetrics(this.componentName).subscribe(response => {
      this.pieChartData = [response.data.unpaid_invoices, response.data.paid_invoices, response.data.partial_invoices];
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    })
  }

  getLoadTypeMetricsFromService() {
    this.dashboardService.getLoadTypeMetrics(this.componentName, this.loadTypeDate).subscribe(response => {
      this.loadTypeMetrics = response.data;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
    })
  }

  getDriverBoardDataFromService() {
    let request = this.generateRequest()
    this.sharedService.getDriverDetails('drivers', request).subscribe((response: DriverDetailsResponse) => {
      this.driverDataSource.data = response.data.driverData;
      this.pageLength = response.data.driverPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
    })
  }

  getDriverOnlyBoardDataFromService() {
    this.dashboardService.getDriverBoardData(this.componentName, this.userDetails.driverId,
      formatDate(this.driverBoardDate, 'y-MM-d', 'en')).subscribe((response: any) => {
        this.driverDataSource.data = response.data;
        setTimeout(() => {
          this.driverDataSource.paginator = this.driverBoardPaginator;
        }, 200)
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
      })
  }

  /* when the order progress date change this function will trigger */
  onChangeOrderProgressDate(event) {
    this.getOrderProgressFromService(new Date(event))
  }

  /* when the driver board date change this function will trigger */
  onChangeDriverBoardDate(event) {
    if (this.userDetails.role === 'Admin') {
      this.initialPage();
      this.getDriverBoardDataFromService();
    } else {
      this.getDriverOnlyBoardDataFromService();
    }
  }

  /* when the driver board pagination change this function will trigger */
  onChangeDriverPage(event: PageEvent) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    if (this.userDetails.role === 'Admin') {
      this.offset = event.pageIndex == 0 ?
        ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize))
        : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

      this.limit = event.pageIndex == 0 ?
        ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize);

      this.getDriverBoardDataFromService();
    }
  }

  /* when the load Type date change this function will trigger */
  onChangeLoadTypeDate(event) {
    this.getLoadTypeMetricsFromService();
  }

  generateRequest() {
    let request: DriverDetailsPostRequest = {
      offset: this.offset,
      limit: this.limit,
      role: this.userDetails.role,
      userId: this.userDetails.userId,
      searchFilter: false,
      searchFilterValue: null,
      columnFilter: false,
      filterData: null,
      column: null,
      direction: null,
      isNotAllDriver: false,
      isInactiveIncluded: false,
      isDriverBoard: true,
      filterDate: new Date(this.driverBoardDate).toDateString()
    }
    return request
  }

  initialPage() {
    this.driverBoardPaginator.pageIndex = 0;
    this.driverBoardPaginator.pageSize = this.driverBoardPaginator.pageSize ? this.driverBoardPaginator.pageSize : 3;
    this.currentPageIndex = 0
    this.offset = 1;
    this.limit = this.driverBoardPaginator.pageSize ? this.driverBoardPaginator.pageSize : 3;
  }
}
