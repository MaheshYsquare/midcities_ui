export namespace Dashboard {

    export interface DashboardMetricsResponse {
        code: number;
        data: DashboardMetrics;
        error?: string;
    }

    export interface DashboardDriverMetricsResponse {
        code: number;
        data: DashboardDriverMetrics;
        error?: string;
    }

    export interface DashboardDriverMetrics {
        activeLegs?: number;
        completedLegs?: number;
        unpaidLegs?: number;
    }

    export interface DashboardMetrics {
        ordersThisWeek: number;
        ordersInProgress: number;
        ordersReady: number;
    }

    export interface OrderProgressResponse {
        code: number;
        data: OrderProgress[];
        error?: string;
    }

    export interface OrderProgress {
        order_number: string,
        est_delivery_to_time: string,
        est_delivery_from_time: string,
        driver_name: string,
        progress: number
    }

    export interface InvoiceMetricsResponse {
        code: number;
        data: InvoiceMetrics;
        error?: string;
    }

    export interface LoadTypeResponse {
        code: number;
        data: { order_count: number; order_flag: string }[];
        error?: string;
    }

    export interface InvoiceMetrics {
        unpaid_invoices: number;
        paid_invoices: number;
        partial_invoices: number;
    }
}
