import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'environments/environment';
import { formatDate } from '@angular/common';
import { SharedService } from 'app/shared/service/shared.service';
import { Dashboard } from './dashboard.model';
import { DriverDataResponse } from 'app/shared/models/driver.model';

@Injectable()
export class DashboardService {

    private baseURL = environment.baseURL;

    public httpOptions = {};

    public HttpUploadOptions = {
        headers: new HttpHeaders({})
    }

    constructor(private httpClient: HttpClient, private sharedService: SharedService) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'access_token': this.sharedService.getAuthToken()
            })
        };
    }

    /* Get Dashoboard metrics Api */
    getDashboardMetrics(actionComponentName: string) {
        const url = `${this.baseURL + actionComponentName}/getDashboardMetrics`;
        return this.httpClient.get<Dashboard.DashboardMetricsResponse>(url, this.httpOptions)
    }

    /* Get Dashoboard Driver metrics Api */
    getDashboardDriverMetrics(actionComponentName: string, driverId: number) {
        const url = `${this.baseURL + actionComponentName}/getDashboardDriverMetrics?driverId=${driverId}`;
        return this.httpClient.get<Dashboard.DashboardDriverMetricsResponse>(url, this.httpOptions)
    }

    /* Get order progress Api */
    getOrderProgress(actionComponentName: String, estimatedDeliveryDate: Date) {
        const url = `${this.baseURL + actionComponentName}/getOrderProgress?date=${formatDate(estimatedDeliveryDate, 'yyyy-MM-dd', 'en')}`;
        return this.httpClient.get<Dashboard.OrderProgressResponse>(url, this.httpOptions)
    }

    /* Get invoice metrics Api */
    getInvoicesMetrics(actionComponentName: string) {
        const url = `${this.baseURL + actionComponentName}/getInvoicesMetrics`;
        return this.httpClient.get<Dashboard.InvoiceMetricsResponse>(url, this.httpOptions)
    }

    getDriverBoardData(actionComponentName: string, driverId: number, filterDate: string) {
        const url = `${this.baseURL + actionComponentName}/getDriverBoardData?driverId=${driverId}&filterDate=${filterDate}`;
        return this.httpClient.get<any>(url, this.httpOptions);
    }

    /* Get load type metrics Api */
    getLoadTypeMetrics(actionComponentName: string, date: Date) {
        const url = `${this.baseURL + actionComponentName}/getLoadTypeMetrics?date=${formatDate(date, 'yyyy-MM-dd', 'en')}`;
        return this.httpClient.get<Dashboard.LoadTypeResponse>(url, this.httpOptions)
    }
}