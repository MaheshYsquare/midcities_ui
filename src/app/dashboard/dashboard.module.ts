import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { loadingInterceptor } from 'app/shared/interceptor/loading.interceptor';
import { DashboardService } from './dashboard.service';
import { MaterialModule } from 'app/shared/demo.module';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    }
]

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        ChartsModule
        // RouterModule.forChild(routes)
    ],
    declarations: [
        DashboardComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: loadingInterceptor,
            multi: true,
        },
        DashboardService
    ]
})

export class DashboardModule { }
