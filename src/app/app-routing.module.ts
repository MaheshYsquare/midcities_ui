import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component'
import { AuthGuard } from './shared/guard/auth.guard';
import { PermissionGuard } from './shared/guard/permission.guard';
import { AppCustomPreloader } from './shared/preloader/custom.preloader';
import { DashboardComponent } from './dashboard/dashboard.component';

const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'authentication',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        canActivate: [PermissionGuard],
        component: DashboardComponent,
        data: { permission: 'Dashboard' }
      },
      {
        path: 'admin-features',
        canActivate: [PermissionGuard],
        loadChildren: './admin/admin.module#AdminComponentsModule',
        data: { permission: 'Admin Features' }
      },
      {
        path: 'rate-management',
        canActivate: [PermissionGuard],
        loadChildren: './rate/rate.module#RateComponentsModule',
        data: { permission: 'Rate Management' }
      },
      {
        path: 'order-management',
        canActivate: [PermissionGuard],
        loadChildren: './order/order.module#OrderComponentsModule',
        data: { permission: 'Order Management' }
      },
      {
        path: 'invoicing',
        canActivate: [PermissionGuard],
        loadChildren: './invoice/invoice.module#InvoiceComponentsModule',
        data: { permission: 'Invoicing' }
      },
      {
        path: 'dispatch',
        canActivate: [PermissionGuard],
        loadChildren: './dispatch/dispatch.module#DispatchComponentsModule',
        data: { permission: 'Dispatch' }
      },
      {
        path: 'driver-management',
        canActivate: [PermissionGuard],
        loadChildren: './driver/driver.module#DriverModule',
        data: { permission: 'Driver Management' }
      },
      {
        path: 'reporting',
        canActivate: [PermissionGuard],
        loadChildren: './report/report.module#ReportModule',
        data: { permission: 'Reporting' }
      },
      {
        path: 'advanced-search',
        canActivate: [PermissionGuard],
        loadChildren: './search/search.module#SearchModule',
        data: { permission: 'Advanced Search' }
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'authentication',
        loadChildren: './session/session.module#SessionModule'
      },
      {
        path: 'error',
        loadChildren: './error/error.module#ErrorModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/error/404'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(AppRoutes,
    {
      preloadingStrategy: AppCustomPreloader,
      useHash: true
    })
  ],
  exports: [RouterModule],
  providers: [AppCustomPreloader]
})

export class AppRoutingModule { }

