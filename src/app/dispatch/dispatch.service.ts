import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SharedService } from 'app/shared/service/shared.service';
import { OrderGetRequest, OrderResponse } from 'app/shared/models/order.model';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class DispatchService {

  HttpUploadOptions = {
    headers: new HttpHeaders({})
  }

  private baseURL = environment.baseURL;

  httpOptions = {};

  private initialRequest: OrderGetRequest = null;

  private requestStateBehaviour = new BehaviorSubject<OrderGetRequest>(this.initialRequest);

  constructor(private http: HttpClient,
    private sharedService: SharedService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_token': this.sharedService.getAuthToken()
      })
    };
  }

  /** Allows subscription to the behavior subject as an observable */
  getState(): Observable<OrderGetRequest> {
    return this.requestStateBehaviour.asObservable();
  }

  /**
   * Allows updating the current state of the behavior subject
   * @param request a object representing the current state
   */
  setState(request: OrderGetRequest): void {
    this.requestStateBehaviour.next(request);
  }


  /** GET Dispacth from the server */
  getDispatchData(actionComponentName: String, request: OrderGetRequest) {
    const url = `${this.baseURL + actionComponentName}/getDispatchData`;
    return this.http.post<OrderResponse>(url, request, this.httpOptions)
  }

  setUserPreference(columnResize: { field: string; width: number; index?: number, scale?: number }[], scale: number) {
    let copy = columnResize.slice();

    copy.forEach(temp => {
      temp.scale = scale;
    });

    localStorage.setItem('updatedUserPreference', JSON.stringify(copy));
  }
}

