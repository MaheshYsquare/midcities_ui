import { Time } from "@angular/common";
import { Timestamp } from "rxjs";

export interface DriverData {
    truck_number: string;
    order_number: string;
    shipper: string;
    consignee: string;
    city: Text;
    date: Date;
    time: Time;
    container_type: string;
    nextel_id: number;
    truck: string;
    sft: string;
    haz: string;
    pay: number;
    driver_name: string;
    driver_id: number;
    contact_number: number;
    driver_status: string;
    driver_type: string;
    ins_exp_date: Date;
    lic_exp_date: Date;
    med_exp_date: Date;
    last_name: string;
    phone_number: string;
    hazmat_certified: string;
}