export interface DispatchGetRequest {
    offset: number,
    limit: number,
    filterDate: Date,
    table: string,
    paginate: boolean,
    columnFilter: boolean,
    quickFilter?: string,
    filterData: Array<{
        column: string,
        filter: string,
        values: string,
        values2: string
    }>,
    column: string,
    direction: string
}

export const FilterObject = {
    order_sequence: "",
    leg_type: "",
    leg_status: "",
    leg_number: "",
    driver_name: "",
    hazmat_req: "",
    tags: "",
    container_size: "",
    triaxle: "",
    order_type: "",
    order_flag: "",
    pu_name: "",
    est_pickup_from_time: "",
    dl_name: "",
    est_delivery_to_time: "",
    container_number: "",
    chassis_number: "",
    miles: "",
    business_name: "",
    p_city: "",
    d_city: "",
    container_name: "",
    chassis_name: ""
}

export const dispatchColumnData = [
	{
		"label": "Chassis Name",
		"value": "chassis_name"
	},
	{
		"label": "Chassis Number",
		"value": "chassis_number"
	},
	{
		"label": "Container Number",
		"value": "container_number"
	},
	{
		"label": "Container Owner",
		"value": "container_name"
	},
	{
		"label": "Container Size",
		"value": "container_size"
	},
	{
		"label": "Customer",
		"value": "business_name"
	},
	{
		"label": "Delivery City",
		"value": "d_city"
	},
	{
		"label": "Delivery Date and Time",
		"value": "est_delivery_to_time"
	},
	{
		"label": "Delivery Name",
		"value": "dl_name"
	},
	{
		"label": "Driver Name",
		"value": "driver_name"
	},
	{
		"label": "Haz Request",
		"value": "hazmat_req"
	},
	{
		"label": "Leg Number",
		"value": "leg_number"
	},
	{
		"label": "Leg Status",
		"value": "leg_status"
	},
	{
		"label": "Leg Type",
		"value": "leg_type"
	},
	{
		"label": "Miles",
		"value": "est_miles"
	},
	{
		"label": "Order Flag",
		"value": "order_flag"
	},
	{
		"label": "Order Number",
		"value": "order_sequence"
	},
	{
		"label": "Order Type",
		"value": "order_type"
	},
	{
		"label": "PickUp City",
		"value": "p_city"
	},
	{
		"label": "PickUp Date and Time",
		"value": "est_pickup_from_time"
	},
	{
		"label": "Pickup Name",
		"value": "pu_name"
	},
	{
		"label": "Tags",
		"value": "tags"
	},
	{
		"label": "Triaxle",
		"value": "triaxle"
	}
]