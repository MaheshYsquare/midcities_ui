
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DispatchComponent } from './dispatch.component';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { DriverinfoComponent } from 'app/shared/modules/driver-info-module/driverinfo.component';
import { EditOrderComponent } from 'app/shared/modules/order-component/edit-order/edit-order.component';
import { LegComponent } from 'app/shared/modules/driver-info-module/leg/leg.component';

const DispatchRoutes: Routes = [
  {
    path: '',
    component: DispatchComponent
  },
  {
    path: 'driver-info',
    component: DriverinfoComponent,
    resolve: {
      'driverData': DriverInfoResolver
    }
  },
  {
    path: ':orderNumber/:sequence/edit',
    component: EditOrderComponent,
    resolve: {
      'editOrder': OrderLegResolver
    }
  },
  {
    path: ':orderNumber/:sequence/leg',
    component: LegComponent,
    resolve: {
      'orderLeg': OrderLegResolver
    }
  },
  {
    path: ':orderNumber/:sequence/leg/driver-info',
    component: DriverinfoComponent,
    resolve: {
      'driverData': DriverInfoResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(DispatchRoutes)],
  exports: [RouterModule]
})

export class DispatchRoutingModule { }