import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { DispatchComponent } from './dispatch.component';
import { DispatchService } from './dispatch.service';
import { loadingInterceptor } from '../shared/interceptor/loading.interceptor';
import { DispatchRoutingModule } from './dispatch-routing.module';
import { CdkDetailRowDirective } from 'app/shared/directives/cdk-detail-row.directive';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { FilterDialogComponent } from './filter-dialog/filter-dialog.component';
import { OrderSharedModule } from 'app/shared/modules/order-component/order-component.module';
import { DriverInfoModule } from 'app/shared/modules/driver-info-module/driver-info-module.module';
import { CustomerSharedModule } from 'app/shared/modules/customer-shared/customer-shared.module';

@NgModule({
  imports: [
    OrderSharedModule,
    DriverInfoModule,
    CustomerSharedModule,
    HttpClientModule,
    DispatchRoutingModule
  ],
  declarations: [
    DispatchComponent,
    CdkDetailRowDirective,
    FilterDialogComponent
  ],
  entryComponents: [
    FilterDialogComponent
  ],
  providers: [
    DispatchService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    },
    OrderLegResolver,
    DriverInfoResolver
  ]
})
export class DispatchComponentsModule { }