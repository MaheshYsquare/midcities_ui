import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss']
})
export class FilterDialogComponent implements OnInit {

  public match = new FormControl();

  public filterHeader: string;

  public normalInput: boolean = false;

  public hazRequest: boolean = false;

  public selectDropdown: boolean = false;

  public multipleSelect: boolean = false;

  public dateField: boolean = false;

  public statusArray: any = [{ label: 'Open', value: 'Open' }, { label: 'Assigned', value: 'Assigned' }, { label: 'Picked Up', value: 'Picked Up' }, { label: 'Delivered', value: 'Delivered' }];

  public hazArray: any = [
    { key: "Yes", value: "1" },
    { key: "No", value: "0" }
  ];

  public sizeArray = [{ label: '20', value: '20' }, { label: '28', value: '28' }, { label: '40', value: '40' }, { label: '45', value: '45' }, { label: '53', value: '53' }, { label: '40H', value: '40H' }, { label: '45H', value: '45H' }];

  public triaxleArray = [{ label: 'Y', value: 'Y' }, { label: 'N', value: 'N' }];

  public positionDialog: any = {};

  public legTypes: any = [];

  public orderTypes: any = [];

  public tagList: any = [];

  public selectDropdownLists: any = [];

  public checkboxArray: any = [];

  public orderFlagList: { label: string; value: number; }[];

  constructor(public dialogRef: MatDialogRef<FilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData) {
    this.filterHeader = dialogData.data.label;
    this.positionDialog = dialogData.position;
    this.legTypes = dialogData.legTypes;
    this.orderTypes = dialogData.orderTypes;
    this.tagList = dialogData.tagList;
    this.orderFlagList = dialogData.orderFlagList;
  }

  ngOnInit() {
    const rightMostPos = window.innerWidth - Number(this.positionDialog.left);
    this.dialogRef.updatePosition({
      top: `${this.positionDialog.top}px`,
      right: `${rightMostPos}px`
    })

    if (this.filterHeader == "Leg Status") {
      this.multipleSelect = true;
      this.checkboxArray = this.statusArray;
    }
    else if (this.filterHeader == "Haz Request") {
      this.hazRequest = true;
    }
    else if (this.filterHeader == "Container Size") {
      this.selectDropdown = true;
      this.selectDropdownLists = this.sizeArray;
    }
    else if (this.filterHeader == "Triaxle") {
      this.selectDropdown = true;
      this.selectDropdownLists = this.triaxleArray;
    }
    else if (this.filterHeader == "Leg Type") {
      this.selectDropdown = true;
      this.selectDropdownLists = this.legTypes;
    }
    else if (this.filterHeader == "Order Type") {
      this.selectDropdown = true;
      this.selectDropdownLists = this.orderTypes;
    }
    else if (this.filterHeader == "PickUp Date and Time" || this.filterHeader == "Delivery Date and Time") {
      this.dateField = true;
    }
    else if (this.filterHeader == "Tags") {
      this.multipleSelect = true;
      this.checkboxArray = this.tagList;
    } else if (this.filterHeader == "Order Flag") {
      this.multipleSelect = true;
      this.checkboxArray = this.orderFlagList;
    }
    else {
      this.normalInput = true
    }
  }

  onChangeOfDateTimePicker(event: any) {
    if (event.value.length !== 0 && event.value[0] !== null && event.value[1] === null) {
      this.match.setValue([event.value[0], event.value[0]])
    }
  }

  onApply() {
    if (this.match.value) {
      let toDispatch = {
        [this.dialogData.data.value]: this.match.value
      }
      return this.dialogRef.close(toDispatch);
    } else {
      this.dialogRef.close();
    }
  }
}
