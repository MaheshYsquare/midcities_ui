import { Component, OnInit, ViewChild, OnDestroy, ElementRef, HostListener, AfterViewInit, Renderer2, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

import {
  MatTableDataSource,
  MatDialogConfig,
  MatDialog,
  MatSort,
  MatPaginator,
  PageEvent,
  Sort,
  MatAutocompleteTrigger,
  MatAutocompleteSelectedEvent
} from '@angular/material';

import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

import * as moment from 'moment';

import { DispatchService } from './dispatch.service';
import { SocketService } from 'app/shared/service/socket.service';
import { rotate, slideInOut, tableExpand, slideUpDown } from 'app/shared/utils/app-animation';
import { OrderData, OrderGetRequest } from 'app/shared/models/order.model';
import { SharedService } from 'app/shared/service/shared.service';
import { DriverDetailsPostRequest, DriverDetailsResponse, DriverDetailsData } from 'app/driver/DriverData';
import { dispatchColumnData, FilterObject } from './dispatch.model'
import { FilterDialogComponent } from './filter-dialog/filter-dialog.component';
import { loginData } from 'app/session/session.model';
import { driverFilterColumns, ordersColumns } from 'app/shared/models/display-column.model';
import { ManageColumnsComponent } from 'app/shared/modules/order-component/manage-columns/manage-columns.component';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';


@Component({
  selector: 'app-dispatch',
  templateUrl: './dispatch.component.html',
  styleUrls: ['./dispatch.component.scss'],
  animations: [
    tableExpand,
    rotate,
    slideInOut,
    slideUpDown
  ],
})
export class DispatchComponent implements OnInit, OnDestroy {

  public componentName: string = "orders";

  public driverComponentName: string = "drivers";

  public legComponentName: string = 'legs';

  public dropdownComponentName: String = "configurations";

  private socketSubscription: Subscription;

  public filterDate = new Date();

  public columnFilterEvent: boolean = false;

  public orderFilterData: OrderGetRequest['filterData'];

  public showQuickFilters: boolean = false;

  public quickFilterEvent: boolean = false;

  public quickFilter: string;

  /* order dispatch table variables */
  public dispatchOrderDatasource: MatTableDataSource<OrderData>;

  @ViewChild('dispatchOrderSort', { read: ElementRef }) private matTableRef: ElementRef;

  @ViewChild('dispatchOrderSort') dispatchOrderSort: MatSort;

  public orderCurrentPageIndex: number = 0;

  public statuses: { label: string; value: number }[] = [
    { label: "Open", value: 1 },
    { label: "Assigned", value: 2 },
    { label: "Picked Up", value: 3 },
    { label: "Delivered", value: 4 }
  ];

  public dispatchOrderColumns: string[] = [];

  public dispatchColumns: { field: string; width: number; index?: number }[];

  @ViewChild('dispatchOrderPaginator') dispatchOrderPaginator: MatPaginator;

  @ViewChild('legPaginator') legPaginator: QueryList<MatPaginator>;

  public dispatchOrderPaginationLength: number = 0;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  public dispatchOffset: number = 1;

  public dispatchLimit: number = 10;

  public dispatchSortDirection: string;

  public dispatchSortColumn: string;

  /* driver table variables */
  public driverFilterEvent: boolean = false;

  public driverFilterData: DriverDetailsPostRequest['filterData']

  public driverDatasource: MatTableDataSource<DriverDetailsData>;

  @ViewChild('driverSort') driverSort: MatSort;

  public driverCurrentPageIndex: number = 0;

  public driverColumns = ['serialNo', 'truck_number', 'driver_name', 'current_order', 'driver_status',
    'last_known_location_name', 'leg_type', 'pu_name', 'dl_name', 'est_delivery_to_time', 'container_type', 'phone_number',
    'hazmat_req', 'hazmat_certified', 'paid'];

  @ViewChild('driverPaginator') driverPaginator: MatPaginator;

  public driverPaginationLength: number;

  public driverOffset: number = 1;

  public driverLimit: number = 30;

  public driverSortColumn: string;

  public driverSortDirection: string;

  /* advanced filter */

  public filterOptionArray = dispatchColumnData;

  public filterAllHeader: any = [];

  public disableArray: any = [];

  public selectedFilterObject = FilterObject; //Filtered column object

  public chipArray: any = [];

  public filterCtrl = new FormControl();

  public filteredHeader: Observable<any[]>;

  @ViewChild('filterInput') filterInput: ElementRef<HTMLInputElement>;

  @ViewChild('filterDialog') filterdialog: ElementRef;

  @ViewChild('trigger', { read: MatAutocompleteTrigger }) autoCompleteTrigger: MatAutocompleteTrigger;

  public searchFilterValue: string = "";

  public searchFilter: boolean = false;

  public showAdvanceFilter: boolean = false;

  public showFilterList: boolean = false;

  public addFilterLabel: boolean = true;

  public legTypes: any = [];

  public orderTypes: any = [];

  public tagList: any = [];

  public orderFlagList: { label: string; value: number }[];

  public moreFilterEvent: boolean = false;

  public loggedInUser: loginData;

  /* Dispatch table Resize Column Variables */
  public pressed = false;

  public currentResizeIndex: number;

  public startX: number;

  public startWidth: number;

  public isResizingRight: boolean;

  public scale: number;

  public request: OrderGetRequest;

  public stateSubscription: Subscription;

  public sideNavToggleSubscription: Subscription;

  public resizableMousemove: () => void;

  public resizableMouseup: () => void;

  public pageSize: number = 10;

  public timeout: any;

  public driverTimeout: any;

  public dispatchTimeout: any;

  public bothTimeout: any;

  public isLoading: boolean = true;

  public dispatchColumnLabel = {
    serialNo: 'ID',
    order_sequence: "Order #",
    leg_status: "Leg Status",
    leg_number: "Leg #",
    driver_name: "Driver",
    container_size: "Size",
    order_type: "Type",
    container_number: "Container #",
    chassis_number: "Chassis #",
    miles: "Miles",
    leg_type: "Leg Type",
    hazmat_req: "Haz",
    triaxle: "Triaxle",
    order_flag: "Order Flag",
    tags: "Tags",
    pickup_date: "SCH PU",
    pickup_time: "PU Time",
    pu_name: "Pickup",
    dl_name: "Delivery",
    delivery_date: "SCH DL",
    delivery_time: "DL Time",
    order_status: "Order Status",
    container_type: "Container Type",
    p_zip: "PU Zip",
    d_zip: "DL Zip",
    business_name: "Customer",
    rail_cut: "Rail Cut",
    pdm: "PDM",
    dmg: "LFD",
    lfd_date: 'LFD',
    chs: "CHS",
    pu_ref: "PU REF",
    ll: "L/L",
    chs_days: "CHS Days",
    container_name: "Container Owner",
    chassis_name: "Chassis Name",
    eta: "ETA",
    action: "Action"
  }

  constructor(public dialog: MatDialog,
    private dispatchService: DispatchService,
    private sharedService: SharedService,
    private router: Router,
    private socketService: SocketService,
    private route: ActivatedRoute,
    private renderer: Renderer2) { }

  ngOnInit() {
    this.stateSubscription = this.dispatchService.getState().subscribe(state => {
      if (state !== null) {
        this.request = state;

        this.dispatchOffset = state.offset;
        this.dispatchLimit = state.limit;
        this.columnFilterEvent = state.columnFilter;
        this.orderFilterData = state.filterData;
        this.dispatchSortColumn = state.column;
        this.dispatchSortDirection = state.direction;
        this.quickFilterEvent = state.quickFilter;
        this.quickFilter = state.quickFilterValue;
        this.moreFilterEvent = state.moreFilter;
        this.filterDate = state.filterDate !== null ? new Date(state.filterDate) : null;

        this.orderCurrentPageIndex = state.onPage['pageIndex'] * state.onPage['pageSize'];

        this.pageSize = state.onPage['pageSize'];

        this.dispatchOrderPaginator.pageIndex = state.onPage['pageIndex'];
        this.dispatchOrderPaginator.pageSize = state.onPage['pageSize'];

        this.showAdvanceFilter = state.onPage['showAdvanceFilter'];
        this.showQuickFilters = state.onPage['showQuickFilters'];

        this.chipArray = state.onPage['chipArray'];
      }
    })

    this.sideNavToggleSubscription = this.sharedService.getSideNavToggleState().subscribe(isToggled => {
      if (isToggled && !this.isLoading) {
        setTimeout(() => {
          this.setTableResize(this.matTableRef.nativeElement.clientWidth);
        }, 450);
      }
    })

    this.dispatchColumns = localStorage.getItem('dispatch_view') ? JSON.parse(localStorage.getItem('dispatch_view')) : [];

    this.setDisplayedColumns();

    this.dispatchOrderDatasource = new MatTableDataSource();

    this.driverDatasource = new MatTableDataSource();

    this.getDispatchDataFromService();

    this.getDriverDataFromService();

    this.getAllDropdownFromService();

    this.loggedInUser = this.sharedService.getUserDetails();

    this.socketSubscription = this.socketService.listenServerThroughSocket('data')
      .subscribe((response: any) => {
        if (!this.isLoading) {
          switch (response.data) {
            case 'driver':
              this.driverTimeout ? clearTimeout(this.driverTimeout) : null;

              this.driverTimeout = setTimeout(() => {
                this.getDriverDataFromService(true);
              }, 15000);

              break;
            case 'order':
              this.dispatchTimeout ? clearTimeout(this.dispatchTimeout) : null;

              this.dispatchTimeout = setTimeout(() => {
                this.getDispatchDataFromService(true);
              }, 15000);

              break;
            default:
              this.bothTimeout ? clearTimeout(this.bothTimeout) : null;

              this.bothTimeout = setTimeout(() => {
                this.getDispatchDataFromService(true);
                this.getDriverDataFromService(true);
              }, 30000);

              break;
          }
        }
      })

    this.filteredHeader = this.filterCtrl.valueChanges.pipe(
      startWith(null),
      map((list: string | null) => list ? this._filter(list) : this.filterOptionArray.slice())
    )

  }

  ngOnDestroy() {
    this.socketSubscription.unsubscribe();

    this.stateSubscription.unsubscribe();

    this.sideNavToggleSubscription.unsubscribe();
  }

  setTableResize(tableWidth: number) {
    let totWidth = 0;
    this.dispatchColumns.forEach((column) => {
      totWidth += column.width;
    });
    this.scale = (tableWidth) / totWidth;
    this.dispatchColumns.forEach((column) => {
      column.width *= this.scale;
      this.setColumnWidth(column);
    });
  }

  setDisplayedColumns() {
    this.dispatchOrderColumns = [];
    this.dispatchColumns.forEach((column, index) => {
      column.index = index;
      this.dispatchOrderColumns[index] = column.field;
    });
  }

  setColumnWidth(column: any) {
    const columnEls = Array.from(document.getElementsByClassName(`mat-column-${column.field} dispatch`));
    columnEls ? columnEls.forEach((el: HTMLDivElement) => {
      el.style.flex = 'none';
      el.style.width = column.width + 'px';
    }) : null;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTableResize(this.matTableRef.nativeElement.clientWidth);
  }

  /* initial get from db */
  getDispatchDataFromService(noLoader?: boolean) {
    this.dispatchService.setState(this.generateDispatchRequest());

    let modifyRequest: any = { ...this.request };

    modifyRequest["noLoader"] = noLoader;

    this.dispatchService.getDispatchData(this.componentName, modifyRequest)
      .subscribe(response => {
        response.data.orderData.forEach(temp => {
          temp.dataSource = new MatTableDataSource(temp.all_legs);
          temp.legColumns = ['leg_number', 'leg_type', 'driver_name', 'leg_status', 'pu_name', 'pickup_time',
            'dl_name', 'delivery_time', 'est_miles', 'container_number', 'chassis_number'];
        })
        this.dispatchOrderDatasource.data = response.data.orderData;
        this.dispatchOrderPaginationLength = response.data.orderPaginationLength;
        setTimeout(() => {
          this.setTableResize(this.matTableRef.nativeElement.clientWidth);
        }, 100);
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
      })
  }

  getDriverDataFromService(noLoader?: boolean) {
    let request = this.generateDriverRequest();

    let modifyRequest: any = { ...request };

    modifyRequest["noLoader"] = noLoader;

    this.sharedService.getDriverDetails(this.driverComponentName, modifyRequest).subscribe((response: DriverDetailsResponse) => {
      this.driverDatasource.data = response.data.driverData;
      this.driverPaginationLength = response.data.driverPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
    })
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown(this.dropdownComponentName, "orders,orderflags,tags,leg")
      .subscribe(response => {
        this.orderTypes = response.orders;
        this.tagList = response.tags;
        this.legTypes = response.leg;
        this.orderFlagList = response.orderflags;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onResizeColumn(event: any, index: number) {
    this.checkResizing(event, index);
    this.currentResizeIndex = index;
    this.pressed = true;
    this.startX = event.pageX;
    this.startWidth = event.target.clientWidth;
    event.preventDefault();
    this.mouseMove(index);
  }

  private checkResizing(event, index) {
    const cellData = this.getCellData(index);
    if ((index === 0) || (Math.abs(event.pageX - cellData.right) < cellData.width / 2 && index !== this.dispatchColumns.length - 1)) {
      this.isResizingRight = true;
    } else {
      this.isResizingRight = false;
    }
  }

  private getCellData(index: number) {
    const headerRow = this.matTableRef.nativeElement.children[0];
    const cell = headerRow.children[index];
    return cell.getBoundingClientRect();
  }

  mouseMove(index: number) {

    this.resizableMousemove = this.renderer.listen('document', 'mousemove', (event) => {
      if (this.pressed && event.buttons) {
        const dx = (this.isResizingRight) ? (event.pageX - this.startX) : (-event.pageX + this.startX);
        const width = this.startWidth + dx;
        if (this.currentResizeIndex === index && width > 50) {
          this.setColumnWidthChanges(index, width);
        }
      }
    });

    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.resizableMouseup = this.renderer.listen('document', 'mouseup', (event) => {
      if (this.pressed) {
        this.pressed = false;
        this.currentResizeIndex = -1;
        this.resizableMousemove();
        this.resizableMouseup();

        let updateUserPreference: any = localStorage.getItem('updatedUserPreference');

        if (updateUserPreference) {

          this.timeout = setTimeout(() => {

            updateUserPreference = JSON.parse(updateUserPreference);

            updateUserPreference.forEach(element => {
              element.width = element.width / element.scale;
            });

            let request = {
              column: 'dispatch_view',
              value: JSON.stringify(updateUserPreference),
              user_id: this.sharedService.getUserDetails().userId,
              noloading: true
            }
            this.sharedService.updateUiSettings('users', request).subscribe(response => {
              localStorage.removeItem('updatedUserPreference');
              localStorage.setItem('dispatch_view', JSON.stringify(updateUserPreference));
            }, error => {
              // this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
            })
          }, 2000);

        }

      }
    });
  }

  setColumnWidthChanges(index: number, width: number) {
    const orgWidth = this.dispatchColumns[index].width;
    const dx = width - orgWidth;
    if (dx !== 0) {
      const j = (this.isResizingRight) ? index + 1 : index - 1;
      const newWidth = this.dispatchColumns[j].width - dx;
      if (newWidth > 5) {
        this.dispatchColumns[index].width = width;
        this.setColumnWidth(this.dispatchColumns[index]);
        this.dispatchColumns[j].width = newWidth;
        this.setColumnWidth(this.dispatchColumns[j]);
        this.dispatchService.setUserPreference(this.dispatchColumns, this.scale);
      }
    }
  }


  /*   Fliter Dispatch Table through Date */
  onChangeDate() {
    this.initialDispatchPage()
    this.getDispatchDataFromService()
  }

  /* clearing the date filter */
  onClickClear() {
    this.filterDate = null;
    this.initialDispatchPage();
    this.getDispatchDataFromService()
  }

  onClickColumnFilterReset() {
    this.columnFilterEvent = false;
    this.orderFilterData = undefined;
    this.initialDispatchPage();
    this.getDispatchDataFromService();
  }

  onClickQuickFilterEnable() {
    this.showQuickFilters = !this.showQuickFilters;
    this.quickFilterEvent ? this.onClickQuickFilterReset() : null;
    this.showAdvanceFilter = this.showAdvanceFilter ? false : false;
    this.moreFilterEvent ? this.clearChip() : null;
  }

  /* quick filter service  */
  onClickQuickFilter(event: string) {
    this.dispatchOrderSort.direction = null;
    this.dispatchOrderSort.active = null;
    this.dispatchSortColumn = null;
    this.dispatchSortDirection = null;
    this.quickFilterEvent = true;
    this.quickFilter = event;
    this.initialDispatchPage();
    this.getDispatchDataFromService();
  }

  onClickQuickFilterReset() {
    this.quickFilterEvent = false;
    this.quickFilter = null;
    this.initialDispatchPage();
    this.getDispatchDataFromService();
  }

  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilter = true;
      this.searchFilterValue = this.searchFilterValue.toLowerCase();
    } else {
      this.searchFilter = false;
      this.searchFilterValue = "";
    }
  }

  onClearSearch() {
    this.searchFilter = false;
    this.searchFilterValue = "";
  }

  onShowFilterList() {
    this.showFilterList = !this.showFilterList;
    if (this.showFilterList) {
      this.showFilterList = true
    } else {
      this.showFilterList = false
    }
  }

  remove(list): void {
    const index = this.chipArray.indexOf(list);
    if (index >= 0) {
      this.chipArray.splice(index, 1);
    }

    const i = this.disableArray.indexOf(list.prop);
    if (i >= 0) {
      this.disableArray.splice(i, 1);
    }

    // Remove from Filtered Object for send DB
    for (let i in this.selectedFilterObject) {
      if (i == list.key) {
        this.selectedFilterObject[i] = ""
      }
    }
    this.getMoreFilterEventService();
  }

  clearChip() {
    this.chipArray.length = 0;
    this.disableArray.length = 0;
    for (let i in this.selectedFilterObject) {
      this.selectedFilterObject[i] = "";
    }
    this.initialDispatchPage();
    this.moreFilterEvent = false;
    this.getDispatchDataFromService();
  }

  selected(event: MatAutocompleteSelectedEvent): void {

    this.filterInput.nativeElement.value = '';
    this.filterCtrl.setValue(null);

    const filterData = {
      top: this.filterdialog.nativeElement.getBoundingClientRect().top,
      left: this.filterdialog.nativeElement.getBoundingClientRect().left + 250
    };

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data: event.option.value,
      position: filterData,
      legTypes: this.legTypes,
      orderTypes: this.orderTypes,
      tagList: this.tagList,
      orderFlagList: this.orderFlagList
    };
    dialogConfig.width = "250px";
    dialogConfig.minHeight = "200px";
    dialogConfig.hasBackdrop = true;
    dialogConfig.backdropClass = "backdrop"

    const dialogRef = this.dialog.open(FilterDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { }
      else {
        let chipObj = {
          prop: event.option.viewValue,
          value: result[event.option.value.value],
          key: event.option.value.value
        };
        this.chipArray.push(chipObj);
        this.disableArray.push(event.option.viewValue);
        this.autoCompleteTrigger.closePanel();
        // Add to Filtered Object for send DB
        for (let i in this.selectedFilterObject) {
          if (i == event.option.value.value) {
            this.selectedFilterObject[i] = chipObj.value;
          }
        }
        this.getMoreFilterEventService();
      }
    })
  }

  onClickOpenPanel() {
    this.autoCompleteTrigger.openPanel();
  }

  getMoreFilterEventService() {
    let values = Object.keys(this.selectedFilterObject).map(temp => this.selectedFilterObject[temp]);
    if (values.filter(temp => temp !== "").length !== 0) {
      this.initialDispatchPage();
      this.moreFilterEvent = true;
      this.getDispatchDataFromService();
    } else if (values.filter(temp => temp === "").length === Object.keys(this.selectedFilterObject).length) {
      this.initialDispatchPage();
      this.moreFilterEvent = false;
      this.getDispatchDataFromService();
    }
  }

  private _filter(value: string) {
    const filterValue = value.toString().toLowerCase();

    return this.filterOptionArray.filter(list => list.label.toLowerCase().indexOf(filterValue) === 0);
  }


  onDispatchOrderSortChangeDirection(event: Sort) {
    this.dispatchSortDirection = event.direction ? event.direction : null;
    this.dispatchSortColumn = (event.active === 'pickup_date' || event.active === 'pickup_time') ?
      'pu_time' : ((event.active === 'delivery_date' || event.active === 'delivery_time') ? 'dl_time' : event.active);
    this.getDispatchDataFromService()
  }

  /* on click of order dispatch table serial number */
  onClickDispatchOrderSerialNo(selectedRow: OrderData) {
    let displayData: any = { ...selectedRow };

    displayData.related_order = displayData.related_order ?
      displayData.related_order.split(',').filter(temp => temp !== (displayData.order_sequence)).join(',')
      : null;

    displayData.pickup_name_location = displayData.pu_name && displayData.pu_loc ?
      `${displayData.pu_name},\n ${displayData.pu_loc}` : null;

    displayData.pickup_date_time = displayData.est_pickup_from_time && displayData.est_pickup_to_time ?
      (displayData.est_pickup_from_time === displayData.est_pickup_to_time ? moment(displayData.est_pickup_from_time).format('M/D/YY HH:mm') :
        `${moment(displayData.est_pickup_from_time).format('M/D/YY HH:mm')} ~ ${moment(displayData.est_pickup_to_time).format('M/D/YY HH:mm')}`.trim())
      : null;

    displayData.delivery_name_location = displayData.dl_name && displayData.dl_loc ?
      `${displayData.dl_name},\n ${displayData.dl_loc}`.trim() : null;

    displayData.delivery_date_time = displayData.est_delivery_from_time && displayData.est_delivery_to_time ?
      (displayData.est_delivery_from_time === displayData.est_delivery_to_time ? moment(displayData.est_delivery_from_time).format('M/D/YY HH:mm') :
        `${moment(displayData.est_delivery_from_time).format('M/D/YY HH:mm')} ~ ${moment(displayData.est_delivery_to_time).format('M/D/YY HH:mm')}`.trim())
      : null;

    displayData.hire_name_location = displayData.hire_name && displayData.hire_dehire_loc ?
      `${displayData.hire_name},\n ${displayData.hire_dehire_loc}`.trim() : null;

    displayData.is_hazmat = displayData.hazmat_req ? 'Y' : 'N';

    let multipleListData = {
      "A": {
        listNames: ['Order #', 'Related Order No', 'Pickup Ref', 'Pickup Name, Location', 'Pickup Phone', 'Pickup Email',
          'Pickup Date & Time', 'Destination Name, Location', 'Delivery Phone', 'Delivery Email',
          'Destination Date & Time'],
        columnNames: ['order_sequence', 'related_order', 'pu_ref', 'pickup_name_location', 'p_phone', 'p_email',
          'pickup_date_time', 'delivery_name_location', 'd_phone', 'd_email', 'delivery_date_time'],
        selectedData: displayData
      },
      "Container Info": {
        listNames: ['Container Number', 'Container Owner', 'Empty Pickup/Return Location', 'Hazmat Required'],
        columnNames: ['container_number', 'container_name', 'hire_name_location', 'is_hazmat'],
        selectedData: displayData
      },
      "Customer Info": {
        listNames: ['Customer', 'Customer Ref Number', 'Point Of Contact', 'Phone Number', 'Email', 'Fax'],
        columnNames: ['business_name', 'customer_reference', 'point_of_contact', 'business_phone', 'email', 'fax'],
        selectedData: displayData
      }
    }

    this.sharedService.openInfo({
      view: "dispatchGrid",
      name: `Order ${displayData.order_sequence}`,
      multipleListData,
      selectedData: displayData
    })
  }

  onClickOrderNumber(selectedRow: OrderData) {
    this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.route });
  }

  onClickChangeStatus(selectedRow: OrderData, status: string, index: number) {
    let request = {
      "leg_id": selectedRow.leg_id,
      "order_id": selectedRow.order_id,
      "order_container_chassis_id": selectedRow.order_container_chassis_id,
      "leg_number": selectedRow.leg_number,
      "driver_id": selectedRow.driver_id,
      "previousStatus": selectedRow.leg_status,
      "currentStatus": status,
      "lastLeg": selectedRow.total_leg,
      "pu_loc": selectedRow.pu_loc,
      "pu_name": selectedRow.pu_name
    }

    if (selectedRow.driver_name || selectedRow.leg_is_brokered) {

      if (selectedRow.leg_status === "Open" && status === "Assigned") {
        request['isTopBottom'] = true;
        this.topToBottomServiceCall(selectedRow, request, index);
      } else if (selectedRow.leg_status === "Assigned" && status === "Picked Up") {
        request['isTopBottom'] = true;
        this.topToBottomServiceCall(selectedRow, request, index);
      } else if (selectedRow.leg_status === "Picked Up" && status === "Delivered") {
        request['isTopBottom'] = true;
        this.topToBottomServiceCall(selectedRow, request, index);
      } else if (selectedRow.leg_status === "Delivered" && status === "Picked Up") {
        request['isTopBottom'] = false;
        this.bottomToTopServiceCall(selectedRow, request, "Delivery Time");
      } else if (selectedRow.leg_status === "Picked Up" && status === "Assigned") {
        request['isTopBottom'] = false;
        this.bottomToTopServiceCall(selectedRow, request, selectedRow.leg_is_brokered ? "Pickup Time" :
          "Pickup Time and If Driver Bobtailed by this leg will get removed");
      } else if (selectedRow.leg_status === "Assigned" && status === "Open") {
        request['isTopBottom'] = false;
        this.bottomToTopServiceCall(selectedRow, request, "Driver");
      }

    } else {
      this.sharedService.openSnackbar('Please Assign Driver', 2000, 'warning-msg');
    }
  }

  topToBottomServiceCall(selectedRow: OrderData, request, index: number) {
    this.sharedService.updateLegStatusTopBottom(this.legComponentName, request).subscribe(response => {
      if (response.code === 200) {
        this.socketService.sendChangesInDriverFromClient('both');
        this.socketService.sendNotification({
          role: 'Dispatch',
          msg: `Order ${selectedRow.order_number}/${selectedRow.container_sequence} Leg ${selectedRow.leg_number}/${selectedRow.total_leg} is ${response.legStatus}`,
          hyperlink: `/dispatch/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        });
        this.getDispatchDataFromService();
        if (response.data.containerStatus === 'ORDER COMPLETED') {
          this.closeOrderToMoveToInvoice(selectedRow, 'closeOrder')
        } else if (response.data.containerStatus === 'ORDER INCOMPLETE' &&
          response.data.nextLegStatus === 'PLEASE ASSIGN LEG') {
          this.sharedService.openSnackbar("Please Make This Changes inside Leg Screen", 2500, "warning-msg");
        }
      } else if (response.code === 103) {
        this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
      } else if (response.code === 104) {
        this.sharedService.openSnackbar('Please Assign Driver', 2000, 'warning-msg')
      } else if (response.code === 105) {
        this.bobtailLegCreation(selectedRow, response.data);
      }
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
    })
  }

  bottomToTopServiceCall(selectedRow: OrderData, request, reset) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "bottomTop",
      previousName: request.previousStatus,
      name: request.currentStatus,
      reset,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.updateLegStatusBottomTop(this.legComponentName, request).subscribe(response => {
          if (response.code === 200) {
            this.socketService.sendChangesInDriverFromClient('both');
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${selectedRow.order_number}/${selectedRow.container_sequence} Leg ${selectedRow.leg_number}/${selectedRow.total_leg} is ${response.legStatus}`,
              hyperlink: `/dispatch/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
            this.getDispatchDataFromService()
          } else {
            this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
        })
      }
    })
  }

  bobtailLegCreation(selectedRow: OrderData, response) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "bobtail",
      previousName: response[0].last_known_location,
      name: selectedRow.pu_loc,
      reset: selectedRow.driver_name,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          "chassis_number": null,
          "container_number": null,
          "leg_number": selectedRow.leg_number,
          "order_id": selectedRow.order_id,
          "order_container_chassis_id": selectedRow.order_container_chassis_id,
          "leg_id": selectedRow.leg_id,
          "pu_name": response[0].last_known_location_name,
          "pu_loc": response[0].last_known_location,
          "dl_name": selectedRow.pu_name,
          "dl_loc": selectedRow.pu_loc,
          "loggedInUser": `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
          "driver_id": selectedRow.driver_id
        }
        this.sharedService.postBobtailLeg(this.legComponentName, request).subscribe(
          response => {
            this.getDispatchDataFromService();
            this.socketService.sendChangesInDriverFromClient('both');
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${selectedRow.order_number}/${selectedRow.container_sequence} Leg ${selectedRow.leg_number}/${selectedRow.total_leg} is Delivered`,
              hyperlink: `/dispatch/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            });
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${selectedRow.order_number}/${selectedRow.container_sequence} Leg ${selectedRow.leg_number + 1}/${selectedRow.total_leg} is Picked Up`,
              hyperlink: `/dispatch/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            });
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  }

  closeOrderToMoveToInvoice(selectedRow: OrderData, action: string) {
    let dialogRef = this.sharedService.openConfirmation({
      action,
      confirmColor: 'green',
      confirmLabel: 'Yes',
      cancelLabel: 'No'
    })

    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          order_container_chassis_id: selectedRow.order_container_chassis_id,
          order_id: selectedRow.order_id,
          flag: result
        }
        this.sharedService.moveOrderToInvoice(this.componentName, request).subscribe(response => {
          this.getDispatchDataFromService();
          if (result === 'Yes, immediately' || result === 'success') {
            this.socketService.sendNotification({
              isMultiple: true,
              multipleRole: ['Admin', 'Order Entry', 'Dispatch', 'Invoice'],
              msg: `Order ${selectedRow.order_number}/${selectedRow.container_sequence} has been successfully completed and ready for Invoicing`,
              hyperlink: {
                'Admin': `/invoicing/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
                'Order Entry': `/order-management/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
                'Dispatch': `/dispatch/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`,
                'Invoice': `/invoicing/${selectedRow.order_number}/${selectedRow.container_sequence}/leg`
              },
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
          }
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    })
  }

  onClickDriverName(selectedRow) {
    this.router.navigate(['dispatch', 'driver-info'], { queryParams: { driverId: selectedRow.driver_id } });
  }

  onClickEditOrder(selectedRow: OrderData) {
    this.router.navigate([selectedRow.order_number, selectedRow.container_sequence, 'edit'], { relativeTo: this.route });
  }

  onClickAddColumn() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      tableView: 'dispatch_view',
      displayColumns: ordersColumns.filter(temp => temp.value !== "created_on")
    }

    const dialogRef = this.dialog.open(ManageColumnsComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dispatchColumns = result;

        this.setDisplayedColumns();

        setTimeout(() => {
          this.setTableResize(this.matTableRef.nativeElement.clientWidth);
        }, 100);

        localStorage.removeItem('updatedUserPreference');
      }
    })
  }

  onDispatchOrderPageChange(event: PageEvent) {
    this.orderCurrentPageIndex = (event.pageIndex * event.pageSize);

    this.dispatchOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.dispatchLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getDispatchDataFromService();
  }

  /*   Fliter Driver Data  */
  onClickDriverFilterColumn() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'filterData': this.driverFilterData,
      filterColumns: driverFilterColumns,
      "key": "driverDetailsData"
    };
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.driverFilterEvent = true;
        this.driverFilterData = result;
        this.initialDriverPage();
        this.getDriverDataFromService();
      }
    });
  };

  onClickDriverFilterReset() {
    this.driverFilterEvent = false;
    this.driverFilterData = undefined;
    this.initialDriverPage();
    this.getDriverDataFromService();
  }

  onChangeDriverSortDirection(event: Sort) {
    this.driverSortDirection = event.direction;
    this.driverSortColumn = event.active == 'dl_date' ? 'dl_time' : event.active;
    this.getDriverDataFromService()
  }

  /* on click of driver table serial number */
  onClickDriverSerialNo(selectedRow: DriverDetailsData, serialNo: number) {

    let displayData: any = { ...selectedRow };

    displayData.serialNo = serialNo;

    displayData.time = displayData.est_delivery_to_time !== null ?
      new Date(displayData.est_delivery_to_time).toLocaleDateString() + ' ' + new Date(displayData.est_delivery_to_time).toLocaleTimeString()
      : null;

    displayData.haz_req = displayData.hazmat_req !== null ? (displayData.hazmat_req === 1 ? 'Yes' : 'No') : null;

    displayData.haz_cert = displayData.hazmat_certified !== null ?
      (displayData.hazmat_certified === 1 ? 'Yes' : 'No') : null;

    this.sharedService.openInfo({
      view: "list",
      name: "Driver",
      listNames: ["ID", "Truck Number", 'Driver Name', 'Status', 'Current Order', 'Leg Type', 'Shipper',
        'Consignee', 'Time', 'Container Type', 'Phone Number', 'Hazmat Required', 'Hazmat Certified'],
      columnNames: ["serialNo", "truck_number", "driver_name", "driver_status", "current_order", "leg_type",
        "pu_name", "dl_name", "time", "container_type", "phone_number", "haz_req", "haz_cert"],
      selectedData: displayData
    })
  }

  onChangeDriverPage(event: PageEvent) {
    this.driverCurrentPageIndex = (event.pageIndex * event.pageSize);

    this.driverOffset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.driverLimit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getDriverDataFromService();
  }

  generateDispatchRequest() {
    let request: OrderGetRequest = {
      offset: this.dispatchOffset,
      limit: this.dispatchLimit,
      columnFilter: this.columnFilterEvent,
      filterData: this.orderFilterData,
      column: this.dispatchSortColumn,
      direction: this.dispatchSortDirection,
      quickFilter: this.quickFilterEvent,
      quickFilterValue: this.quickFilter,
      moreFilter: this.moreFilterEvent,
      selectedFilterData: this.selectedFilterObject,
      filterDate: this.filterDate !== null ? this.filterDate.toDateString() : null,
      currentDate: new Date(),
      onPage: {
        pageIndex: this.dispatchOrderPaginator.pageIndex,
        pageSize: this.dispatchOrderPaginator.pageSize ? this.dispatchOrderPaginator.pageSize : 10,
        chipArray: this.chipArray,
        showQuickFilters: this.showQuickFilters,
        showAdvanceFilter: this.showAdvanceFilter
      },
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }

  initialDispatchPage() {
    this.dispatchOrderPaginator.pageIndex = 0;
    this.dispatchOrderPaginator.pageSize = this.dispatchOrderPaginator.pageSize ? this.dispatchOrderPaginator.pageSize : 10;
    this.orderCurrentPageIndex = 0
    this.dispatchOffset = 1;
    this.dispatchLimit = this.dispatchOrderPaginator.pageSize ? this.dispatchOrderPaginator.pageSize : 10;
  }

  generateDriverRequest() {
    let request: DriverDetailsPostRequest = {
      offset: this.driverOffset,
      limit: this.driverLimit,
      role: null,
      userId: null,
      searchFilter: false,
      searchFilterValue: null,
      columnFilter: this.driverFilterEvent,
      filterData: this.driverFilterData,
      column: this.driverSortColumn,
      direction: this.driverSortDirection,
      isNotAllDriver: false,
      isInactiveIncluded: false
    }
    return request
  }

  initialDriverPage() {
    this.driverPaginator.pageIndex = 0;
    this.driverPaginator.pageSize = this.driverPaginator.pageSize ? this.driverPaginator.pageSize : 30;
    this.driverCurrentPageIndex = 0
    this.driverOffset = 1;
    this.driverLimit = this.driverPaginator.pageSize ? this.driverPaginator.pageSize : 30;
  }

}