export interface columnInterface {
    label: string;
    value: string;
    flex?: string;
}

export interface UiSettings {
    field: string;
    width: number;
    index?: number;
    scale?: number;
}

export interface FilterInterface {
    name: string;
    value: string;
    inputType: string;
    isDefaultDropdown?: boolean;
    dropdown?: string;
}

export const ordersColumns: columnInterface[] = [
    {
        "label": "CHS",
        "value": "chs"
    },
    {
        "label": "CHS Days",
        "value": "chs_days"
    },
    {
        "label": "Chassis #",
        "value": "chassis_number"
    },
    {
        "label": "Chassis Name",
        "value": "chassis_name"
    },
    {
        "label": "Container #",
        "value": "container_number"
    },
    {
        "label": "Container Owner",
        "value": "container_name"
    },
    {
        "label": "Container Size",
        "value": "container_size"
    },
    {
        "label": "Container Type",
        "value": "container_type"
    },
    {
        "label": "Created On",
        "value": "created_on"
    },
    {
        "label": "Customer",
        "value": "business_name"
    },
    {
        "label": "DL Zip",
        "value": "d_zip"
    },
    {
        "label": "Delivery Date",
        "value": "delivery_date"
    },
    {
        "label": "Delivery Name",
        "value": "dl_name"
    },
    {
        "label": "Delivery Time",
        "value": "delivery_time"
    },
    {
        "label": "Driver Name",
        "value": "driver_name"
    },
    {
        "label": "ETA",
        "value": "eta"
    },
    {
        "label": "Hazmat Required",
        "value": "hazmat_req"
    },
    {
        "label": "L/L",
        "value": "ll"
    },
    {
        "label": "LFD",
        "value": "lfd_date"
    },
    {
        "label": "Leg Number",
        "value": "leg_number"
    },
    {
        "label": "Leg Status",
        "value": "leg_status"
    },
    {
        "label": "Leg Type",
        "value": "leg_type"
    },
    {
        "label": "Miles",
        "value": "miles"
    },
    {
        "label": "Order #",
        "value": "order_sequence"
    },
    {
        "label": "Order Flag",
        "value": "order_flag"
    },
    {
        "label": "Order Status",
        "value": "order_status"
    },
    {
        "label": "Order Type",
        "value": "order_type"
    },
    {
        "label": "PDM",
        "value": "pdm"
    },
    {
        "label": "PU REF",
        "value": "pu_ref"
    },
    {
        "label": "PU Zip",
        "value": "p_zip"
    },
    {
        "label": "Pickup Date",
        "value": "pickup_date"
    },
    {
        "label": "Pickup Name",
        "value": "pu_name"
    },
    {
        "label": "Pickup Time",
        "value": "pickup_time"
    },
    {
        "label": "Rail Cut",
        "value": "rail_cut"
    },
    {
        "label": "Tags",
        "value": "tags"
    },
    {
        "label": "Triaxle",
        "value": "triaxle"
    }
]

export const invoiceColumns: columnInterface[] = [
    {
        "label": "Booking #",
        "value": "booking_number"
    },
    {
        "label": "CHS",
        "value": "chs"
    },
    {
        "label": "CHS Days",
        "value": "chs_days"
    },
    {
        "label": "Chassis #",
        "value": "order_chassis_number"
    },
    {
        "label": "Chassis Name",
        "value": "chassis_name"
    },
    {
        "label": "Container #",
        "value": "order_container_number"
    },
    {
        "label": "Container Owner",
        "value": "container_name"
    },
    {
        "label": "Container Size",
        "value": "container_size"
    },
    {
        "label": "Container Type",
        "value": "container_type"
    },
    {
        "label": "Customer",
        "value": "business_name"
    },
    {
        "label": "Customer Ref #",
        "value": "customer_reference"
    },
    {
        "label": "DL Zip",
        "value": "d_zip"
    },
    {
        "label": "Delivery Date",
        "value": "order_delivery_date"
    },
    {
        "label": "Delivery Name",
        "value": "order_dl_name"
    },
    {
        "label": "Delivery Time",
        "value": "order_delivery_time"
    },
    {
        "label": "ETA",
        "value": "eta"
    },
    {
        "label": "Hazmat Required",
        "value": "hazmat_req"
    },
    {
        "label": "L/L",
        "value": "ll"
    },
    {
        "label": "LFD",
        "value": "lfd_date"
    },
    {
        "label": "Miles",
        "value": "order_miles"
    },
    {
        "label": "Order #",
        "value": "order_sequence"
    },
    {
        "label": "Order Flag",
        "value": "order_flag"
    },
    {
        "label": "Order Status",
        "value": "order_status"
    },
    {
        "label": "Order Type",
        "value": "order_type"
    },
    {
        "label": "PDM",
        "value": "pdm"
    },
    {
        "label": "PU REF",
        "value": "pu_ref"
    },
    {
        "label": "PU Zip",
        "value": "p_zip"
    },
    {
        "label": "Pickup Date",
        "value": "order_pickup_date"
    },
    {
        "label": "Pickup Name",
        "value": "order_pu_name"
    },
    {
        "label": "Pickup Time",
        "value": "order_pickup_time"
    },
    {
        "label": "Rail Cut",
        "value": "rail_cut"
    },
    {
        "label": "Tags",
        "value": "tags"
    },
    {
        "label": "Triaxle",
        "value": "triaxle"
    }
]

export const ordersFilterColumns: FilterInterface[] = [
    {
        "name": "CHS",
        "value": "chs",
        "inputType": "text"
    },
    {
        "name": "CHS Days",
        "value": "chs_days",
        "inputType": "text"
    },
    {
        "name": "Chassis #",
        "value": "chassis_number",
        "inputType": "text"
    },
    {
        "name": "Chassis Name",
        "value": "chassis_name",
        "inputType": "text"
    },
    {
        "name": "Container #",
        "value": "container_number",
        "inputType": "text"
    },
    {
        "name": "Container Owner",
        "value": "container_name",
        "inputType": "text"
    },
    {
        "name": "Container Type",
        "value": "container_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'container'
    },
    {
        "name": "Created On",
        "value": "created_on",
        "inputType": "date"
    },
    {
        "name": "Customer",
        "value": "business_name",
        "inputType": "text"
    },
    {
        "name": "Customer #",
        "value": "customer_reference",
        "inputType": "text"
    },
    {
        "name": "DL Zip",
        "value": "d_zip",
        "inputType": "text"
    },
    {
        "name": "Delivery Date",
        "value": "est_delivery_to_time",
        "inputType": "date"
    },
    {
        "name": "Delivery Name",
        "value": "dl_name",
        "inputType": "text"
    },
    {
        "name": "Driver",
        "value": "driver_name",
        "inputType": "text"
    },
    {
        "name": "ETA",
        "value": "eta",
        "inputType": "date"
    },
    {
        "name": "Haz",
        "value": "hazmat_req",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "L/L",
        "value": "ll",
        "inputType": "text"
    },
    {
        "name": "LFD",
        "value": "lfd_date",
        "inputType": "date"
    },
    {
        "name": "Leg Number",
        "value": "leg_number",
        "inputType": "text"
    },
    {
        "name": "Leg Status",
        "value": "leg_status",
        "inputType": "multiSelect",
        isDefaultDropdown: true
    },
    {
        "name": "Leg Type",
        "value": "leg_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'leg'
    },
    {
        "name": "Miles",
        "value": "miles",
        "inputType": "text"
    },
    {
        "name": "Order #",
        "value": "order_sequence",
        "inputType": "text"
    },
    {
        "name": "Order Flag",
        "value": "order_flag",
        "inputType": "multiSelect",
        isDefaultDropdown: false,
        dropdown: 'orderflags'
    },
    {
        "name": "Order Status",
        "value": "order_status",
        "inputType": "multiSelect",
        isDefaultDropdown: true
    },
    {
        "name": "Order Type",
        "value": "order_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'orders'
    },
    {
        "name": "PDM",
        "value": "pdm",
        "inputType": "text"
    },
    {
        "name": "PU REF",
        "value": "pu_ref",
        "inputType": "text"
    },
    {
        "name": "PU Zip",
        "value": "p_zip",
        "inputType": "text"
    },
    {
        "name": "Pickup Date",
        "value": "est_pickup_from_time",
        "inputType": "date"
    },
    {
        "name": "Pickup Name",
        "value": "pu_name",
        "inputType": "text"
    },
    {
        "name": "Rail Cut",
        "value": "rail_cut",
        "inputType": "date"
    },
    {
        "name": "Size",
        "value": "container_size",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "Tags",
        "value": "tags",
        "inputType": "multiSelect",
        isDefaultDropdown: false,
        dropdown: 'tags'
    },
    {
        "name": "Triaxle",
        "value": "triaxle",
        "inputType": "select",
        isDefaultDropdown: true
    }
]

export const invoiceFilterColumns: FilterInterface[] = [
    {
        "name": "Booking #",
        "value": "booking_number",
        "inputType": "text"
    },
    {
        "name": "CHS",
        "value": "chs",
        "inputType": "text"
    },
    {
        "name": "CHS Days",
        "value": "chs_days",
        "inputType": "text"
    },
    {
        "name": "Chassis #",
        "value": "order_chassis_number",
        "inputType": "text"
    },
    {
        "name": "Chassis Name",
        "value": "chassis_name",
        "inputType": "text"
    },
    {
        "name": "Container #",
        "value": "order_container_number",
        "inputType": "text"
    },
    {
        "name": "Container Owner",
        "value": "container_name",
        "inputType": "text"
    },
    {
        "name": "Container Type",
        "value": "container_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'container'
    },
    {
        "name": "Created On",
        "value": "created_on",
        "inputType": "date"
    },
    {
        "name": "Customer",
        "value": "business_name",
        "inputType": "text"
    },
    {
        "name": "Customer #",
        "value": "customer_number",
        "inputType": "text"
    },
    {
        "name": "Customer #",
        "value": "customer_reference",
        "inputType": "text"
    },
    {
        "name": "DL Zip",
        "value": "d_zip",
        "inputType": "text"
    },
    {
        "name": "Delivery Date",
        "value": "est_delivery_to_time",
        "inputType": "date"
    },
    {
        "name": "Delivery Name",
        "value": "order_dl_name",
        "inputType": "text"
    },
    {
        "name": "ETA",
        "value": "eta",
        "inputType": "date"
    },
    {
        "name": "Haz",
        "value": "hazmat_req",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "L/L",
        "value": "ll",
        "inputType": "text"
    },
    {
        "name": "LFD",
        "value": "lfd_date",
        "inputType": "date"
    },
    {
        "name": "Miles",
        "value": "order_miles",
        "inputType": "text"
    },
    {
        "name": "Order #",
        "value": "order_sequence",
        "inputType": "text"
    },
    {
        "name": "Order Flag",
        "value": "order_flag",
        "inputType": "multiSelect",
        isDefaultDropdown: false,
        dropdown: 'orderflags'
    },
    {
        "name": "Order Status",
        "value": "order_status",
        "inputType": "multiSelect",
        isDefaultDropdown: true
    },
    {
        "name": "Order Type",
        "value": "order_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'orders'
    },
    {
        "name": "PDM",
        "value": "pdm",
        "inputType": "text"
    },
    {
        "name": "PU REF",
        "value": "pu_ref",
        "inputType": "text"
    },
    {
        "name": "PU Zip",
        "value": "p_zip",
        "inputType": "text"
    },
    {
        "name": "Pickup Date",
        "value": "est_pickup_from_time",
        "inputType": "date"
    },
    {
        "name": "Pickup Name",
        "value": "order_pu_name",
        "inputType": "text"
    },
    {
        "name": "Rail Cut",
        "value": "rail_cut",
        "inputType": "date"
    },
    {
        "name": "Size",
        "value": "container_size",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "Tags",
        "value": "tags",
        "inputType": "multiSelect",
        isDefaultDropdown: false,
        dropdown: 'tags'
    },
    {
        "name": "Triaxle",
        "value": "triaxle",
        "inputType": "select",
        isDefaultDropdown: true
    }
]

export const archivedFilterColumns: FilterInterface[] = [
    {
        "name": "Booking #",
        "value": "booking_number",
        "inputType": "text"
    },
    {
        "name": "Chassis #",
        "value": "chassis_number",
        "inputType": "text"
    },
    {
        "name": "Container #",
        "value": "container_number",
        "inputType": "text"
    },
    {
        "name": "Customer",
        "value": "business_name",
        "inputType": "text"
    },
    {
        "name": "Customer #",
        "value": "customer_reference",
        "inputType": "text"
    },
    {
        "name": "Date Of Payment",
        "value": "date_of_payment",
        "inputType": "date"
    },
    {
        "name": "Delivery Name",
        "value": "dl_name",
        "inputType": "text"
    },
    {
        "name": "Haz",
        "value": "hazmat_req",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "Order #",
        "value": "order_sequence",
        "inputType": "text"
    },
    {
        "name": "Order Status",
        "value": "order_status",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "Order Type",
        "value": "order_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'orders'
    },
    {
        "name": "Pickup Name",
        "value": "pu_name",
        "inputType": "text"
    },
    {
        "name": "Triaxle",
        "value": "triaxle",
        "inputType": "select",
        isDefaultDropdown: true
    }
]

export const invoiceMgmtFilterColumns: FilterInterface[] = [
    {
        "name": "Booking #",
        "value": "booking_number",
        "inputType": "text"
    },
    {
        "name": "Container #",
        "value": "container_number",
        "inputType": "text"
    },
    {
        "name": "Customer",
        "value": "business_name",
        "inputType": "text"
    },
    {
        "name": "Customer #",
        "value": "customer_reference",
        "inputType": "text"
    },
    {
        "name": "Date",
        "value": "date",
        "inputType": "date"
    },
    {
        "name": "Date Of Payment",
        "value": "date_of_payment",
        "inputType": "date"
    },
    {
        "name": "Invoice #",
        "value": "invoice_number",
        "inputType": "text"
    },
    {
        "name": "Order #",
        "value": "order_sequence",
        "inputType": "text"
    },
    {
        "name": "Order Type",
        "value": "order_type",
        "inputType": "select",
        isDefaultDropdown: false,
        dropdown: 'orders'
    },
    {
        "name": "Outstanding",
        "value": "outstanding",
        "inputType": "text"
    },
    {
        "name": "Paid",
        "value": "paid",
        "inputType": "text"
    },
    {
        "name": "Quickbook Status",
        "value": "quickbook_status",
        "inputType": "select",
        isDefaultDropdown: true
    },
    {
        "name": "Total Amount",
        "value": "total_amount",
        "inputType": "text"
    },
    {
        "name": "Write Off",
        "value": "write_off",
        "inputType": "text"
    }
]

export const driverFilterColumns: FilterInterface[] = [
    {
        "name": "Consignee",
        "value": "dl_name",
        "inputType": "text"
    },
    {
        "name": "Container Type",
        "value": "container_type",
        "inputType": "select",
        "isDefaultDropdown": false,
        "dropdown": "container"
    },
    {
        "name": "Current Order",
        "value": "current_order",
        "inputType": "text"
    },
    {
        "name": "Driver Name",
        "value": "driver_name",
        "inputType": "text"
    },
    {
        "name": "Driver Status",
        "value": "driver_status",
        "inputType": "select",
        "isDefaultDropdown": true
    },
    {
        "name": "Email",
        "value": "email",
        "inputType": "text"
    },
    {
        "name": "Hazmat",
        "value": "hazmat_req",
        "inputType": "select",
        "isDefaultDropdown": true
    },
    {
        "name": "Leg Type",
        "value": "leg_type",
        "inputType": "select",
        "isDefaultDropdown": false,
        "dropdown": "leg"
    },
    {
        "name": "Phone #",
        "value": "phone_number",
        "inputType": "text"
    },
    {
        "name": "Shipper",
        "value": "pu_name",
        "inputType": "text"
    },
    {
        "name": "Truck #",
        "value": "truck_number",
        "inputType": "text"
    },
    {
        "name": "User Status",
        "value": "user_status",
        "inputType": "select",
        "isDefaultDropdown": true
    }
]

export const driverPayFilterColumns: FilterInterface[] = [
    {
        "name": "Accessorial Charge",
        "value": "driver_extra_charge",
        "inputType": "text"
    },
    {
        "name": "Chassis #",
        "value": "chassis_number",
        "inputType": "text"
    },
    {
        "name": "Container #",
        "value": "container_number",
        "inputType": "text"
    },
    {
        "name": "Delivery Name",
        "value": "dl_name",
        "inputType": "text"
    },
    {
        "name": "Delivery Time",
        "value": "dl_time",
        "inputType": "date"
    },
    {
        "name": "Driver Name",
        "value": "driver_name",
        "inputType": "text"
    },
    {
        "name": "Driver Rate",
        "value": "driver_rate",
        "inputType": "text"
    },
    {
        "name": "Leg Type",
        "value": "leg_type",
        "inputType": "select",
        "isDefaultDropdown": false,
        "dropdown": "leg"
    },
    {
        "name": "Miles",
        "value": "miles",
        "inputType": "text"
    },
    {
        "name": "Order #",
        "value": "order_sequence",
        "inputType": "text"
    },
    {
        "name": "Paid",
        "value": "is_paid",
        "inputType": "select",
        "isDefaultDropdown": true
    },
    {
        "name": "Phone #",
        "value": "phone_number",
        "inputType": "text"
    },
    {
        "name": "Pickup Name",
        "value": "pu_name",
        "inputType": "text"
    },
    {
        "name": "Pickup Time",
        "value": "pu_time",
        "inputType": "date"
    },
    {
        "name": "Total Amount",
        "value": "total_driver_rate",
        "inputType": "text"
    }
]