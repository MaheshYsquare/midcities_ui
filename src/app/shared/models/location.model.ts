export class LocationResponse {
    code: number
    error?: string
    data: {
        locationData: LocationData[]
        locationPaginationLength: number
    }
}

export interface LocationColumnFilter {
    location_type: string;
    location_name: string;
    address: string;
    phone_number: string;
    // receiving_hours_from: string;
    // receiving_hours_to: string;
    receiving_hours: string;
    email: string;
}

export class LocationData {
    s_no?: number;
    location_id?: number;
    location_name: string;
    location_type_id?: number;
    address: string;
    city: string;
    state: string;
    street: string;
    postal_code: string;
    phone_number: string;
    location_type?: string;
    receiving_hours_from: string;
    receiving_hours_to: string;
    email?: string;
}

