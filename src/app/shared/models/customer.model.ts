import { AccessoriesData } from "./accessories.model";
import { RoutesData } from "./routes.model";

export interface CustomerResponse {
    code: number;
    error?: string;
    data: {
        customerData: CustomerData[];
        customerPaginationLength: number;
    };
}

export interface CustomerRouteAccessoriesResponse {
    code: number;
    error?: string;
    data: {
        customerRouteRate: RoutesData[];
        customerAccessories: AccessoriesData[];
    };
}

export interface CustomerColumnFilter {
    business_name: string,
    point_of_contact: string,
    business_phone: string,
    email: string,
    is_active: string
}

export interface CustomerData {
    serialNo?: number;
    customer_id?: number;
    business_name: string;
    customer_ref_number: string;
    point_of_contact: string;
    business_phone: string;
    email: string;
    b_address: string;
    b_street: string;
    b_city: string;
    b_state: string;
    b_postal_code: string;
    poc_phone: string;
    fax: string;
    p_address: string;
    p_city: string;
    p_street: string;
    p_state: string;
    p_postal_code: string;
    customer_notes: string;
    preferred_invoice: string;
    billing_name: string;
    physical_name: string;
    customer_quickbooks_id: string;
    customer_quickbooks_sync: string;
    is_same_address: boolean;
    is_active: boolean;
    preferred_documents: string;
    contact_details?:emailList[];
}

export interface emailList {
    email:string,
    poc_phone:string,
    point_of_contact:string
}
export interface CustomerActiveOrInactiveRequest {
    is_active: boolean
    customer_quickbooks_id: any
    customer_quickbooks_sync: any
    customer_id: number
}

export interface CustomerQuickbooks {
    Id: any,
    DisplayName: any,
    Active: any
}