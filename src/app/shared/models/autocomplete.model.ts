export interface GeneralAutoComplete {
    searchValue: string;
    hazardousConfirm?: boolean;
    id?: number;
    legStatus?: string;
    orderLevelId?: number;
    containerLevelId?: number;
    legToAssign?: number;
    hazmat?: boolean;
    orderType?: string;
}