import { AccessoriesData } from './accessories.model';

export interface LegResponse {
    code: number;
    error?: string;
    data?: {
        legs: LegData[];
        charges: AccessoriesData[];
    };
}

export interface LegData {
    dmg?: any;
    rate?: number;
    email?: string;
    dl_loc: string;
    leg_id?: number;
    pu_loc: string;
    dl_name: string;
    dl_time: any;
    pu_name: string;
    pu_time: any
    deadline?: string;
    leg_type?: string;
    order_id?: number;
    driver_id: number;
    est_miles?: number;
    created_by?: string;
    created_on?: any;
    leg_number: number;
    leg_status?: string;
    driver_name?: string;
    leg_type_id: number;
    chassis_number: string;
    lastupdated_by?: string;
    lastupdated_on?: any;
    container_number: string;
    order_container_chassis_id?: number;
    is_selected?: boolean;
    bobtailed_by_leg?: number;
    pu_street?: string;
    pu_city?: string;
    pu_state?: string;
    pu_postal_code?: string;
    dl_street?: string;
    dl_city?: string;
    dl_state?: string;
    dl_postal_code?: string;
    is_auto_created?: any;
    dl_time_to?: any;
    pu_time_to?: any;
    pickup_time?: any;
    delivery_time?: any;
    is_brokered?: any;
    carrier_id?: any;
    carrier_rate?: any;
    carrier_name?: any;
    carrier_email?:any;
}