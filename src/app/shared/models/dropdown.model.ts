export interface DropdownData {
    location?: { label: string, value: number }[],
    orders?: { label: string, value: number }[],
    container?: { label: string, value: number }[],
    tags?: { label: string, value: number, color: string }[],
    orderflags?: { label: string, value: number }[],
    driver?: { label: string, value: number }[],
    leg?: { label: string, value: number }[],
    equipmentTypes?: { label: string, value: number }[],
    equipmentLocations?: { label: string, value: number }[],
    attendenceCategory?: { label: string, value: number }[]
}