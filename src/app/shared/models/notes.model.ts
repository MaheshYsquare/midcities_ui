export interface NotesResponse {
    notes: NotesData[];
    notes_description: string
}

export interface NotesData {
    created_by: string;
    created_on?: Date;
    lastupdated_by?: string;
    lastupdated_on?: Date;
    notes: string
    notes_description?: string
    notes_id?: number
    order_container_chassis_id: number
    user_id?: number
}

