export interface InvoiceManagementRequest {
  isQuickbooks: boolean;
  isUniqueByCustomer: boolean;
  isQuickbooksInitial: boolean;
  offset?: number;
  limit?: number;
  direction?: string;
  column?: string;
  columnFilter?: boolean;
  columnFilterValue?: any;
  customerId?: number;
  paidOffset?: number;
  paidLimit?: number;
  unpaidOffset?: number;
  unpaidLimit?: number;
  paidDirection?: string;
  paidColumn?: string;
  unpaidDirection?: string;
  unpaidColumn?: string;
  paidColumnFilter?: boolean;
  unpaidColumnFilter?: boolean;
  paidColumnFilterValue?: any;
  unpaidColumnFilterValue?: any;
  table?: string;
  timeZone?: any;
}

export interface InvoiceManagementResponse {
  code: number
  error: string
  data: {
    paidInvoices: invoiceResponse
    unpaidInvoices: invoiceResponse
  }
}

export interface CustomerInvoiceManagementResponse {
  code: number;
  error: string;
  data: invoiceResponse;
}

export interface invoiceResponse {
  invoiceData: InvoiceManagementData[];
  invoicePaginationLength: number;
}

export interface InvoiceManagementData {
  customer_id: number
  manual_invoice_id: number
  order_id: number
  order_container_chassis_id: number
  invoice_id: number
  date: string
  date_of_payment: string
  due_date: string
  invoice_type: string
  invoice_number: string
  outstanding: any
  paid: number
  payment_reference: string
  payment_type: string
  total_amount: any
  write_off: any
  created_by: string
  created_on: string
  lastupdated_by: string
  lastupdated_on: string
  file_name: string
  is_file_uploaded: number
  business_name: string
  email: string;
  business_phone: string;
  b_address: string;
  order_number: string;
  container_sequence: string
  order_sequence: string
  booking_number: string
  customer_reference: string
  container_number: string;
  shipperAddress: string;
  shipperName: string;
  shipperPhone: string;
  consigneePhone: string;
  consigneeName: string;
  consigneeAddress: string;
  random_notes: string;
  delivery_date: any;
  preferred_documents: string;
  pu_street?: any;
  pu_city?: any;
  pu_state?: any;
  pu_postal_code?: any;
  dl_street?: any;
  dl_city?: any;
  dl_state?: any;
  dl_postal_code?: any;
  b_street?: any;
  b_city?: any;
  b_state?: any;
  b_postal_code?: any;
}

export interface InvoiceColumnFilter {
  order_number?: string
  quickbook_status?: string
  business_name?: string
  customer_reference: string
  invoice_number: string
  date: any
  total_amount: string
  paid: string
  outstanding: string
  write_off: string
  payment_type?: string
  payment_reference?: string
  date_of_payment: string
  booking_number: string
  container_number: string
}

export interface ChassisInvoiceData {
  data: ChassisInvoiceReconcilationData[];
  code: number;
  error?: string;
}

export interface ChassisInvoiceReconcilationData {
  container_number: string
  chassis_number: string
  match: string,
  hire_date: string,
  off_hire_date: string,
  days_billed: number,
  days_dispatched: number,
  days_variance: number
}