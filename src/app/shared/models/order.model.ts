import { AccessoriesData } from "./accessories.model";
import { LegData } from "./leg.model";
import { MatTableDataSource } from "@angular/material";

export interface OrderGetRequest {
    offset: number;
    limit: number;
    columnFilter: boolean;
    filterData: Array<{
        column: string,
        filter?: string,
        values: string,
        values2?: string
    }>,
    column: string;
    direction: string;
    isUnique?: boolean;
    orderNumber?: number;
    quickFilter?: boolean;
    quickFilterValue?: string;
    filterDate?: any;
    table?: string;
    moreFilter?: boolean;
    selectedFilterData?: Object;
    currentDate?: any;
    searchFilter?: boolean;
    searchValue?: string;
    onPage?: Object;
    container_name?: any;
    timeZone: any;
}

export interface OrderResponse {
    code: number;
    error: string;
    data: {
        orderData: OrderData[];
        orderPaginationLength: number;
    };
}

export interface OrderData {
    order_id: number;
    order_sequence: string;
    is_selected?: boolean;
    customer_id: number;
    customer_name: string,
    business_name: string,
    customer_ref_number: string;
    pu_name: string;
    pu_loc: string;
    p_city: string;
    p_zip: number;
    p_phone: string;
    p_email: number;
    dl_name: string;
    dl_loc: string;
    d_city: string;
    d_zip: number;
    d_phone: string;
    d_email: number;
    hire_dehire_loc: string;
    lat_lon: string;
    est_pickup_from_time: any;
    est_pickup_to_time: any;
    act_pickup_time: any;
    est_delivery_from_time: any;
    est_delivery_to_time: any;
    act_delivery_time: any;
    order_notes: string;
    order_type: string;
    order_type_id: any;
    order_status: String;
    leg: number;
    equipment_type: string;
    rail_cut: string;
    pdm: string;
    dmg: string;
    chs: string;
    ll: string;
    miles: number;
    chs_days: number;
    is_dispatch: any;
    created_by: string;
    created_on: any;
    lastupdated_by: string;
    lastupdated_on: any;
    order_number: Number;
    driver_name: string;
    leg_number: number;
    leg_status: string;
    est_miles: number;
    chassis_id: number;
    container_id: number
    container_number: string;
    order_container_chassis_id: number;
    container_name: string,
    container_status: string;
    container_size: string;
    weight: number;
    container_type: string;
    chassis_name: string;
    pu_ref: string;
    dl_ref: string;
    lfd_date: string;
    pieces: any;
    seal_no: any;
    hazmat_req: any;
    un_number: string;
    haz_classes: string;
    haz_contact: number;
    haz_name: string;
    haz_page_ref: number;
    haz_weight: number;
    container_sequence: number;
    customer_reference: any;
    booking_number: any;
    chassis_number: any;
    triaxle: any;
    is_invoice_generated: any;
    customer_quickbooks_id: any;
    related_order: any;
    tags: any;
    tag_color: any;
    tag_id: any;
    order_flag: any;
    formated_container: string;
    isContainerSelected: boolean;
    leg_id: number;
    driver_id: number;
    hire_name: any;
    point_of_contact: any;
    poc_phone: any;
    business_phone: any;
    email: any;
    b_address: any;
    p_address: any;
    preferred_invoice: any;
    customer_notes: any;
    rate_type: string;
    invoice_quickbooks_id: any;
    invoice_quickbooks_sync: any,
    invoice_number: any,
    total_amount: any,
    date: any,
    due_date: any,
    sequenceCount: any;
    orderLegAccessories?: {
        order_number: any;
        accessories: AccessoriesData[],
        legs: LegData[]
    }
    req?: any;
    preferred_documents: any;
    total_leg: any;
    isReadyToInvoice: boolean;
    leg_type: string;
    format_address: string;
    is_draft: any;
    driver_notes: any;
    order_pu_loc: any;
    order_dl_loc: any;
    order_pu_name: any;
    order_dl_name: any;
    order_miles: any;
    order_container_number: any;
    order_chassis_number: any;
    dispatch_driver_notes: any;
    format_order_type: any;
    notes_created_by: string;
    notes_created_on: any;
    driver_notes_created_by: string;
    driver_notes_created_on: any;
    all_legs?: LegData[];
    dataSource?: MatTableDataSource<LegData>;
    legColumns?: string[];
    pageLength?: number;
    rate: any;
    container_email: any;
    pu_time?: any;
    dl_time?: any;
    pu_time_to?: any;
    dl_time_to?: any;
    pu_street?: any;
    pu_city?: any;
    pu_state?: any;
    pu_postal_code?: any;
    dl_street?: any;
    dl_city?: any;
    dl_state?: any;
    dl_postal_code?: any;
    b_street?: any;
    b_city?: any;
    b_state?: any;
    b_postal_code?: any;
    temperature: any;
    vehicle_id?: any;
    paid?: any;
    outstanding?: any;
    eta: any;
    is_brokered?: any;
    carrier_id?: any;
    carrier_name?: any;
    carrier_rate?: any;
    leg_is_brokered?: boolean;
    leg_carrier_id?: any;
    leg_carrier_name?: any;
    leg_carrier_rate?: any;
}







