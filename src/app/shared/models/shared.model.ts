export interface GetRequest {
    offset: number;
    limit: number;
    searchFilter?: boolean;
    searchFilterValue?: string;
    column: string;
    direction: string;
    columnFilter?: boolean;
    columnFilterValue?: any;
    isHubLocation?: boolean;
}

export interface ConfirmationData {
    action: string;
    name?: string;
    previousName?: string;
    reset?: string;
    cancelLabel?: string;
    confirmLabel?: string;
    confirmColor?: string;
    secondConfirmLabel?: string;
    secondConfirmColor?: string;
}

export interface InformationData {
    view: string;
    name?: string;
    listNames?: string[];
    columnNames?: string[];
    selectedData?: any;
    secondaryName?: string;
    secondaryColumnNames?: string[];
    selectedArray?: any[];
    isHover?: boolean;
    multipleListData?: any;
    documents?: any;
}

export const MY_CUSTOM_FORMATS = {
    fullPickerInput: 'l HH:mm',
    parseInput: 'l HH:mm',
    datePickerInput: 'l',
    timePickerInput: 'HH:mm',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
}

export const DocumentsName = [
    { label: "Prenote", value: "prenote" },
    { label: "Proof Of Delivery", value: "proof_of_delivery" },
    { label: "Ingate", value: "ingate" },
    { label: "Outgate", value: "outgate" },
    { label: "Bill Of Lading", value: "bill_of_landing" },
    { label: "Miscellaneous", value: "miscellaneous" },
    { label: "Hazmat", value: "hazmat" },
    { label: "Chassis", value: "chassis" },
    { label: "Interchange", value: "interchange" },
    { label: "Scale Tickets", value: "scale_tickets" },
    { label: "Incidents", value: "incidents" }
]
