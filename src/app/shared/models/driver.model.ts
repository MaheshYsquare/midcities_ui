export interface DriverPayRequest {
    offset: number
    limit: number
    searchFilter: boolean
    searchFilterValue: string
    role: string
    user_id: number
    isListById: boolean
    driver_id?: number
    is_paid?: number
    columnFilter: boolean
    filterData: {
        column: string
        filters: string
        values: string
    }[]
    column: string
    direction: string;
    roleDirection: string;
    filterDate?: any;
    timeZone?: any;
}

export interface DriverPayResponse {
    code: number;
    error: string;
    data: {
        driverPayData: DriverPayData[]
        driverPayPaginationLength: number;
        driverPendingAmount: number;
    }
}

export interface DriverPayData {
    leg_number: number;
    leg_type: string;
    pu_loc: string;
    dl_loc: string;
    pu_name: string;
    dl_name: string;
    container_number: string;
    chassis_number: string;
    miles: number;
    pu_time: Date;
    dl_time: Date;
    order_number: number;
    order_sequence: number;
    driver_id: number;
    user_id: number;
    is_paid: number;
    rate: number;
    driver_rate: number;
    driver_name: string;
    container_sequence: string;
    order_type: string;
    order_container_chassis_id: number;
    truck_number: string;
    leg_id: number;
    driver_pay_id: number;
    is_rate_edited?: any;
    is_charges_added?: boolean;
    driver_charges?: DriverChargesData[];
    driver_extra_charge?: number;
    total_driver_rate?: number;
}

export interface DriverChargesData {
    driver_accessorial_charges_id: number,
    rate: number,
    accessories_name: string;
}

export interface DriverInfoData {
    driver_id: number;
    driver_type: string;
    driver_status: string;
    pu_loc: string;
    pu_name: string;
    dl_loc: string;
    dl_name: string;
    current_order: string;
    first_name: string;
    last_name: string;
    phone_number: number
    lic_exp_date: Date;
    ins_exp_date: Date;
    med_exp_date: Date;
    truck_number: string;
    nextel_id: number;
    truck: string;
    shift: string;
    email: string;
    driver_notes: string;
    hazmat_certified: number;
    user_id: number;
    fuel_card_number: string;
    ssn: string;
    street: string;
    state: string;
    city: string;
    zip: string;
    hazmat_exp_date: Date;
    is_deactivated: number;
    termination_date: Date;
    reason: string;
    drug_testing_result: string;
    accident_summary: string;
    rehirable: string;
    profile_link: string;
    emailVerified: number;
    user_status: string;
    driver_special_endorsement: DriverSpecialEndorsement[];
    driver_attendence_vacation: DriverAttendenceVacation[];
    driver_attendence_sickday: DriverAttendenceSickday[];
    driver_attendence_ncns: DriverAttendenceNcns[];
    driver_incident: DriverIncident[];
    driver_attendence: DriverAttendence[];
    src_link: any;
    last_known_location: string;
    last_known_location_name: string;
    vehicle_id?: any;
    license_no?: any;
    elog_no?: any;
}

export interface DriverSpecialEndorsement {
    driver_special_endorsement_id: number;
    special_endorsement_name: string;
    special_endorsement_exp_date: any;
}

export interface DriverAttendenceVacation {
    driver_attendence_vacation_id: number;
    vacation_name: string;
    vacation_from_date: any;
    vacation_to_date: any;
}

export interface DriverAttendenceSickday {
    driver_attendence_sickday_id: number;
    sickday_name: string;
    sickday_from_date: any;
    sickday_to_date: any;
}

export interface DriverAttendenceNcns {
    driver_attendence_ncns_id: number;
    ncns_name: string;
    ncns_from_date: any;
    ncns_to_date: any;
}

export interface DriverIncident {
    driver_incident_id: number;
    name: string;
    description: string;
    action_taken: string;
    file_name: string;
}

export interface DriverAttendence {
    driver_attendence_id: number;
    name: string;
    date_from: any;
    date_to: string;
    driver_attendence_category_id: any;
    attendence_category: any;
}

export interface DriverDataResponse {
    code: number;
    error: string;
    data: DriverData[];
}

export interface DriverData {
    business_name: string;
    dl_loc: string;
    dl_name: string;
    dl_time: string;
    driver_id: number;
    driver_name: string;
    hazmat_req: string;
    leg_status: string;
    order_number: string;
    pu_loc: string;
    pu_name: string;
    pu_time: string;
    truck_number: string;
}

export interface DriverRelatedLegData {
    order_number: number;
    container_sequence: any;
    leg_status: string;
    leg_id: number;
    order_container_chassis_id: number;
    order_id: number;
    driver_id: number;
    dl_time: any;
    dl_name: string;
    dl_loc: string;
    pu_time: any;
    pu_name: string;
    pu_loc: string;
    container_number: string;
    chassis_number: string;
    leg_number: number;
    leg_type: string;
}