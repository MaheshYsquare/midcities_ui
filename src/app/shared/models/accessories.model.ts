export interface AccessoriesData {
    acc_charge_id: number;
    customer_id: number;
    accessories_name: string;
    accessories_interval: string;
    accessories_value: string;
    description: string;
    rate: string;
 }