export class RoutesData {
    is_selected?: boolean;
    route_rate_id?: number;
    customer_id: number;
    start_loc: string;
    des_loc: string;
    order_type?: string;
    order_type_id?: number;
    rate: any;
    distance?: number;
    pu_name: string;
    dl_name: string;
    format_address?: string;
}

export interface GeneralRouteRateColumnFilter {
    pu_name: string;
    dl_name: string;
    order_type: string;
    rate: string;
    distance: string;
    format_address: string;
}

export class RouteResponse {
    code: number
    error?: string
    data: {
        routeData: RoutesData[]
        routePaginationLength: number
    }
}

