import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './directives/accordion';
import { ToggleFullscreenDirective } from './directives/fullscreen/toggle-fullscreen.directive';
import { HelperModule } from 'app/shared/modules/helper/helper.module';

@NgModule({
  imports: [
    CommonModule,
    HelperModule
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullscreenDirective
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullscreenDirective,
    HelperModule
  ]
})
export class SharedModule { }
