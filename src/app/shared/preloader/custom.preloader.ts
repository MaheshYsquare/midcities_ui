import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';

import { Observable, of } from 'rxjs';

import { SharedService } from '../service/shared.service';

@Injectable()
export class AppCustomPreloader implements PreloadingStrategy {
    constructor(private sharedService: SharedService) { }
    preload(route: Route, load: Function): Observable<any> {
        let userDetails: any = this.sharedService.getUserDetails();
        return route.data && userDetails && userDetails.permissions.includes(route.data.permission) ? load() : of(null);
    }
}