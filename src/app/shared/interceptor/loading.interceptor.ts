
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';

import { tap, catchError } from 'rxjs/operators';

import { SharedService } from 'app/shared/service/shared.service';


@Injectable()

export class loadingInterceptor implements HttpInterceptor {

  constructor(private sharedService: SharedService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement

    loadingContainer.style.display = 'flex';

    if (req.url.includes('/locationAutoSuggest') ||
      req.url.includes('/driverAutoSuggest') ||
      req.url.includes('/orderAutoSuggest') ||
      req.url.includes('/checkTokenExpired') ||
      req.url.includes('/updateUserNotification') ||
      req.url.includes('/getUserNotification') ||
      req.url.includes('/advancedSearchAutoSuggest') ||
      req.url.includes('/searchCustomerAutosuggest') ||
      req.url.includes('/searchContainerAutosuggest') ||
      req.url.includes('/filterAutoSuggest') ||
      req.url.includes('/getRailTraceHistory')) {
      loadingContainer.style.display = 'none';
    }

    if (req.body && req.body.noLoader) {
      loadingContainer.style.display = 'none';
    }

    if (req.url.includes('/updateUiSettings') && req.body.noloading) {
      loadingContainer.style.display = 'none';
    }

    let authReq;
    if (!req.url.includes('/login') && !req.url.includes('/reset-password') && !req.url.includes('/reset')) {
      authReq = req.clone({
        headers: new HttpHeaders({
          'accesstoken': this.sharedService.getAuthToken()
        })
      })
    } else {
      authReq = req.clone()
    }


    return next.handle(authReq).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          loadingContainer.style.display = 'none';
        }
      }), catchError((err) => {
        loadingContainer.style.display = 'none';
        return observableThrowError(err);
      })
    )
  }
}

