import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SharedService } from 'app/shared/service/shared.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private sharedService: SharedService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.sharedService.getAuthToken() != "") {

      return true;

    }

    this.router.navigate(['/']);

    return false;
  }
}
