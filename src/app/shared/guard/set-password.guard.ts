import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SetPasswordGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const queryParams = next.queryParams;

    if (Object.keys(queryParams).length && queryParams["access_token"]) {

      if (localStorage.getItem('login') == 'true') {

        this.router.navigate(['/dashboard']);

        return false;
      }

      return true;

    }

    this.router.navigate(['authentication', 'login']);
    
    return false;
  }
}
