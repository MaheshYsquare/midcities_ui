import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { SharedService } from '../service/shared.service';
import { loginData } from 'app/session/session.model';
import { MenuService } from '../service/menu-items.service.';

@Injectable({
  providedIn: 'root'
})
export class SubMenuGuard implements CanActivate {

  constructor(private menuService: MenuService,
    private sharedService: SharedService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let loggedInUser: loginData = this.sharedService.getUserDetails();

    if (loggedInUser) {

      for (let temp of this.menuService.getSubMenu(state.url.split('/')[1])) {

        if (temp.state === state.url.split('/')[2]) {
          return true;
        }

      }

    }

  }

}
