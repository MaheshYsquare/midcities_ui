import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SharedService } from 'app/shared/service/shared.service';
import { loginData } from 'app/session/session.model';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router,
    private sharedService: SharedService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const queryParams = route.queryParams;

    const loggedInUser: loginData = this.sharedService.getUserDetails();

    if (queryParams["access_token"]) {

      this.router.navigate(["/authentication/new"], { queryParams });

      return true;

    }

    if (loggedInUser) {

      this.sharedService.checkTokenExpired().subscribe((response: boolean) => {
        if (response) {

          this.router.navigate(['/dashboard']);

          return false;
        }

        localStorage.clear();

        this.router.navigate(['authentication', 'login']);

        return true;

      }, error => {
        localStorage.clear();

        this.router.navigate(['authentication', 'login']);

        return true;
      })

    } else {

      return true;

    }

  }

}
