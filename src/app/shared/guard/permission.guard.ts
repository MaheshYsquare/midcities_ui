import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { SharedService } from 'app/shared/service/shared.service';
import { MenuService } from '../service/menu-items.service.';

@Injectable({
  providedIn: 'root'
})

export class PermissionGuard implements CanActivate {

  constructor(private menuService: MenuService, private sharedService: SharedService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let loggedInUser: any = this.sharedService.getUserDetails();

    if (loggedInUser) {

      for (let temp of this.menuService.getAllMenu()) {

        if (temp.state === state.url.split('/')[1]) {
          return true;
        }

      }

    }

  }
}
