import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs';

import { SharedService } from '../service/shared.service';
import { loginData } from 'app/session/session.model';

@Injectable({
  providedIn: 'root'
})
export class ErrorGuard implements CanActivate {

  constructor(private router: Router,
    private sharedService: SharedService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const loggedInUser: loginData = this.sharedService.getUserDetails();

    if (loggedInUser) {
      return true;
    }

    this.router.navigate(['authentication', 'login']);

    return false;
  }
}
