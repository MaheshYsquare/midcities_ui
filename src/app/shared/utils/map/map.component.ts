/// <reference path="../../../../../node_modules/bingmaps/types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="../../../../../node_modules/bingmaps/types/MicrosoftMaps/Microsoft.Maps.d.ts" />

import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  public map: Microsoft.Maps.Map;

  public location: Microsoft.Maps.Location;

  public pushpin: Microsoft.Maps.Pushpin;

  constructor() { }

  ngOnInit() {
  }

  loadMapScenario(): void {
    // console.log(document.getElementById('myMap'));
    setTimeout(() => {
      this.map = new Microsoft.Maps.Map(
        document.getElementById('myMap'),
        {
          credentials: environment.bingMapKey
        }
      );
    });

  }

  updateLocation(latitude, longitude) {
    this.location = new Microsoft.Maps.Location(
      latitude,
      longitude);

    this.pushpin = new Microsoft.Maps.Pushpin(this.location);

    if (this.map) {
      this.map.entities.push(this.pushpin);

      this.pushpin.setOptions({ enableHoverStyle: true, enableClickedStyle: true });

      this.map.setView({ center: this.location, zoom: 15 });
    }
  }

  liveUpdateLocation(latitude, longitude) {
    this.location = new Microsoft.Maps.Location(
      latitude,
      longitude);

    if (this.map && this.pushpin) {
      this.pushpin.setLocation(this.location);

      this.map.setView({ center: this.location });
    } else {
      this.pushpin = new Microsoft.Maps.Pushpin(this.location);

      if (this.map) {
        this.map.entities.push(this.pushpin);

        this.pushpin.setOptions({ enableHoverStyle: true, enableClickedStyle: true });

        this.map.setView({ center: this.location, zoom: 15 });
      } else {
        this.loadMapScenario();
      }
    }


  }

}
