import { FormGroup, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}

export function stateValidator(control: FormControl): { [s: string]: boolean } {

    let stateList = localStorage.getItem("states") ?
        JSON.parse(localStorage.getItem("states")) : [];

    if (!stateList.map(state => state.code).includes(control.value)) {
        return { 'notValidState': true };
    }

    return null;
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {

        let password = group.controls[passwordKey];

        let passwordConfirmation = group.controls[passwordConfirmationKey];

        if (password.value &&
            passwordConfirmation.value &&
            password.value !== passwordConfirmation.value) {

            return passwordConfirmation.setErrors({ mismatchedPasswords: true })

        } else if (password.value === passwordConfirmation.value) {

            if (passwordConfirmation.hasError('mismatchedPasswords')) {
                return passwordConfirmation.setErrors(null)
            }

        }
    }
}

export function feildsMatched(formControlName1: string, formControlName2: string) {
    return (group: FormGroup) => {
        let formControl1 = group.controls[formControlName1];

        let formControl2 = group.controls[formControlName2];

        if (formControl1.value &&
            formControl2.value &&
            formControl1.value === formControl2.value) {

            return formControl2.setErrors({ matched: true })

        } else if (formControl1.value !== formControl2.value) {

            if (formControl2.hasError('matched')) {
                return formControl2.setErrors(null)
            }

        }
    }
}

export const crossFormMatchedFields: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const formControl1 = control.get('shipper');
    const formControl2 = control.get('consignee');

    return formControl1.value &&
        formControl2.value &&
        formControl1.value === formControl2.value ? { 'matched': true } :
        null;
};