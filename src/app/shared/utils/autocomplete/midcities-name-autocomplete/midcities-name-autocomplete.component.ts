import { Component, OnInit, Input, Output, forwardRef, EventEmitter, ChangeDetectionStrategy, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';

import { MatAutocomplete, MatDialogConfig, MatDialog } from '@angular/material';

import { Observable, of, combineLatest, Subscription } from 'rxjs';
import {
  startWith,
  distinctUntilChanged,
  debounceTime,
  switchMap,
  map,
  catchError,
  tap,
  shareReplay,
  share
} from 'rxjs/operators';

import { LocationData } from 'app/shared/models/location.model';
import { SharedService } from 'app/shared/service/shared.service';
import { skipAfter } from '../../rxjs-opertators';
import { EditLocationComponent } from 'app/shared/modules/location-shared/edit-location.component';


@Component({
  selector: 'app-midcities-name-autocomplete',
  templateUrl: './midcities-name-autocomplete.component.html',
  styleUrls: ['./midcities-name-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MidcitiesNameAutocompleteComponent),
    multi: true,
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MidcitiesNameAutocompleteComponent),
    multi: true,
  }
  ]
})
export class MidcitiesNameAutocompleteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @Input() label: string;

  @Input() styleClass: string;

  @Input() isRequired: boolean;

  @Input() isSame?: boolean;

  @Input() compareValue?: string;

  @Input() compareLabel?: string;

  @Input() locationTypes: { label: string, value: number }[];

  @Input() isLocationAdded: boolean;

  @Input() isInitEmit?: boolean;

  @Output() street: EventEmitter<string> = new EventEmitter();

  @Output() city: EventEmitter<string> = new EventEmitter();

  @Output() postalCode: EventEmitter<string> = new EventEmitter();

  @Output() state: EventEmitter<string> = new EventEmitter();

  @Output() address: EventEmitter<string> = new EventEmitter();

  @Output() phoneNo: EventEmitter<string> = new EventEmitter();

  @Output() email: EventEmitter<string> = new EventEmitter();

  @Output() name: EventEmitter<string> = new EventEmitter();

  @Output() seletedLocationData: EventEmitter<LocationData> = new EventEmitter();

  @ViewChild(MatAutocomplete) autoComplete: MatAutocomplete;

  public locationName: string;

  public _onChange: any = () => { };

  public control: FormControl = new FormControl();

  public controlAutocomplete: Observable<string[]>;

  public addressCheck: Observable<any>;

  public locationData: LocationData[];

  public showLoading: boolean;

  public isButtonVisible: boolean = true;

  public initial: boolean = true;

  public isBtnDisabled: boolean = false;

  public subscriptions: Subscription[] = [];

  @Output() valueEmptied: EventEmitter<boolean> = new EventEmitter();

  constructor(private sharedService: SharedService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.control.valueChanges.subscribe(value => {
        this._onChange(value);
      })
    );

    this.controlAutocomplete = this.control.valueChanges
      .pipe(startWith(""), debounceTime(400), distinctUntilChanged()).pipe(
        (skipAfter(this.autoComplete.optionSelected.asObservable())))
      .pipe(
        switchMap(value => {
          this.showLoading = true;
          value !== null && value.trim() === "" ? this.valueEmptied.emit(true) : null;
          return value ? this.sharedService.locationAutoSuggest('locations', value, 'name') : of([]);
        }),
        map(data => {
          if ((this.label == 'Shipper' || this.label == 'Consignee')) {
            this.locationData = data.filter(temp => temp.location_type == 'Consignor/Consignee')
            return this.locationData.map(temp => temp.location_name)
          } else {
            this.locationData = data
            if (this.isInitEmit) {
              this.initEmit(this.locationName);
            }
            return this.locationData.map(temp => temp.location_name)
          }
        }),
        shareReplay(1),
        tap(_ => (this.showLoading = false), _ => (this.showLoading = false)),
        catchError(err => {
          return of([])
        })
      )

    if (this.isLocationAdded) {
      this.addressCheck = combineLatest(
        this.control.valueChanges,
        this.controlAutocomplete
      ).pipe(
        map(([value, location]) => {
          const selectedAddressName = location.find(temp => temp.toLowerCase() === (value !== null ?
            value.toLowerCase() : value));
          if (selectedAddressName) {
            this.isButtonVisible = false;
            this._onChange(selectedAddressName);
          } else {
            this.isButtonVisible = true;
          }
          return !selectedAddressName;
        })
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    setTimeout(() => {
      this.control.setValue(value);
      this.locationName = value;
    }, 0)
  }

  registerOnChange(fn: any) {
    this._onChange = fn
  }

  registerOnTouched(fn: any) { }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable();
    this.isBtnDisabled = isDisabled;
  }

  public validate(c: FormControl) {
    return (this.compareValue != null && this.compareValue != "" && this.compareValue == c.value) ?
      this.control.setErrors({ customError: true }) : null;
  }

  onClickAddLocation() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": false,
      "locationTypes": this.locationTypes,
      "locationName": this.control.value
    };
    let dialogRef = this.dialog.open(EditLocationComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: LocationData) => {
      if (result != undefined) {
        this.locationData = [result];
        this.control.setValue(result.location_name);
        this.onOptionSelected(result.location_name);
        this.initial = false;
        this.isButtonVisible = false;
      }
    });
  }

  initEmit(event) {
    let location: LocationData = this.locationData.find(temp => temp.location_name == event);

    if (location) {
      this.seletedLocationData.emit(location);
    }

  }

  onOptionSelected(event) {
    let location: LocationData = this.locationData.find(temp => temp.location_name == event);

    if (location) {
      this.address.emit(location.address);
      this.street.emit(location.street);
      this.city.emit(location.city);
      this.state.emit(location.state);
      this.postalCode.emit(location.postal_code);
      this.seletedLocationData.emit(location);
      this.phoneNo.emit(location.phone_number);
      this.email.emit(location.email);
    }
  }

  onChangeName() {
    this.name.emit(this.control.value);
  }

}
