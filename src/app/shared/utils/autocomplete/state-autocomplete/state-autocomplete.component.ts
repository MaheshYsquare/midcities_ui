import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';

import { coerceBooleanProperty } from '@angular/cdk/coercion';

import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
	selector: 'app-state-autocomplete',
	templateUrl: './state-autocomplete.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => StateAutocompleteComponent),
			multi: true
		},
		{
			provide: NG_VALIDATORS,
			useExisting: forwardRef(() => StateAutocompleteComponent),
			multi: true
		}
	]
})
export class StateAutocompleteComponent implements ControlValueAccessor, OnInit, OnDestroy {

	private _required = false;

	@Input()
	get required(): boolean {
		return this._required;
	}
	set required(value: boolean) {
		this._required = coerceBooleanProperty(value);
	}


	@Input() placeholder: string;

	public _onChange: any = () => { };

	public subscriptions: Subscription[] = [];

	public states: {
		"state": string;
		"code": string;
	}[];

	public control = new FormControl(null, this.required ? Validators.required : null);

	public controlAutocomplete: Observable<{
		"state": string;
		"code": string;
	}[]>;

	ngOnInit() {
		this.subscriptions.push(
			this.control.valueChanges.subscribe(value => {
				this._onChange(value);
			})
		);

		this.states = localStorage.getItem("states") ? JSON.parse(localStorage.getItem("states")) : [];

		this.controlAutocomplete = this.control.valueChanges.pipe(
			startWith(''),
			map(state => state ? this.stateFilter(state) : [])
		)
	}

	ngOnDestroy() {
		this.subscriptions.forEach(s => s.unsubscribe());
	}

	writeValue(value: string): void {
		value ? this.control.setValue(value) : null;
	}

	registerOnChange(fn: (value: string) => void) {
		this._onChange = fn;
	}

	registerOnTouched(fn: () => void) { }

	setDisabledState(isDisabled: boolean) {
		isDisabled ? this.control.disable() : this.control.enable();
	}

	public validate(control: FormControl) {
		return (control.value && !this.states.map(state => state.code).includes(control.value)) ?
			this.control.setErrors({ customError: true }) :
			this.control.hasError('required') ? null : this.control.setErrors(null);
	}

	stateFilter(value: string) {
		return this.states.filter(state => state.code.toLowerCase().startsWith(value.toLowerCase()));
	}

}
