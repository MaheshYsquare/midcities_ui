/// <reference path="../../../../../../node_modules/bingmaps/types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="../../../../../../node_modules/bingmaps/types/MicrosoftMaps/Microsoft.Maps.d.ts" />
/// <reference path="../../../../../../node_modules/bingmaps/types/MicrosoftMaps/Modules/Autosuggest.d.ts" />

import { Component, OnInit, Input, Output, forwardRef, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { environment } from 'environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bing-autocomplete',
  templateUrl: './bing-autocomplete.component.html',
  styleUrls: ['./bing-autocomplete.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => BingAutocompleteComponent),
    multi: true,
  }]
})
export class BingAutocompleteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  public control: FormControl = new FormControl();

  public bingAddressAutoComplete: any = [];

  public suggestionResult: Microsoft.Maps.ISuggestionResult[];

  public statesData: {
    "state": string;
    "code": string;
  }[];

  public _onChange: any = () => { }

  @Input() label: string;

  @Input() styleClass: string;

  @Input() isSame?: boolean;

  @Output() street: EventEmitter<string> = new EventEmitter();

  @Output() city: EventEmitter<string> = new EventEmitter();

  @Output() postalCode: EventEmitter<string> = new EventEmitter();

  @Output() state: EventEmitter<string> = new EventEmitter();

  @Output() address: EventEmitter<string> = new EventEmitter();

  public subscriptions: Subscription[] = [];

  constructor() { }

  ngOnInit() {
    this.statesData = localStorage.getItem('states') ? JSON.parse(localStorage.getItem('states')) : [];

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.control.valueChanges.subscribe(value => {
        this._onChange(value);
        this.loadSuggestion(value)
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    this.control.setValue(value)
  }

  registerOnChange(fn: any) {
    this._onChange = fn
  }

  registerOnTouched(fn: any) { }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable()
  }

  loadSuggestion(value) {
    Microsoft.Maps.loadModule('Microsoft.Maps.AutoSuggest', {
      callback: () => {
        this.onLoadBingMaps(value)
      },
      errorCallback: this.onErrorBingMaps,
      credentials: environment.bingMapKey
    });
  }

  onErrorBingMaps() { }

  onLoadBingMaps(value) {
    var options = { maxResults: 5, countryCode: 'US', placeSuggestions: true };
    var manager = new Microsoft.Maps.AutosuggestManager(options);
    manager.getSuggestions(value, (result) => {
      this.suggestionAddress(result)
    })
  }

  suggestionAddress(event: Microsoft.Maps.ISuggestionResult[]) {
    this.suggestionResult = event;
    let address = event.map(temp => temp.address.formattedAddress);
    this.bingAddressAutoComplete = address;
  }

  onBingAddressOptionSelected(event) {
    let address = this.suggestionResult.find(temp => temp.address.formattedAddress === event);

    if (address) {

      let formatAddress = event.split(',').join(' ');
      let state = this.statesData.find(temp => temp['state'] == address.address.adminDistrict);

      this.street.emit(address.address.addressLine ? address.address.addressLine : null);
      this.city.emit(address.address.locality ? address.address.locality : null);
      this.postalCode.emit(address.address.postalCode ? address.address.postalCode : null);
      this.control.setValue(formatAddress);
      this.state.emit(state ? state.code : null);
      this.bingAddressAutoComplete = [];
    }

  }

  onChangeAddress() {
    this.address.emit(this.control.value);
  }

}
