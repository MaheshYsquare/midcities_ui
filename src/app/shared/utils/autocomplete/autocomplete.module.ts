import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MatAutocompleteModule, MatInputModule, MatProgressSpinnerModule, MatIconModule, MatButtonModule, MatSelectModule, MatTooltipModule } from '@angular/material';

import { MidcitiesAddressAutocompleteComponent } from './midcities-address-autocomplete/midcities-address-autocomplete.component';
import { StateAutocompleteComponent } from './state-autocomplete/state-autocomplete.component';
import { ContainerAutocompleteComponent } from './container-autocomplete/container-autocomplete.component';
import { MidcitiesNameAutocompleteComponent } from './midcities-name-autocomplete/midcities-name-autocomplete.component';
import { BingAutocompleteComponent } from './bing-autocomplete/bing-autocomplete.component';
import { GeneralAutocompleteComponent } from './general-autocomplete/general-autocomplete.component';
import { CustomerAutocompleteComponent } from './customer-autocomplete/customer-autocomplete.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatTooltipModule
  ],
  declarations: [
    BingAutocompleteComponent,
    MidcitiesAddressAutocompleteComponent,
    ContainerAutocompleteComponent,
    MidcitiesNameAutocompleteComponent,
    GeneralAutocompleteComponent,
    CustomerAutocompleteComponent,
    StateAutocompleteComponent,

  ],
  exports: [
    BingAutocompleteComponent,
    MidcitiesAddressAutocompleteComponent,
    MidcitiesNameAutocompleteComponent,
    GeneralAutocompleteComponent,
    CustomerAutocompleteComponent,
    ContainerAutocompleteComponent,
    StateAutocompleteComponent
  ]
})
export class AutocompleteModule { }
