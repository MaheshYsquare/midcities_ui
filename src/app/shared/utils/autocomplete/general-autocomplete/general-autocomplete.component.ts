import { Component, OnInit, ChangeDetectionStrategy, forwardRef, ViewChild, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';

import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material';

import { Observable, of, Subscription } from 'rxjs';
import {
  startWith,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  map,
  shareReplay,
  tap,
  catchError
} from 'rxjs/operators';

import { skipAfter } from '../../rxjs-opertators';
import { SharedService } from 'app/shared/service/shared.service';
import { GeneralAutoComplete } from 'app/shared/models/autocomplete.model';

@Component({
  selector: 'app-general-autocomplete',
  templateUrl: './general-autocomplete.component.html',
  styleUrls: ['./general-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => GeneralAutocompleteComponent),
    multi: true,
  }]
})
export class GeneralAutocompleteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  public _onChange: any = () => { };

  public control: FormControl = new FormControl();

  public controlAutocomplete: Observable<string[]>;

  public autoSuggestion: GeneralAutoComplete[];

  public showLoading: boolean;

  @ViewChild(MatAutocomplete) autoComplete: MatAutocomplete;

  @Output() autoSuggestions: EventEmitter<GeneralAutoComplete[]> = new EventEmitter();

  @Output() legsToAssign: EventEmitter<number> = new EventEmitter();

  @Input() label: string;

  @Input() styleClass: string;

  @Input() isRequired: boolean;

  @Input() for: string;

  @Input() searchBy?: string;

  @Input() key?: string;

  public subscriptions: Subscription[] = [];

  constructor(private sharedService: SharedService) { }

  ngOnInit() {

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.control.valueChanges.subscribe(value => {
        this._onChange(value);
      })
    );

    this.controlAutocomplete = this.control.valueChanges
      .pipe(startWith(""), debounceTime(400), distinctUntilChanged()).pipe(
        (skipAfter(this.autoComplete.optionSelected.asObservable())))
      .pipe(
        switchMap(value => {
          this.showLoading = true;
          return value ?
            this.sharedService.generalAutoSuggest(this.for,
              value,
              this.for === 'searches' || this.for === 'filter' ? this.searchBy : null,
              this.for === 'filter' ? this.key : null) :
            of([]);
        }),
        map((data: any[]) => {
          if (this.for === 'searches' || this.for === 'filter') {
            return data
          }
          this.autoSuggestion = data;
          this.autoSuggestions.emit(data);
          return data.map(temp => temp.searchValue)
        }),
        shareReplay(1),
        tap(_ => (this.showLoading = false), _ => (this.showLoading = false)),
        catchError(err => {
          return of([])
        })
      )
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    setTimeout(() => {
      this.control.setValue(value);
    }, 100)
  }

  registerOnChange(fn: any) {
    this._onChange = fn
  }

  registerOnTouched(fn: any) { }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable()
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    if (this.for === 'searches' || this.for === 'filter') {
      return
    }
    this.autoSuggestions.emit(this.autoSuggestion.filter(temp => temp.searchValue === event.option.value));
    if (this.for === 'orders') {
      let data = this.autoSuggestion.filter(temp => temp.searchValue === event.option.value)[0];
      this.legsToAssign.emit(data.legToAssign);
    }
  }
}
