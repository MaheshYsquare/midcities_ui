import { Component, OnInit, ChangeDetectionStrategy, forwardRef, Input, EventEmitter, Output, ViewChild, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl, NG_VALIDATORS } from '@angular/forms';

import { MatAutocomplete, MatDialogConfig, MatDialog } from '@angular/material';

import { Observable, combineLatest, of, Subscription } from 'rxjs';
import {
  startWith,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  map,
  shareReplay,
  tap,
  catchError
} from 'rxjs/operators';

import { CustomerData } from 'app/shared/models/customer.model';

import { skipAfter } from '../../rxjs-opertators';
import { SharedService } from 'app/shared/service/shared.service';
import { EditCustomerComponent } from 'app/shared/modules/customer-shared/edit-customer.component';

@Component({
  selector: 'app-customer-autocomplete',
  templateUrl: './customer-autocomplete.component.html',
  styleUrls: ['./customer-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomerAutocompleteComponent),
    multi: true
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => CustomerAutocompleteComponent),
    multi: true
  }
  ]
})
export class CustomerAutocompleteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @Input() label: string;

  @Input() isCreatable: boolean;

  @Input() locationTypes: { label: string, value: number }[];

  @Input() styleClass?: string;

  @Input() editMode?: boolean;

  @Output() customerObject: EventEmitter<CustomerData> = new EventEmitter();

  @Output() customerAutoSuggestions: EventEmitter<CustomerData[]> = new EventEmitter();

  @ViewChild(MatAutocomplete) autoComplete: MatAutocomplete;

  public currentCustomerName: string;

  public _onChange: any = () => { };

  public _onTouch: any = () => { };

  public control: FormControl = new FormControl();

  public controlAutocomplete: Observable<string[]>;

  public customerNameCheck: Observable<any>;

  public customerData: CustomerData[];

  public showLoading: boolean;

  public isButtonVisible: boolean = true;

  public initial: boolean = true;

  public isInitial: boolean = true;

  public subscriptions: Subscription[] = [];

  constructor(private dialog: MatDialog,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.control.valueChanges.subscribe(value => {
        this._onChange(value);
      })
    )

    this.controlAutocomplete = this.control.valueChanges
      .pipe(startWith(""), debounceTime(400), distinctUntilChanged()).pipe(
        (skipAfter(this.autoComplete.optionSelected.asObservable())))
      .pipe(
        switchMap(value => {
          this.showLoading = true;
          return value ? this.sharedService.searchCustomerAutosuggest('customers', value) : of([]);
        }),
        map(data => {
          this.customerData = data;
          this.customerAutoSuggestions.emit(data);
          this.isInitial ? this.control.updateValueAndValidity() : null;
          this.isInitial = false;
          return this.customerData.map(temp => temp.business_name)
        }),
        shareReplay(1),
        tap(_ => (this.showLoading = false), _ => (this.showLoading = false)),
        catchError(err => {
          return of([])
        })
      )

    if (this.isCreatable) {
      this.customerNameCheck = combineLatest(
        this.control.valueChanges,
        this.controlAutocomplete
      ).pipe(
        map(([value, location]) => {
          const selectedCustomerName = location.find(temp => temp.toLowerCase() === (value !== null ?
            value.toLowerCase() : value));
          if (selectedCustomerName) {
            this.isButtonVisible = false;
            this._onChange(selectedCustomerName);
          } else {
            this.isButtonVisible = true;
          }
          return !selectedCustomerName;
        })
      );
    }

    this.control.setValidators(this.customerValidator.bind(this));
    this.control.updateValueAndValidity();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    setTimeout(() => {
      this.control.setValue(value);
      this.currentCustomerName = value;
    }, 0);

    setTimeout(() => {
      this.editMode ? this.control.markAsTouched() : null;
    }, 300);
  }

  registerOnChange(fn: any) {
    this._onChange = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouch = fn;
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable();
  }

  onClickAddCustomer() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": false,
      "locationTypes": this.locationTypes,
      "business_name": this.control.value
    };
    let dialogRef = this.dialog.open(EditCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: CustomerData) => {
      if (result != undefined) {
        this.customerData = [result];
        this.control.setValue(result.business_name);
        this.onOptionSelected(result.business_name);
        this.initial = false;
        this.isButtonVisible = false;
      }
    });
  }

  onOptionSelected(event) {
    this.customerAutoSuggestions.emit(this.customerData.filter(temp => temp.business_name == event));
  }

  customerValidator(control: FormControl): { [s: string]: boolean } {
    let validatorCheck = this.customerData && this.customerData.length !== 0 ?
      this.customerData
        .filter(temp => temp.business_name.toLowerCase() === (control.value !== null ?
          control.value.toLowerCase() : control.value))
      : [];
    if (control.value !== null && control.value !== "" && validatorCheck.length === 0) {
      return { 'incorrectValue': true }
    } else {
      return null;
    }
  }

  // communicate the inner form validation to the parent form
  validate(c: FormControl) {
    return this.control.valid ? null : { customer_name: { valid: false } };
  }

}
