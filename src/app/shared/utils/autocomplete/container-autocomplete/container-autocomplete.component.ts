import { Component, OnInit, OnDestroy, forwardRef, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor } from '@angular/forms';
import { Subscription, Observable, of } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, switchMap, map, catchError, shareReplay } from 'rxjs/operators';
import { ContainerData } from 'app/admin/container/container.model';
import { SharedService } from 'app/shared/service/shared.service';

export interface ContainerValues {
  container_name: string;
  chassis_name: string;
  hire_name: string;
  hire_dehire_loc: string;
}

@Component({
  selector: 'app-container-autocomplete',
  templateUrl: './container-autocomplete.component.html',
  styleUrls: ['./container-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContainerAutocompleteComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ContainerAutocompleteComponent),
      multi: true
    }
  ]
})
export class ContainerAutocompleteComponent implements OnInit, OnDestroy, ControlValueAccessor {

  @Input() isRequired: boolean;

  @Input() editMode: boolean;

  @Input() draftMode: boolean;

  public form: FormGroup;

  public subscriptions: Subscription[] = [];

  public onChange: any = () => { };

  public containerAutocomplete: Observable<string[]>;

  public containerAutoSuggestions: ContainerData[];

  public chassisNames: string[];

  public hireDehireLocations: { hireAddress: string; hire_name: string; isDefault: boolean }[];

  public isInitial: boolean = true;

  set value(value: ContainerValues) {
    this.form.setValue(value);
    this.onChange(value);
  }

  constructor(private sharedService: SharedService) { }

  ngOnInit() {

    this.form = new FormGroup({
      container_name: new FormControl(null, [this.containerValidator.bind(this)]),
      chassis_name: new FormControl(),
      hire_name: new FormControl(),
      hire_dehire_loc: new FormControl()
    })

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
      })
    );

    this.containerAutocomplete = this.form.get('container_name').valueChanges.pipe(
      startWith(""),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(value => {
        return value ? this.sharedService.searchContainerAutosuggest('containers', value) : of([]);
      }),
      map(data => {
        this.containerAutoSuggestions = data;
        this.isInitial && this.containerAutoSuggestions.length !== 0 ?
          this.onContainerOptionSelected(this.form.get('container_name').value) : null;
        return this.containerAutoSuggestions.map(temp => temp.container_name)
      }),
      shareReplay(1),
      catchError(err => {
        return of([])
      })
    )
  }

  containerValidator(control: FormControl): { [s: string]: boolean } {
    let validatorCheck = this.containerAutoSuggestions && this.containerAutoSuggestions.length !== 0 ?
      this.containerAutoSuggestions
        .filter(data => data.container_name === control.value)
      : [];
    if (control.value !== null && control.value !== "" && validatorCheck.length === 0) {
      return { 'incorrectValue': true }
    } else {
      return null;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    setTimeout(() => {
      if (value) {
        this.value = value;
      }
    }, 0)
  }

  registerOnChange(fn: any) {
    this.onChange = fn
  }

  registerOnTouched(fn: any) { }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.form.disable() : this.form.enable();
  }

  // communicate the inner form validation to the parent form
  validate(_: FormControl) {
    return this.form.valid ? null : { containerNameDetails: { valid: false } };
  }

  /* perform on each Keystroke */
  onContainerKeyup(event) {
    if (event.target.value === '') {
      this.form.get('chassis_name').setValue(null);
      this.form.get('hire_name').setValue(null);
      this.form.get('hire_dehire_loc').setValue(null);
      this.hireDehireLocations = [];
      this.chassisNames = [];
    }
    // else {
    // let control = this.form.get('container_name');

    // let containerValidator: any = this.containerAutoSuggestions.length !== 0 ? this.containerAutoSuggestions
    //   .find(temp => temp.container_name === control.value)
    //   : null;

    // if (containerValidator) {
    //   this.hireDehireLocations = containerValidator.hire_location;
    //   this.chassisNames = containerValidator.preferred_chassis_rental;

    //   let defaultHireLocation = containerValidator.hire_location.find(temp => temp.isDefault === true);

    //   if ((this.editMode || this.draftMode) ? (defaultHireLocation && !this.isInitial) : defaultHireLocation) {
    //     this.form.get('hire_name').setValue(defaultHireLocation.hire_name);

    //     this.form.get('hire_dehire_loc').setValue(defaultHireLocation.hireAddress);
    //   }

    //   this.isInitial = false;
    // } else {
    //   this.hireDehireLocations = [];
    //   this.chassisNames = [];
    //   this.form.get('chassis_name').setValue(null);
    //   this.form.get('hire_name').setValue(null);
    //   this.form.get('hire_dehire_loc').setValue(null);
    // }
    // }
  }

  onContainerOptionSelected(event: string) {
    if ((this.editMode || this.draftMode) ? !this.isInitial : true) {
      this.hireDehireLocations = [];
      this.chassisNames = [];
      this.form.get('chassis_name').setValue(null);
      this.form.get('hire_name').setValue(null);
      this.form.get('hire_dehire_loc').setValue(null);
    }

    let containerValidator: any = this.containerAutoSuggestions.find(temp => temp.container_name === event);

    if (containerValidator) {
      this.hireDehireLocations = containerValidator.hire_location;
      this.chassisNames = containerValidator.preferred_chassis_rental;

      let defaultHireLocation = containerValidator.hire_location ?
        containerValidator.hire_location.find(temp => temp.isDefault === true) : null;

      if ((this.editMode || this.draftMode) ? (defaultHireLocation && !this.isInitial) : defaultHireLocation) {
        this.form.get('hire_name').setValue(defaultHireLocation.hire_name);

        this.form.get('hire_dehire_loc').setValue(defaultHireLocation.hireAddress);
      }

      this.isInitial ?
        this.form.get('container_name').updateValueAndValidity() : null;

      this.isInitial = false;
    } else {
      this.hireDehireLocations = [];
      this.chassisNames = [];
      this.form.get('chassis_name').setValue(null);
      this.form.get('hire_name').setValue(null);
      this.form.get('hire_dehire_loc').setValue(null);
    }
  }

  onHireDehireSelectionChange(event: string) {
    let hireDehireLocation = (event !== null) ?
      this.hireDehireLocations.find(temp => temp.hire_name === event) : null;

    if (hireDehireLocation) {
      this.form.get('hire_dehire_loc').setValue(hireDehireLocation.hireAddress);
    }
  }
}
