import { Component, OnInit, Input, Output, forwardRef, EventEmitter, ChangeDetectionStrategy, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatAutocomplete } from '@angular/material';

import { Observable, of, Subscription } from 'rxjs';
import {
  startWith,
  distinctUntilChanged,
  debounceTime,
  switchMap,
  map,
  catchError,
  tap,
  shareReplay
} from 'rxjs/operators';

import { LocationData } from 'app/shared/models/location.model';
import { SharedService } from 'app/shared/service/shared.service';
import { skipAfter } from '../../rxjs-opertators';

@Component({
  selector: 'app-midcities-address-autocomplete',
  templateUrl: './midcities-address-autocomplete.component.html',
  styleUrls: ['./midcities-address-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MidcitiesAddressAutocompleteComponent),
    multi: true,
  }]
})
export class MidcitiesAddressAutocompleteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @Input() label: string;

  @Input() styleClass: string;

  @Input() isRequired: boolean;

  @Input() isSame?: boolean;

  @Output() street: EventEmitter<string> = new EventEmitter();

  @Output() city: EventEmitter<string> = new EventEmitter();

  @Output() postalCode: EventEmitter<string> = new EventEmitter();

  @Output() state: EventEmitter<string> = new EventEmitter();

  @Output() phoneNo: EventEmitter<string> = new EventEmitter();

  @Output() email: EventEmitter<string> = new EventEmitter();

  @Output() name: EventEmitter<string> = new EventEmitter();

  @Output() address: EventEmitter<string> = new EventEmitter();

  @Output() seletedLocationData: EventEmitter<LocationData> = new EventEmitter();

  @ViewChild(MatAutocomplete) autoComplete: MatAutocomplete;

  public _onChange: any = () => { };

  public control: FormControl = new FormControl();

  public controlAutocomplete: Observable<string[]>;

  public locationData: LocationData[];

  public showLoading: boolean;

  public subscriptions: Subscription[] = [];

  constructor(private sharedService: SharedService) { }

  ngOnInit() {

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.control.valueChanges.subscribe(value => {
        this._onChange(value);
      })
    );

    this.controlAutocomplete = this.control.valueChanges
      .pipe(startWith(""), debounceTime(400), distinctUntilChanged()).pipe(
        (skipAfter(this.autoComplete.optionSelected.asObservable())))
      .pipe(
        switchMap(value => {
          this.showLoading = true;
          return value ? this.sharedService.locationAutoSuggest('locations', value, 'address') : of([]);
        }),
        map(data => {
          this.locationData = data;
          return data.map(temp => temp.address)
        }),
        shareReplay(1),
        tap(_ => (this.showLoading = false), _ => (this.showLoading = false)),
        catchError(err => {
          return of([])
        })
      )
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value: any) {
    this.control.setValue(value)
  }

  registerOnChange(fn: any) {
    this._onChange = fn
  }

  registerOnTouched(fn: any) { }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable()
  }

  onOptionSelected(event) {
    let location: LocationData = this.locationData.find(temp => temp.address == event);

    if (location) {
      this.name.emit(location.location_name)
      this.street.emit(location.street);
      this.city.emit(location.city);
      this.state.emit(location.state);
      this.postalCode.emit(location.postal_code);
      this.seletedLocationData.emit(location);
      this.phoneNo.emit(location.phone_number);
      this.email.emit(location.email);
    }

  }

  onChangeAddress() {
    this.address.emit(this.control.value)
  }
}
