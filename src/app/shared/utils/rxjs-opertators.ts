import { Observable, pipe } from "rxjs";
import { filter, map, merge, scan, catchError } from "rxjs/operators";

export const mergeLastTwoEvents = scan((events: any, event) => {
  return {
    prev: events.curr,
    curr: event,
  };
}, {});

export const skipAfter = (ob: Observable<any>) =>
  pipe(
    map(e1 => ({
      value: e1,
    })),
    merge(ob.pipe(map(_ => ({ type: "skip" })))),
    mergeLastTwoEvents,
    filter(events => {
      // console.log("Events", events);
      const { prev = {}, curr = {} } = events;
      return (prev as any).type !== "skip" && (curr as any).type !== "skip";
    }),
    map(event => event.curr.value)
  );

