import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { environment } from "environments/environment";
import { SharedService } from "../service/shared.service";


@Injectable()
export class DriverInfoResolver implements Resolve<any> {
    private baseURL = environment.baseURL;
    httpOptions = {};
    constructor(private http: HttpClient, private sharedService: SharedService, private router: Router) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const url = `${this.baseURL}drivers/getSelectedDriverInfoData?driver_id=${route.queryParams['driverId']}`;
        return this.http.get(url, this.httpOptions).pipe(
            catchError(err => {
                // localStorage.clear();
                // window.location.reload();
                return of([])
            })
        )
    }
}