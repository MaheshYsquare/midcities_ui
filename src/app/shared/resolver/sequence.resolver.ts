import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";

import { Observable, of } from "rxjs";

import { environment } from "environments/environment";
import { catchError } from "rxjs/operators";
import { SharedService } from "../service/shared.service";

@Injectable()
export class SequenceResolver implements Resolve<any> {
    private baseURL = environment.baseURL;
    httpOptions = {};
    constructor(private http: HttpClient, private sharedService: SharedService, private router: Router) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const url = `${this.baseURL}manualInvoices/getManualInvoiceSequence`;
        return this.http.get(url, this.httpOptions).pipe(
            catchError(err => {
                // localStorage.clear();
                // window.location.reload();
                return of([])
            })
        )
    }
}