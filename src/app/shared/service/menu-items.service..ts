import { Injectable } from '@angular/core';
import { loginData } from 'app/session/session.model';
import { SharedService } from '../service/shared.service';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public rolePermissions: any;

  public loggedInUser: loginData;

  public menuItems: Menu[] = [];

  public routesIcon: any = {
    "Admin Features": 'supervisor_account',
    "Rate Management": 'attach_money',
    "Order Management": 'shopping_cart',
    "Dispatch": 'explore',
    "Invoicing": 'print',
    "Driver Management": 'assignment_ind',
    "Advanced Search": 'assignment',
    "Reporting": 'find_in_page'
  }

  constructor(private sharedService: SharedService) {
    this.loggedInUser = this.sharedService.getUserDetails();

    if (this.loggedInUser) {
      this.setMenuItem();
    }
  }

  setMenuItem() {
    this.menuItems = [];

    this.loggedInUser = this.sharedService.getUserDetails();

    this.rolePermissions = JSON.parse(this.loggedInUser.role_permissions);

    this.menuItems.push({
      state: 'dashboard',
      name: 'Dashboard',
      type: 'link',
      icon: 'dashboard'
    });

    Object.keys(this.rolePermissions).forEach(key => {

      if (typeof this.rolePermissions[key] === 'object' &&
        this.rolePermissions[key].length) {

        let childrenObject = {};

        let subMenuItems = [];

        this.rolePermissions[key].forEach(permission => {
          childrenObject = {
            state: permission.split(' ').join('-').toLowerCase(),
            name: permission
          }

          subMenuItems.push(childrenObject);
        });

        this.menuItems.push({
          state: key.split(' ').join('-').toLowerCase(),
          name: key,
          type: 'sub',
          icon: this.routesIcon[key],
          children: subMenuItems
        });

      } else if (typeof this.rolePermissions[key] === 'boolean' &&
        this.rolePermissions[key]) {

        this.menuItems.push({
          state: key.split(' ').join('-').toLowerCase(),
          name: key,
          type: 'link',
          icon: this.routesIcon[key]
        });

      }

    });
  }

  /**
   * getAllMenu
   * to fetch all menu based on given user permission
   */
  public getAllMenu(): Menu[] {
    const menu = [];

    Object.keys(this.rolePermissions).forEach(key => { // loop through the user permission object
      if (typeof this.rolePermissions[key] === 'object' &&
        this.rolePermissions[key].length) {

        menu.push(key);

      } else if (typeof this.rolePermissions[key] === 'boolean' &&
        this.rolePermissions[key]) {

        menu.push(key);

      }
    });

    return this.menuItems
      .filter(temp => temp.name === 'Dashboard' || menu.includes(temp.name));
  }

  /**
   * getSubMenu
   * to fetch the sub menu based on giver user permssion
   */
  public getSubMenu(key): ChildrenItems[] {

    let parentKey = key.split('-').join(' '); // join route to form parent key i.e., admin-features -> admin features

    let parentMenu = this.menuItems.find(temp => temp.state === key); // get the parent menu by using key

    let permission = [];

    let subMenu = [];

    Object.keys(this.rolePermissions).forEach(key => { // loop through the permission object to get sub menus permission
      if (key.toLowerCase() === parentKey) {
        permission = this.rolePermissions[key];
      }
    });

    if (parentMenu) { //if we have parent menu and the permission [] form the valid sub menu for allow user to open the screen

      subMenu = parentMenu.children.filter(temp => permission.includes(temp.name));

    }

    return subMenu;
  }

}
