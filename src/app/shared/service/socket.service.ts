import { Injectable } from '@angular/core';

import { Observable, Observer } from 'rxjs';

import * as socketIo from 'socket.io-client';

import { Socket } from '../../shared/interfaces/interfaces';
import { SharedService } from './shared.service';
import { environment } from 'environments/environment';

export interface NotificationConfig {
  isNewUserCreated?: boolean;
  newUserId?: number;
  isMultiple?: boolean;
  multipleRole?: string[];
  isDriver?: boolean;
  driverUserId?: number;
  role?: string;
  msg: string;
  hyperlink?: any;
  icon?: any;
  styleClass?: any;
}

@Injectable()

export class SocketService {

  public socket: Socket;

  public observer: Observer<any>;

  public notification: any;

  public users: any;

  private baseURL = environment.baseURL;

  constructor(private sharedService: SharedService) {
    this.socket = socketIo(this.baseURL.replace('/api', ''));
  }

  listenServerThroughSocket(listenTo: string) {

    let observable: Observable<any> = new Observable(observer => {
      this.socket.on(listenTo, (res) => observer.next(res));
    });

    return observable;
  }

  sendLoginDataFromClient() {
    this.socket.emit('login', { userDetails: this.sharedService.getUserDetails() })
  }

  sendLogoutDataFromClient() {
    this.socket.emit('logout', { userId: this.sharedService.getUserDetails().userId, id: this.sharedService.getUserDetails().id, permissions: this.sharedService.getUserDetails().permissions })
  }

  sendChangesInDriverFromClient(table) {
    this.socket.emit('changesInOrderDriver', {
      userId: this.sharedService.getUserDetails().userId,
      id: this.sharedService.getUserDetails().id,
      permissions: this.sharedService.getUserDetails().permissions,
      table: table
    })
  }

  sendNotification(config: NotificationConfig) {
    let request = {
      ...config,
      userId: this.sharedService.getUserDetails().userId
    }

    this.socket.emit('sendNotification', request);
  }
}
