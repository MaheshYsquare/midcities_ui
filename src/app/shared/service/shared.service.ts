import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from 'environments/environment';
import { DriverInfoData, DriverPayRequest, DriverPayResponse, DriverRelatedLegData } from '../models/driver.model';
import { Router } from '@angular/router';
import { LocationData, LocationResponse } from 'app/shared/models/location.model';
import { NotesResponse } from '../models/notes.model';
import { DriverDetailsResponse, DriverDetailsPostRequest } from 'app/driver/DriverData';
import { MatSnackBar, MatSnackBarConfig, MatDialog, MatDialogConfig } from '@angular/material';
import { AccessoriesData } from '../models/accessories.model';
import { CustomerActiveOrInactiveRequest, CustomerData, CustomerRouteAccessoriesResponse } from '../models/customer.model';
import { ConfirmationComponent } from '../modules/helper/confirmation/confirmation.component';
import { Observable, BehaviorSubject } from 'rxjs';
import { ConfirmationData, GetRequest, InformationData } from '../models/shared.model';
import { GeneralAutoComplete } from '../models/autocomplete.model';
import { LegResponse } from '../models/leg.model';
import { OrderGetRequest, OrderResponse } from '../models/order.model';
import { InformationComponent } from '../modules/helper/information/information.component';
import { SnackbarComponent } from 'app/shared/modules/helper/snackbar/snackbar.component';
import { DropdownData } from '../models/dropdown.model';
import { ContainerData } from 'app/admin/container/container.model';
import { InvoiceManagementRequest } from '../models/invoice.model';
import { RoutesData } from '../models/routes.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private baseURL = environment.baseURL;

  private userDetails: any;

  public httpOptions = {};

  private sideNavToggleBehaviour = new BehaviorSubject<boolean>(true);

  public HttpUploadOptions = {
    headers: new HttpHeaders({})
  }

  public isNewOrderCreated: boolean = false;

  public language: string = "en";

  constructor(private httpClient: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  /** Allows subscription to the behavior subject as an observable */
  getSideNavToggleState(): Observable<boolean> {
    return this.sideNavToggleBehaviour.asObservable();
  }

  /**
   * Allows updating the current state of the behavior subject
   * @param request a object representing the current state
   */
  setSideNavToggleState(toggle: boolean): void {
    this.sideNavToggleBehaviour.next(toggle);
  }

  getUserDetails() {
    if (!this.userDetails) {
      return JSON.parse(localStorage.getItem("userDetails"));
    }
    return this.userDetails;
  }

  getAuthToken() {
    let storedToken: string = localStorage.getItem("userDetails");
    if (!storedToken) {
      this.router.navigate(["/authentication/login"]);
      return "";
    } else {
      let token: string = JSON.parse(storedToken).id
      return token;
    }
  }

  checkTokenExpired() {
    const url = `${this.baseURL}users/checkTokenExpired`
    return this.httpClient.get(url, this.httpOptions)
  }

  locationAutoSuggest(actionComponentName: String, search: string, searchBy: string) {
    const url = `${this.baseURL + actionComponentName}/locationAutoSuggest?search=${search}&searchBy=${searchBy}`;
    return this.httpClient.get<LocationData[]>(url, this.httpOptions)
  }

  quickbooksConnection(submitFn) {
    let config = new MatSnackBarConfig();
    config.panelClass = ['warning-msg'];
    config.duration = 10000;
    let snackbarRef = this.snackBar.open("Please Connect To Quickbooks", 'Click Here To Connect', config);
    snackbarRef.onAction().subscribe(() => {
      this.authorizeQuickBooksConnection('invoices').subscribe(response => {
        let parameters = "location=1,width=800,height=650";
        parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;
        let win = window.open(response, 'connectPopup', parameters);
        let pollOAuth = window.setInterval(() => {
          try {
            if (win.closed) {
              window.clearInterval(pollOAuth);
            }
            if (win.document.URL.indexOf("code") != -1) {
              window.clearInterval(pollOAuth);
              win.close();
              localStorage.setItem('quickbookSync', 'true');
              submitFn();
              this.openSnackbar("Successfully Connected With Quickbooks", 2000, "success-msg");
            }
          } catch (e) { }
        }, 100);
      })
    })
  }

  reconnectQuickBooksConnection(actionComponentName: string) {
    const url = `${this.baseURL + actionComponentName}/reconnectQuickBooksConnection`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  openSnackbar(message, duration, panelClass) {
    this.snackBar.open(message, null, {
      duration,
      panelClass
    })
  }

  openConfirmation(confirmationData: ConfirmationData): Observable<any> {
    let dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = confirmationData;
    let dialog = this.matDialog.open(ConfirmationComponent, dialogConfig);
    return dialog.afterClosed()
  }

  openInfo(informationData: InformationData): Observable<any> {
    let dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.data = informationData;
    let dialog = this.matDialog.open(InformationComponent, dialogConfig);
    return dialog.afterClosed()
  }

  openSnackbarWithAction(message, duration, panelClass, isIcon, icon) {
    let config = new MatSnackBarConfig();
    config.panelClass = [panelClass];
    config.duration = duration;
    config.data = {
      message: message,
      icon: icon,
      isNormal: isIcon
    }
    let snackbarRef = this.snackBar.openFromComponent(SnackbarComponent, config);
    return snackbarRef
  }

  getCarriersList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getCarriersList`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  makeLegBrokered(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/makeLegBrokered`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  getDropdownList(actionComponentName: String, dropdownType: string) {
    const url = `${this.baseURL + actionComponentName}/getDropdown?dropdownType=${dropdownType}`;
    return this.httpClient.get<{ label: string, value: number }[]>(url, this.httpOptions)
  }

  getAllDropdown(actionComponentName: String, dropdownType: string) {
    const url = `${this.baseURL + actionComponentName}/getAllDropdown?dropdownType=${dropdownType}`;
    return this.httpClient.get<DropdownData>(url, this.httpOptions)
  }

  getVehiclesList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getVehiclesList`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  getVehicleLocation(actionComponentName: String, id: any) {
    const url = `${this.baseURL + actionComponentName}/getVehicleLocation?id=${id}&date=${new Date().toISOString()}`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  getTruckNumberList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getTruckNumberList`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  postOrder(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/postOrderManagement`;
    return this.httpClient.post(url, request, this.httpOptions)
  }

  getSelectedCustomer(componentName: any, customer_id: any) {
    const url = `${this.baseURL + componentName}/getSelectedCustomer?customer_id=${customer_id}`;
    return this.httpClient.get(url, this.httpOptions)
  }

  /** POST: add a new  Driverrate to the server */
  postRouteData(actionComponentName: String, newRoutesData: RoutesData) {
    const url = `${this.baseURL + actionComponentName}/postRouteRate`;
    return this.httpClient.post<RoutesData>(url, newRoutesData, this.httpOptions)
  }

  /** Put: edit a roues rates to the server */
  updateRouteData(actionComponentName: String, newRoutesData: RoutesData) {
    const url = `${this.baseURL + actionComponentName}/updateRouteRate`;
    return this.httpClient.put<RoutesData>(url, newRoutesData, this.httpOptions)
  }

  getLegAccessories(actionComponentName: String, containerLevelId: number) {
    const url = `${this.baseURL + actionComponentName}/getLegAccessories?containerLevelId=${containerLevelId}`;
    return this.httpClient.get<LegResponse>(url, this.httpOptions)
  }

  searchCustomerAutosuggest(actionComponentName: String, searchString: string, type?: any): Observable<CustomerData[]> {
    const url = `${this.baseURL + actionComponentName}/searchCustomerAutosuggest?searchString=${searchString}&type=${type}`;
    return this.httpClient.get<CustomerData[]>(url, this.httpOptions)
  }

  searchContainerAutosuggest(actionComponentName: any, searchString: string) {
    const url = `${this.baseURL + actionComponentName}/searchContainerAutosuggest?searchString=${searchString}`;
    return this.httpClient.get<ContainerData[]>(url, this.httpOptions)
  }

  getUserNotification(actionComponentName: String, userId) {
    const url = `${this.baseURL + actionComponentName}/getUserNotification?userId=${userId}`;
    return this.httpClient.get(url, this.httpOptions)
  }

  updateUserNotification(actionComponentName: String, userId) {
    const url = `${this.baseURL + actionComponentName}/updateUserNotification?userId=${userId}`;
    return this.httpClient.put(url, null, this.httpOptions)
  }

  putOrder(actionComponentName: String, newOrderData: any) {
    const url = `${this.baseURL + actionComponentName}/updateOrderManagement`;
    return this.httpClient.put<any>(url, newOrderData, this.httpOptions)
  }

  afterInvoiceEdit(actionComponentName: String, newOrderData: any) {
    const url = `${this.baseURL + actionComponentName}/afterInvoiceEdit`;
    return this.httpClient.put<any>(url, newOrderData, this.httpOptions)
  }

  getAccessoriesChargeList(actionComponentName: String, customerid: number) {
    const url = `${this.baseURL + actionComponentName}/getAccessoriesCharges?customer_id=${customerid}`;
    return this.httpClient.get<String[]>(url, this.httpOptions)
  }

  getAccessoriesNameList(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/getAccessoriesNames`;
    return this.httpClient.get<string[]>(url, this.httpOptions)
  }

  /** POST: add a new Customer to the server */
  postCustomerData(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/postCustomerData`;
    return this.httpClient.post<any>(url, newCustomerData, this.httpOptions)
  }

  /** Put: edit a customer to the server */
  makeActiveOrInactive(actionComponentName: String, request: CustomerActiveOrInactiveRequest) {
    const url = `${this.baseURL + actionComponentName}/makeActiveOrInactive`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  /** GET Customers info from the server */
  getCustomerRateAccessories(actionComponentName: String, customerid: number) {
    const url = `${this.baseURL + actionComponentName}/getCustomerRateAccessories?customer_id=${customerid}`;
    return this.httpClient.get<CustomerRouteAccessoriesResponse>(url, this.httpOptions)
  }

  /** DELETE: delete the customer from the server */
  deleteCustomerSpecificRouteRate(actionComponentName: String, routeRateId: number) {
    const url = `${this.baseURL + actionComponentName}/${routeRateId}`;
    return this.httpClient.delete<any>(url, this.httpOptions);
  }

  /** POST: add a new Customer Acc to the server */
  postCustomerAccessories(actionComponentName: String, request: AccessoriesData) {
    const url = `${this.baseURL + actionComponentName}`;
    return this.httpClient.post<AccessoriesData>(url, request, this.httpOptions);
  }

  /** Put: edit a customer acc to the server */
  editCustomerAccessories(actionComponentName: String, request: AccessoriesData) {
    const url = `${this.baseURL + actionComponentName}`;
    return this.httpClient.put<AccessoriesData>(url, request, this.httpOptions);
  }

  /** DELETE: delete the customer acc from the server */
  deleteCustomerAccessories(actionComponentName: String, AccessorialChargeId: number) {
    const url = `${this.baseURL + actionComponentName}/${AccessorialChargeId}`;
    return this.httpClient.delete<any>(url, this.httpOptions);
  }

  customerRateBulkUpdate(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateBulkRate`;
    return this.httpClient.put<any>(url, newCustomerData, this.httpOptions)
  }

  getInvoiceManagementData(actionComponentName: String, request: InvoiceManagementRequest) {
    const url = `${this.baseURL + actionComponentName}/getInvoiceManagementData`;
    return this.httpClient.post(url, request, this.httpOptions)
  }

  updateInvoiceManagement(actionComponentName: String, newInvoiceData: any) {
    const url = `${this.baseURL + actionComponentName}/updateInvoiceManagement`;
    return this.httpClient.put<any>(url, newInvoiceData, this.httpOptions)
  }

  updateManageBilling(actionComponentName: String, newBilling: any) {
    const url = `${this.baseURL + actionComponentName}/updateManageBilling`;
    return this.httpClient.put<any>(url, newBilling, this.httpOptions)
  }

  deleteFile(actionComponentName: String, deleteFileData: any) {
    const url = `${this.baseURL + actionComponentName}/deleteFile`;
    return this.httpClient.post<any>(url, deleteFileData, this.HttpUploadOptions)
  }

  getFile(actionComponentName: String, fileDetail: any) {
    const url = `${this.baseURL + actionComponentName}/getFile`;
    return this.httpClient.post<any>(url, fileDetail, this.httpOptions)
  }

  deleteAutoInvoice(actionComponentName: String, data: any) {
    const url = `${this.baseURL + actionComponentName}/deleteAutoInvoice`;
    return this.httpClient.post<any>(url, data, this.HttpUploadOptions)
  }

  deleteManualInvoice(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/deleteManualInvoice`;
    return this.httpClient.post(url, request, this.httpOptions)
  }

  authorizeQuickBooksConnection(actionComponentName: String) {
    const url = `${this.baseURL + actionComponentName}/authorizeQuickBooksConnection`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  updateCustomerData(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateCustomerData`;
    return this.httpClient.put<any>(url, newCustomerData, this.httpOptions)
  }

  updateDriverNotes(actionComponentName: String, newCustomerData: any) {
    const url = `${this.baseURL + actionComponentName}/updateDriverNotes`;
    return this.httpClient.post<any>(url, newCustomerData, this.httpOptions)
  }

  /** GET Driver info from the server */
  getDriverPayData(actionComponentName: String, request: DriverPayRequest) {
    let url = `${this.baseURL + actionComponentName}/getDriverPayData`;
    return this.httpClient.post<DriverPayResponse>(url, request, this.httpOptions)
  }

  getDriverRelatedLeg(actionComponentName: String, driverId: number) {
    let url = `${this.baseURL + actionComponentName}/getDriverRelatedLeg?driverId=${driverId}`;
    return this.httpClient.get<DriverRelatedLegData[]>(url, this.httpOptions)
  }

  /* edit driver info */
  updateDriverMoreInfoData(actionComponentName: String, driverData: any) {
    const url = `${this.baseURL + actionComponentName}/updateDriverMoreInfo`;
    return this.httpClient.put<any>(url, driverData, this.httpOptions)
  }

  postMoreInfo(actionComponentName: String, driverData: any) {
    const url = `${this.baseURL + actionComponentName}/postMoreInfo`;
    return this.httpClient.post<any>(url, driverData, this.httpOptions)
  }

  /* get selected driver data */
  getSelectedDriverInfoData(actionComponentName: String, driver_id: number) {
    const url = `${this.baseURL + actionComponentName}/getSelectedDriverInfoData?driver_id=${driver_id}`;
    return this.httpClient.get<DriverInfoData[]>(url, this.httpOptions)
  }

  getDriverDocumentLink(actionComponentName: String, driverid: number) {
    const url = `${this.baseURL + actionComponentName}/getDocumentLinks?driver_id=${driverid}`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  fetchCustomerUploadDocsList(actionComponentName: String, id: number) {
    const url = `${this.baseURL + actionComponentName}/fetchCustomerUploadDocsList?id=${id}`;
    return this.httpClient.get<any>(url, this.httpOptions)
  }

  getOrderUploadedDocumentList(actionComponentName: String, ordercontainerchassisid: number) {
    const url = `${this.baseURL + actionComponentName}/getOrderUploadedDocumentList?order_container_chassis_id=${ordercontainerchassisid}`;
    return this.httpClient.get<{ code: number; data: string[] }>(url, this.httpOptions)
  }

  uploadfile(actionComponentName: String, request: FormData) {
    const url = `${this.baseURL + actionComponentName}/uploadFile`;
    return this.httpClient.post<any>(url, request, this.HttpUploadOptions)
  }

  uploadCustomerFile(actionComponentName: String, request: FormData) {
    const url = `${this.baseURL + actionComponentName}/uploadCustomerFile`;
    return this.httpClient.post<any>(url, request, this.HttpUploadOptions)
  }

  getNotes(actionComponentName: String, id: number) {
    const url = `${this.baseURL + actionComponentName}/getNotes?order_container_chassis_id=${id}`;
    return this.httpClient.get<NotesResponse[]>(url, this.httpOptions)
  }

  editNotes(actionComponentName: String, newNotes: any) {
    const url = `${this.baseURL + actionComponentName}/putNotes`;
    return this.httpClient.put<any>(url, newNotes, this.httpOptions)
  }

  postNotes(actionComponentName: String, newNote: any) {
    const url = `${this.baseURL + actionComponentName}/postNotes`;
    return this.httpClient.post<any>(url, newNote, this.httpOptions)
  }

  deleteNotes(actionComponentName: String, notes_id: number, ordercontainerchassisid: number) {
    const url = `${this.baseURL + actionComponentName}/deleteNotes?notes_id=${notes_id}&order_container_chassis_id=${ordercontainerchassisid}`;
    return this.httpClient.delete<any>(url, this.HttpUploadOptions)
  }

  /** GET Driver from the server */
  getDriverDetails(actionComponentName: String, request: DriverDetailsPostRequest) {
    const url = `${this.baseURL + actionComponentName}/getDriverDetails`;
    return this.httpClient.post<DriverDetailsResponse>(url, request, this.httpOptions)
  }

  uploadDriverFile(actionComponentName, file: FormData) {
    const url = `${this.baseURL + actionComponentName}/uploadDriverFile`;
    return this.httpClient.post<any>(url, file, this.HttpUploadOptions)
  }

  getDriverFile(actionComponentName: String, data: any) {
    const url = `${this.baseURL + actionComponentName}/getDriverFile`;
    return this.httpClient.post<any>(url, data, this.HttpUploadOptions)
  }

  deleteDriverFile(actionComponentName: String, data: any) {
    const url = `${this.baseURL + actionComponentName}/deleteDriverFile`;
    return this.httpClient.post<any>(url, data, this.HttpUploadOptions)
  }

  updateLegStatusTopBottom(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateLegStatusTopBottom`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  updateLegStatusBottomTop(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateLegStatusBottomTop`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  postHubLeg(actionComponentName: String, newLegTableData: any) {
    const url = `${this.baseURL + actionComponentName}/postHubLegManagement`;
    return this.httpClient.post<any>(url, newLegTableData, this.httpOptions)
  }

  postLeg(actionComponentName: String, newLegTableData: any) {
    const url = `${this.baseURL + actionComponentName}/postLegManagement`;
    return this.httpClient.post<any>(url, newLegTableData, this.httpOptions)
  }

  deleteLeg(actionComponentName: String, newLegTableData: any) {
    const url = `${this.baseURL + actionComponentName}/deleteLegData`;
    return this.httpClient.post<any>(url, newLegTableData, this.httpOptions)
  }

  editLeg(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateLegManagement`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  uploadOrderfile(actionComponentName: String, request: FormData) {
    const url = `${this.baseURL + actionComponentName}/uploadFile`;
    return this.httpClient.post<any>(url, request, this.HttpUploadOptions)
  }

  getOrderFile(actionComponentName: String, fileDetail: any) {
    const url = `${this.baseURL + actionComponentName}/getFile`;
    return this.httpClient.post<any>(url, fileDetail, this.httpOptions)
  }

  getOrderFileSize(actionComponentName: String, fileDetail: any) {
    const url = `${this.baseURL + actionComponentName}/getOrderFileSize`;
    return this.httpClient.post<any>(url, fileDetail, this.httpOptions)
  }

  getFileSize(actionComponentName: String, fileDetail: any) {
    const url = `${this.baseURL + actionComponentName}/getFileSize`;
    return this.httpClient.post<any>(url, fileDetail, this.httpOptions)
  }

  deleteOrderFile(actionComponentName: String, deleteFileData: any) {
    const url = `${this.baseURL + actionComponentName}/deleteFile`;
    return this.httpClient.post<any>(url, deleteFileData, this.HttpUploadOptions)
  }

  deleteCustomerFile(actionComponentName: String, deleteFileData: any) {
    const url = `${this.baseURL + actionComponentName}/deleteCustomerFile`;
    return this.httpClient.post<any>(url, deleteFileData, this.HttpUploadOptions)
  }

  editAccessories(actionComponentName: String, newLegTableData: AccessoriesData) {
    const url = `${this.baseURL + actionComponentName}`;
    return this.httpClient.put<AccessoriesData>(url, newLegTableData, this.httpOptions)
  }

  deleteAccessories(actionComponentName: String, order: String) {
    let id: String = order;
    const url = `${this.baseURL + actionComponentName}/${id}`;
    return this.httpClient.delete<String>(url, this.httpOptions)
  }

  rearrangeLegNumber(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/rearrangeLegNumber`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  postBobtailLeg(actionComponentName: String, newLegTableData: any) {
    const url = `${this.baseURL + actionComponentName}/postBobtailLegManagement`;
    return this.httpClient.post<any>(url, newLegTableData, this.httpOptions)
  }

  sparseUpdateLegData(actionComponentName: String, newOrderData: any) {
    const url = `${this.baseURL + actionComponentName}/sparseUpdateLegData`;
    return this.httpClient.put<any>(url, newOrderData, this.httpOptions)
  }

  updateOrdersMiles(actionComponentName: String, newOrderData: any) {
    const url = `${this.baseURL + actionComponentName}/updateOrdersMiles`;
    return this.httpClient.post<any>(url, newOrderData, this.httpOptions)
  }

  updateChargesRate(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateChargesRate`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  postAccessories(actionComponentName: String, newLegTableData: any, id: number) {
    const url = `${this.baseURL + actionComponentName}`;
    return this.httpClient.post<any>(url, newLegTableData, this.httpOptions)
  }

  sendDriverLoadsheetEmail(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/sendEmail`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  sendEmailToContainerOwner(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/sendEmailToContainerOwner`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  emailInvoiceWithAttachment(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/emailInvoiceWithAttachment`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  sendEmailWithMergeDocs(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/sendEmailWithMergeDocs`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  viewWithMergeDocs(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/viewWithMergeDocs`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  checkRouteRate(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/checkRouteRate`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  generalAutoSuggest(component: String, searchString: string, searchBy?: string, key?: string): Observable<any[]> {
    let url;
    switch (component) {
      case 'orders':
        url = `${this.baseURL + component}/orderAutoSuggest?search=${encodeURIComponent(searchString)}`;
        break;

      case 'drivers':
        url = `${this.baseURL + component}/driverAutoSuggest?search=${encodeURIComponent(searchString)}`;
        break;

      case 'filter':
        url = `${this.baseURL}searches/filterAutoSuggest?module=${key}&search=${encodeURIComponent(searchString)}&searchBy=${searchBy}`;
        break;

      default:
        url = `${this.baseURL + component}/advancedSearchAutoSuggest?search=${encodeURIComponent(searchString)}&searchBy=${searchBy}`;
        break;
    }

    return this.httpClient.get<any[]>(url, this.httpOptions)
  }

  driverAutoSuggest(actionComponentName: String, searchString: string): Observable<GeneralAutoComplete[]> {
    const url = `${this.baseURL + actionComponentName}/driverAutoSuggest?search=${searchString}`;
    return this.httpClient.get<GeneralAutoComplete[]>(url, this.httpOptions)
  }

  orderAutoSuggest(actionComponentName: String, searchString: string): Observable<GeneralAutoComplete[]> {
    const url = `${this.baseURL + actionComponentName}/orderAutoSuggest?search=${searchString}`;
    return this.httpClient.get<GeneralAutoComplete[]>(url, this.httpOptions)
  }

  postDriverPayData(actionComponentName: String, request: any): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/postDriverPayData`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  updateDriverPayData(actionComponentName: String, request: any): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/updateDriverPayData`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  deleteDriverPayData(actionComponentName: String, request: { containerLevelId: number; legNumber: number; driverPayId: number }): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/deleteDriverPayData`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  postAttendenceData(actionComponentName: String, request: any): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/postAttendenceData`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  updateAttendenceData(actionComponentName: String, request: any): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/updateAttendenceData`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  deleteAttendenceData(actionComponentName: String, request: any): Observable<any> {
    const url = `${this.baseURL + actionComponentName}/deleteAttendenceData`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  getLocationManagementList(actionComponentName: String, request: GetRequest) {
    const url = `${this.baseURL + actionComponentName}/getLocationManagementData`;
    return this.httpClient.post<LocationResponse>(url, request, this.httpOptions)
  }

  quickbooksCustomer(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/quickbooksCustomer`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  importQuickbooksCustomer(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/importQuickbooksCustomer`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  postAutoInvoice(actionComponentName: String, autoInvoice: any) {
    const url = `${this.baseURL + actionComponentName}/postAutoInvoice`;
    return this.httpClient.post<any>(url, autoInvoice, this.httpOptions)
  }

  moveOrderToInvoice(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/moveOrderToInvoice`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  moveOrderBackToDispatch(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/moveOrderBackToDispatch`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  makeOrderAsNoInvoice(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/makeOrderAsNoInvoice`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  cancelOrder(actionComponentName: String, request: { order_container_chassis_id; isDispatch }) {
    const url = `${this.baseURL + actionComponentName}/cancelOrder`;
    return this.httpClient.post<any>(url, request, this.httpOptions);
  }

  getSelectedOrder(actionComponentName: String, order_number, sequence) {
    const url = `${this.baseURL + actionComponentName}/getSelectedOrder?order_number=${order_number}&sequence=${sequence}`;
    return this.httpClient.get(url, this.httpOptions)
  }

  /** GET Orders from the server */
  getOrderList(actionComponentName: String, request: OrderGetRequest) {
    const url = `${this.baseURL + actionComponentName}/getOrderManagement`;
    return this.httpClient.post<OrderResponse>(url, request, this.httpOptions)
  }

  performStreetTurn(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/performStreetTurn`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  performBobtailTo(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/performBobtailTo`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  sparseUpdateUser(actionComponentName: String, request: { column: string; value: any; user_id: number }) {
    const url = `${this.baseURL + actionComponentName}/sparseUpdateUser`;
    return this.httpClient.put(url, request, this.httpOptions)
  }

  updateUiSettings(actionComponentName: String, request: { column: string; value: any; user_id: number }) {
    const url = `${this.baseURL + actionComponentName}/updateUiSettings`;
    return this.httpClient.put(url, request, this.httpOptions)
  }

  getUsersUiSettings(actionComponentName: String, userId) {
    const url = `${this.baseURL + actionComponentName}/getUsersUiSettings?userId=${userId}`;
    return this.httpClient.get(url, this.httpOptions)
  }

  postManualInvoice(actionComponentName: String, request: FormData) {
    const url = `${this.baseURL + actionComponentName}/postManualInvoice`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  updateManualInvoice(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/updateManualInvoice`;
    return this.httpClient.put<any>(url, request, this.httpOptions)
  }

  getManualInvoice(actionComponentName: String, invoice_number) {
    const url = `${this.baseURL + actionComponentName}/getManualInvoice?invoice_number=${invoice_number}`;
    return this.httpClient.get(url, this.httpOptions)
  }

  getManualInvoiceFile(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/getManualInvoiceFile`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  uploadManualInvoiceFile(actionComponentName: String, request: FormData) {
    const url = `${this.baseURL + actionComponentName}/uploadManualInvoiceFile`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  deleteManualInvoiceFiles(actionComponentName: String, request: any) {
    const url = `${this.baseURL + actionComponentName}/deleteManualInvoiceFiles`;
    return this.httpClient.post<any>(url, request, this.httpOptions)
  }

  // list of state and charges values get method
  getStatesAndChargesConfig() {
    const url = `${this.baseURL}utils/getStatesAndChargesConfig`;
    return this.httpClient.get<LocationData[]>(url, this.httpOptions)
  }
}
