export const error = {
    en: {
        "msg": "Sorry, an error has occured, Requested page not found!",
        "btn": "Take me to home",
        "title": "Oops!",
        "subtitle": "Not Found"
    }
}