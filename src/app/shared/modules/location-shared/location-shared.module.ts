import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    OwlNativeDateTimeModule,
    OwlDateTimeModule,
    DateTimeAdapter,
    OWL_DATE_TIME_LOCALE,
    OWL_DATE_TIME_FORMATS
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

import { MaterialModule } from 'app/shared/demo.module';
import { EditLocationComponent } from './edit-location.component';
import { MY_CUSTOM_FORMATS } from 'app/shared/models/shared.model';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule
    ],
    declarations: [
        EditLocationComponent
    ],
    entryComponents: [
        EditLocationComponent
    ],
    exports: [
        EditLocationComponent
    ],
    providers: [
        { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
    ]
})
export class LocationSharedModule { }
