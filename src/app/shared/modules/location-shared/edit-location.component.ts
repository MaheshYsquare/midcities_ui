import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AdminService } from 'app/admin/admin.service';
import { LocationData } from 'app/shared/models/location.model';
import { SharedService } from 'app/shared/service/shared.service';
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-edit-location',
  templateUrl: './edit-location.component.html',
  styleUrls: ['./edit-location.component.scss']
})
export class EditLocationComponent implements OnInit {

  public editMode: boolean

  public locationTypes: { label: string; value: number; }[];

  public selectedLocation: LocationData;

  public locationName: string;

  public locationForm: FormGroup;

  public addButtonColor = '#2196f3'

  public editButtonColor = 'green';

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  public phoneNumberExtMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  constructor(public dialogRef: MatDialogRef<EditLocationComponent>,
    @Inject(MAT_DIALOG_DATA) injectedData,
    private adminService: AdminService,
    private sharedService: SharedService) {
    if (injectedData) {
      this.editMode = injectedData.modeSelect
      this.selectedLocation = injectedData.selectedValue
      this.locationTypes = injectedData.locationTypes
      this.locationName = injectedData.locationName
    }
  }

  ngOnInit() {
    this.initForm()

    if (this.editMode) {
      (<any>Object).values(this.locationForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }

  }

  private initForm() {
    let location_name = null;
    let location_type = null;
    let address = null;
    let street = null;
    let city = null;
    let postal_code = null;
    let state = null;
    let phone_number = null;
    let ext = null;
    let fromTime = null;
    let toTime = null;
    let email = null;

    if (this.editMode) {
      let phoneArray = this.selectedLocation.phone_number ? this.selectedLocation.phone_number.split('x') : [];
      location_name = this.selectedLocation.location_name;
      location_type = this.selectedLocation.location_type_id;
      address = this.selectedLocation.address;
      street = this.selectedLocation.street;
      city = this.selectedLocation.city;
      postal_code = this.selectedLocation.postal_code;
      state = this.selectedLocation.state;
      phone_number = this.selectedLocation.phone_number ? phoneArray[0] : null;
      ext = this.selectedLocation.phone_number && phoneArray.length > 1 ? phoneArray[1] : null;

      fromTime = this.selectedLocation.receiving_hours_from !== null ?
        new Date(this.selectedLocation.receiving_hours_from) : null;

      toTime = this.selectedLocation.receiving_hours_to !== null ?
        new Date(this.selectedLocation.receiving_hours_to) : null;

      email = this.selectedLocation.email;
    }

    location_name = this.locationName ? this.locationName : (!this.editMode ? null : this.selectedLocation.location_name);

    this.locationForm = new FormGroup({
      'locationName': new FormControl(location_name, Validators.required),
      'locationType': new FormControl(location_type, Validators.required),
      'address': new FormControl(address, [Validators.required]),
      'street': new FormControl(street),
      'city': new FormControl(city),
      'postalCode': new FormControl(postal_code, [Validators.pattern(/[0-9]{5}/)]),
      'state': new FormControl(state, stateValidator),
      'phoneNumber': new FormControl(phone_number, [Validators.required,
      Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)]),
      'ext': new FormControl(ext),
      'fromTime': new FormControl(fromTime),
      'toTime': new FormControl(toTime),
      'email': new FormControl(email, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)),
    });
  }

  onSubmit() {
    if (this.locationForm.valid) {
      let phoneNumber = this.locationForm.get('ext').value && this.locationForm.get('ext').value.trim() ?
        `${this.locationForm.get('phoneNumber').value}x${this.locationForm.get('ext').value}` :
        this.locationForm.get('phoneNumber').value;

      if (this.editMode) {
        var editData = {
          'location_id': this.selectedLocation.location_id,
          'location_name': this.locationForm.get('locationName').value,
          'location_type_id': this.locationForm.get('locationType').value,
          'address': this.locationForm.get('address').value,
          'street': this.locationForm.get('street').value,
          'city': this.locationForm.get('city').value,
          'postal_code': this.locationForm.get('postalCode').value,
          'state': this.locationForm.get('state').value,
          'phone_number': phoneNumber,
          'receiving_hours_from': this.locationForm.get('fromTime').value,
          'receiving_hours_to': this.locationForm.get('toTime').value,
          email: this.locationForm.get('email').value,
        }
        this.adminService.updateLocationData('locations', editData).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.locationForm.get('locationName').reset(this.selectedLocation.location_name)
            this.sharedService.openSnackbar("Location Name Already Exists", 2000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
      } else {
        var addData = {
          'location_name': this.locationForm.get('locationName').value,
          'location_type_id': this.locationForm.get('locationType').value,
          'address': this.locationForm.get('address').value,
          'street': this.locationForm.get('street').value,
          'city': this.locationForm.get('city').value,
          'postal_code': this.locationForm.get('postalCode').value,
          'state': this.locationForm.get('state').value,
          'phone_number': phoneNumber,
          'receiving_hours_from': this.locationForm.get('fromTime').value,
          'receiving_hours_to': this.locationForm.get('toTime').value,
          email: this.locationForm.get('email').value,
        }
        this.adminService.postLocationData('locations', addData).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close(addData)
          } else if (response.code == 422) {
            this.locationForm.get('locationName').reset()
            this.sharedService.openSnackbar("Location Name Already Exists", 2000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
