import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';
import { stateValidator } from 'app/shared/utils/app-validators';


@Component({
  selector: 'app-edit-driverinfo',
  templateUrl: './edit-driverinfo.component.html',
  styleUrls: ['./edit-driverinfo.component.scss']
})

export class EditDriverinfoComponent implements OnInit {

  public componentName: string = 'drivers';

  public driverInfoForm: FormGroup;

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  public ssnMask = [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];

  public formData: FormData;

  public serviceCallFlag: boolean;

  constructor(public dialogRef: MatDialogRef<EditDriverinfoComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData: any,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.initForm();

    (<any>Object).values(this.driverInfoForm.controls).forEach(control => {
      control.markAsTouched();
    });

  }

  initForm() {
    let truck_number = this.injectedData.truck_number;
    let vehicle_id = this.injectedData.vehicle_id;
    let driver_notes = this.injectedData.driver_notes;
    let license_no = this.injectedData.license_no;
    let fuel_card_number = this.injectedData.fuel_card_number;
    let ssn = this.injectedData.ssn;
    let elog_no = this.injectedData.elog_no;
    let street = this.injectedData.street;
    let city = this.injectedData.city;
    let state = this.injectedData.state;
    let zip = this.injectedData.zip;
    let phone_number = this.injectedData.phone_number;
    let email = this.injectedData.email;
    let lic_exp_date = this.injectedData.lic_exp_date;
    let hazmat_certified = this.injectedData.hazmat_certified.toString();
    let hazmat_exp_date = this.injectedData.hazmat_exp_date;
    let med_exp_date = this.injectedData.med_exp_date;
    let driver_id = this.injectedData.driver_id;
    let specialEndorsement = new FormArray([]);
    let incident = new FormArray([]);

    this.injectedData.driver_special_endorsement.forEach(temp => {
      specialEndorsement.push(
        this.specialEndorsementForm(temp.driver_special_endorsement_id,
          temp.special_endorsement_name,
          new Date(temp.special_endorsement_exp_date)
        )
      )
    });

    this.injectedData.driver_incident.forEach(temp => {
      incident.push(
        this.incidentForm(temp.driver_incident_id,
          temp.name,
          temp.description,
          temp.action_taken,
          temp.file_name)
      )
    });

    this.driverInfoForm = new FormGroup({
      license_no: new FormControl(license_no),
      fuel_card_number: new FormControl(fuel_card_number),
      ssn: new FormControl(ssn, Validators.pattern(/^[a-zA-Z0-9]{4}$/)),
      elog_no: new FormControl(elog_no, Validators.pattern(/^[a-zA-Z0-9]{0,20}$/)),
      phone_number: new FormControl(phone_number, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      truck_number: new FormControl(truck_number),
      vehicle_id: new FormControl(vehicle_id),
      email: new FormControl(email, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/)),
      street: new FormControl(street),
      city: new FormControl(city),
      state: new FormControl(state, stateValidator),
      zip: new FormControl(zip, Validators.pattern("^[0-9]{5}$")),
      driver_notes: new FormControl(driver_notes),
      lic_exp_date: new FormControl(lic_exp_date),
      hazmat_certified: new FormControl(hazmat_certified),
      hazmat_exp_date: new FormControl(hazmat_exp_date),
      med_exp_date: new FormControl(med_exp_date),
      driver_id: new FormControl(driver_id),
      specialEndorsement: specialEndorsement,
      incident: incident
    })
  }

  specialEndorsementForm(id?, name?, date?) {
    return new FormGroup({
      driver_special_endorsement_id: new FormControl(id),
      special_endorsement_name: new FormControl(name, Validators.required),
      special_endorsement_exp_date: new FormControl(date, Validators.required)
    })
  }

  incidentForm(id?, name?, description?, action_taken?, file_name?) {
    return new FormGroup({
      driver_incident_id: new FormControl(id),
      name: new FormControl(name, Validators.required),
      description: new FormControl(description, Validators.required),
      action_taken: new FormControl(action_taken, Validators.required),
      file_name: new FormControl(file_name)
    })
  }

  getControls(type) {
    if (type == 'specialEndorsement') {
      return (<FormArray>this.driverInfoForm.get('specialEndorsement')).controls
    } else if (type == 'incident') {
      return (<FormArray>this.driverInfoForm.get('incident')).controls
    }
  }

  onSelectTruck(event: MatSelectChange) {
    let truck = this.injectedData.truckList.find(temp => temp.truck_no === event.value);

    if (truck) {
      this.driverInfoForm.get('vehicle_id').setValue(truck.vehicle_id);
    } else {
      this.driverInfoForm.get('vehicle_id').setValue(null);
    }
  }

  onClickRemoveControls(item, index) {
    switch (item) {
      case 'specialEndorsement':
        (<FormArray>this.driverInfoForm.get('specialEndorsement')).removeAt(index)
        break;
      case 'incident':
        (<FormArray>this.driverInfoForm.get('incident')).removeAt(index)
        break;
    }
  }

  onChangeSelectedFile(event, index: number) {
    let driverName = `${this.injectedData.first_name} ${this.injectedData.last_name}`
    let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1];
    let fields = {
      driverId: this.injectedData.driver_id,
      driverName,
      driverIncidentId: (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('driver_incident_id').value,
      fileRelatedTo: 'incident'
    }
    if (fileExtension == 'pdf' || fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {
      this.formData = new FormData()
      this.formData.append("files", event.target.files[0], `incident-${index}.${fileExtension}`);
      this.formData.append("fields", JSON.stringify(fields));
      this.sharedService.uploadDriverFile(this.componentName, this.formData)
        .subscribe(response => {
          this.serviceCallFlag = true;
          (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('file_name').setValue(response.Key);
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
    } else {
      this.sharedService.openSnackbar("file format must be .png,.jpg,.pdf", 2000, "warning-msg");
    }
  }

  onViewUploadedFile(index) {
    let request = {
      fileName: (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('file_name').value
    }
    this.sharedService.getDriverFile(this.componentName, request).subscribe(response => {
      let utf = new Uint8Array(response.buffer.data);
      let file = new Blob([utf], { type: response.ContentType });
      let fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    })
  }

  onDeleteFile(index) {
    let request = {
      fileName: (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('file_name').value,
      incident_id: (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('driver_incident_id').value
    }
    this.sharedService.deleteDriverFile(this.componentName, request).subscribe(response => {
      (<FormArray>this.driverInfoForm.get('incident')).controls[index].get('file_name').setValue(null);
      this.serviceCallFlag = true;
    })
  }

  onSubmit() {
    let specialEndorsement = this.setDeleteOrUpdate(this.injectedData.driver_special_endorsement,
      this.driverInfoForm.get('specialEndorsement').value,
      'driver_special_endorsement_id',
      'special_endorsement_name',
      'special_endorsement_exp_date');

    let incident = this.setDeleteOrUpdate(this.injectedData.driver_incident,
      this.driverInfoForm.get('incident').value,
      'driver_incident_id',
      'name',
      null,
      'action_taken',
      'description',
      'file_name');

    if (this.driverInfoForm.valid) {

      let request = {
        truck_number: this.driverInfoForm.get('truck_number').value,
        vehicle_id: this.driverInfoForm.get('vehicle_id').value,
        driver_notes: this.driverInfoForm.get('driver_notes').value,
        license_no: this.driverInfoForm.get('license_no').value,
        fuel_card_number: this.driverInfoForm.get('fuel_card_number').value,
        ssn: this.driverInfoForm.get('ssn').value,
        elog_no: this.driverInfoForm.get('elog_no').value,
        street: this.driverInfoForm.get('street').value,
        city: this.driverInfoForm.get('city').value,
        state: this.driverInfoForm.get('state').value,
        zip: this.driverInfoForm.get('zip').value,
        phone_number: this.driverInfoForm.get('phone_number').value,
        email: this.driverInfoForm.get('email').value,
        lic_exp_date: this.driverInfoForm.get('lic_exp_date').value,
        hazmat_certified: Number(this.driverInfoForm.get('hazmat_certified').value),
        hazmat_exp_date: this.driverInfoForm.get('hazmat_exp_date').value,
        med_exp_date: this.driverInfoForm.get('med_exp_date').value,
        driver_id: this.injectedData.driver_id,
        specialEndorsement,
        incident
      }

      this.dialogRef.close({ flag: false, result: request });

    }

  }

  onCancel(): void {
    this.dialogRef.close({ flag: this.serviceCallFlag });
  }

  setDeleteOrUpdate(previous, current, id, name, date, action?, desc?, file?) {
    let oldValue;
    let newValue;
    oldValue = previous;
    newValue = current;
    if (oldValue.length !== 0) {
      if (newValue.length === 0) {
        oldValue.forEach(temp => {
          temp.isUpdate = false;
          temp.isDelete = true;
        })
        return oldValue
      } else if (newValue.length !== 0 && oldValue.length > newValue.length) {
        let map = new Map();

        newValue.forEach(temp => {
          map.set(temp[id], temp)
        })


        oldValue.forEach(temp => {
          let compareValue = map.get(temp[id])

          if (compareValue) {
            temp[name] = compareValue[name]
            if (action) {
              temp[action] = compareValue[action];
              temp[desc] = compareValue[desc];
              temp[file] = compareValue[file];
            } else {
              temp[date] = compareValue[date]
            }
            temp.isUpdate = true;
            temp.isDelete = false;
          } else {
            temp.isUpdate = false;
            temp.isDelete = true;
          }
        })
        return oldValue
      } else if (newValue.length !== 0 && oldValue.length == newValue.length) {
        newValue.forEach(temp => {
          temp.isUpdate = true;
          temp.isDelete = false;
        })
        return newValue
      }
    } else {
      return oldValue
    }
  }
}