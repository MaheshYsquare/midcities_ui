import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig, MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DriverService } from 'app/driver/driver.service';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-deactivate-user',
  templateUrl: './deactivate-user.component.html',
  styleUrls: ['./deactivate-user.component.scss']
})
export class DeactivateUserComponent implements OnInit {

  public editMode: boolean;

  public driver_id: number;

  public user_id: number;

  public componentName: string = 'drivers'

  public driverInfoDeactivateData: FormGroup;

  constructor(public dialogRef: MatDialogRef<DeactivateUserComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private driverService: DriverService,
    private sharedService: SharedService,
    private dialog: MatDialog) {
    if (data) {
      this.driver_id = data.driver_id;
      this.user_id = data.user_id;
    }
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.driverInfoDeactivateData = new FormGroup({
      termination_date: new FormControl(null, Validators.required),
      reason: new FormControl(null, Validators.required),
      rehirable: new FormControl(null, Validators.required),
      drug_testing_result: new FormControl(null, Validators.required),
      accident_summary: new FormControl(null, Validators.required),
      driver_id: new FormControl(this.driver_id),
      user_id: new FormControl(this.user_id)
    })
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.driverInfoDeactivateData.valid) {
      let dialogRef = this.sharedService.openConfirmation({
        action: 'Deactivate',
        name: 'Driver',
        cancelLabel: 'Cancel',
        confirmLabel: 'Deactivate',
        confirmColor: 'red'
      })

      dialogRef.subscribe(result => {
        if (result) {
          this.driverService.deactivateDriver(this.componentName, this.driverInfoDeactivateData.value)
            .subscribe(response => {
              if (response.code === 200) {
                this.dialogRef.close('success');
              } else if (response.code === 422) {
                this.sharedService.openSnackbar("Driver is Assigned. Cannot deactivate", 2000, "warning-msg");
              }

            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        }
      })
    }
  }
}
