import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

import {
  MatDialog,
  MatSlideToggleChange,
  MatTableDataSource,
  MatPaginator,
  MatDialogConfig,
  MatSort,
  MatSelectChange
} from '@angular/material';

import { Subscription } from 'rxjs';

import { EditDriverinfoComponent } from './edit-driverinfo/edit-driverinfo.component';
import { UploadComponent } from './upload/upload.component';
import { DeactivateUserComponent } from './deactivate-user/deactivate-user.component';
import { AttendenceEditComponent } from './attendence-edit/attendence-edit.component';
import { DropdownData } from 'app/shared/models/dropdown.model';
import {
  DriverAttendence,
  DriverInfoData,
  DriverPayData,
  DriverPayRequest,
  DriverPayResponse,
  DriverRelatedLegData
} from 'app/shared/models/driver.model';
import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { EditLegComponent } from '../order-component/edit-leg/edit-leg.component';
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-driverinfo',
  templateUrl: './driverinfo.component.html',
  styleUrls: ['./driverinfo.component.scss']
})

export class DriverinfoComponent implements OnInit {

  public componentName: string = 'drivers';

  public uploadComponentName: string = 'uploaddriverdocumentlinks';

  public legComponentName: string = 'legs';

  public driverId: number;

  public driverRole: boolean;

  public selectedDriver: DriverInfoData;

  public isPaid: boolean = false;

  public srcLink: any;

  public pendingPayment: number = 0;

  public driverPayDatasource: MatTableDataSource<DriverPayData>;

  public driverPayHeaderColumns: string[] = ['order_sequence', 'leg_number', 'leg_type',
    'pu_name', 'pu_time', 'dl_name', 'dl_time', 'miles', 'container_number', 'chassis_number',
    'hazmat_req', 'driver_rate', 'driver_extra_charge', 'total_driver_rate', 'is_paid', 'action'];

  public driverDocumentsLink: any;

  public isDocumentUploaded: boolean;

  public userDetails: any;

  public profilePictureFormData: FormData;

  public generalInfoForm: FormGroup;

  public categories: DropdownData['attendenceCategory'];

  public attendenceForm: FormGroup;

  public attendenceDatasource: MatTableDataSource<DriverAttendence>;

  public attendenceColumns: string[] = ['attendence_category', 'name', 'date_from', 'date_to', 'action'];

  @ViewChild('attendencePaginator') attendencePaginator: MatPaginator;

  public incidentForm: FormGroup;

  public incidentFormData: FormData;

  public adminRole: boolean;

  public driverPayRole: boolean;

  public offset: number = 1;

  public limit: number = 10;

  public sortDirection: string;

  public sortColumn: string;

  public pageLength: number = 0;

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  public ssnMask = [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];

  @ViewChild('driverPaySort') driverPaySort: MatSort;

  @ViewChild('driverPayPaginator') driverPayPaginator: MatPaginator;

  public driverRelatedLegDatasource: MatTableDataSource<DriverRelatedLegData>;

  public driverRelatedLegHeaderColumns: string[] = ['order_number', 'leg_status', 'leg_number', 'leg_type', 'pu_name', 'pu_time',
    'dl_name', 'dl_time', 'container_number', 'chassis_number'];

  @ViewChild('driverRelatedLegPaginator') driverRelatedLegPaginator: MatPaginator;

  @ViewChild('driverRelatedLegSort') driverRelatedLegSort: MatSort;

  public legTypes: { label: string; value: number }[];

  public adminDriverPay: boolean;

  public accessoriesComponentName: String = "accessorial_charges";

  public accessoriesNames: String[];

  public isDriver: boolean = true;

  public filterDate: Date;

  public socketSubscription: Subscription;

  public truckList: any[] = [];

  constructor(public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private socketService: SocketService,
    public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.userDetails = this.sharedService.getUserDetails();

    this.driverId = +this.route.snapshot.queryParams['driverId'];

    this.driverRole = this.userDetails.role == 'Driver';

    this.adminRole = this.userDetails.role == 'Admin' || this.userDetails.role == 'Order Entry' || this.userDetails.role == 'Dispatch' || this.userDetails.role == 'Driver Pay';

    this.adminDriverPay = this.userDetails.role == 'Admin' || this.userDetails.role == 'Driver Pay';

    this.driverPayRole = this.userDetails.role == 'Driver Pay';

    this.isDriver = window.location.href.includes('driver-detail') ||
      window.location.href.includes('dispatch/driver-info');

    this.route.data.subscribe((data: Data) => {
      this.selectedDriver = data['driverData'].length !== 0 ? data['driverData'][0] : this.router.navigate(['/error/404']);
      if (this.selectedDriver && this.selectedDriver.profile_link) {
        this.getDriverProfileImage(this.selectedDriver.profile_link)
      }

      if (this.selectedDriver && this.adminDriverPay) {
        this.attendenceDatasource = new MatTableDataSource();

        this.attendenceDatasource.data = this.selectedDriver.driver_attendence;
        setTimeout(() => this.attendenceDatasource.paginator = this.attendencePaginator);
      }
    })

    this.driverPayDatasource = new MatTableDataSource();

    this.driverRelatedLegDatasource = new MatTableDataSource();

    this.driverPayHeaderColumns = this.userDetails.role === 'Driver' ? ['order_sequence', 'leg_number', 'leg_type',
      'pu_name', 'pu_time', 'dl_name', 'dl_time', 'miles', 'container_number', 'chassis_number',
      'hazmat_req', 'driver_rate', 'driver_extra_charge', 'total_driver_rate', 'is_paid'] : this.driverPayHeaderColumns;

    this.getDriverPayDataFromService();

    this.getDriverRelatedLegFromService();

    this.getDocumentLinkDatafromService();

    this.generalInfoInitForm();

    this.incidentInitForm();

    this.getLegTypesDataFromService();

    this.getAccessorialDropdownNamesFromService();

    this.getAllDropdownFromService();

    this.getTruckListFromService();

    this.attendenceForm = new FormGroup({
      attendence_category: new FormControl(null, Validators.required),
      name: new FormControl(null),
      date: new FormControl(null, Validators.required)
    })
  }

  getSelectedDriverInfoDataFromService(panel?): void {
    this.sharedService.getSelectedDriverInfoData(this.componentName, this.selectedDriver.driver_id)
      .subscribe(response => {
        this.selectedDriver = response.length !== 0 ? response[0] : this.selectedDriver
        if (panel == 'general') {
          this.generalInfoInitForm()
        } else if (panel == 'incident') {
          this.incidentInitForm();
        } else if (panel == 'all') {
          this.generalInfoInitForm();
          this.incidentInitForm();
        }

        if (this.selectedDriver && this.adminDriverPay) {
          this.attendenceDatasource.data = this.selectedDriver.driver_attendence;
          setTimeout(() => this.attendenceDatasource.paginator = this.attendencePaginator);
          this.attendenceForm.reset();
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
  }

  getDocumentLinkDatafromService(): void {
    this.sharedService.getDriverDocumentLink(this.uploadComponentName, this.selectedDriver.driver_id)
      .subscribe(response => {
        this.driverDocumentsLink = response.data;
        this.isDocumentUploaded = response.data.length !== 0;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  getDriverRelatedLegFromService() {
    this.sharedService.getDriverRelatedLeg(this.legComponentName, this.driverId).subscribe(response => {
      this.driverRelatedLegDatasource.data = response;
      setTimeout(() => this.driverRelatedLegDatasource.paginator = this.driverRelatedLegPaginator);
    })
  }

  getTruckListFromService() {
    this.sharedService.getTruckNumberList('equipments').subscribe(response => {
      this.truckList = response.data;
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg'));
  }

  getLegTypesDataFromService(): void {
    this.sharedService.getDropdownList("configurations", 'leg')
      .subscribe(response => {
        this.legTypes = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
      });
  }

  getAccessorialDropdownNamesFromService(): void {
    this.sharedService.getAccessoriesNameList(this.accessoriesComponentName)
      .subscribe(response => {
        this.accessoriesNames = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown('configurations', "attendenceCategory")
      .subscribe(response => {
        this.categories = response.attendenceCategory;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  getDriverProfileImage(key) {
    let request = {
      fileName: key
    }
    this.sharedService.getDriverFile('drivers', request).subscribe(response => {
      let utf = new Uint8Array(response.buffer.data);
      const STRING_CHAR = utf.reduce((data, byte) => {
        return data + String.fromCharCode(byte);
      }, '');
      let base64String = btoa(STRING_CHAR);
      this.srcLink = this.sanitizer.bypassSecurityTrustUrl(`data:image/jpg;base64,${base64String}`)
    })
  }

  onEditDriverInfo() {

    let selectedDriver: any = { ...this.selectedDriver };

    selectedDriver['truckList'] = this.truckList;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = selectedDriver;
    let dialogRef = this.dialog.open(EditDriverinfoComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result.flag != undefined) {
        if (result.flag === false) {
          this.sharedService.updateDriverMoreInfoData(this.componentName, result.result)
            .subscribe(data => {
              this.getSelectedDriverInfoDataFromService('all');
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        } else if (result.flag === true) {
          this.getSelectedDriverInfoDataFromService('incident');
        }
      }
    });
  }

  onClickUpload() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "driverId": this.selectedDriver.driver_id,
      "uploadedDocumentList": this.driverDocumentsLink
    };
    let dialogRef = this.dialog.open(UploadComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; isDeleted: boolean, cancel: boolean }) => {
      if (result) {
        if (!result.isDeleted && !result.cancel) {
          let fields = {
            driver_id: this.selectedDriver.driver_id,
            driver_name: `${this.selectedDriver.first_name} ${this.selectedDriver.last_name}`,
            userId: this.userDetails.userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadfile(this.uploadComponentName, result.fileFormData).subscribe(responseData => {
            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement;

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getDocumentLinkDatafromService();

              this.socketSubscription.unsubscribe();
            });

          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else if (result.isDeleted && result.cancel) {
          this.getDocumentLinkDatafromService();
        }
      }
    })
  };

  onDeactivateUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "driver_id": this.selectedDriver.driver_id,
      "user_id": this.selectedDriver.user_id
    }
    let dialogRef = this.dialog.open(DeactivateUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.getSelectedDriverInfoDataFromService()
        this.socketService.sendNotification({
          role: 'Admin',
          msg: `Driver ${this.selectedDriver.first_name} ${this.selectedDriver.last_name} has been deactivated`,
          hyperlink: `/driver-management/driver-detail`,
          icon: 'info',
          styleClass: 'mat-text-accent'
        });
        this.sharedService.openSnackbar("Driver is Deactivated", 2000, "success-msg");
      }
    })
  }

  onChangeProfilePicture(event) {
    let driverName = `${this.selectedDriver.first_name} ${this.selectedDriver.last_name}`
    let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1];
    let fields = {
      driverId: this.driverId,
      driverName,
      fileRelatedTo: 'driver'
    }
    if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {
      this.profilePictureFormData = new FormData()
      this.profilePictureFormData.append("files", event.target.files[0], `${driverName}.${fileExtension}`);
      this.profilePictureFormData.append("fields", JSON.stringify(fields));
      this.sharedService.uploadDriverFile(this.componentName, this.profilePictureFormData)
        .subscribe(response => {
          this.getDriverProfileImage(response.Key)
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        });
    } else {
      this.sharedService.openSnackbar("file format must be .png,.jpg", 2000, "warning-msg");
    }
  }

  generalInfoInitForm() {
    let truck_number = this.selectedDriver.truck_number;
    let vehicle_id = this.selectedDriver.vehicle_id;
    let driver_notes = this.selectedDriver.driver_notes;
    let fuel_card_number = this.selectedDriver.fuel_card_number;
    let license_no = this.selectedDriver.license_no;
    let ssn = this.selectedDriver.ssn;
    let elog_no = this.selectedDriver.elog_no;
    let street = this.selectedDriver.street;
    let city = this.selectedDriver.city;
    let state = this.selectedDriver.state;
    let zip = this.selectedDriver.zip;
    let phone_number = this.selectedDriver.phone_number;
    let email = this.selectedDriver.email;
    let lic_exp_date = this.selectedDriver.lic_exp_date;
    let hazmat_certified = this.selectedDriver.hazmat_certified.toString();
    let hazmat_exp_date = this.selectedDriver.hazmat_exp_date;
    let med_exp_date = this.selectedDriver.med_exp_date;
    let driver_id = this.driverId;
    let specialEndorsement = new FormArray([]);


    this.selectedDriver.driver_special_endorsement.forEach(temp => {
      temp.special_endorsement_exp_date = new Date(temp.special_endorsement_exp_date)
    })

    this.selectedDriver.driver_special_endorsement.length !== 0 ?
      this.selectedDriver.driver_special_endorsement.forEach(temp => {
        specialEndorsement.push(
          this.specialEndorsementForm(temp.driver_special_endorsement_id,
            temp.special_endorsement_name,
            temp.special_endorsement_exp_date)
        )
      }) : null;

    this.generalInfoForm = new FormGroup({
      license_no: new FormControl({ value: license_no, disabled: this.disableFn(license_no) }),
      fuel_card_number: new FormControl({ value: fuel_card_number, disabled: this.disableFn(fuel_card_number) }),
      ssn: new FormControl({ value: ssn, disabled: this.disableFn(ssn) }, Validators.pattern(/^[a-zA-Z0-9]{4}$/)),
      elog_no: new FormControl({ value: elog_no, disabled: this.disableFn(elog_no) }, Validators.pattern(/^[a-zA-Z0-9]{0,20}$/)),
      phone_number: new FormControl({ value: phone_number, disabled: this.disableFn(phone_number) }, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      truck_number: new FormControl({ value: truck_number, disabled: this.disableFn(truck_number) }),
      vehicle_id: new FormControl(vehicle_id),
      email: new FormControl({ value: email, disabled: this.disableFn(email) }, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/)),
      street: new FormControl({ value: street, disabled: this.disableFn(street) }),
      city: new FormControl({ value: city, disabled: this.disableFn(city) }),
      state: new FormControl({ value: state, disabled: this.disableFn(state) }, stateValidator),
      zip: new FormControl({ value: zip, disabled: this.disableFn(zip) }, Validators.pattern("^[0-9]{5}$")),
      driver_notes: new FormControl({ value: driver_notes, disabled: this.disableFn(driver_notes) }),
      lic_exp_date: new FormControl({ value: lic_exp_date, disabled: this.disableFn(lic_exp_date) }),
      hazmat_certified: new FormControl({ value: hazmat_certified, disabled: this.disableFn(hazmat_certified) }),
      hazmat_exp_date: new FormControl({ value: hazmat_exp_date, disabled: this.disableFn(hazmat_exp_date) }),
      med_exp_date: new FormControl({ value: med_exp_date, disabled: this.disableFn(med_exp_date) }),
      driver_id: new FormControl(driver_id),
      specialEndorsement
    })
  }

  incidentInitForm() {
    let incident = new FormArray([]);

    this.selectedDriver.driver_incident.length !== 0 ?
      this.selectedDriver.driver_incident.forEach(temp => {
        incident.push(this.incident(temp.driver_incident_id,
          temp.name,
          temp.description,
          temp.action_taken,
          temp.file_name))
      }) : null;

    this.incidentForm = new FormGroup({
      incident
    })
  }

  disableFn(value) {
    return value && value.toString().trim() ? true : false;
  }

  specialEndorsementForm(id?, name?, date?) {
    return new FormGroup({
      driver_special_endorsement_id: new FormControl(id),
      special_endorsement_name: new FormControl({ value: name, disabled: this.disableFn(name) }, Validators.required),
      special_endorsement_exp_date: new FormControl({ value: date, disabled: this.disableFn(date) }, Validators.required)
    })
  }

  incident(id?, name?, description?, action_taken?, file_name?) {
    return new FormGroup({
      driver_incident_id: new FormControl(id),
      name: new FormControl({ value: name, disabled: this.disableFn(name) }, Validators.required),
      description: new FormControl({ value: description, disabled: this.disableFn(description) }, Validators.required),
      action_taken: new FormControl({ value: action_taken, disabled: this.disableFn(action_taken) }, Validators.required),
      file_name: new FormControl(file_name)
    })
  }

  getControls(type) {
    if (type == 'specialEndorsement') {
      return (<FormArray>this.generalInfoForm.get('specialEndorsement')).controls
    } else if (type == 'incident') {
      return (<FormArray>this.incidentForm.get('incident')).controls
    }
  }

  onClickAddOrRemoveItems(selectedForm, index) {
    switch (selectedForm) {
      case 'specialEndorsement':
        let specialEndorsement = this.generalInfoForm.get('specialEndorsement') as FormArray;
        if (index !== 'add') {
          specialEndorsement.removeAt(index)
        } else {
          specialEndorsement.push(this.specialEndorsementForm(null, null, null));
        }
        break;
      case 'incident':
        let incident = this.incidentForm.get('incident') as FormArray;
        if (index !== 'add') {
          incident.removeAt(index)
        } else {
          incident.push(this.incident(null, null, null, null, null));
        }
        break;
    }
  }

  onSelectTruck(event: MatSelectChange) {
    let truck = this.truckList.find(temp => temp.truck_no === event.value);

    if (truck) {
      this.generalInfoForm.get('vehicle_id').setValue(truck.vehicle_id);
    } else {
      this.generalInfoForm.get('vehicle_id').setValue(null);
    }
  }

  onIncidentUpload(event, index: number) {
    let driverName = `${this.selectedDriver.first_name} ${this.selectedDriver.last_name}`
    let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1];
    let fields = {
      driverId: this.driverId,
      driverName,
      driverIncidentId: (<FormArray>this.incidentForm.get('incident')).controls[index].get('driver_incident_id').value,
      fileRelatedTo: 'incident'
    }
    if (fileExtension == 'pdf' || fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {
      this.incidentFormData = new FormData();
      this.incidentFormData.append("files", event.target.files[0], `incident-${index}.${fileExtension}`);
      this.incidentFormData.append("fields", JSON.stringify(fields));
      this.sharedService.uploadDriverFile("drivers", this.incidentFormData)
        .subscribe(response => {
          if ('id' in response) {
            (<FormArray>this.incidentForm.get('incident')).controls[index].get('driver_incident_id').setValue(response.id)
          }
          (<FormArray>this.incidentForm.get('incident')).controls[index].get('file_name').setValue(response.Key)
          this.getSelectedDriverInfoDataFromService()
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
    } else {
      this.sharedService.openSnackbar("file format must be .png,.jpg,.pdf", 2000, "warning-msg");
    }
  }

  onViewUploadedFile(index) {
    let request = {
      fileName: (<FormArray>this.incidentForm.get('incident')).controls[index].get('file_name').value
    }
    this.sharedService.getDriverFile('drivers', request).subscribe(response => {
      let utf = new Uint8Array(response.buffer.data);
      let file = new Blob([utf], { type: response.ContentType });
      let fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    })
  }

  onSubmitGeneralInfoForm() {
    if (this.generalInfoForm.valid) {
      let specialEndorsement = this.setUpdateOrPost((<FormArray>this.generalInfoForm.get('specialEndorsement')).getRawValue(), 'driver_special_endorsement_id')
      let request = {
        event: 'general',
        truck_number: this.generalInfoForm.get('truck_number').value,
        vehicle_id: this.generalInfoForm.get('vehicle_id').value,
        driver_notes: this.generalInfoForm.get('driver_notes').value,
        license_no: this.generalInfoForm.get('license_no').value,
        fuel_card_number: this.generalInfoForm.get('fuel_card_number').value,
        ssn: this.generalInfoForm.get('ssn').value,
        elog_no: this.generalInfoForm.get('elog_no').value,
        street: this.generalInfoForm.get('street').value,
        city: this.generalInfoForm.get('city').value,
        state: this.generalInfoForm.get('state').value,
        zip: this.generalInfoForm.get('zip').value,
        phone_number: this.generalInfoForm.get('phone_number').value,
        email: this.generalInfoForm.get('email').value,
        lic_exp_date: this.generalInfoForm.get('lic_exp_date').value,
        hazmat_certified: Number(this.generalInfoForm.get('hazmat_certified').value),
        hazmat_exp_date: this.generalInfoForm.get('hazmat_exp_date').value,
        med_exp_date: this.generalInfoForm.get('med_exp_date').value,
        driver_id: this.driverId,
        specialEndorsement
      }
      this.postServiceCall(request, 'general');
    }
  }

  onClickEditAttendence(selectedRow: DriverAttendence) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "name": selectedRow.name,
      "attendence_category": selectedRow.driver_attendence_category_id,
      "categories": this.categories,
      "driver_attendence_id": selectedRow.driver_attendence_id,
      "date": [selectedRow.date_from ? new Date(selectedRow.date_from) : null,
      selectedRow.date_to ? new Date(selectedRow.date_to) : null]
    };
    let dialogRef = this.dialog.open(AttendenceEditComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.updateAttendenceData(this.componentName, result).subscribe(response => {
          this.getSelectedDriverInfoDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg');
        })
      }
    })


  }

  onClickDeleteAttendence(selectedRow: DriverAttendence) {
    let dialog = this.sharedService.openConfirmation({
      action: 'Delete',
      cancelLabel: 'Cancel',
      confirmColor: 'red',
      confirmLabel: 'Delete',
      name: 'Attendence'
    })
    dialog.subscribe(result => {
      if (result) {
        let request = {
          attendence_id: selectedRow.driver_attendence_id
        };
        this.sharedService.deleteAttendenceData(this.componentName, request).subscribe(response => {
          this.getSelectedDriverInfoDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg');
        })
      }
    })

  }

  onResetAttendenceForm() {
    this.attendenceForm.reset();
  }

  onSubmitAttendenceForm() {
    let request = {
      name: this.attendenceForm.get('name').value,
      date_from: this.attendenceForm.get('date').value && this.attendenceForm.get('date').value.length ?
        formatDate(this.attendenceForm.get('date').value[0], 'yyyy-MM-dd', 'en') : null,
      date_to: this.attendenceForm.get('date').value && this.attendenceForm.get('date').value.length ?
        formatDate(this.attendenceForm.get('date').value[1], 'yyyy-MM-dd', 'en') : null,
      attendence_category: this.attendenceForm.get('attendence_category').value,
      driver_id: this.selectedDriver.driver_id
    };
    this.sharedService.postAttendenceData(this.componentName, request).subscribe(response => {
      this.getSelectedDriverInfoDataFromService();
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg');
    })
  }

  onSubmitIncidentForm() {
    let incident = this.setUpdateOrPost((<FormArray>this.incidentForm.get('incident')).getRawValue(), 'driver_incident_id');
    let request = {
      event: 'incident',
      driver_id: this.driverId,
      incident
    }
    this.postServiceCall(request, 'incident');
  }

  setUpdateOrPost(data, id) {
    data.length !== 0 ? data.forEach(temp => {
      if (temp[id] !== null) {
        temp.isUpdate = true;
        temp.isDelete = false;
        temp.isInsert = false;
      } else {
        temp.isUpdate = false;
        temp.isDelete = false;
        temp.isInsert = true;
      }
    }) : [];

    return data
  }

  postServiceCall(request, event) {
    this.sharedService.postMoreInfo(this.componentName, request)
      .subscribe(data => {
        this.getSelectedDriverInfoDataFromService(event);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
  }

  onClickOrderNumber(selectedRow: DriverRelatedLegData) {
    this.router.navigate(['../', selectedRow.order_number, selectedRow.container_sequence, 'leg'], { relativeTo: this.route })
  }

  getDriverPayDataFromService(): void {
    let request = this.generateRequest()
    this.sharedService.getDriverPayData(this.componentName, request)
      .subscribe((response: DriverPayResponse) => {
        if (response.code == 200) {
          this.driverPayDatasource.data = response.data.driverPayData;
          this.pageLength = response.data.driverPayPaginationLength;
          if (!this.isPaid) {
            this.pendingPayment = +response.data.driverPendingAmount.toFixed(2);
          }
        } else {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        }
      }, error => { });
  }

  onChangeSlideToggle(event: MatSlideToggleChange) {
    this.isPaid = event.checked;
    this.initialPage();
    this.getDriverPayDataFromService();
  }

  /*   Fliter Dispatch Table through Date */
  onChangeDate() {
    this.initialPage();
    this.getDriverPayDataFromService();
  }

  /* clearing the date filter */
  onClickClear() {
    this.filterDate = null;
    this.initialPage();
    this.getDriverPayDataFromService();
  }

  onClickCreateDriverPay() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": false,
      "legTypes": this.legTypes,
      "creationLegTime": new Date(),
      "driverName": `${this.selectedDriver.first_name} ${this.selectedDriver.last_name}`,
      "isLeg": false,
      "accessoriesName": this.accessoriesNames,
      "orderType": null
    };
    let dialogRef = this.dialog.open(EditLegComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.postDriverPayData(this.componentName, result).subscribe(response => {
          this.initialPage();
          this.getDriverPayDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg")
        })
      }
    });
  }

  onChangeSortDirection(event) {
    this.sortDirection = event.direction == '' ? null : event.direction;
    this.sortColumn = event.active;
    this.getDriverPayDataFromService();
  }

  onClickEditDriverPay(selectedRow: DriverPayData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "selectedValue": selectedRow,
      "modeSelect": true,
      "legTypes": this.legTypes,
      "creationLegTime": new Date(),
      "isLeg": false,
      "accessoriesName": this.accessoriesNames,
      "orderType": selectedRow.order_type
    };
    let dialogRef = this.dialog.open(EditLegComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.updateDriverPayData(this.componentName, result).subscribe(response => {
          this.getDriverPayDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg")
        })
      }
    });
  }

  onClickDeleteDriverPay(selectedRow: DriverPayData) {
    let dialog = this.sharedService.openConfirmation({
      action: 'Delete',
      cancelLabel: 'Cancel',
      confirmColor: 'red',
      confirmLabel: 'Delete',
      name: 'Driver Pay Leg'
    })
    dialog.subscribe(result => {
      if (result) {
        let request = {
          containerLevelId: selectedRow.order_container_chassis_id,
          legNumber: selectedRow.leg_number,
          driverPayId: selectedRow.driver_pay_id
        }
        this.sharedService.deleteDriverPayData(this.componentName, request).subscribe(response => {
          this.getDriverPayDataFromService();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg")
        })
      }
    })
  }

  onChangePage(event) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getDriverPayDataFromService();
  }

  generateRequest() {
    let request: DriverPayRequest = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: false,
      searchFilterValue: null,
      role: null,
      user_id: null,
      isListById: true,
      driver_id: this.driverId,
      is_paid: this.isPaid ? 1 : 0,
      columnFilter: false,
      filterData: undefined,
      column: this.sortColumn,
      direction: this.sortDirection,
      roleDirection: this.userDetails.role == 'Driver Pay' ?
        'asc' : 'desc',
      filterDate: this.filterDate ? this.filterDate.toDateString() : null,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    return request
  }

  initialPage() {
    this.driverPayPaginator.pageIndex = 0;
    this.driverPayPaginator.pageSize = this.driverPayPaginator.pageSize ? this.driverPayPaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.driverPayPaginator.pageSize ? this.driverPayPaginator.pageSize : 10;
  }
}
