import { Component, OnInit, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  public documents: { label: string, value: string, isUploaded: boolean }[] = [
    { label: "Insurance Copy", value: 'insurancecopy', isUploaded: false },
    { label: "Driver's License", value: 'driverslicense', isUploaded: false },
    { label: 'Miscellaneous', value: 'miscellaneous', isUploaded: false }
  ];

  public fileUploadFormData: FormData = new FormData();

  public isFilesAdded: boolean = false;

  public uploadComponentName: string = "uploaddriverdocumentlinks";

  public isDeleted: boolean = false;

  constructor(public dialogRef: MatDialogRef<UploadComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.injectedData && this.injectedData.uploadedDocumentList ?
      this.injectedData.uploadedDocumentList.forEach(element => {
        this.documents.forEach(temp => {
          if (element.includes(temp.value)) {
            temp.isUploaded = true;
          }
        })
      }) : null;
  }

  onChangeFileInput(event: any, documentName: string, fileInput: any): void {
    if (event.target.files && event.target.files.length) {

      if (event.target.files[0].size <= 5000000) {

        let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1].toLowerCase();

        if (fileExtension === 'pdf' || fileExtension === 'png' || fileExtension === 'jpg' || fileExtension === 'jpeg') {

          this.removeExistingOrUnwantedFile(documentName);

          this.fileUploadFormData.append("files", event.target.files[0], `${documentName}.${fileExtension}`);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        } else {

          fileInput.value = null;

          this.removeExistingOrUnwantedFile(documentName);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

          this.sharedService.openSnackbar("File Format Must be pdf,jpg,png", 5000, "warning-msg");
        }

      } else {
        fileInput.value = null;

        this.removeExistingOrUnwantedFile(documentName);

        this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        this.sharedService.openSnackbar("File Size Must be less than 5MB", 5000, "warning-msg");
      }
    }
  }

  removeExistingOrUnwantedFile(documentName: string) {
    let fileIndex = this.fileUploadFormData.getAll("files").findIndex((temp: any) => temp.name.includes(documentName));

    if (fileIndex !== -1) {
      let filesList = this.fileUploadFormData.getAll("files");

      filesList.splice(fileIndex, 1);

      this.fileUploadFormData.delete("files");

      filesList.forEach(temp => {
        this.fileUploadFormData.append("files", temp);
      })
    }
  }

  onClickViewFile(documentName: string) {

    this.injectedData.uploadedDocumentList.forEach(element => {
      if (element.includes(documentName)) {
        this.sharedService.getOrderFile(this.uploadComponentName, { fileName: element }).subscribe(response => {
          let utf = new Uint8Array(response.buffer.data);
          let file = new Blob([utf], { type: response.ContentType });
          let fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    });

  }

  onClickDeleteFile(documentName: string) {

    this.injectedData.uploadedDocumentList.forEach(element => {
      if (element.includes(documentName)) {
        let request = {
          'fileName': element,
          'driver_id': this.injectedData.driverId
        }
        this.sharedService.deleteOrderFile(this.uploadComponentName, request)
          .subscribe(responseData => {
            let documentsIndex = this.documents.findIndex(temp => temp.value === documentName);

            this.documents[documentsIndex].isUploaded = false;

            this.isDeleted = true;
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });

  }

  onClickCancel() {
    this.dialogRef.close({
      isDeleted: this.isDeleted,
      cancel: true,
      fileFormData: this.fileUploadFormData
    });
  }

  onClickSave() {
    if (this.isFilesAdded) {
      this.dialogRef.close({
        fileFormData: this.fileUploadFormData,
        isDeleted: false,
        cancel: false
      });
    }
  }

}
