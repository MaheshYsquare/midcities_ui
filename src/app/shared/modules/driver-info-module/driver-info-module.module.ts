import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OwlDateTimeModule, OwlNativeDateTimeModule, DateTimeAdapter, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { TooltipModule } from 'ng2-tooltip-directive';

import { DriverinfoComponent } from './driverinfo.component';
import { EditDriverinfoComponent } from './edit-driverinfo/edit-driverinfo.component';
import { DeactivateUserComponent } from './deactivate-user/deactivate-user.component';
import { UploadComponent } from './upload/upload.component';
import { LegComponent } from './leg/leg.component';
import { StreetturnComponent } from './leg/streetturn/streetturn.component';
import { TohubComponent } from './leg/tohub/tohub.component';
import { BobtailToComponent } from './leg/bobtail-to/bobtail-to.component';
import { AttendenceEditComponent } from './attendence-edit/attendence-edit.component';
import { OrderSharedModule } from '../order-component/order-component.module';
import { MapModule } from 'app/shared/utils/map/map.module';
import { MY_CUSTOM_FORMATS } from 'app/shared/models/shared.model';
import { MaterialModule } from 'app/shared/demo.module';
import { BrokeredComponent } from './leg/brokered/brokered.component';

@NgModule({
  imports: [
    OrderSharedModule,
    MapModule,
    TooltipModule
  ],
  declarations: [
    DriverinfoComponent,
    UploadComponent,
    EditDriverinfoComponent,
    DeactivateUserComponent,
    LegComponent,
    StreetturnComponent,
    TohubComponent,
    BobtailToComponent,
    AttendenceEditComponent,
    BrokeredComponent
  ],
  entryComponents: [
    UploadComponent,
    EditDriverinfoComponent,
    DeactivateUserComponent,
    StreetturnComponent,
    TohubComponent,
    BobtailToComponent,
    AttendenceEditComponent,
    BrokeredComponent
  ],
  exports: [
    CommonModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DriverinfoComponent,
    UploadComponent,
    EditDriverinfoComponent,
    DeactivateUserComponent,
    LegComponent,
    StreetturnComponent,
    TohubComponent,
    BobtailToComponent,
    AttendenceEditComponent
  ],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ]
})
export class DriverInfoModule { }


