import { formatDate } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-attendence-edit',
  templateUrl: './attendence-edit.component.html',
  styleUrls: ['./attendence-edit.component.scss']
})
export class AttendenceEditComponent implements OnInit {

  attendenceForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AttendenceEditComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    this.attendenceForm = new FormGroup({
      attendence_category: new FormControl(this.injectedData.attendence_category, Validators.required),
      name: new FormControl(this.injectedData.name),
      date: new FormControl(this.injectedData.date, Validators.required)
    })
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {

    if (this.attendenceForm.valid) {
      let request = {
        name: this.attendenceForm.get('name').value,
        date_from: this.attendenceForm.get('date').value && this.attendenceForm.get('date').value.length ?
          formatDate(this.attendenceForm.get('date').value[0], 'yyyy-MM-dd', 'en') : null,
        date_to: this.attendenceForm.get('date').value && this.attendenceForm.get('date').value.length ?
          formatDate(this.attendenceForm.get('date').value[1], 'yyyy-MM-dd', 'en') : null,
        attendence_category: this.attendenceForm.get('attendence_category').value,
        attendence_id: this.injectedData.driver_attendence_id
      }

      this.dialogRef.close(request);
    }
  }

}
