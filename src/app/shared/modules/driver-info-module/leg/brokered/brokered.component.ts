import { Component, OnInit, ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { CarrierData } from 'app/admin/carrier-management/carrier.model';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-brokered',
  templateUrl: './brokered.component.html',
  styleUrls: ['./brokered.component.scss']
})
export class BrokeredComponent implements OnInit {

  public componentName: string = 'carriers';

  public carrierDatasource: MatTableDataSource<CarrierData>;

  @ViewChild(MatSort) carrierSort: MatSort;

  public carrierColumns: string[] = ['select', 'carrier_name', 'email', 'phone_no', 'carrier_rate'];

  @ViewChild(MatPaginator) carrierPaginator: MatPaginator;

  public selectedCarrier: CarrierData;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  constructor(public dialog: MatDialog,
    public dialogRef: MatDialogRef<BrokeredComponent>,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.carrierDatasource = new MatTableDataSource();

    this.getCarriersListFromService();
  }

  getCarriersListFromService(): void {
    this.sharedService.getCarriersList(this.componentName).subscribe(response => {
      this.carrierDatasource.data = response;
      this.carrierDatasource.sort = this.carrierSort;
      this.carrierDatasource.paginator = this.carrierPaginator;
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg'));
  }

  onChangeRadioButton(selectedRow: CarrierData) {
    this.selectedCarrier = selectedRow;
  }

  onCancel() {
    this.dialogRef.close()
  }

  onSubmit() {
    if (this.selectedCarrier && this.selectedCarrier.carrier_rate &&
      /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/.test(this.selectedCarrier.carrier_rate)) {

      this.dialogRef.close(this.selectedCarrier);

    } else {
      this.sharedService.openSnackbar("Please Fill the Rate", 3000, 'warning-msg');
    }
  }
}
