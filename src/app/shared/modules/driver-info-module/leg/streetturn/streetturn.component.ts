import { Component, OnInit, ViewChild, Inject } from '@angular/core';

import {
  MatTableDataSource,
  MatSort,
  MatDialogRef,
  MatPaginator,
  MAT_DIALOG_DATA,
} from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';
import { OrderData } from 'app/shared/models/order.model';

@Component({
  selector: 'app-streetturn',
  templateUrl: './streetturn.component.html',
  styleUrls: ['./streetturn.component.scss']
})
export class StreetturnComponent implements OnInit {

  public componentName: String = "orders"; // api component name for api fetch

  public searchFilterEvent: boolean = false; // filter flag whether search filter is enabled

  public searchFilterValue: string = ""; // store search filter value

  /* table source,sort,pagination and cache data fetch variable  */
  public orderListDatasource: MatTableDataSource<OrderData>;

  public orderListColumns: string[] = ['select', 'order_sequence', 'order_status',
    'order_type', 'container_size', 'pu_name', 'dl_name'];

  public currentPageIndex: number = 0;

  public offset: number = 1;

  public limit: number = 3;

  public sortDirection: string;

  public sortColumn: string;

  public pageLength: number = 0;

  @ViewChild('orderListPaginator') orderListPaginator: MatPaginator;

  @ViewChild('orderListSort') orderListSort: MatSort;

  public selectedOrder: OrderData;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  constructor(public dialogRef: MatDialogRef<StreetturnComponent>,
    private sharedService: SharedService,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    this.orderListDatasource = new MatTableDataSource();

    this.getOrderDataFromService();
  }

  getOrderDataFromService(): void {
    let request = this.generateRequest();
    this.sharedService.getOrderList(this.componentName, request)
      .subscribe(response => {
        this.orderListDatasource.data = response.data.orderData;
        this.pageLength = response.data.orderPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
  }

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilterEvent = true;
      this.initialPage();
      this.getOrderDataFromService();
    } else {
      this.searchFilterEvent = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getOrderDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilterEvent = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getOrderDataFromService();
  }

  onChangeSortDirection(event) {
    this.sortDirection = event.direction == '' ? null : event.direction;
    this.sortColumn = event.active;
    this.getOrderDataFromService();
  }

  onChangePage(event) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getOrderDataFromService();
  }

  onChangeRadioButton(selectedRow: OrderData) {
    this.selectedOrder = selectedRow;
  }

  onSubmit() {
    this.dialogRef.close(this.selectedOrder);
  }

  generateRequest() {
    let request = {
      table: 'streetturn',
      offset: this.offset,
      limit: this.limit,
      searchFilter: this.searchFilterEvent,
      searchValue: this.searchFilterValue,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: false,
      filterData: null,
      container_name: this.injectedData.container_name,
      container_size: this.injectedData.container_size,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    return request
  }

  initialPage() {
    this.orderListPaginator.pageIndex = 0;
    this.orderListPaginator.pageSize = 3;
    this.offset = 1;
    this.limit = 3;
  }

}