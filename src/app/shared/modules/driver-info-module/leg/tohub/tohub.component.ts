import { Component, OnInit, ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';

import { GetRequest } from 'app/shared/models/shared.model';
import { SharedService } from 'app/shared/service/shared.service';
import { LocationData } from 'app/shared/models/location.model';

@Component({
  selector: 'app-tohub',
  templateUrl: './tohub.component.html',
  styleUrls: ['./tohub.component.scss']
})
export class TohubComponent implements OnInit {

  public hubAddressDatasource: MatTableDataSource<any>;

  public hubAddressHeaderColumns: string[] = ['select', 'location_name'];

  @ViewChild('paginator') paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  public locationLength: number = 0;

  public sortColumn: string;

  public sortDirection: string;

  public offset: number = 1;

  public limit: number = 3;

  public selectedAddress: LocationData;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  constructor(public dialog: MatDialog,
    public dialogRef: MatDialogRef<TohubComponent>,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.hubAddressDatasource = new MatTableDataSource();

    this.getLocationsDataFromService();
  }

  getLocationsDataFromService(): void {
    let request = this.generateRequest()
    this.sharedService.getLocationManagementList('locations', request)
      .subscribe(response => {
        this.hubAddressDatasource.data = response.data.locationData;
        this.locationLength = response.data.locationPaginationLength;
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
      });
  }

  onChangeRadioButton(selectedRow: LocationData) {
    this.selectedAddress = selectedRow;
  }

  onCancel() {
    this.dialogRef.close()
  }

  onSubmit() {
    this.dialogRef.close(this.selectedAddress)
  }

  onChangePage(event: PageEvent) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getLocationsDataFromService();

  }

  generateRequest() {
    let request: GetRequest = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: false,
      searchFilterValue: null,
      column: this.sortColumn,
      direction: this.sortDirection,
      columnFilter: false,
      columnFilterValue: null,
      isHubLocation: true
    }
    return request
  }
}
