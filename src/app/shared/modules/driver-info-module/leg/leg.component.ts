import { Component, OnInit, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router, Data, ActivatedRoute } from '@angular/router';

import { MatTableDataSource, MatDialogConfig, MatDialog, MatSort, MatPaginator, MatCheckboxChange } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { Subscription } from 'rxjs';

import * as moment from 'moment';

import { StreetturnComponent } from './streetturn/streetturn.component';
import { TohubComponent } from './tohub/tohub.component';
import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { OrderData } from 'app/shared/models/order.model';
import { LegData } from 'app/shared/models/leg.model';
import { AccessoriesData } from 'app/shared/models/accessories.model';
import { loginData } from 'app/session/session.model';
import { LocationData } from 'app/shared/models/location.model';
import { DriverDetailsData } from 'app/driver/DriverData';
import { BobtailToComponent } from './bobtail-to/bobtail-to.component';
import { MapComponent } from 'app/shared/utils/map/map.component';
import { NotesComponent } from '../../order-component/notes/notes.component';
import { EditLegComponent } from '../../order-component/edit-leg/edit-leg.component';
import { UploadDocsComponent } from '../../order-component/upload-docs/upload-docs.component';
import { AssignDriverComponent } from '../../order-component/assigndriver/assigndriver.component';
import { PrintLoadSheetComponent } from '../../order-component/print-load-sheet/print-load-sheet.component';
import { EditAccessoriesComponent } from '../../accessories-shared/edit-accessories.component';
import { BrokeredComponent } from './brokered/brokered.component';
import { CarrierData } from 'app/admin/carrier-management/carrier.model';

@Component({
  selector: 'app-leg',
  templateUrl: './leg.component.html',
  styleUrls: ['./leg.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegComponent implements OnInit, OnDestroy {

  public dropdownComponentName: String = "configurations";

  public legTypes: { label: string; value: number }[];

  public uploadComponentName: string = "uploaddocumentlinks";

  public legComponentName: String = "legs";

  public orderComponentName: string = "orders";

  public generalAccessoriesComponent: String = "accessorial_charges";

  public accessoriesNames: String[];

  public accessoriesCharges: any;

  public transactionAccessoriesComponent = "order_accessorial_charges";

  public selectedOrder: OrderData;

  public isDispatch: boolean = true;

  public panelOpenState: boolean;

  public uploadedDocumentTemplate: string;

  public isDocumentUploaded: boolean = false;

  public uploadedDocumentList: string[];

  public isStreetTurn: boolean = false;

  public isBobtailTo: boolean = false; // flag which enable bobtail to in more option when condition meets

  public isHubEndOrder: boolean = false;

  public isHub: boolean = false;

  public isBrokered: boolean = false;

  public brokeredLeg: LegData[] = [];

  public locationSubscription: Subscription;

  @ViewChild('bingMap') bingMap: MapComponent;

  public legDatasource: MatTableDataSource<LegData>;

  @ViewChild('legSort') legSort: MatSort;

  public legTableSelection = new SelectionModel<LegData>(true, []);

  public statuses: { label: string; value: number }[] = [
    { label: "Open", value: 1 },
    { label: "Assigned", value: 2 },
    { label: "Picked Up", value: 3 },
    { label: "Delivered", value: 4 }
  ];

  public editEnableColor: string = '#ff9800';

  public editDisableColor: string = '#e6bc86';

  public deleteEnableColor: string = '#f44336';

  public deleteDisableColor: string = '#ffafa9';

  public legColumns: string[] = ['select', 'leg_number', 'leg_type', 'driver_name', 'deadline', 'leg_status',
    'pu_name', 'pickup_time', 'dl_name', 'delivery_time', 'est_miles', 'dmg', 'container_number', 'chassis_number', 'action'];

  @ViewChild('legsPaginator') legsPaginator: MatPaginator;

  public isDriverNotesEditable: boolean = false; // flag used for the driver notes in edit or not

  public dispatchDriverNotesEdit: string; // to store the customer notes when click edit inorder to reassign notes if user cancel edited notes

  public isDispatchDriverNotesAdded: boolean = false; // flag used to show the entry of dispatch driver notes

  public driverNotesUserName: string;

  public driverNotesCreatedOn: Date;

  public dispatchDriverNotes: string; // initial storage of dispatch driver notes

  public loadedLegDatasource: MatTableDataSource<any>;

  public loadedLegColumns: string[] = ['leg_number', 'leg_type', 'pu_name', 'dl_name',
    'est_miles', 'rate', 'action'];

  public chargesInlineInput: { miles: any, rate: any } = { miles: 0, rate: 0 };

  public selectedLegId: number = 0;

  public accessoriesDatasource: MatTableDataSource<AccessoriesData>;

  @ViewChild('accessoriesSort') accessoriesSort: MatSort;

  public loggedInUser: loginData;

  public accessoriesColumns: string[] = ['accessories_name', 'accessories_interval', 'accessories_value',
    'description', 'rate', 'action'];

  @ViewChild('accessoriesPaginator') accessoriesPaginator: MatPaginator;

  public legCount: number = 0;

  public legInjectData: any;

  public legDuplicate: LegData[];

  public streetTurnData: LegData;

  public bobtailToData: LegData;

  public selectedHubLeg: LegData;

  public dialogConfig = new MatDialogConfig();

  public isOrderClosed: boolean;

  @ViewChild('notes') notes: NotesComponent;

  public streetTurnGetCall: boolean = false;

  public bobtailedLeg: number[] = [];

  public socketSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private router: Router,
    private socketService: SocketService,
    private route: ActivatedRoute,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.isDispatch = window.location.href.includes('driver') ? false : true;

    this.legDatasource = new MatTableDataSource();

    this.loadedLegDatasource = new MatTableDataSource();

    this.accessoriesDatasource = new MatTableDataSource();

    this.loggedInUser = this.sharedService.getUserDetails();

    this.loggedInUser.role === 'Dispatch' ? this.loadedLegColumns.pop() : null;

    this.route.data.subscribe((data: Data) => {
      this.selectedOrder = data['orderLeg'].length !== 0 ? data['orderLeg'][0] : this.router.navigate(['/error/404']);
      this.getLegAccessoriesDataFromService();
      this.getUploadedDocumentListFromService();
      this.getAccessorialDropdownNamesFromService();
      this.getAccessorialPrefillChargesFromService();
      if (this.streetTurnGetCall) {
        this.notes.orderContainerChassisId = this.selectedOrder.order_container_chassis_id;
        this.notes.getNotesFromService()
      }
      if (this.selectedOrder) {
        this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
          moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
        this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
          moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

        this.loadedLegDatasource.data = [{
          leg_number: 1,
          leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
          pu_name: this.selectedOrder.pu_name,
          pu_loc: this.selectedOrder.pu_loc,
          dl_name: this.selectedOrder.dl_name,
          dl_loc: this.selectedOrder.dl_loc,
          est_miles: this.selectedOrder.miles,
          rate: this.selectedOrder.rate,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          pu_street: this.selectedOrder.pu_street,
          pu_city: this.selectedOrder.pu_city,
          pu_state: this.selectedOrder.pu_state,
          pu_postal_code: this.selectedOrder.pu_postal_code,
          dl_street: this.selectedOrder.dl_street,
          dl_city: this.selectedOrder.dl_city,
          dl_state: this.selectedOrder.dl_state,
          dl_postal_code: this.selectedOrder.dl_postal_code
        }];

        this.bingMap.loadMapScenario();

        this.selectedOrder.vehicle_id ?
          this.getVehicleLocationFromService(this.selectedOrder.vehicle_id) : null;
      }
    });

    this.getLegTypesDataFromService();

    this.dialogConfig.disableClose = true;

    this.dialogConfig.autoFocus = true;
  }

  ngOnDestroy() {
    this.locationSubscription ? this.locationSubscription.unsubscribe() : null;
  }

  getLegAccessoriesDataFromService(isBobtailTo?: boolean): void {
    this.sharedService.getLegAccessories(this.legComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.legDatasource.data = response.data.legs;
        this.legDatasource.paginator = this.legsPaginator;
        this.accessoriesDatasource.data = response.data.charges;
        this.accessoriesDatasource.paginator = this.accessoriesPaginator;
        this.isStreetTurn = false;
        this.streetTurnData = null;
        this.isHub = false;
        this.isHubEndOrder = false;
        this.legCount = this.legDatasource.data.length;
        this.legDuplicate = [... this.legDatasource.data];
        this.legDuplicate.push(this.generateLegData());
        this.legInjectData = {
          dl_loc: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_loc : null,
          dl_name: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_name : null,
          leg_number: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].leg_number + 1 : 1,
          order_id: this.selectedOrder.order_id,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          container_number: this.selectedOrder.container_number,
          chassis_number: this.selectedOrder.chassis_number,
          hazmat_req: this.selectedOrder.hazmat_req,
          username: `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
          format_address: this.selectedOrder.format_address,
          customerId: this.selectedOrder.customer_id,
          orderTypeId: this.selectedOrder.order_type_id,
          order_type: this.selectedOrder.order_type,
          isDispatch: this.selectedOrder.is_dispatch,
          is_brokered: this.selectedOrder.is_brokered,
          carrier_id: this.selectedOrder.carrier_id,
          carrier_name: this.selectedOrder.carrier_name
        }
        this.bobtailedLeg = this.legDatasource.data.map(temp => temp.bobtailed_by_leg);
        this.isOrderClosed = this.legCount !== 0 ? this.legDatasource.data
          .filter(temp => temp.leg_status === 'Delivered').length === this.legCount : false;

        // isBobtailTo ? this.checkAndAssignDriver() : null;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getVehicleLocationFromService(id) {
    this.sharedService.getVehicleLocation('keeptruckins', id).subscribe(response => {
      if (response.data) {
        this.bingMap.liveUpdateLocation(response.data.vehicle_location.lat, response.data.vehicle_location.lon);
      }
    })
  }

  getUploadedDocumentListFromService(): void {
    this.sharedService.getOrderUploadedDocumentList(this.uploadComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.uploadedDocumentList = response.data;
        this.isDocumentUploaded = response.data.length !== 0;
        let checkFileList = response.data.map(temp => temp.split('/')[2].split('.')[0]);
        this.uploadedDocumentTemplate = this.generateTemplateForTooltip(checkFileList);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialDropdownNamesFromService(): void {
    this.sharedService.getAccessoriesNameList(this.generalAccessoriesComponent)
      .subscribe(accessoriesnamelist => {
        this.accessoriesNames = accessoriesnamelist;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialPrefillChargesFromService(): void {
    this.sharedService.getAccessoriesChargeList(this.generalAccessoriesComponent, this.selectedOrder.customer_id)
      .subscribe(accessoriesChargesList => {
        this.accessoriesCharges = accessoriesChargesList;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getLegTypesDataFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'leg')
      .subscribe(response => {
        this.legTypes = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getSelectedOrder(isLoadMap?: boolean) {
    this.sharedService.getSelectedOrder(this.orderComponentName,
      this.selectedOrder.order_number, this.selectedOrder.container_sequence).subscribe((response: OrderData[]) => {

        this.selectedOrder = response.length !== 0 ? response[0] : null;

        if (this.selectedOrder) {
          this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
            moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
          this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
            moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

          this.loadedLegDatasource.data = [{
            leg_number: 1,
            leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
            pu_name: this.selectedOrder.pu_name,
            pu_loc: this.selectedOrder.pu_loc,
            dl_name: this.selectedOrder.dl_name,
            dl_loc: this.selectedOrder.dl_loc,
            est_miles: this.selectedOrder.miles,
            rate: this.selectedOrder.rate,
            order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
            pu_street: this.selectedOrder.pu_street,
            pu_city: this.selectedOrder.pu_city,
            pu_state: this.selectedOrder.pu_state,
            pu_postal_code: this.selectedOrder.pu_postal_code,
            dl_street: this.selectedOrder.dl_street,
            dl_city: this.selectedOrder.dl_city,
            dl_state: this.selectedOrder.dl_state,
            dl_postal_code: this.selectedOrder.dl_postal_code
          }];

          if (isLoadMap) {
            this.selectedOrder.vehicle_id ?
              this.getVehicleLocationFromService(this.selectedOrder.vehicle_id) : null;
          }
        }

      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onClickEditOrder() {
    this.router.navigate(['/dispatch/', this.selectedOrder.order_number, this.selectedOrder.container_sequence,
      'edit'], { relativeTo: this.route, skipLocationChange: true });
  }

  onListenToWebhooks() {
    this.panelOpenState = true;

    this.locationSubscription = this.socketService.listenServerThroughSocket('locationUpdated').subscribe((response: any) => {

      if ('vehicle_id' in response && this.selectedOrder.vehicle_id &&
        +this.selectedOrder.vehicle_id === response.vehicle_id) {

        this.bingMap.liveUpdateLocation(response.lat, response.lon);

      }
    })
  }

  onStopListenToWebhooks() {
    this.panelOpenState = false;

    this.locationSubscription ? this.locationSubscription.unsubscribe() : null;
  }

  onClickAddLeg() {
    if (this.selectedOrder.order_status !== 'Delivered') {
      this.dialogConfig.data = {
        "modeSelect": false,
        "legTypes": this.legTypes,
        "commonData": this.legInjectData,
        "legDuplicate": this.legDuplicate,
        "isLeg": true
      };
      let dialogRef = this.dialog.open(EditLegComponent, this.dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.sharedService.postLeg(this.legComponentName, result).subscribe(
            response => {
              if (response.code === 200) {
                this.getLegAccessoriesDataFromService();
              } else {
                this.sharedService.openSnackbar("Order Closed. Cannot Add New leg", 2500, "warning-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            });
        }
      });
    } else {
      this.sharedService.openSnackbar("Order Closed. Cannot Add New leg", 2500, "warning-msg");
    }
  };

  onClickViewOrUpload() {
    this.dialogConfig.data = {
      "containerLevelId": this.selectedOrder.order_container_chassis_id,
      "uploadedDocumentList": this.uploadedDocumentList,
      "fileName": `orders/${this.selectedOrder.order_number}-${this.selectedOrder.container_sequence}/`,
      "isManualInvoice": false
    };
    let dialogRef = this.dialog.open(UploadDocsComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; tempPreviewFile: any, isDeleted: boolean, cancel: boolean }) => {
      if (result) {
        if (!result.isDeleted && !result.cancel) {
          let fields = {
            id: this.selectedOrder.order_container_chassis_id,
            order_number: this.selectedOrder.order_number,
            sequence: this.selectedOrder.container_sequence,
            userId: this.loggedInUser.userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadOrderfile(this.uploadComponentName, result.fileFormData).subscribe(response => {

            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement;

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getUploadedDocumentListFromService();

              this.socketSubscription.unsubscribe();
            });
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else if (result.isDeleted && result.cancel) {
          this.getUploadedDocumentListFromService();
        }
      }
    });
  };

  onClickStreetTurn() {
    if (this.selectedOrder.order_status !== 'Delivered') {

      let isLegAvailable = this.legDatasource.data
        .filter(temp => temp.leg_number > this.streetTurnData.leg_number).length;

      this.sharedService.openConfirmation({
        action: isLegAvailable ? 'streetTurnWithLeg' : 'streetTurnWithoutLeg',
        confirmColor: 'green',
        confirmLabel: 'Yes',
        cancelLabel: 'No'
      }).subscribe(onConfirm => {

        if (onConfirm) {

          let request = {};
          request['flag'] = "Yes, end of business day";
          this.toSelectOrderAndToPerformStreetTurn(request);

        } else {
          this.moreOptionInitialState();
        }

      })

    } else {
      this.sharedService.openSnackbar("Order Closed. Cannot Street Turn", 2500, "warning-msg");
    }
  }

  toSelectOrderAndToPerformStreetTurn(request) {
    this.dialogConfig.data = {
      container_name: this.selectedOrder.container_name,
      container_size: this.selectedOrder.container_size
    };
    let dialogRef = this.dialog.open(StreetturnComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe((result: OrderData) => {
      if (result) {
        request.curentOrderId = this.selectedOrder.order_id;
        request.currentOrderContainerId = this.selectedOrder.order_container_chassis_id;
        request.currentLegNumber = this.streetTurnData.leg_number;
        request.currentChassisNumber = this.streetTurnData.chassis_number;
        request.currentContainerNumber = this.streetTurnData.container_number;
        request.currentDeliveryLocation = this.streetTurnData.dl_loc;
        request.currentDeliveryLocName = this.streetTurnData.dl_name;
        request.currentDriverId = this.streetTurnData.driver_id;
        request.newOrderId = result.order_id;
        request.newOrderContainerId = result.order_container_chassis_id;
        request.newPickupLocation = result.pu_loc;
        request.newPickupLocName = result.pu_name;
        request.loggedInUser = `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`;
        request.oldNotes = `It has been street turned to Order ${result.order_sequence}`;
        request.newNotes = `This has been street turned from Order ${this.selectedOrder.order_sequence}`;

        request.notesDescription = (this.loggedInUser.role === 'Admin' ||
          this.loggedInUser.role === 'Order Entry') ? 'order_notes'
          : (this.loggedInUser.role === 'Dispatch' ? 'dispatch_notes' : 'invoice_notes');

        request.loggedInUserId = this.loggedInUser.userId;

        this.sharedService.performStreetTurn(this.legComponentName, request).subscribe(
          response => {
            if (response.code === 200) {
              this.moreOptionInitialState()
              this.router.navigate(['../../../',
                result.order_number, result.container_sequence, 'leg'], { relativeTo: this.route });
              this.streetTurnGetCall = true;
              this.sharedService.openSnackbar("Chassis Number & Container Number have been updated.", 2500, "success-msg")
            } else {
              this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
              this.moreOptionInitialState();
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            this.moreOptionInitialState();
          });
      } else {
        this.moreOptionInitialState();
      }
    })
  }

  /* to perform bobtail and assign different driver  */
  onClickBobtailTo() {
    if (this.selectedOrder.order_status !== 'Delivered') {
      this.dialogConfig.data = {
        "title": "Bobtail To"
      };
      let dialogRef = this.dialog.open(BobtailToComponent, this.dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let request = {
            ...this.bobtailToData,
            bobtail_to_name: result.bobtail_to_name,
            bobtail_to_loc: result.bobtail_to_loc,
            loggedInUser: `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`
          }

          this.sharedService.performBobtailTo(this.legComponentName, request).subscribe(response => {
            this.getLegAccessoriesDataFromService(true);
            this.isBobtailTo = false;
            this.bobtailToData = null;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
          })
        }
      })
    }
  }

  onClickToHub(isInbetween) {
    if (this.selectedOrder.order_status !== 'Delivered') {
      let dialogConfig = new MatDialogConfig()
      dialogConfig.width = !window.matchMedia(`(min-width: 980px)`).matches ? null : '32%';
      let dialogRef = this.dialog.open(TohubComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((result: LocationData) => {
        if (result) {
          let request = {
            "chassis_number": this.selectedHubLeg.leg_type === 'Bobtail' ? null :
              this.selectedHubLeg.chassis_number,
            "container_number": this.selectedHubLeg.leg_type === 'Bobtail' || this.selectedHubLeg.leg_type === 'Chassis'
              ? null : this.selectedHubLeg.container_number,
            "leg_number": this.selectedHubLeg.leg_number,
            "previousStatus": this.selectedHubLeg.leg_status === 'Picked Up' ? 'Delivered' :
              this.selectedHubLeg.leg_status,
            "leg_status": (this.selectedHubLeg.leg_status === 'Picked Up' ||
              this.selectedHubLeg.leg_status === 'Delivered') ? 'Assigned' :
              (this.selectedHubLeg.driver_id !== null ? 'Assigned' : 'Open'),
            "leg_type_id": this.selectedHubLeg.leg_type_id,
            "pu_name": isInbetween ? result.location_name : this.selectedHubLeg.dl_name,
            "pu_loc": isInbetween ? result.address : this.selectedHubLeg.dl_loc,
            "dl_name": isInbetween ? this.selectedHubLeg.dl_name : result.location_name,
            "dl_loc": isInbetween ? this.selectedHubLeg.dl_loc : result.address,
            "hub_name": result.location_name,
            "hub_loc": result.address,
            "prev_loc": this.selectedHubLeg.pu_loc,
            "prev_name": this.selectedHubLeg.pu_name,
            "prev_miles": this.selectedHubLeg.est_miles,
            "prev_rate": this.selectedHubLeg.rate,
            "order_id": this.selectedHubLeg.order_id,
            "order_container_chassis_id": this.selectedOrder.order_container_chassis_id,
            "driver_id": this.selectedHubLeg.driver_id,
            "leg_id": this.selectedHubLeg.leg_id,
            isInbetween,
            "loggedInUser": `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
            "pu_time": this.selectedHubLeg.pu_time ? new Date(this.selectedHubLeg.pu_time) : null,
            "dl_time": this.selectedHubLeg.dl_time ? new Date(this.selectedHubLeg.dl_time) : null,
            "dl_time_to": this.selectedHubLeg.dl_time ? new Date(this.selectedHubLeg.dl_time_to) : null,
            format_address: this.selectedOrder.format_address,
            customerId: this.selectedOrder.customer_id,
            orderTypeId: this.selectedOrder.order_type_id,
            is_brokered: this.selectedOrder.is_brokered,
            carrier_id: this.selectedOrder.carrier_id
          }
          if (request.pu_loc !== null && request.dl_loc !== null && request.hub_loc !== null &&
            request.prev_loc !== null) {
            this.sharedService.postHubLeg(this.legComponentName, request).subscribe(
              response => {
                if (response.code === 200) {
                  this.getLegAccessoriesDataFromService();
                  this.socketService.sendChangesInDriverFromClient('both');
                  this.moreOptionInitialState();
                } else {
                  this.moreOptionInitialState();
                  this.sharedService.openSnackbar("Order Closed. Cannot Make To Hub", 2500, "failure-msg");
                }
              }, error => {
                this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg")
              });
          } else {
            this.sharedService.openSnackbar("leg pickup and delivery location should not empty", 3000, "warning-msg");
          }
        }
      });
    } else {
      this.sharedService.openSnackbar("Order Closed. Cannot Make To Hub", 2500, "warning-msg");
    }
  }

  onClickBrokered() {
    let valid = true;

    let filteredLegs = this.brokeredLeg.filter(temp => temp ? temp.leg_id : false);

    filteredLegs.forEach(temp => {
      if (temp.leg_status !== 'Open') {
        valid = false;
      }
    })

    if (valid) {

      let dialogConfig = new MatDialogConfig();
      let dialogRef = this.dialog.open(BrokeredComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((result: CarrierData) => {
        if (result) {

          let request = {
            carrier_id: result.carrier_id,
            carrier_rate: result.carrier_rate,
            leg_id: filteredLegs.map(temp => temp.leg_id).join(','),
            id: this.selectedOrder.order_container_chassis_id,
            order_status: this.selectedOrder.order_status === 'Open' ? 'Assigned' :
              this.selectedOrder.order_status
          }

          this.sharedService.makeLegBrokered(this.legComponentName, request).subscribe(response => {
            this.getLegAccessoriesDataFromService();
            this.getSelectedOrder();
            this.moreOptionInitialState();
          }, error => this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg'));

        }

      })
    } else {
      this.sharedService.openSnackbar("Please Select the leg with status Open", 3000, 'warning-msg');
    }
  }

  onSelectCheckbox(selectedRow: LegData, event: MatCheckboxChange, index: number) {
    this.isHubEndOrder = false;
    this.isHub = false;
    this.selectedHubLeg = null;
    this.isStreetTurn = false;
    this.streetTurnData = null;
    this.isBobtailTo = false;
    this.bobtailToData = null;

    let dataSelection = [];

    let exactSelectedRow: LegData = null;

    let exactIndex: number = null;

    this.legDatasource.data.forEach((row, i) => {
      dataSelection.push(this.legTableSelection.isSelected(row))
      if (this.legTableSelection.isSelected(row)) {
        exactSelectedRow = row;
        exactIndex = i;
      }
    });

    dataSelection = dataSelection.filter(temp => temp === true);

    /* condtion is that selected list length must be 1 to enable more option */
    if (dataSelection.length === 0 || dataSelection.length > 1) {

      this.selectedHubLeg = null;
      this.isHub = false;
      this.isHubEndOrder = false;
      this.isStreetTurn = false;
      this.isBobtailTo = false;
      this.streetTurnData = null;

    } else if (dataSelection.length === 1) {

      /** to hub conditions
       * selected leg may be delivered or not(for to hub end order) 
       * leg status must not be delivered(for to hub order)
       * next leg must not be picked up or delivered
       */
      if (exactSelectedRow.leg_status === 'Delivered' &&
        (this.legDatasource.data[exactIndex + 1] === undefined ||
          (this.legDatasource.data[exactIndex + 1].leg_status !== 'Delivered' &&
            this.legDatasource.data[exactIndex + 1].leg_status !== "Picked Up"))) { // only to hub end order get activate on this condition
        this.isHubEndOrder = true;
        this.isHub = false;
        this.selectedHubLeg = exactSelectedRow;
      } else if (exactSelectedRow.leg_status !== 'Delivered' &&
        (this.legDatasource.data[exactIndex + 1] === undefined ||
          (this.legDatasource.data[exactIndex + 1].leg_status !== 'Delivered' &&
            this.legDatasource.data[exactIndex + 1].leg_status !== "Picked Up"))) { // both to hub get activate on this condition
        this.isHubEndOrder = true;
        this.isHub = true;
        this.selectedHubLeg = exactSelectedRow;
      }


      /** street turn conditions
       * order type must be inbound
       * selected leg_type must be loaded or empty and leg status must be delivered
       * if next leg is available it must not be in picked up or delivered
       */
      if (this.selectedOrder.order_type.includes('Inbound') &&
        (exactSelectedRow.leg_type === 'Loaded' || exactSelectedRow.leg_type === 'Empty') &&
        exactSelectedRow.leg_status === 'Delivered' &&
        (
          !this.legDatasource.data[exactIndex + 1] ||
          (this.legDatasource.data[exactIndex + 1] &&
            this.legDatasource.data[exactIndex + 1].leg_status !== 'Picked Up' &&
            this.legDatasource.data[exactIndex + 1].leg_status !== 'Delivered')
        )) { // street turn get activate on this condition

        this.isStreetTurn = true;
        this.streetTurnData = exactSelectedRow;
      }

      /** bobtail to condition
       * selected leg must be either pickedup or delivered
       * next leg status must not be pickedup or delivered
       */
      if ((exactSelectedRow.leg_status === 'Picked Up' || exactSelectedRow.leg_status === 'Delivered') &&
        (!this.legDatasource.data[exactIndex + 1] ||
          (this.legDatasource.data[exactIndex + 1] &&
            this.legDatasource.data[exactIndex + 1].leg_status !== 'Picked Up' &&
            this.legDatasource.data[exactIndex + 1].leg_status !== 'Delivered'))) { // condition to activate bobtail to
        this.isBobtailTo = true;
        this.bobtailToData = exactSelectedRow;
      }

    }

    /* logic for broker the leg, multiple legs can be selected */
    if (!dataSelection.length) {
      this.isBrokered = false;
      this.brokeredLeg = [];
    } else {
      this.isBrokered = true;
      this.brokeredLeg[index] = event.checked ? selectedRow : null;
    }
  }

  onClickDriverName(selectedRow: LegData) {
    this.router.navigate(['driver-info'], { queryParams: { driverId: selectedRow.driver_id }, relativeTo: this.route });
  }

  onClickAssignDriver(selectedRow: LegData, index: number) {
    if (this.selectedOrder.order_status !== 'Delivered') {
      this.dialogConfig.data = {
        "driverId": index === 0 ? null : this.legDatasource.data[index - 1].driver_id,
        "isHazmat": this.selectedOrder.hazmat_req,
        "pickupLocation": selectedRow.pu_loc,
        "pickupLocationName": selectedRow.pu_name
      };

      if (index === 0) {

        this.openPopupToSelectDriver(this.dialogConfig, selectedRow);

      } else if (index !== 0 && this.legDatasource.data[index - 1].driver_id) {

        let dialogRef = this.sharedService.openConfirmation({
          action: "sameDriver",
          name: this.legDatasource.data[index - 1].driver_name,
          cancelLabel: "No",
          confirmColor: "#4d97f3",
          confirmLabel: "Yes"
        });

        dialogRef.subscribe(result => {
          if (result) {

            this.updateDriverServiceCall(selectedRow,
              this.legDatasource.data[index - 1].driver_id, null);

            return;

          }

          this.openPopupToSelectDriver(this.dialogConfig, selectedRow)

        });

      } else if (index !== 0 && !this.legDatasource.data[index - 1].driver_id) {

        if (this.legDatasource.data[index - 1].is_brokered) {

          this.openPopupToSelectDriver(this.dialogConfig, selectedRow);

          return;
        }

        this.sharedService.openSnackbar("Please Assign Driver From Top Bottom", 2000, "warning-msg");
      }

    } else {
      this.sharedService.openSnackbar("Order Closed Can't Able to Assign Driver", 2500, "warning-msg");
    }
  }

  openPopupToSelectDriver(dialogConfig: MatDialogConfig, selectedRow: LegData) {
    let dialogRef = this.dialog.open(AssignDriverComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: DriverDetailsData) => {
      if (result) {
        this.updateDriverServiceCall(selectedRow, result.driver_id, result.user_id);
      }
    });
  }

  updateDriverServiceCall(selectedRow: LegData, driverId: number, userId: number) {

    let request = {
      "order_id": selectedRow.order_id,
      "order_container_chassis_id": selectedRow.order_container_chassis_id,
      "leg_id": selectedRow.leg_id,
      "driver_id": driverId,
      "sparseTitle": "driver"
    }
    this.sharedService.sparseUpdateLegData(this.legComponentName, request).subscribe(response => {
      if (response.code === 200) {
        this.socketService.sendChangesInDriverFromClient('both');

        this.socketService.sendNotification({
          role: 'Dispatch',
          msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is Assigned`,
          hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        })

        if (userId) {
          this.socketService.sendNotification({
            isDriver: true,
            driverUserId: userId,
            msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} has been assigned to you`,
            hyperlink: `/driver-management/driver-detail/driver-info?driverId=${driverId}`,
            icon: 'info',
            styleClass: 'mat-text-accent'
          })
        }

        this.getLegAccessoriesDataFromService();
        this.getSelectedOrder(true);
      } else if (response.code === 103) {
        this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
      }
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
    });

  }

  onClickChangeStatus(selectedRow: LegData, status: string, index: number) {

    if (this.selectedOrder.order_status !== 'Delivered') {

      let request = {
        "leg_id": selectedRow.leg_id,
        "order_id": selectedRow.order_id,
        "order_container_chassis_id": selectedRow.order_container_chassis_id,
        "leg_number": selectedRow.leg_number,
        "driver_id": selectedRow.driver_id,
        "previousStatus": selectedRow.leg_status,
        "currentStatus": status,
        "lastLeg": this.legDatasource.data.length,
        "pu_loc": selectedRow.pu_loc,
        "pu_name": selectedRow.pu_name
      }

      if (selectedRow.driver_name || selectedRow.is_brokered) {

        if (selectedRow.pu_loc && selectedRow.dl_loc
          && selectedRow.pu_name && selectedRow.dl_name) {

          if (selectedRow.leg_status === "Open" && status === "Assigned") {

            request['isTopBottom'] = true;
            this.topToBottomServiceCall(selectedRow, request, index);

          } else if (selectedRow.leg_status === "Assigned" && status === "Picked Up") {

            /**
             * To change status from assigned to picked up
             * chassis number needs to be filled for loaded and empty legs
             * other leg type and truck move order type will be changed without above check 
             * also if the leg is brokered it will change the status */
            if (
              (selectedRow.chassis_number &&
                selectedRow.chassis_number.trim() &&
                (selectedRow.leg_type === "Loaded" || selectedRow.leg_type === "Empty")
              ) ||
              this.selectedOrder.order_type === 'Truck Move' ||
              (selectedRow.leg_type !== "Loaded" && selectedRow.leg_type !== "Empty") ||
              selectedRow.is_brokered
            ) {
              request['isTopBottom'] = true;
              this.topToBottomServiceCall(selectedRow, request, index);
              return
            }

            this.sharedService.openSnackbar("Please fill the Chassis # to proceed", 2000, "warning-msg");

          } else if (selectedRow.leg_status === "Picked Up" && status === "Delivered") {
            request['isTopBottom'] = true;

            /** if last leg loaded or delivered except truck move order type
             * will open dialog to create or not chassis leg
            */
            if (this.selectedOrder.order_type !== "Truck Move" &&
              !this.legDatasource.data[index + 1] &&
              (selectedRow.leg_type === 'Loaded' ||
                selectedRow.leg_type === 'Empty')) {

              this.confirmChassisLegCreate(selectedRow, request, index);
              return;
            }

            this.topToBottomServiceCall(selectedRow, request, index);

          } else if (selectedRow.leg_status === "Delivered" && status === "Picked Up") {

            request['isTopBottom'] = false;
            this.bottomToTopServiceCall(selectedRow, request, "Delivery Time", false);
          } else if (selectedRow.leg_status === "Picked Up" && status === "Assigned") {

            let resetMsg = this.bobtailedLeg.find(temp => temp === selectedRow.leg_id) ?
              "Pickup Time and Driver Bobtailed by this leg will get removed" : "Pickup Time"

            request['isTopBottom'] = false;
            this.bottomToTopServiceCall(selectedRow, request,
              resetMsg, this.bobtailedLeg.includes(selectedRow.leg_id));

          } else if (selectedRow.leg_status === "Assigned" && status === "Open") {

            request['isTopBottom'] = false;
            this.bottomToTopServiceCall(selectedRow, request, "Driver", false);
          }

        } else {
          this.sharedService.openSnackbar("Please Complete the Pickup and Delivery Address to Continue", 3000, "warning-msg")
        }
      } else {
        if (selectedRow.pu_loc && selectedRow.dl_loc
          && selectedRow.pu_name && selectedRow.dl_name) {

          this.onClickAssignDriver(selectedRow, index);

        } else {
          this.sharedService.openSnackbar("Please Complete the Pickup and Delivery Address to Continue", 3000, "warning-msg")
        }
      }
    } else {
      this.sharedService.openSnackbar("Order Closed Can't Able to Change Status", 2500, "warning-msg");
    }
  }

  confirmChassisLegCreate(selectedRow: LegData, request: any, index: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "chassisCreate",
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.topToBottomServiceCall(selectedRow, request, index);
        return
      }

      this.dialogConfig.data = {
        "title": "Enter Chassis Dropoff Location"
      };

      let dropOffLocationRef = this.dialog.open(BobtailToComponent, this.dialogConfig);

      dropOffLocationRef.afterClosed().subscribe(result => {
        if (result) {

          request["chassis_name"] = result.bobtail_to_name;
          request["chassis_address"] = result.bobtail_to_loc;

          this.topToBottomServiceCall(selectedRow, request, index);
        }

      });

    })
  }

  topToBottomServiceCall(selectedRow: LegData, request, index: number) {
    this.sharedService.updateLegStatusTopBottom(this.legComponentName, request).subscribe(response => {
      if (response.code === 200) {
        this.socketService.sendChangesInDriverFromClient('both');
        this.socketService.sendNotification({
          role: 'Dispatch',
          msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is ${response.legStatus}`,
          hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        })
        this.getLegAccessoriesDataFromService();
        this.getSelectedOrder();
        if (response.data.containerStatus === 'ORDER COMPLETED') {
          this.closeOrderToMoveToInvoice("Yes, end of business day", false);
        }

        if (response.legStatus === "Delivered" &&
          this.legDatasource.data[index + 1] &&
          !this.legDatasource.data[index + 1].driver_id) {

          selectedRow.is_brokered ? null : this.confirmDialogToAssignDriver(index);

        }

        if (response.legStatus === "Picked Up" && selectedRow.leg_type === 'Loaded') {
          this.onClickPrintLoadsheet(selectedRow, true);
        }
      } else if (response.code === 103) {
        this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
      } else if (response.code === 104) {
        this.onClickAssignDriver(selectedRow, index);
      } else if (response.code === 105) {
        this.bobtailLegCreation(selectedRow, response.data);
      }
    }, error => {
      this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
    })
  }

  confirmDialogToAssignDriver(index: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "assignDriverConfirm",
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.updateDriverServiceCall(this.legDatasource.data[index + 1],
          this.legDatasource.data[index].driver_id, null);
      }
    })
  }

  bottomToTopServiceCall(selectedRow: LegData, request, reset: string, isPickedupUndo: boolean) {
    let dialogRef = this.sharedService.openConfirmation({
      action: isPickedupUndo ? "bottomTopPickedupUndo" : "bottomTop",
      previousName: request.previousStatus,
      name: request.currentStatus,
      reset,
      cancelLabel: "No",
      confirmLabel: isPickedupUndo ? "Yes, Remove Bobtail" : "Yes",
      confirmColor: "green",
      secondConfirmLabel: "Yes, Keep Bobtail",
      secondConfirmColor: "rgb(24, 102, 219)"
    });

    dialogRef.subscribe(result => {
      if (result) {

        isPickedupUndo ? request['isBobtailRemoved'] = (result === 'yes' ? 1 : 0) : null;

        this.sharedService.updateLegStatusBottomTop(this.legComponentName, request).subscribe(response => {
          if (response.code === 200) {
            this.socketService.sendChangesInDriverFromClient('both');
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount} is ${response.legStatus}`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
            this.getLegAccessoriesDataFromService();
            this.getSelectedOrder();
          } else {
            this.sharedService.openSnackbar(response.error, 2500, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
        })
      }
    })
  }

  bobtailLegCreation(selectedRow: LegData, response) {
    let dialogRef = this.sharedService.openConfirmation({
      action: "bobtail",
      previousName: response[0].last_known_location_name !== null ? response[0].last_known_location_name :
        response[0].last_known_location,
      name: selectedRow.pu_name !== null ? selectedRow.pu_name : selectedRow.pu_loc,
      reset: selectedRow.driver_name,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialogRef.subscribe(result => {
      if (result) {
        let request = {
          "chassis_number": null,
          "container_number": null,
          "leg_number": selectedRow.leg_number,
          "order_id": this.selectedOrder.order_id,
          "order_container_chassis_id": this.selectedOrder.order_container_chassis_id,
          "leg_id": selectedRow.leg_id,
          "pu_name": response[0].last_known_location_name,
          "pu_loc": response[0].last_known_location,
          "dl_name": selectedRow.pu_name,
          "dl_loc": selectedRow.pu_loc,
          "loggedInUser": `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
          "driver_id": selectedRow.driver_id,
          format_address: this.selectedOrder.format_address,
          customerId: this.selectedOrder.customer_id,
          orderTypeId: this.selectedOrder.order_type_id
        }
        this.sharedService.postBobtailLeg(this.legComponentName, request).subscribe(
          response => {
            this.getLegAccessoriesDataFromService();
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number}/${this.legCount + 1} is Delivered`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            });
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} Leg ${selectedRow.leg_number + 1}/${this.legCount + 1} is Picked Up`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            });
            this.socketService.sendChangesInDriverFromClient('both');
            this.getSelectedOrder();
            if (selectedRow.leg_type === 'Loaded') {
              selectedRow.leg_number = selectedRow.leg_number + 1;
              this.onClickPrintLoadsheet(selectedRow, true);
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  }

  closeOrderToMoveToInvoice(flag: string, isConfirm: boolean) {

    if (!isConfirm) {
      this.orderCloseService(flag);
      return;
    }

    let dialogRef = this.sharedService.openConfirmation({
      action: "closeOrderManual",
      confirmColor: 'green',
      confirmLabel: 'Yes',
      cancelLabel: 'No'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.orderCloseService(flag);
      }
    })
  }

  orderCloseService(flag: string) {
    let data: any = [...this.legDatasource.data];

    data = data.filter(temp => temp.leg_type !== 'Bobtail' && temp.chassis_number);

    data.forEach(element => {
      element.pickupDate = element.pu_time ? new Date(element.pu_time).toDateString() : null;
      element.deliveryDate = element.dl_time ? new Date(element.dl_time).toDateString() : null;
    });

    let days;

    let timeDifference;

    if (data.length !== 0 && data[data.length - 1].deliveryDate && data[0].pickupDate) {

      timeDifference = new Date(data[data.length - 1].deliveryDate).getTime() - new Date(data[0].pickupDate).getTime();

      days = Math.round(timeDifference / (1000 * 3600 * 24)) + 1;
    } else {
      days = 0;
    }

    let request = {
      order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
      order_id: this.selectedOrder.order_id,
      flag,
      customer_id: this.selectedOrder.customer_id,
      order_type: this.selectedOrder.order_type,
      days
    }

    this.sharedService.moveOrderToInvoice(this.orderComponentName, request).subscribe(response => {
      this.getSelectedOrder();
      if (flag === 'yes') {
        this.getLegAccessoriesDataFromService();
        this.socketService.sendNotification({
          isMultiple: true,
          multipleRole: ['Admin', 'Order Entry', 'Dispatch', 'Invoice'],
          msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} has been successfully completed and ready for Invoicing`,
          hyperlink: {
            'Admin': `/invoicing/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
            'Order Entry': `/order-management/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
            'Dispatch': `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
            'Invoice': `/invoicing/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`
          },
          icon: 'event_note',
          styleClass: 'mat-deep-purple'
        })
      }
    })
  }

  onClickSortLeg(selectedRow: LegData, isUp: boolean, index: number) {
    if (this.legDatasource.data[index].leg_status === selectedRow.leg_status &&
      this.legDatasource.data[index].driver_id === selectedRow.driver_id) {
      let request = {
        "leg_number": selectedRow.leg_number,
        "order_id": selectedRow.order_id,
        "order_container_chassis_id": selectedRow.order_container_chassis_id,
        isUp
      }
      this.sharedService.rearrangeLegNumber(this.legComponentName, request).subscribe(response => {
        this.getLegAccessoriesDataFromService();
      })
    } else {
      this.sharedService.openSnackbar("status & driver is not same", 2500, "warning-msg")
    }
  }

  onClickEditLeg(selectedRow: LegData) {
    this.dialogConfig.data = {
      "selectedValue": selectedRow,
      "modeSelect": true,
      "legTypes": this.legTypes,
      "commonData": this.legInjectData,
      "isLeg": true
    };
    let dialogRef = this.dialog.open(EditLegComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.sharedService.editLeg(this.legComponentName, result).subscribe(
          responseData => {
            this.getLegAccessoriesDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteLeg(selectedRow: LegData) {
    if (this.selectedOrder.order_status !== "Delivered") {
      let dialogRef = this.sharedService.openConfirmation({
        action: 'Delete',
        name: 'Leg',
        cancelLabel: 'Cancel',
        confirmLabel: 'Delete',
        confirmColor: 'red'
      })
      dialogRef.subscribe(result => {
        if (result) {
          let request = {
            order_id: selectedRow.order_id,
            containerLevelId: selectedRow.order_container_chassis_id,
            leg_number: selectedRow.leg_number,
            leg_id: selectedRow.leg_id,
            isDispatch: this.selectedOrder.is_dispatch
          }
          this.sharedService.deleteLeg(this.legComponentName, request).subscribe(
            response => {
              if (response.code === 200) {
                this.getLegAccessoriesDataFromService();
              } else {
                this.sharedService.openSnackbar("Order Closed. Cannot Delete Leg", 2500, "warning-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            });
        }
      })
    } else {
      this.sharedService.openSnackbar("Order Closed. Cannot Delete Leg", 2500, "warning-msg");
    }
  };

  onClickPrintLoadsheet(selectedRow: LegData, isTriggerd: boolean) {

    if (selectedRow.driver_id || selectedRow.is_brokered) {

      this.dialogConfig.data = {
        "selectedleg": selectedRow,
        "selectedorder": this.selectedOrder,
        "senderAddress": selectedRow.is_brokered ? selectedRow.carrier_email : selectedRow.email
      }
      let dialogRef = this.dialog.open(PrintLoadSheetComponent, this.dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        if (this.selectedOrder.order_type === 'Outbound Intermodal' &&
          (selectedRow.is_auto_created || selectedRow.pu_name === this.selectedOrder.pu_name ||
            selectedRow.pu_loc === this.selectedOrder.pu_loc || selectedRow.dl_name === this.selectedOrder.dl_name ||
            selectedRow.dl_loc === this.selectedOrder.dl_loc) && isTriggerd) {

          this.sendEmailToContainerOwner();

        }
      })

    } else {
      this.sharedService.openSnackbar("Please Assign Driver", 2000, "warning-msg");
    }
  }

  sendEmailToContainerOwner() {
    if (this.selectedOrder.container_email) {
      let dialogRef = this.sharedService.openConfirmation({
        action: "sendEmail",
        name: this.selectedOrder.container_email,
        cancelLabel: "No",
        confirmColor: "#4d97f3",
        confirmLabel: "Yes"
      })

      dialogRef.subscribe(result => {
        if (result) {
          let request = {
            "senderAddress": this.selectedOrder.container_email ? this.selectedOrder.container_email.split(",") : [],
            "subject": `Booking ${this.selectedOrder.booking_number} Please Rail Bill`,
            "container_number": this.selectedOrder.container_number,
            "weight": this.selectedOrder.weight,
            "piece": this.selectedOrder.pieces,
            "seal_no": this.selectedOrder.seal_no
          }
          this.sharedService.sendEmailToContainerOwner(this.legComponentName, request).subscribe(response => {
            this.sharedService.openSnackbar("Successfully Mail Sent", 2500, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Somthing Went Wrong", 2500, "failure-msg");
          })
        }
      })
    }
  }

  onClickEditDriverNotes() {
    this.isDriverNotesEditable = !this.isDriverNotesEditable;
    this.dispatchDriverNotesEdit = this.selectedOrder.dispatch_driver_notes;
  }

  onClickEditDriverNotesSave() {
    if (this.selectedOrder.dispatch_driver_notes && this.selectedOrder.dispatch_driver_notes != "") {
      let request = {
        dispatch_driver_notes: this.selectedOrder.dispatch_driver_notes,
        order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
        loggedInUser: this.loggedInUser.first_name + ' ' + this.loggedInUser.last_name,
        init: this.selectedOrder.driver_notes_created_by ? false : true
      }
      this.sharedService.updateDriverNotes(this.orderComponentName, request).subscribe(
        response => {
          this.isDriverNotesEditable = !this.isDriverNotesEditable;
          this.getSelectedOrder();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        });
    } else {
      this.sharedService.openSnackbar("Notes can't be Empty to save", 2000, "warning-msg")
    }
  }

  onClickEditDriverNotesCancel() {
    this.isDriverNotesEditable = !this.isDriverNotesEditable;
    this.selectedOrder.dispatch_driver_notes = this.dispatchDriverNotesEdit;
  }

  onClickAddDispatchDriverNotes() {
    this.driverNotesUserName = this.loggedInUser.first_name + ' ' + this.loggedInUser.last_name;
    this.driverNotesCreatedOn = new Date();
    this.isDispatchDriverNotesAdded = true;
  }

  onClickSaveDispatchDriverNotes() {
    if (this.dispatchDriverNotes && this.dispatchDriverNotes.trim() != "") {
      let request = {
        dispatch_driver_notes: this.dispatchDriverNotes,
        order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
        loggedInUser: this.driverNotesUserName,
        init: true
      }
      this.sharedService.updateDriverNotes(this.orderComponentName, request).subscribe(
        response => {
          this.isDispatchDriverNotesAdded = false;
          this.getSelectedOrder();
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        });
    } else {
      this.sharedService.openSnackbar("Notes can't be Empty to save", 2000, "warning-msg")
    }
  }

  onClickCancelDispatchDriverNotes() {
    this.isDispatchDriverNotesAdded = false;
    this.dispatchDriverNotes = null;
  }

  onClickEditMilesAndRate(selectedRow: any) {
    this.selectedLegId = selectedRow.order_container_chassis_id;
    this.chargesInlineInput.miles = selectedRow.est_miles;
    this.chargesInlineInput.rate = selectedRow.rate;
  }

  onClickSaveMilesAndRate(selectedRow: any) {
    if (/^[0-9]*$/.test(this.chargesInlineInput.miles) && /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/.test(this.chargesInlineInput.rate)) {
      let request = {
        order_container_chassis_id: selectedRow.order_container_chassis_id,
        miles: this.chargesInlineInput.miles,
        rate: this.chargesInlineInput.rate,
        order_id: this.selectedOrder.order_id,
        customer_id: this.selectedOrder.customer_id
      }
      this.sharedService.updateOrdersMiles(this.orderComponentName, request).subscribe(response => {
        this.selectedLegId = 0;
        this.getSelectedOrder();
        this.getLegAccessoriesDataFromService();
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
    } else {
      this.sharedService.openSnackbar("miles and rate must be number", 2500, "warning-msg");
    }
  }

  onClickAddAccessoriesCharge() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': false,
      'title': "New Accessorial Charge",
      'orderFlag': true
    }
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.postAccessories(this.transactionAccessoriesComponent, result, result.order_container_chassis_id).subscribe(
          response => {
            this.accessoriesDatasource.data.unshift(response);
            this.accessoriesDatasource.paginator = this.accessoriesPaginator;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  };

  onClickEditAccessoriesCharge(selectedRow: AccessoriesData) {
    this.dialogConfig.data = {
      'selectedValue': selectedRow,
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': true,
      'title': "Accessorial Charge",
      'orderFlag': true
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.editAccessories(this.transactionAccessoriesComponent, result).subscribe(
          response => {
            this.accessoriesDatasource.data = this.accessoriesDatasource.data
              .filter(temp => temp.acc_charge_id !== response.acc_charge_id);
            this.accessoriesDatasource.data.unshift(response);
            this.accessoriesDatasource.paginator = this.accessoriesPaginator;
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteAccessorialCharge(accessoryChargeId) {
    if (this.selectedOrder.order_status !== 'Delivered') {
      let dialogRef = this.sharedService.openConfirmation({
        action: 'Delete',
        name: 'Accessorial Charge',
        cancelLabel: 'Cancel',
        confirmLabel: 'Delete',
        confirmColor: 'red'
      })
      dialogRef.subscribe(result => {
        if (result) {
          this.sharedService.deleteAccessories(this.transactionAccessoriesComponent, accessoryChargeId).subscribe(
            response => {
              this.accessoriesDatasource.data = this.accessoriesDatasource.data.filter(temp => temp.acc_charge_id !== accessoryChargeId);
              this.accessoriesDatasource.paginator = this.accessoriesPaginator;
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            });
        }
      })
    } else {
      this.sharedService.openSnackbar("Order Closed Can't Able to delete charges", 2500, "warning-msg");
    }
  };

  generateLegData(): LegData {
    return {
      chassis_number: this.selectedOrder.chassis_number,
      container_number: this.selectedOrder.container_number,
      pu_loc: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_loc : null,
      pu_name: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_name : null,
      dl_loc: null,
      dl_name: null,
      leg_number: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].leg_number + 1 : 1,
      pu_time: null,
      dl_time: null,
      driver_id: null,
      leg_type_id: null,
      driver_name: null,
      leg_status: 'Open'
    }
  }

  generateTemplateForTooltip(checkFileList) {
    let wrong = `❌`;
    let correct = `✔️`;
    return `
    <table>
      <tr>
        <th align="center" colspan="2">Uploaded Documents</th>
      </tr>
      <tr>
      <td align="left">Prenote</td>
      <td align="right">${checkFileList.includes('prenote') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Proof of Delivery</td>
      <td align="right">${checkFileList.includes('proof_of_delivery') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Outgate</td>
      <td align="right">${checkFileList.includes('outgate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Ingate</td>
      <td align="right">${checkFileList.includes('ingate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Bill of Lading</td>
      <td align="right">${checkFileList.includes('bill_of_landing') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Miscellaneous</td>
      <td align="right">${checkFileList.includes('miscellaneous') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Hazmat</td>
      <td align="right">${checkFileList.includes('hazmat') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Chassis</td>
      <td align="right">${checkFileList.includes('chassis') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Interchange</td>
      <td align="right">${checkFileList.includes('interchange') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Scale Tickets</td>
      <td align="right">${checkFileList.includes('scale_tickets') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Incidents</td>
      <td align="right">${checkFileList.includes('incidents') ? correct : wrong}</td>
      </tr>
    </table>`
  }

  moreOptionInitialState() {
    this.isStreetTurn = false;
    this.streetTurnData = null;
    this.isHub = false;
    this.selectedHubLeg = null;
    this.isHubEndOrder = false;
    this.isBobtailTo = false;
    this.bobtailToData = null;
    this.isBrokered = false;
    this.brokeredLeg = [];
    this.legTableSelection.clear();
  }

}
