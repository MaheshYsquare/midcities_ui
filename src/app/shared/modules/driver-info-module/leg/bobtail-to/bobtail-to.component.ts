import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-bobtail-to',
  templateUrl: './bobtail-to.component.html',
  styleUrls: ['./bobtail-to.component.scss']
})
export class BobtailToComponent implements OnInit {

  public bobtailToForm: FormGroup;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  constructor(public dialogRef: MatDialogRef<BobtailToComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    this.bobtailToForm = new FormGroup({
      'bobtail_to_name': new FormControl(null, Validators.required),
      'bobtail_to_loc': new FormControl(null, Validators.required)
    })
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.bobtailToForm.valid) {
      this.dialogRef.close(this.bobtailToForm.value);
    }
  }

}