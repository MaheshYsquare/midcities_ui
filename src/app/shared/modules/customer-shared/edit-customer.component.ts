import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { MAT_DIALOG_DATA, MatDialogRef, MatCheckboxChange } from '@angular/material';

import { RxwebValidators } from '@rxweb/reactive-form-validators';

import { SharedService } from 'app/shared/service/shared.service';
import { CustomerData } from 'app/shared/models/customer.model';
import { DocumentsName } from 'app/shared/models/shared.model'
import { stateValidator } from 'app/shared/utils/app-validators';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {

  public editMode: boolean

  public selectedCustomer: CustomerData;

  public stateList: any;

  public locationTypes: { label: string; value: number }[];

  public customerForm: FormGroup;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'rgb(45, 185, 26)';

  public customer_name: string;

  public componentName: string = 'customers';

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  public phoneNumberExtMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  public fileNames: { label: string; value: string }[]

  public requiredFlag = [];

  public isValidate: boolean = false;
  constructor(public dialogRef: MatDialogRef<EditCustomerComponent>, @Inject(MAT_DIALOG_DATA) data,
    private sharedService: SharedService) {
    if (data) {
      this.selectedCustomer = data.selectedValue;
      this.editMode = data.modeSelect;
      this.customer_name = data.business_name;
      this.locationTypes = data.locationTypes;
    }
  }

  ngOnInit() {
    this.fileNames = DocumentsName;

    this.initForm()

    if (this.editMode) {
      (<any>Object).values(this.customerForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }

  }

  private initForm() {

    let businessName = null;
    let pointOfContact = null;
    let pocPhone = null;
    let pocExt = null;
    let businessPhone = null;
    let businessExt = null;
    let fax = null;
    let preferredInvoice = 'email';
    let preferredDocuments = null;
    let billing_name = null;
    let billingAddress = null;
    let billingStreet = null;
    let billingState = null;
    let billingPostalCode = null;
    let billingCity = null;
    let physical_name = null;
    let physicalAddress = null;
    let physicalStreet = null;
    let physicalState = null;
    let physicalPostalCode = null;
    let physicalCity = null;
    let customerNotes = null;
    let isSameAddress = true;
    let emailLists = new FormArray([this.emailLists(null)]);

    if (this.customer_name !== undefined) {
      businessName = this.customer_name;
    }

    if (this.editMode) {
      let pocArray = this.selectedCustomer.poc_phone ? this.selectedCustomer.poc_phone.split('x') : [];
      let businessArray = this.selectedCustomer.business_phone ? this.selectedCustomer.business_phone.split('x') : [];
      businessName = this.selectedCustomer.business_name;
      pointOfContact = this.selectedCustomer.point_of_contact;
      pocPhone = this.selectedCustomer.poc_phone ? pocArray[0] : null;
      businessPhone = this.selectedCustomer.business_phone ? businessArray[0] : null;
      pocExt = this.selectedCustomer.poc_phone && pocArray.length > 1 ? pocArray[1] : null;
      businessExt = this.selectedCustomer.business_phone && businessArray.length > 1 ? businessArray[1] : null;
      fax = this.selectedCustomer.fax
      physical_name = this.selectedCustomer.physical_name;
      billing_name = this.selectedCustomer.billing_name;
      preferredInvoice = this.selectedCustomer.preferred_invoice;
      preferredDocuments = this.selectedCustomer.preferred_documents ?
        this.selectedCustomer.preferred_documents.split(',') : null;
      billingAddress = this.selectedCustomer.b_address
      billingStreet = this.selectedCustomer.b_street
      billingState = this.selectedCustomer.b_state
      billingPostalCode = this.selectedCustomer.b_postal_code
      billingCity = this.selectedCustomer.b_city
      physicalAddress = this.selectedCustomer.p_address
      physicalStreet = this.selectedCustomer.p_street
      physicalState = this.selectedCustomer.p_state
      physicalPostalCode = this.selectedCustomer.p_postal_code
      physicalCity = this.selectedCustomer.p_city
      customerNotes = this.selectedCustomer.customer_notes
      isSameAddress = this.selectedCustomer.is_same_address;
      emailLists = new FormArray([]);
      // this.selectedCustomer.email ? this.selectedCustomer.email.split(',').forEach((temp,i) => {
      //   emailLists.push(this.emailLists(temp,this.getPionOfConact(i),this.getPOCPhone(i)));
      // }) : emailLists.push(this.emailLists(null));
      let emailDup;
      let phoneDup;
      let pocDup;
      let extDup;
        if(this.selectedCustomer.contact_details && this.selectedCustomer.contact_details.length >= 1) {
          this.selectedCustomer.contact_details.forEach((object,index)=>{
            console.log("object",object)

            if(object.email) {
              emailDup = object.email;
            }else {
              emailDup = null;
            }
        
            if(object.point_of_contact) {
              pocDup = object.point_of_contact;
            } else {
              pocDup = null;
            }
            extDup = null;
            if(object.poc_phone){
              object.poc_phone.split('x').forEach((x,y)=>{
                if(y==0){
                  if(x && x != 'null'){
                    phoneDup = x;
                  } else {
                    phoneDup = null;
                  }
                }
                if(y > 0 && y==1) {
                  if(x && x != 'null'){
                    extDup = x;
                  } else {
                    extDup = null;
                  }
                } else {
                  extDup = null;
                }
              });
            } else {
              phoneDup = null;
            }
            emailLists.push(this.emailLists(emailDup,pocDup,phoneDup,extDup));
        });
        } else {
            emailDup = null ;
            phoneDup = null ;
            pocDup = null ;
        emailLists.push(this.emailLists(emailDup,pocDup,phoneDup,extDup));
        }  
    }

    

    this.customerForm = new FormGroup({
      'businessName': new FormControl(businessName, [Validators.required, Validators.minLength(2), Validators.pattern(/^([A-Za-z0-9,'*-]+\s)*[A-Za-z0-9,'*-]+$/)]),
      // 'pointOfContact': new FormControl(pointOfContact),
      // 'pocPhone': new FormControl(pocPhone, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      // 'pocExt': new FormControl(pocExt),
      'businessPhone': new FormControl(businessPhone, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      'businessExt': new FormControl(businessExt),
      'fax': new FormControl(fax, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      'preferredInvoice': new FormControl(preferredInvoice),
      'preferredDocuments': new FormControl(preferredDocuments),
      'physicalPreferredAddress': new FormControl('db'),
      'physical_name': new FormControl(physical_name),
      'physicalAddress': new FormControl(physicalAddress),
      'physicalStreet': new FormControl(physicalStreet),
      'physicalCity': new FormControl(physicalCity),
      'physicalState': new FormControl(physicalState, stateValidator),
      'physicalPostalCode': new FormControl(physicalPostalCode, [Validators.pattern('[0-9]{5}')]),
      'billingPreferedAddress': new FormControl('db'),
      'billing_name': new FormControl(billing_name),
      'billingAddress': new FormControl(billingAddress),
      'billingStreet': new FormControl(billingStreet),
      'billingCity': new FormControl(billingCity),
      'billingState': new FormControl(billingState, stateValidator),
      'billingPostalCode': new FormControl(billingPostalCode, [Validators.pattern('[0-9]{5}')]),
      'customerNotes': new FormControl(customerNotes),
      'isSameAddress': new FormControl(isSameAddress),
      'emailLists': emailLists,
    });

    if (isSameAddress) {
      this.customerForm.get('physicalPreferredAddress').disable();
      this.customerForm.get('physicalAddress').disable();
      this.customerForm.get('physical_name').disable();
      this.customerForm.get('physicalStreet').disable();
      this.customerForm.get('physicalCity').disable();
      this.customerForm.get('physicalPostalCode').disable();
      this.customerForm.get('physicalState').disable();
    }
  }

  emailLists(email,poc?,phone?,ext?) {
    return new FormGroup({
      'email': new FormControl(email, [ Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/), RxwebValidators.unique()]),
      'point_of_contact': new FormControl(poc),
      'poc_phone': new FormControl(phone, Validators.pattern(/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/)),
      'poc_ext': new FormControl(ext),
    })
  }

  getControls() {
    return (<FormArray>this.customerForm.get('emailLists')).controls;
  }

  onClickAddEmailList(index: number) {
    let emailArray = (<FormArray>this.customerForm.get('emailLists'));
    emailArray.controls[index].get('email').valid && emailArray.controls[index].get('point_of_contact').valid ?
    emailArray.push(this.emailLists(null)) : null;
  }

  onClickDeleteEmailList(index: number) {
    (<FormArray>this.customerForm.get('emailLists')).removeAt(index);
    this.validateEmail();
  }

  validateEmail() {
    let resultEmails = (<FormArray>this.customerForm.get('emailLists')).controls.reduce((prev, curr) => {
      if (prev.includes(curr.value.email)) {
        prev.push(false);
      } else {
        prev.push(curr.value.email);
      }
      return prev;
    }, []);
    for (const emailIndex in resultEmails) {
      if (!resultEmails[emailIndex] == false) {
        (<FormArray>this.customerForm.get('emailLists')).controls[emailIndex].get('email').setErrors(null);
      }
    }
  }

  onSelectSameAsBillingAddress(event: MatCheckboxChange) {
    if (event.checked) {
      this.customerForm.get('physicalAddress').disable();
      this.customerForm.get('physical_name').disable();
      this.customerForm.get('physicalStreet').disable();
      this.customerForm.get('physicalCity').disable();
      this.customerForm.get('physicalPostalCode').disable();
      this.customerForm.get('physicalState').disable();

      this.setSameAddress()
    } else {
      this.customerForm.get('physicalAddress').enable()
      this.customerForm.get('physical_name').enable()
      this.customerForm.get('physicalStreet').enable();
      this.customerForm.get('physicalCity').enable();
      this.customerForm.get('physicalPostalCode').enable();
      this.customerForm.get('physicalState').enable();
    }
  }

  onSubmit() {
    if (this.customerForm.valid) {

      let emailListArr = [];
      let emailListObj = {email:'',point_of_contact:'',poc_phone:''};
      this.customerForm.get('emailLists').value.forEach((x,y)=>{
        emailListObj = {email:'',point_of_contact:'',poc_phone:''};
        if(x.email && x.email != '' && x.email != null) {
          emailListObj.email = x.email;
        } else {
          emailListObj.email = null
        }
        if(x.point_of_contact && x.point_of_contact != '' && x.point_of_contact != null) {
          emailListObj.point_of_contact = x.point_of_contact;
        } else {
          emailListObj.point_of_contact = null;
        }
        if(x.poc_phone && x.poc_phone != '' && x.poc_phone != null && x.poc_ext){
          emailListObj.poc_phone = x.poc_phone+"x"+x.poc_ext;
        } else {
          if(x.poc_phone && x.poc_phone != '' && x.poc_phone != null) {
            emailListObj.poc_phone = x.poc_phone;
          } else {
            emailListObj.poc_phone = null;
          }
        }
        emailListArr.push(emailListObj);
      })

      let businessPhone = this.customerForm.get('businessExt').value && this.customerForm.get('businessExt').value.trim() ?
        `${this.customerForm.get('businessPhone').value}x${this.customerForm.get('businessExt').value}` :
        this.customerForm.get('businessPhone').value;

      if (this.editMode) {
        var editOutput = {
          "customer_id": this.selectedCustomer.customer_id,
          "business_name": this.customerForm.get('businessName').value,
          "b_address": this.customerForm.get('billingAddress').value,
          "b_street": this.customerForm.get('billingStreet').value,
          "b_city": this.customerForm.get('billingCity').value,
          "b_postal_code": this.customerForm.get('billingPostalCode').value,
          "b_state": this.customerForm.get('billingState').value,
          "p_address": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingAddress').value : this.customerForm.get('physicalAddress').value,
          "p_street": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingStreet').value : this.customerForm.get('physicalStreet').value,
          "p_city": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingCity').value : this.customerForm.get('physicalCity').value,
          "p_postal_code": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingPostalCode').value : this.customerForm.get('physicalPostalCode').value,
          "p_state": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingState').value : this.customerForm.get('physicalState').value,
          // "point_of_contact": this.customerForm.get('pointOfContact').value,
          // "poc_phone": pocPhone,
          "business_phone": businessPhone,
          "contact_details":emailListArr,
          // "contact_details": this.customerForm.get('emailLists').value,
          // "email": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.email).join(',') : null,
          //   "point_of_contact": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.pointOfContact).join(',') : null,
          //   "poc_phone": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.pocPhone).join(',') : null,
          "fax": this.customerForm.get('fax').value,
          "customer_notes": this.customerForm.get('customerNotes').value,
          "preferred_mode": this.customerForm.get('preferredInvoice').value,
          "physical_name": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billing_name').value : this.customerForm.get('physical_name').value,
          "billing_name": this.customerForm.get('billing_name').value,
          "is_same_address": this.customerForm.get('isSameAddress').value,
          "customer_quickbooks_id": this.selectedCustomer.customer_quickbooks_id,
          "customer_quickbooks_sync": this.selectedCustomer.customer_quickbooks_sync,
          "preferred_documents": this.customerForm.get('preferredDocuments').value !== null ?
            this.customerForm.get('preferredDocuments').value.toString() :
            null
        }
        this.sharedService.updateCustomerData(this.componentName, editOutput).subscribe(
          response => {
            if (response.code == 200) {
              this.dialogRef.close(editOutput)
            } else if (response.code == 422) {
              this.customerForm.get('businessName').reset();
              this.sharedService.openSnackbar("Customer Already Exists", 2000, "warning-msg");
            } else if (response.code == 100) {
              this.sharedService.openSnackbar(response.error, 2000, "warning-msg");
            } else if (response.code == 103) {
              this.sharedService.quickbooksConnection(this.onSubmit.bind(this));
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });

      } else {
        var addOutput = {
          "business_name": this.customerForm.get('businessName').value,
          "b_address": this.customerForm.get('billingAddress').value,
          "b_street": this.customerForm.get('billingStreet').value,
          "b_city": this.customerForm.get('billingCity').value,
          "b_postal_code": this.customerForm.get('billingPostalCode').value,
          "b_state": this.customerForm.get('billingState').value,
          "p_address": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingAddress').value : this.customerForm.get('physicalAddress').value,
          "p_street": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingStreet').value : this.customerForm.get('physicalStreet').value,
          "p_city": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingCity').value : this.customerForm.get('physicalCity').value,
          "p_postal_code": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingPostalCode').value : this.customerForm.get('physicalPostalCode').value,
          "p_state": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billingState').value : this.customerForm.get('physicalState').value,
          // "point_of_contact": this.customerForm.get('pointOfContact').value,
          // "poc_phone": pocPhone,
          "business_phone": businessPhone,
          // "email": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.email).join(',') : null,
          //   "point_of_contact": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.pointOfContact).join(',') : null,
          //   "poc_phone": this.customerForm.get('emailLists').value ?
          //   this.customerForm.get('emailLists').value.map(temp => temp.pocPhone).join(',') : null,
          // "fax": this.customerForm.get('fax').value,
          // "contact_details": this.customerForm.get('emailLists').value,
          "contact_details":emailListArr,
          "customer_notes": this.customerForm.get('customerNotes').value,
          "preferred_mode": this.customerForm.get('preferredInvoice').value,
          "physical_name": this.customerForm.get('isSameAddress').value ? this.customerForm.get('billing_name').value : this.customerForm.get('physical_name').value,
          "billing_name": this.customerForm.get('billing_name').value,
          "is_same_address": this.customerForm.get('isSameAddress').value,
          "preferred_documents": this.customerForm.get('preferredDocuments').value !== null ?
            this.customerForm.get('preferredDocuments').value.toString() :
            null
        }
        this.sharedService.postCustomerData(this.componentName, addOutput).subscribe(
          response => {
            if (response.code == 200) {
              this.dialogRef.close(addOutput)
            } else if (response.code == 422) {
              this.customerForm.get('businessName').reset();
              this.sharedService.openSnackbar("Customer Already Exists", 2000, "warning-msg");
            } else if (response.code == 100) {
              this.sharedService.openSnackbar(response.error, 2000, "warning-msg");
            } else if (response.code == 103) {
              this.sharedService.quickbooksConnection(this.onSubmit.bind(this));
            }
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          })
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

  setSameAddress() {
    if (this.customerForm.get('isSameAddress').value) {
      setTimeout(() => {
        this.customerForm.get('physicalAddress').setValue(this.customerForm.get('billingAddress').value);
        this.customerForm.get('physical_name').setValue(this.customerForm.get('billing_name').value);
        this.customerForm.get('physicalStreet').setValue(this.customerForm.get('billingStreet').value);
        this.customerForm.get('physicalCity').setValue(this.customerForm.get('billingCity').value);
        this.customerForm.get('physicalPostalCode').setValue(this.customerForm.get('billingPostalCode').value);
        this.customerForm.get('physicalState').setValue(this.customerForm.get('billingState').value);
      }, 100);
    }
  }
}
