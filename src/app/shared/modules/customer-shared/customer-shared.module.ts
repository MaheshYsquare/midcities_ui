import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from 'app/shared/demo.module';
import { EditCustomerComponent } from './edit-customer.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    EditCustomerComponent
  ],
  entryComponents: [
    EditCustomerComponent
  ],
  exports: [
    EditCustomerComponent
  ]
})
export class CustomerSharedModule { }
