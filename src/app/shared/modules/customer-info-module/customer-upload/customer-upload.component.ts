import { Component, Inject, OnInit } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-customer-upload',
  templateUrl: './customer-upload.component.html',
  styleUrls: ['./customer-upload.component.scss']
})
export class CustomerUploadComponent implements OnInit {

  public documents: { label: string, value: string, isUploaded: boolean; file_size?: any }[] = [
    { label: "Invoice Spreadsheet", value: 'invoicespreadsheet', isUploaded: false, file_size: 0 },
    { label: "FSC Table", value: 'fsctable', isUploaded: false, file_size: 0 },
    { label: 'Contract', value: 'contract', isUploaded: false, file_size: 0 },
    { label: 'Bid Results', value: 'bidresults', isUploaded: false, file_size: 0 }
  ];

  public fileUploadFormData: FormData = new FormData();

  public isFilesAdded: boolean = false;

  public isDeleted: boolean = false;

  constructor(public dialogRef: MatDialogRef<CustomerUploadComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.injectedData && this.injectedData.uploadedDocumentList ?
      this.injectedData.uploadedDocumentList.forEach(element => {
        this.documents.forEach(temp => {
          if (element.includes(temp.value)) {
            temp.isUploaded = true;
          }
        })
      }) : null;

    this.documents.some(temp => temp.isUploaded) ? this.getFileSizeFromAws() : null;
  }

  getFileSizeFromAws() {
    this.sharedService.getFileSize("utils", { fileName: this.injectedData.fileName })
      .subscribe(response => {
        this.injectedData.uploadedDocumentList.forEach(element => {
          this.documents.forEach(temp => {
            if (element.includes(temp.value)) {
              temp.file_size = response.data[element];
            }
          })
        });

        this.documents.push({
          label: "Total File Size",
          value: "total_file",
          isUploaded: true,
          file_size: this.documents.map(temp => temp.file_size)
            .reduce((prev, curr) => prev + curr, 0)
        })
      })
  }

  onChangeFileInput(event: any, documentName: string, fileInput: any): void {
    if (event.target.files && event.target.files.length) {

      if (event.target.files[0].size <= 5000000) {

        let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1].toLowerCase();

        if (fileExtension === 'pdf' || fileExtension === 'png' || fileExtension === 'jpg' || fileExtension === 'jpeg') {

          this.removeExistingOrUnwantedFile(documentName);

          this.fileUploadFormData.append("files", event.target.files[0], `${documentName}.${fileExtension}`);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        } else {

          fileInput.value = null;

          this.removeExistingOrUnwantedFile(documentName);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

          this.sharedService.openSnackbar("File Format Must be pdf,jpg,png", 5000, "warning-msg");
        }

      } else {
        fileInput.value = null;

        this.removeExistingOrUnwantedFile(documentName);

        this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        this.sharedService.openSnackbar("File Size Must be less than 5MB", 5000, "warning-msg");
      }
    }
  }

  removeExistingOrUnwantedFile(documentName: string) {
    let fileIndex = this.fileUploadFormData.getAll("files").findIndex((temp: any) => temp.name.includes(documentName));

    if (fileIndex !== -1) {
      let filesList = this.fileUploadFormData.getAll("files");

      filesList.splice(fileIndex, 1);

      this.fileUploadFormData.delete("files");

      filesList.forEach(temp => {
        this.fileUploadFormData.append("files", temp);
      })
    }
  }

  onClickViewFile(documentName: string) {

    this.injectedData.uploadedDocumentList.forEach(element => {
      if (element.includes(documentName)) {
        this.sharedService.getFile("utils", { fileName: element }).subscribe(response => {
          let utf = new Uint8Array(response.buffer.data);
          let file = new Blob([utf], { type: response.ContentType });
          let fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    });

  }

  onClickDeleteFile(documentName: string) {

    this.injectedData.uploadedDocumentList.forEach(element => {
      if (element.includes(documentName)) {
        let request = {
          'fileName': element,
          'customer_id': this.injectedData.customerId
        }
        this.sharedService.deleteCustomerFile("customerdocuments", request)
          .subscribe(responseData => {
            let documentsIndex = this.documents.findIndex(temp => temp.value === documentName);

            this.documents[documentsIndex].isUploaded = false;

            this.isDeleted = true;
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          });
      }
    });

  }

  onClickCancel() {
    this.dialogRef.close({
      isDeleted: this.isDeleted,
      cancel: true,
      fileFormData: this.fileUploadFormData
    });
  }

  onClickSave() {
    if (this.isFilesAdded) {
      this.dialogRef.close({
        fileFormData: this.fileUploadFormData,
        isDeleted: false,
        cancel: false
      });
    }
  }
}
