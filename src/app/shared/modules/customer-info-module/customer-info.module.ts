import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgxFilesizeModule } from 'ngx-filesize';

import { CustomerinfoComponent } from './customerinfo.component';
import { ManualInvoiceComponent } from './manual-invoice/manual-invoice.component';
import { PreviewInvoiceComponent } from './manual-invoice/preview-invoice/preview-invoice.component';
import { LegComponent } from './leg/leg.component';
import { ViewInvoiceComponent } from './leg/view-invoice/view-invoice.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { InvoiceMailComponent } from './leg/view-invoice/invoice-mail/invoice-mail.component';
import { RouteRateSharedModule } from '../route-rate-shared/route-rate-shared.module';
import { OrderSharedModule } from '../order-component/order-component.module';
import { MaterialModule } from 'app/shared/demo.module';
import { CustomerUploadComponent } from './customer-upload/customer-upload.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TooltipModule,
    OrderSharedModule,
    MaterialModule,
    RouteRateSharedModule,
    NgxFilesizeModule
  ],
  declarations: [
    CustomerinfoComponent,
    ManualInvoiceComponent,
    PreviewInvoiceComponent,
    LegComponent,
    ViewInvoiceComponent,
    InvoiceMailComponent,
    CustomerUploadComponent
  ],
  entryComponents: [
    InvoiceMailComponent,
    CustomerUploadComponent
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    CustomerinfoComponent,
    ManualInvoiceComponent,
    PreviewInvoiceComponent,
    LegComponent,
    ViewInvoiceComponent,
    OrderSharedModule
  ]
})
export class CustomerInfoModule { }
