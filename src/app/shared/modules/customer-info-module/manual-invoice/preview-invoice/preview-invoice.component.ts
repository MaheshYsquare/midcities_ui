import { Component, OnInit } from '@angular/core';
import { InvoiceService } from 'app/invoice/invoice.service';
import { Router, ActivatedRoute, Data } from '@angular/router';

import { SharedService } from 'app/shared/service/shared.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { InvoiceMailComponent } from '../../leg/view-invoice/invoice-mail/invoice-mail.component';

@Component({
  selector: 'app-preview-invoice',
  templateUrl: './preview-invoice.component.html',
  styleUrls: ['./preview-invoice.component.scss']
})
export class PreviewInvoiceComponent implements OnInit {

  public selectedInvoice: any;

  public invoiceComponentName: string = 'manualInvoices';

  public locationComponentName: string = 'locations';

  public customerComponentName: string = 'customers';

  public tempAccessoriesCharge: any;

  public cancelButton: boolean;

  constructor(private invoiceService: InvoiceService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.cancelButton = this.router.url.includes('invoice-management') || this.router.url.includes('customer') ? false : true;

    this.tempAccessoriesCharge = this.invoiceService.accessoriesCharge;

    this.route.data.subscribe((data: Data) => {
      this.selectedInvoice = data['invoiceData'].length !== 0 ? data['invoiceData'][0] : this.router.navigate(['/error/404']);
    })
  }

  onClickPrint() {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Invoice</title>
          <style>
        
          .logo {
            width: 30%;
            min-width: 200px;
            min-height: 150px;
            margin-right: 1rem;
            margin-left: 2rem;
          }
          
          .address {
            display: inline-block;
            width: 35%;
            vertical-align: bottom;
          }
          
          th,td {
              text-align: left;
          }
          
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  onClickView() {
    let result = {}
    result['id'] = this.selectedInvoice.manual_invoice_id;
    result['multiple'] = false;
    result['isNeedToFetch'] = false;
    result['file_names'] = this.selectedInvoice.file_names;
    result['invoice_type'] = 'manual';
    result['invoiceGenerationData'] = {
      invoice_number: this.selectedInvoice.invoice_number,
      order_number: this.selectedInvoice.order_number,
      date: this.selectedInvoice.date ? new Date(this.selectedInvoice.date).toLocaleDateString() : null,
      customerName: this.selectedInvoice.business_name,
      customerAddress: this.selectedInvoice.b_address,
      customerPhone: this.selectedInvoice.business_phone,
      customerEmail: this.selectedInvoice.email,
      shippingName: this.selectedInvoice.shipper_name,
      shippingAddress: this.selectedInvoice.shipper_address,
      shippingPhone: this.selectedInvoice.shipper_phone,
      consigneeName: this.selectedInvoice.consignee_name,
      consigneeAddress: this.selectedInvoice.consignee_address,
      consigneePhone: this.selectedInvoice.consignee_phone,
      customer_reference: this.selectedInvoice.customer_reference,
      booking_number: this.selectedInvoice.booking_number,
      delivery_date: this.selectedInvoice.delivery_date ? new Date(this.selectedInvoice.delivery_date).toLocaleDateString() : null,
      container_number: this.selectedInvoice.container_number,
      accessoriesCharge: this.selectedInvoice.accessoriesCharge,
      random_notes: this.selectedInvoice.random_notes,
      due_date: this.selectedInvoice.due_date ? new Date(this.selectedInvoice.due_date).toLocaleDateString() : null,
      total_amount: this.selectedInvoice.total_amount,
      pu_street: this.selectedInvoice.pu_street,
      pu_city: this.selectedInvoice.pu_city,
      pu_state: this.selectedInvoice.pu_state,
      pu_postal_code: this.selectedInvoice.pu_postal_code,
      dl_street: this.selectedInvoice.dl_street,
      dl_city: this.selectedInvoice.dl_city,
      dl_state: this.selectedInvoice.dl_state,
      dl_postal_code: this.selectedInvoice.dl_postal_code,
      b_street: this.selectedInvoice.b_street,
      b_city: this.selectedInvoice.b_city,
      b_state: this.selectedInvoice.b_state,
      b_postal_code: this.selectedInvoice.b_postal_code,
    }
    result['loggedInUser'] = this.sharedService.getUserDetails().userId;

    this.sharedService.viewWithMergeDocs("invoices", result)
      .subscribe(response => {
        let file = new Blob([response.data], { type: 'application/pdf' });
        let fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
  }

  onClickEmail() {
    const emailDialogConfig = new MatDialogConfig();
    emailDialogConfig.data = {
      to: this.selectedInvoice.email,
      prefferedDocuments: this.selectedInvoice.preferred_documents ?
        this.selectedInvoice.preferred_documents.split(',') : [],
      subject: `Mid Cities Invoice #${this.selectedInvoice.invoice_number}`
    }
    const emailDialogRef = this.dialog.open(InvoiceMailComponent, emailDialogConfig);
    emailDialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.id = this.selectedInvoice.manual_invoice_id;
        result.multiple = false;
        result.isNeedToFetch = false;
        result.file_names = this.selectedInvoice.file_names;
        result.invoice_type = 'manual';
        result.invoiceGenerationData = {
          invoice_number: this.selectedInvoice.invoice_number,
          order_number: this.selectedInvoice.order_number,
          date: this.selectedInvoice.date ? new Date(this.selectedInvoice.date).toLocaleDateString() : null,
          customerName: this.selectedInvoice.business_name,
          customerAddress: this.selectedInvoice.b_address,
          customerPhone: this.selectedInvoice.business_phone,
          customerEmail: this.selectedInvoice.email,
          shippingName: this.selectedInvoice.shipper_name,
          shippingAddress: this.selectedInvoice.shipper_address,
          shippingPhone: this.selectedInvoice.shipper_phone,
          consigneeName: this.selectedInvoice.consignee_name,
          consigneeAddress: this.selectedInvoice.consignee_address,
          consigneePhone: this.selectedInvoice.consignee_phone,
          customer_reference: this.selectedInvoice.customer_reference,
          booking_number: this.selectedInvoice.booking_number,
          delivery_date: this.selectedInvoice.delivery_date ? new Date(this.selectedInvoice.delivery_date).toLocaleDateString() : null,
          container_number: this.selectedInvoice.container_number,
          accessoriesCharge: this.selectedInvoice.accessoriesCharge,
          random_notes: this.selectedInvoice.random_notes,
          due_date: this.selectedInvoice.due_date ? new Date(this.selectedInvoice.due_date).toLocaleDateString() : null,
          total_amount: this.selectedInvoice.total_amount,
          pu_street: this.selectedInvoice.pu_street,
          pu_city: this.selectedInvoice.pu_city,
          pu_state: this.selectedInvoice.pu_state,
          pu_postal_code: this.selectedInvoice.pu_postal_code,
          dl_street: this.selectedInvoice.dl_street,
          dl_city: this.selectedInvoice.dl_city,
          dl_state: this.selectedInvoice.dl_state,
          dl_postal_code: this.selectedInvoice.dl_postal_code,
          b_street: this.selectedInvoice.b_street,
          b_city: this.selectedInvoice.b_city,
          b_state: this.selectedInvoice.b_state,
          b_postal_code: this.selectedInvoice.b_postal_code,
        }
        result.loggedInUser = this.sharedService.getUserDetails().userId;

        if (!result.mergeDocs) {
          this.sharedService.emailInvoiceWithAttachment("invoices", result)
            .subscribe(response => {
              if (response.code === 200) {
                this.sharedService.openSnackbar(response.data, 2000, "success-msg");
              } else {
                this.sharedService.openSnackbar("Mail Not Sent", 2000, "failure-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        } else {
          this.sharedService.sendEmailWithMergeDocs("invoices", result)
            .subscribe(response => {
              this.sharedService.openSnackbar(response.data, 2000, response.code === 103 ? "warning-msg" : "success-msg");
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        }
      }
    })
  }

  onClickCancel() {
    let request = {
      manual_invoice_quickbooks_id: this.selectedInvoice.manual_invoice_quickbooks_id,
      manual_invoice_id: this.selectedInvoice.manual_invoice_id,
      fileName: `manual-invoice/${this.selectedInvoice.invoice_number}/`
    }

    let dialogRef = this.sharedService.openConfirmation({
      action: "cancel",
      name: "generated Invoice",
      cancelLabel: "No",
      confirmColor: "red",
      confirmLabel: "Yes"
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteManualInvoice(this.invoiceComponentName, request).subscribe((response: any) => {
          if (response.code === 200) {
            this.router.navigate(['../../'], { relativeTo: this.route, replaceUrl: true })
          } else if (response.code == 103) {
            this.sharedService.quickbooksConnection(this.onClickCancel.bind(this));
          } else if (response.code == 422) {
            this.sharedService.openSnackbar(response.error, 3000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
      }
    })

  }

}
