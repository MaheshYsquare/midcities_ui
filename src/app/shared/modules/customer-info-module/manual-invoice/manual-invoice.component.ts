import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Data } from '@angular/router';

import { Subscription } from 'rxjs';

import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { crossFormMatchedFields } from 'app/shared/utils/app-validators';
import { MidcitiesNameAutocompleteComponent } from 'app/shared/utils/autocomplete/midcities-name-autocomplete/midcities-name-autocomplete.component';
import { CustomerData } from 'app/shared/models/customer.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { loginData } from 'app/session/session.model';
import { DropdownData } from 'app/shared/models/dropdown.model';
import { UploadDocsComponent } from '../../order-component/upload-docs/upload-docs.component';

@Component({
  selector: 'app-manual-invoice',
  templateUrl: './manual-invoice.component.html',
  styleUrls: ['./manual-invoice.component.scss']
})
export class ManualInvoiceComponent implements OnInit {

  public invoiceForm: FormGroup;

  public customerComponentName: string = 'customers';

  public locationComponentName: string = 'locations';

  public accessComponentName: String = "accessorial_charges";

  public componentName: string = "manualInvoices";

  public dropdownComponentName: string = "configurations"; // api endpoint for dropdown

  public locationData: any;

  public accessoriesCharges: any;

  public accessoriesNames: any;

  public disableAccessoriesName: any = [];

  public businessAddress: any;

  public businessPhone: any;

  public businessEmail: any;

  public preferredInvoice: any;

  public shipperAddress: any;

  public shipperPhone: any;

  public consigneeAddress: any;

  public consigneePhone: any;

  public prefixedRate: any = [];

  public businessName: any;

  public shipperName: any;

  public consigneeName: any;

  public customerId: any;

  public editMode: boolean;

  public isArchivedMode: boolean;

  public invoiceNumber: number;

  public selectedInvoice: any;

  public user: loginData;

  public customerQuickbooksId: any;

  public customerAutoSuggestions: CustomerData[];

  public socketSubscription: Subscription;

  public postFormData: FormData = new FormData();

  public isFileUploaded: boolean = false;

  public locationTypes: DropdownData['location']; // dropDown list for the location type

  @ViewChild('shipper') shipperAutocompleteInput: MidcitiesNameAutocompleteComponent;

  @ViewChild('consignee') consigneeAutocompleteInput: MidcitiesNameAutocompleteComponent;

  public sequence: { order_number: number; invoice_number: number };

  public tempPreviewFiles: any;

  public tempFileSize: any;

  public uploadedDocumentTemplate: any;

  public init: boolean = true;

  public shipperData: any = {};

  public consigneeData: any = {};

  public customerData: any = {};

  public documents: { label: string, value: string, isUploaded: boolean }[] = [
    { label: 'Prenote', value: 'prenote', isUploaded: false },
    { label: 'Proof of Delivery', value: 'proof_of_delivery', isUploaded: false },
    { label: 'Outgate', value: 'outgate', isUploaded: false },
    { label: 'Ingate', value: 'ingate', isUploaded: false },
    { label: 'Bill of Lading', value: 'bill_of_landing', isUploaded: false },
    { label: 'Miscellaneous', value: 'miscellaneous', isUploaded: false },
    { label: 'Hazmat', value: 'hazmat', isUploaded: false },
    { label: 'Chassis', value: 'chassis', isUploaded: false },
    { label: 'Interchange', value: 'interchange', isUploaded: false },
    { label: 'Scale tickets', value: 'scale_tickets', isUploaded: false },
    { label: 'Incidents', value: 'incidents', isUploaded: false }
  ];

  constructor(private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private socketService: SocketService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.editMode = this.router.url.includes('invoice-management') || this.router.url.includes('customer') ? true : false;

    this.isArchivedMode = window.location.href.includes('archived');

    if (this.editMode || this.isArchivedMode) {
      this.route.data.subscribe((data: Data) => {
        this.selectedInvoice = data['invoiceData'].length !== 0 ? data['invoiceData'][0] : this.router.navigate(['/error/404']);

        if (this.selectedInvoice) {

          this.selectedInvoice.file_names.forEach(element => {

            this.documents.forEach(temp => {
              if (element.includes(temp.value)) {
                temp.isUploaded = true;
              }
            })

          });

          this.isFileUploaded = this.selectedInvoice.file_names.length !== 0;

          this.uploadedDocumentTemplate = this.generateTemplateForTooltip();
        }
      })
    } else {
      this.route.data.subscribe((data: Data) => {
        data['sequence'].length !== 0 ? this.sequence = data['sequence'][0]
          : { order_number: null, invoice_number: null };
      })

      this.uploadedDocumentTemplate = this.generateTemplateForTooltip();
    }

    this.initForm();

    this.user = this.sharedService.getUserDetails();

    this.getAccessorialNamesfromService();

    this.getAllDropdownFromService();

    this.getAccessorialChargesfromService(this.editMode ? this.selectedInvoice.customer_id : 0);
  }

  private initForm() {
    let invoice_number = this.sequence ? this.sequence.invoice_number : null;
    let order_number = this.sequence ? this.sequence.order_number : null;
    let date = null;
    let customer_reference = null;
    let booking_number = null;
    let delivery_date = null;
    let container_number = null;
    let due_date = null;
    let total_amount = null;
    let businessName = null;
    let consigneeName = null;
    let shipperName = null;
    let random_notes = `I.C.C. regulations require payment within seven days. 
    PLEASE REMIT TO:  MIDCITIES MOTOR FREIGHT, INC. 
    PO BOX 4025 
    St. Joseph, MO 64504 - 0025`;
    let accessoriesCharge = new FormArray([this.accessoryCharge()]);
    this.consigneeData.street = null;
    this.consigneeData.city = null;
    this.consigneeData.state = null;
    this.consigneeData.postal_code = null;
    this.shipperData.street = null;
    this.shipperData.city = null;
    this.shipperData.state = null;
    this.shipperData.postal_code = null;
    this.customerData.street = null;
    this.customerData.city = null;
    this.customerData.state = null;
    this.customerData.postal_code = null;

    if (this.editMode || this.isArchivedMode) {
      invoice_number = this.selectedInvoice.invoice_number;
      order_number = this.selectedInvoice.order_number;
      date = this.selectedInvoice.date;
      customer_reference = this.selectedInvoice.customer_reference;
      booking_number = this.selectedInvoice.booking_number;
      delivery_date = this.selectedInvoice.delivery_date;
      container_number = this.selectedInvoice.container_number;
      due_date = this.selectedInvoice.due_date;
      total_amount = this.selectedInvoice.total_amount;
      random_notes = this.selectedInvoice.random_notes;
      shipperName = this.selectedInvoice.shipper_name;
      consigneeName = this.selectedInvoice.consignee_name;
      businessName = this.selectedInvoice.business_name;
      accessoriesCharge = new FormArray([]);
      this.selectedInvoice.accessoriesCharge.forEach(element => {
        this.disableAccessoriesName.push(element.accessories_name);
        accessoriesCharge.push(this.accessoryCharge(element.accessories_name, element.description, element.rate))
      });
      this.consigneePhone = this.selectedInvoice.consignee_phone
      this.consigneeAddress = this.selectedInvoice.consignee_address
      this.shipperAddress = this.selectedInvoice.shipper_address
      this.shipperPhone = this.selectedInvoice.shipper_phone
      this.businessAddress = this.selectedInvoice.b_address;
      this.businessPhone = this.selectedInvoice.business_phone;
      this.businessEmail = this.selectedInvoice.email;
      this.preferredInvoice = this.selectedInvoice.preferred_invoice;
      this.customerId = this.selectedInvoice.customer_id;
      this.customerQuickbooksId = this.selectedInvoice.customer_quickbooks_id;
      this.consigneeData.street = this.selectedInvoice.dl_street;
      this.consigneeData.city = this.selectedInvoice.dl_city;
      this.consigneeData.state = this.selectedInvoice.dl_state;
      this.consigneeData.postal_code = this.selectedInvoice.dl_postal_code;
      this.shipperData.street = this.selectedInvoice.pu_street;
      this.shipperData.city = this.selectedInvoice.pu_city;
      this.shipperData.state = this.selectedInvoice.pu_state;
      this.shipperData.postal_code = this.selectedInvoice.pu_postal_code;
      this.customerData.street = this.selectedInvoice.b_street;
      this.customerData.city = this.selectedInvoice.b_city;
      this.customerData.state = this.selectedInvoice.b_state;
      this.customerData.postal_code = this.selectedInvoice.b_postal_code;
    }

    this.invoiceForm = new FormGroup({
      'invoice_number': new FormControl({ value: invoice_number, disabled: this.editMode }),
      'order_number': new FormControl({ value: order_number, disabled: (this.editMode && order_number !== null) }),
      'date': new FormControl(date),
      'business_name': new FormControl(businessName, Validators.required),
      'shipper': new FormControl(shipperName),
      'consignee': new FormControl(consigneeName),
      'customer_reference': new FormControl(customer_reference, Validators.pattern(/^[a-zA-Z0-9,'*-_.!#+"\$\&]{0,25}$/)),
      'booking_number': new FormControl(booking_number, Validators.pattern(/^[a-zA-Z0-9,'*-_.!#+"\$\&]{0,25}$/)),
      'delivery_date': new FormControl(delivery_date),
      'container_number': new FormControl(container_number, Validators.pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$")),
      'accessoriesCharge': accessoriesCharge,
      'due_date': new FormControl(due_date),
      'total_amount': new FormControl(total_amount, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/)),
      'random_notes': new FormControl(random_notes)
    }, { validators: crossFormMatchedFields })
  }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown(this.dropdownComponentName, "location")
      .subscribe(response => {
        this.locationTypes = response.location;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  accessoryCharge(accessories_name?, description?, rate?) {
    return new FormGroup({
      'accessories_name': new FormControl(accessories_name, Validators.required),
      'description': new FormControl(description, Validators.required),
      'rate': new FormControl(rate, Validators.required)
    })
  }

  getAccessorialChargesfromService(customerId): void {
    this.sharedService.getAccessoriesChargeList(this.accessComponentName, customerId)
      .subscribe(accessoriesChargesList => {
        this.accessoriesCharges = accessoriesChargesList;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  getAccessorialNamesfromService(): void {
    this.sharedService.getAccessoriesNameList(this.accessComponentName)
      .subscribe(accessoriesnamelist => {
        this.accessoriesNames = accessoriesnamelist;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  getManualInvoiceFromService() {
    this.sharedService.getManualInvoice(this.componentName, this.selectedInvoice.invoice_number)
      .subscribe((response: any) => {
        this.selectedInvoice = response.length !== 0 ? response[0] : {};

        let checkList = this.selectedInvoice.file_names.map(temp => temp.split('/')[2].split('.')[0]);

        this.documents.forEach(temp => {
          temp.isUploaded = checkList.includes(temp.value);
        })

        this.isFileUploaded = this.selectedInvoice.file_names.length !== 0;

        this.uploadedDocumentTemplate = this.generateTemplateForTooltip();
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  onClickOpenUploadDocs() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      "manual_invoice_id": this.editMode ? this.selectedInvoice.manual_invoice_id : null,
      "uploadedDocumentList": this.editMode ? this.selectedInvoice.file_names : [],
      "tempPreviewFiles": this.tempPreviewFiles ? this.tempPreviewFiles : null,
      "tempFileSize": this.tempFileSize ? this.tempFileSize : null,
      "formData": this.postFormData,
      "isManualInvoice": true,
      "fileName": this.editMode ? `manual-invoice/${this.selectedInvoice.invoice_number}/` : null,
      "editMode": this.editMode
    };
    let dialogRef = this.dialog.open(UploadDocsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; tempPreviewFiles: any, tempFileSize: any, isDeleted: boolean, cancel: boolean }) => {

      if (!this.editMode) {

        if (!result.isDeleted && !result.cancel) {
          this.isFileUploaded = result.fileFormData.getAll('files').length !== 0;

          this.tempPreviewFiles = result.tempPreviewFiles;

          this.tempFileSize = result.tempFileSize;

          this.postFormData = result.fileFormData;

          this.documents.forEach(temp => {
            temp.isUploaded = result.tempPreviewFiles[temp.value] ? true : false;
          })

          this.uploadedDocumentTemplate = this.generateTemplateForTooltip();

        } else if (result.isDeleted && result.cancel) {
          this.isFileUploaded = result.fileFormData.getAll('files').length !== 0;

          this.tempPreviewFiles = result.tempPreviewFiles;

          this.tempFileSize = result.tempFileSize;

          this.postFormData = result.fileFormData;

          this.documents.forEach(temp => {
            temp.isUploaded = result.tempPreviewFiles[temp.value] ? true : false;
          })

          this.uploadedDocumentTemplate = this.generateTemplateForTooltip();
        }

      } else {
        if (!result.isDeleted && !result.cancel) {
          let fields = {
            id: this.selectedInvoice.manual_invoice_id,
            invoice_number: this.selectedInvoice.invoice_number,
            userId: this.user.userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadManualInvoiceFile(this.componentName, result.fileFormData).subscribe(response => {

            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getManualInvoiceFromService();

              this.socketSubscription.unsubscribe();
            });
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });

        } else if (result.isDeleted && result.cancel) {
          this.getManualInvoiceFromService();
        }
      }
    })
  }

  containerMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);
    let alphanumericLength = 0;
    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }
    if (alphanumericLength > 10) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/];
    } else if (alphanumericLength > 9) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    }
  }

  onTypeCustomerName(event: CustomerData[]) {
    this.customerAutoSuggestions = event;

    let control = this.invoiceForm.get('business_name');

    let customerValidator: any = this.customerAutoSuggestions.length !== 0 ? this.customerAutoSuggestions
      .find(temp => temp.business_name.toLowerCase() === (control.value !== null ?
        control.value.toLowerCase() : control.value))
      : null;

    if (customerValidator) {
      this.businessName = customerValidator.business_name;
      this.businessAddress = customerValidator.b_address;
      this.businessPhone = customerValidator.business_phone;
      this.businessEmail = customerValidator.email;
      this.preferredInvoice = customerValidator.preferred_invoice;
      this.customerData.street = customerValidator.b_street;
      this.customerData.city = customerValidator.b_city;
      this.customerData.state = customerValidator.b_state;
      this.customerData.postal_code = customerValidator.b_postal_code;
      this.customerId = customerValidator.customer_id;
      this.customerQuickbooksId = customerValidator.customer_quickbooks_id;
      this.editMode && !this.init ? this.getAccessorialChargesfromService(this.customerId) : null;
      this.init = false;
    } else {
      this.businessName = null;
      this.businessAddress = null;
      this.businessPhone = null;
      this.businessEmail = null;
      this.preferredInvoice = null;
      this.customerId = null;
      this.customerQuickbooksId = null;
    }
  }

  getControls() {
    return (<FormArray>this.invoiceForm.get('accessoriesCharge')).controls;
  }

  onAddCharge() {
    (<FormArray>this.invoiceForm.get('accessoriesCharge')).push(this.accessoryCharge());
  }

  onDeleteCharge(index: number) {
    let accessoriesFormArray = (<FormArray>this.invoiceForm.get('accessoriesCharge')).controls[index];

    let totalAmount = +this.invoiceForm.get('total_amount').value;

    let ratePerCharge = +accessoriesFormArray.get('rate').value;

    this.invoiceForm.get('total_amount').setValue(+(totalAmount - ratePerCharge).toFixed(2));

    this.disableAccessoriesName.splice(index, 1);

    this.prefixedRate.splice(index, 1);

    (<FormArray>this.invoiceForm.get('accessoriesCharge')).removeAt(index);
  }

  onChangeRate(index) {
    let accessoriesFormArray = (<FormArray>this.invoiceForm.get('accessoriesCharge')).controls[index];

    let totalAmount = +this.invoiceForm.get('total_amount').value;

    let ratePerCharge = +accessoriesFormArray.get('rate').value;

    if (ratePerCharge < this.prefixedRate[index]) {

      this.invoiceForm.get('total_amount').setValue(+(totalAmount - (this.prefixedRate[index] - ratePerCharge)).toFixed(2));

      this.prefixedRate[index] = ratePerCharge;

    } else if (ratePerCharge > this.prefixedRate[index]) {

      if (totalAmount < this.prefixedRate[index]) {

        this.invoiceForm.get('total_amount').setValue(+((this.prefixedRate[index] - totalAmount) + ratePerCharge).toFixed(2));

        this.prefixedRate[index] = ratePerCharge;

      } else {

        this.invoiceForm.get('total_amount').setValue(+((totalAmount - this.prefixedRate[index]) + ratePerCharge).toFixed(2));

        this.prefixedRate[index] = ratePerCharge

      }

    }
  }

  onAccessoryChargeSelected(index, event) {
    let accessoriesCharges = this.accessoriesCharges ? this.accessoriesCharges.filter(data => data.accessories_name === event) : [];

    let customerSpecific = accessoriesCharges.find(temp => temp.customer_id !== 0);

    let general = accessoriesCharges.find(temp => temp.customer_id === 0);

    let accessoriesFormArray = (<FormArray>this.invoiceForm.get('accessoriesCharge')).controls[index];

    if (customerSpecific) {
      accessoriesFormArray.get('description').setValue(customerSpecific.description);
      accessoriesFormArray.get('rate').setValue(+customerSpecific.rate);
    } else if (general) {
      accessoriesFormArray.get('description').setValue(general.description);
      accessoriesFormArray.get('rate').setValue(+general.rate);
    }

    this.disableAccessoriesName[index] = accessoriesFormArray.get('accessories_name').value;

    let totalAmount = +this.invoiceForm.get('total_amount').value;

    let ratePerCharge = +accessoriesFormArray.get('rate').value;

    if (this.prefixedRate[index] !== undefined) {

      if (totalAmount > this.prefixedRate[index]) {

        this.invoiceForm.get('total_amount').setValue(+((totalAmount - this.prefixedRate[index]) + ratePerCharge).toFixed(2));

      } else {

        this.invoiceForm.get('total_amount').setValue(+((this.prefixedRate[index] - totalAmount) + ratePerCharge).toFixed(2));

      }

    } else {

      this.invoiceForm.get('total_amount').setValue(+(totalAmount + ratePerCharge).toFixed(2));

    }

    this.prefixedRate[index] = ratePerCharge;
  }

  onListenShipperAddress(event) {
    this.shipperAddress = event
    if (!this.invoiceForm.hasError('matched')) {
      this.consigneeAutocompleteInput.control.setErrors(null);
    }
  }

  onShipperSelected(event) {
    this.shipperData.street = event.street;
    this.shipperData.city = event.city;
    this.shipperData.state = event.state;
    this.shipperData.postal_code = event.postal_code;
  }

  onShipperValueEmptied(event: boolean) {
    if (event) {
      this.shipperAddress = null;
      this.shipperPhone = null;
      this.invoiceForm.get('shipper').setValue(null);
    }
  }

  onListenConsigneeAddress(event) {
    this.consigneeAddress = event
    if (!this.invoiceForm.hasError('matched')) {
      this.shipperAutocompleteInput.control.setErrors(null)
    }
  }

  onConsigneeSelected(event) {
    this.consigneeData.street = event.street;
    this.consigneeData.city = event.city;
    this.consigneeData.state = event.state;
    this.consigneeData.postal_code = event.postal_code;
  }

  onConsigneeValueEmptied(event: boolean) {
    if (event) {
      this.consigneeAddress = null;
      this.consigneePhone = null;
      this.invoiceForm.get('consignee').setValue(null);
    }
  }

  onSubmit() {
    if (this.invoiceForm.valid) {

      let request = this.generateRequest();

      if (this.editMode) {

        this.sharedService.updateManualInvoice(this.componentName, request).subscribe(response => {
          if (response.code === 200) {
            this.socketService.sendNotification({
              isMultiple: true,
              multipleRole: ['Admin', 'Invoice'],
              msg: `Manual Invoice for ${this.invoiceForm.get('order_number').value} has been updated`,
              hyperlink: `/invoicing/invoice-management/manual-invoice/${this.selectedInvoice.invoice_number}/preview-invoice`,
              icon: 'info',
              styleClass: 'mat-text-accent'
            });

            this.router.navigate(['preview-invoice'], { relativeTo: this.route, replaceUrl: true });
          } else if (response.code == 103) {
            this.sharedService.quickbooksConnection(this.onSubmit.bind(this));
          } else if (response.code == 422) {
            this.sharedService.openSnackbar(response.error, 3000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })

      } else {

        this.postFormData.append("fields", JSON.stringify(request));

        this.sharedService.postManualInvoice(this.componentName, this.postFormData).subscribe(response => {
          if (response.code === 200) {
            if (this.isFileUploaded) {

              let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement

              loadingContainer.style.display = 'flex';

              this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

                loadingContainer.style.display = 'none';

                if (result.code !== 200) {
                  this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
                }

                this.socketService.sendNotification({
                  isMultiple: true,
                  multipleRole: ['Admin', 'Invoice'],
                  msg: `Manual Invoice for ${this.invoiceForm.get('order_number').value} has been generated`,
                  hyperlink: `/invoicing/invoice-management/manual-invoice/${this.invoiceForm.get('invoice_number').value}/preview-invoice`,
                  icon: 'info',
                  styleClass: 'mat-text-accent'
                })

                this.router.navigate([this.invoiceForm.get('invoice_number').value, 'preview-invoice'], { relativeTo: this.route, replaceUrl: true });

                this.socketSubscription.unsubscribe();
              });

            } else {
              this.socketService.sendNotification({
                isMultiple: true,
                multipleRole: ['Admin', 'Invoice'],
                msg: `Manual Invoice for ${this.invoiceForm.get('order_number').value} has been generated`,
                hyperlink: `/invoicing/invoice-management/manual-invoice/${this.invoiceForm.get('invoice_number').value}/preview-invoice`,
                icon: 'info',
                styleClass: 'mat-text-accent'
              })

              this.router.navigate([this.invoiceForm.get('invoice_number').value, 'preview-invoice'], { relativeTo: this.route, replaceUrl: true });
            }

          } else if (response.code == 103) {
            this.sharedService.quickbooksConnection(this.onSubmit.bind(this));
          } else if (response.code == 422) {
            this.sharedService.openSnackbar(response.error, 3000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })

      }


    }
  }

  onClickCancel() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  generateRequest() {
    let quickbookLine = [];

    this.invoiceForm.get('accessoriesCharge').value.forEach(temp => {
      quickbookLine.push({
        "DetailType": "SalesItemLineDetail",
        "Description": temp.accessories_name,
        "Amount": +temp.rate,
        "SalesItemLineDetail": {
          "ItemRef": {
            "name": "Services",
            "value": "19"
          }
        }
      })
    })

    let request = this.invoiceForm.getRawValue();

    request['customer_id'] = this.customerId;
    request['created_by'] = this.user.first_name + " " + this.user.last_name;
    request['consignee_address'] = this.consigneeAddress;
    request['consignee_phone'] = this.consigneePhone;
    request['consignee_address'] = this.consigneeAddress;
    request['shipper_address'] = this.shipperAddress;
    request['shipper_phone'] = this.shipperPhone;
    request['customer_quickbooks_id'] = this.customerQuickbooksId;
    request['quickbook_line'] = quickbookLine;
    request['userId'] = this.user.userId;
    request['manual_invoice_id'] = this.editMode ? this.selectedInvoice.manual_invoice_id : null;
    request['manual_invoice_quickbooks_id'] = this.editMode ? this.selectedInvoice.manual_invoice_quickbooks_id : null;

    return request;
  }

  generateTemplateForTooltip() {
    let wrong = `❌`;
    let correct = `✔️`;
    // <tr>
    // <th align="center" colspan="2">Uploaded Documents</th>
    // </tr>
    return `
    <br>
    <br>
    <table>
      ${this.documents.map(temp =>
      `<tr>
        <td align="left">${temp.label}</td>
        <td align="right">${temp.isUploaded ? correct : wrong}</td>
        </tr>`
    )}
    </table>`
  }

}