import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, Data } from "@angular/router";

import { SelectionModel } from "@angular/cdk/collections";
import { MatPaginator, MatTableDataSource, MatDialog, MatDialogConfig, PageEvent, Sort, MatCheckboxChange, MatSort } from "@angular/material";

import { Subscription } from "rxjs";

import * as _ from 'lodash';

import { InvoiceMailComponent } from "./leg/view-invoice/invoice-mail/invoice-mail.component";
import { BulkUpdateComponent } from "../route-rate-shared/bulk-update/bulk-update.component";
import { EditRouteRateComponent } from "../route-rate-shared/edit-route-rate/edit-route-rate.component";
import { CustomerData,emailList } from "app/shared/models/customer.model";
import {
  CustomerInvoiceManagementResponse,
  InvoiceManagementData,
  InvoiceManagementRequest
} from "app/shared/models/invoice.model";
import { GeneralRouteRateColumnFilter, RoutesData } from "app/shared/models/routes.model";
import { DropdownData } from "app/shared/models/dropdown.model";
import { AccessoriesData } from "app/shared/models/accessories.model";
import { SharedService } from "app/shared/service/shared.service";
import { EditCustomerComponent } from "../customer-shared/edit-customer.component";
import { EditAccessoriesComponent } from "../accessories-shared/edit-accessories.component";
import { OrderGetRequest } from "app/shared/models/order.model";
import { invoiceMgmtFilterColumns } from "app/shared/models/display-column.model";
import { FilterComponent } from "../order-component/filter/filter.component";
import { CustomerUploadComponent } from "./customer-upload/customer-upload.component";
import { SocketService } from "app/shared/service/socket.service";

@Component({
  selector: 'app-customerinfo',
  templateUrl: './customerinfo.component.html',
  styleUrls: ['./customerinfo.component.scss']
})
export class CustomerinfoComponent implements OnInit {

  public invoiceComponentName: string = 'invoices'; //to store endpoint to call api

  public dropdownComponentName: string = 'configurations';//to store endpoint to call api

  public customerComponentName: string = 'customers'; //to store endpoint to call api

  public routeComponentName: string = "route_rates"; //to store endpoint to call api

  public accessoriesComponentName: String = "accessorial_charges"; //to store endpoint to call api

  public selectedCustomer: CustomerData; // to selected customer object

  public isDocumentUploaded: boolean; // flag to check whether the customer documents uploaded or not

  public customerDocumentsLink: string[]; // to store uploaded file names

  public socketSubscription: Subscription; // to store and handle the subscription to avoid memory leak 

  /* customer invoice table */
  public customerInvoiceDatasource: MatTableDataSource<InvoiceManagementData>; // customer invoice table datasource

  public isInvoiceFilterEnabled: boolean = false; // flag which is used to check customer invoice filter mode or not

  public invoiceColumnFilterValue: OrderGetRequest['filterData']; // to store invoice filter object

  public customerInvoiceColumns: string[] = ['order_type', 'customer_reference', 'invoice_number',
    'date', 'total_amount', 'paid', 'outstanding', 'write_off', 'date_of_payment', 'booking_number',
    'container_number', 'action']; // customer invoice column

  @ViewChild('customerInvoicePaginator') customerInvoicePaginator: MatPaginator; // paginator for customer invoice

  public customerInvoicePageLength: number = 0; // to store customer invoice page length for server side pagination

  public offset: number = 1; // to store page data start for server side pagination

  public limit: number = 5; // to store page data size for server side pagination

  public direction: string; // to store direction for server side sorting

  public column: string; // to store direction for server side sorting

  /* route rate table */
  public isBulkUpdateEnabled: boolean = false; // flag which is used to enable bulk update if any data is selected

  public customerSpecificRouteRateDataSource: MatTableDataSource<RoutesData>; // customer routes rate table datasource

  public customerRouteRatesData: RoutesData[] // to store copy of retrieved route rate response for filter

  public isRouteRatesFilterEnabled: boolean = false; // flag which is used to check customre route rate is in filter mode or not

  public ratesColumnFilterValue: GeneralRouteRateColumnFilter = {
    pu_name: '', dl_name: '', distance: '', rate: '', order_type: '', format_address: ''
  }; // to store customer route rate filter object

  public formatTypes: string[] = ["P2P", "C2C", "P2C", "Z2Z", "S2S", "P2S", "S2P", "C2P"]; // format address array

  public orderTypes: DropdownData['orders']; // to store array of order types which is retrieved from server

  public customerRouteRateSelection = new SelectionModel<RoutesData>(true, []); // selection model for customer route rate

  public customerSpecificRouteRateFilterHeader: string[] = ["dummy", "pickup", "delivery", "formatType",
    "orderType", "miles", "rateFilter", "icon"]; // filter header

  public customerSpecificRouteRateColumns: string[] = ["select", "pu_name", "dl_name", "format_address",
    "order_type", "distance", "rate", "action"]; // to store the route rate column to display in a table

  @ViewChild('customerSpecificRouteRatePaginator') customerSpecificRouteRatePaginator: MatPaginator;

  public locationTypes: DropdownData['location']; // to store array of order types which is retrieved from server

  /* customer Accessories charges table */

  public customerAccessoriesDatasource: MatTableDataSource<AccessoriesData>; // customer accessories table datasource

  @ViewChild('customerAccessoriesSort', { read: MatSort }) customerAccessoriesSort: MatSort; // sort accessories table

  public customerAccessoriesColumns: string[] = ["accessories_name", "accessories_interval", "accessories_value",
    "description", "rate", "action"]; // to store the customer Accessories charge column to display in a table

  @ViewChild('customerAccessoriesPaginator') customerAccessoriesPaginator: MatPaginator; //paginator for customer accessories charge

  public accessoriesNames: string[]; // dropdown of accessories name
  public pocValue:string;
  public emailValue:string;
  public pocPhoneValue:string;
  public emailList = [];

  constructor(private router: Router,
    public route: ActivatedRoute,
    private sharedService: SharedService,
    private socketService: SocketService,
    private dialog: MatDialog) { }

  ngOnInit() {
    /* retrive the cutomer object based on customer id */
    this.route.data.subscribe((data: Data) => {
      this.selectedCustomer = data['customerData'].length ?
        data['customerData'][0] : this.router.navigate(['/error/404']);
    })

    let emailDup = [];
    let pocDup = [];
    let phoeDup = [];
    this.selectedCustomer.contact_details.forEach((x,y)=>{
      if(x.email){
        emailDup.push(x.email);
      } else {
        emailDup.push(null);
      }

      if(x.point_of_contact){
        pocDup.push(x.point_of_contact);
      } else {
        pocDup.push(null);
      }

      if(x.poc_phone){
        phoeDup.push(x.poc_phone);
      } else {
        phoeDup.push(null);
      }
    })

    this.selectedCustomer.email = emailDup.join(',');
    this.selectedCustomer.point_of_contact = pocDup.join(',');
    this.selectedCustomer.poc_phone = phoeDup.join(',');

    this.emailValue = emailDup.filter(x=>x).join(',');
    this.pocValue = pocDup.filter(x=>x).join(',');
    this.pocPhoneValue = phoeDup.filter(x=>x).join(',');

    /* initialize customer invoice table */
    this.customerInvoiceDatasource = new MatTableDataSource();

    this.getSelectedCustomerInvoiceFromService();

    /* retrieve dropdown configuration */
    this.getAllDropdownFromService();

    /* initialize customer specific route rate table */
    this.customerSpecificRouteRateDataSource = new MatTableDataSource();

    this.getCustomerRateAccessoriesFromService();

    /* initialize customer Accessories charges table */
    this.customerAccessoriesDatasource = new MatTableDataSource();

    this.getAccessoriesNamesFromService();
    
    /* to fetch uploaded docs */
    this.getDocumentLinkDatafromService();
  }

  getSelectedCustomerFromService() {
    this.sharedService.getSelectedCustomer(this.customerComponentName, this.selectedCustomer.customer_id)
      .subscribe((response: any) => {
        this.selectedCustomer = response.length ? response[0] : {};

    let emailDup = [];
    let pocDup = [];
    let phoeDup = [];
    this.selectedCustomer.contact_details.forEach((x,y)=>{
      if(x.email){
        emailDup.push(x.email);
      } else {
        emailDup.push(null);
      }

      if(x.point_of_contact){
        pocDup.push(x.point_of_contact);
      } else {
        pocDup.push(null);
      }

      if(x.poc_phone){
        phoeDup.push(x.poc_phone);
      } else {
        phoeDup.push(null);
      }
    })

    this.selectedCustomer.email = emailDup.join(',');
    this.selectedCustomer.point_of_contact = pocDup.join(',');
    this.selectedCustomer.poc_phone = phoeDup.join(',');

    this.emailValue = emailDup.filter(x=>x).join(',');
    this.pocValue = pocDup.filter(x=>x).join(',');
    this.pocPhoneValue = phoeDup.filter(x=>x).join(',');
      })
  }

  getSelectedCustomerInvoiceFromService() {
    let request = this.generateRequest()

    this.sharedService.getInvoiceManagementData(this.invoiceComponentName, request)
      .subscribe((response: CustomerInvoiceManagementResponse) => {
        if (response.code == 200) {
          this.customerInvoiceDatasource.data = response.data.invoiceData;
          this.customerInvoicePageLength = response.data.invoicePaginationLength;
        } else if (response.code == 103) {
          localStorage.setItem('quickbookSync', 'false');
          this.sharedService.quickbooksConnection(this.getSelectedCustomerInvoiceFromService.bind(this));
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
  }

  getAllDropdownFromService() {
    this.sharedService.getAllDropdown(this.dropdownComponentName, "orders,location")
      .subscribe(response => {
        this.orderTypes = response.orders;
        this.locationTypes = response.location;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  getCustomerRateAccessoriesFromService(): void {
    this.sharedService.getCustomerRateAccessories(this.customerComponentName, this.selectedCustomer.customer_id)
      .subscribe(response => {
        this.customerSpecificRouteRateDataSource.data = response.data.customerRouteRate;
        this.customerRouteRatesData = response.data.customerRouteRate;
        this.customerSpecificRouteRateDataSource.paginator = this.customerSpecificRouteRatePaginator;
        this.customerAccessoriesDatasource.data = response.data.customerAccessories;
        this.customerAccessoriesDatasource.paginator = this.customerAccessoriesPaginator;
        this.customerAccessoriesDatasource.sort = this.customerAccessoriesSort;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  getAccessoriesNamesFromService(): void {
    this.sharedService.getAccessoriesNameList(this.accessoriesComponentName)
      .subscribe(response => {
        this.accessoriesNames = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  getDocumentLinkDatafromService(): void {
    this.sharedService.fetchCustomerUploadDocsList("customerdocuments", this.selectedCustomer.customer_id)
      .subscribe(response => {
        this.customerDocumentsLink = response.data;
        this.isDocumentUploaded = response.data.length !== 0;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  onClickEditCustomer() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "modeSelect": true,
      "selectedValue": this.selectedCustomer,
      "locationTypes": this.locationTypes
    };
    let dialogRef = this.dialog.open(EditCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getSelectedCustomerFromService();
        this.sharedService.openSnackbar('Customer Updated Successfully', 500, 'success-msg');
      }
    });
  }

  onClickUploadDocuments() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "customerId": this.selectedCustomer.customer_id,
      "uploadedDocumentList": this.customerDocumentsLink,
      "fileName": `customers/${this.selectedCustomer.business_name}/`
    };
    let dialogRef = this.dialog.open(CustomerUploadComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; isDeleted: boolean, cancel: boolean }) => {
      if (result) {

        if (!result.isDeleted && !result.cancel) {
          let fields = {
            id: this.selectedCustomer.customer_id,
            customer_name: this.selectedCustomer.business_name,
            userId: this.sharedService.getUserDetails().userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadCustomerFile("customerdocuments", result.fileFormData).subscribe(responseData => {

            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement;

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getDocumentLinkDatafromService();

              this.socketSubscription.unsubscribe();
            });

          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else if (result.isDeleted && result.cancel) {
          this.getDocumentLinkDatafromService();
        }
      }
    })
  };

  onClickFilterColumns() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "filterData": this.invoiceColumnFilterValue,
      "filterColumns": invoiceMgmtFilterColumns,
      isInvoiceMgmt: true,
      "key": "invoice_mgmt"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.invoiceColumnFilterValue = result;
        this.isInvoiceFilterEnabled = true;
        this.initialPage();
        this.getSelectedCustomerInvoiceFromService();
      }
    });

  };

  onClickReset() {
    this.isInvoiceFilterEnabled = false;
    this.invoiceColumnFilterValue = undefined;
    this.initialPage();
    this.getSelectedCustomerInvoiceFromService();
  }

  onChangeInvoiceSortDirection(event: Sort) {
    this.column = event.active;
    this.direction = event.direction ? event.direction : null;
    this.getSelectedCustomerInvoiceFromService();
  }

  onClickInvoiceNumber(selectedRow: InvoiceManagementData) {
    if (selectedRow.invoice_type === 'auto') {
      this.router.navigate(['../', selectedRow.order_number, selectedRow.container_sequence, 'leg'],
        { relativeTo: this.route });
    } else {
      this.router.navigate(['../', 'manual-invoice', selectedRow.invoice_number],
        { relativeTo: this.route });
    }
  }

  onClickSendEmail(selectedRow: InvoiceManagementData) {
    let dialog = this.sharedService.openConfirmation({
      action: "mailConfirmation",
      name: null,
      cancelLabel: "No",
      confirmLabel: "Yes",
      confirmColor: "green"
    })
    dialog.subscribe(result => {
      if (result) {
        const emailDialogConfig = new MatDialogConfig();
        emailDialogConfig.data = {
          to: selectedRow.email,
          prefferedDocuments: selectedRow.preferred_documents ?
            selectedRow.preferred_documents.split(',') : [],
          subject: `Mid Cities Invoice #${selectedRow.invoice_number}`
        };
        const emailDialogRef = this.dialog.open(InvoiceMailComponent, emailDialogConfig);
        emailDialogRef.afterClosed().subscribe(result => {
          if (result) {
            result.id = selectedRow.invoice_type === 'auto' ?
              selectedRow.order_container_chassis_id : selectedRow.manual_invoice_id;

            result.invoice_type = selectedRow.invoice_type;
            result.multiple = false;
            result.isNeedToFetchLeg = true;
            result.isNeedToFetch = true;
            result.invoiceGenerationData = this.invoiceMailRequestGeneration(selectedRow);
            result.loggedInUser = this.sharedService.getUserDetails().userId;

            if (!result.mergeDocs) {
              this.sharedService.emailInvoiceWithAttachment(this.invoiceComponentName, result)
                .subscribe(response => {
                  if (response.code === 200) {
                    this.sharedService.openSnackbar(response.data, 2000, "success-msg");
                  } else {
                    this.sharedService.openSnackbar("Mail Not Sent", 2000, "failure-msg");
                  }
                }, error => {
                  this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
                })
            } else {
              this.sharedService.sendEmailWithMergeDocs(this.invoiceComponentName, result)
                .subscribe(response => {
                  this.sharedService.openSnackbar(response.data, 2000, response.code === 103 ? "warning-msg" : "success-msg");
                }, error => {
                  this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
                })
            }

          }
        })
      }
    })
  }

  invoiceMailRequestGeneration(selectedRow: InvoiceManagementData) {
    return {
      invoice_number: selectedRow.invoice_number,
      order_number: selectedRow.order_sequence,
      date: new Date(selectedRow.date).toLocaleDateString(),
      customerName: selectedRow.business_name,
      customerAddress: selectedRow.b_address,
      customerPhone: selectedRow.business_phone,
      customerEmail: selectedRow.email,
      shippingName: selectedRow.shipperName,
      shippingAddress: selectedRow.shipperAddress,
      shippingPhone: selectedRow.shipperPhone,
      consigneeName: selectedRow.consigneeName,
      consigneeAddress: selectedRow.consigneeAddress,
      consigneePhone: selectedRow.consigneePhone,
      customer_reference: selectedRow.customer_reference,
      booking_number: selectedRow.booking_number,
      delivery_date: new Date(selectedRow.delivery_date).toLocaleDateString(),
      container_number: selectedRow.container_number,
      leg_mgmt: '',
      accessoriesCharge: '',
      random_notes: selectedRow.invoice_type === 'auto' ?
        `I.C.C. regulations require payment within seven days. 
         PLEASE REMIT TO:  MIDCITIES MOTOR FREIGHT, INC. 
         PO BOX 4025 
         St. Joseph, MO 64504 - 0025` : selectedRow.random_notes,
      due_date: new Date(selectedRow.due_date).toLocaleDateString(),
      total_amount: selectedRow.total_amount
    }
  }

  onChangeCustomerInvoicePage(event: PageEvent) {
    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getSelectedCustomerInvoiceFromService();
  }

  generateRequest() {
    let request: InvoiceManagementRequest = {
      isQuickbooks: localStorage.getItem('quickbookSync') && localStorage.getItem('quickbookSync') == 'true' ? true : false,
      isQuickbooksInitial: false,
      isUniqueByCustomer: true,
      offset: this.offset,
      limit: this.limit,
      direction: this.direction,
      column: this.column,
      columnFilter: this.isInvoiceFilterEnabled,
      columnFilterValue: this.invoiceColumnFilterValue,
      customerId: this.selectedCustomer.customer_id,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    return request
  }

  initialPage() {
    this.customerInvoicePaginator.pageIndex = 0;
    this.customerInvoicePaginator.pageSize = 5;
    this.offset = 1;
    this.limit = 5;
  }

  /* customer specific route rate table */

  /* to open bulk update component and call the service api to bulk update rate */
  onClickBulkUpdate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "title": "Customer Specific Route Rates Bulk Update"
    };
    let dialogRef = this.dialog.open(BulkUpdateComponent, dialogConfig);

    let selectedRows = this.customerSpecificRouteRateDataSource.data.filter(data => data.is_selected == true)
      .map(selected => selected.route_rate_id).join(",");

    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let request = {
          "is_increase": result.operation == "Increase" ? 1 : 0,
          "increase_type": result.mode,
          "amount": result.value,
          "customer_id": this.selectedCustomer.customer_id,
          "route_rate": selectedRows
        }

        this.sharedService.customerRateBulkUpdate(this.routeComponentName, request)
          .subscribe(response => {

            let matchResponse;

            response.forEach(element => {

              matchResponse = this.customerSpecificRouteRateDataSource.data
                .find(temp => temp.route_rate_id == element.route_rate_id);

              if (matchResponse) {
                matchResponse.rate = element.rate;
              }

            });

            this.customerRouteRateSelection.clear();

            this.isBulkUpdateEnabled = false;

            this.sharedService.openSnackbar("Customer Route Rate BulkUpdated successfully", 2000, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          })
      }
    });
  }

  /* to add the customer route rate data */
  onClickAddCustomerSpecificRate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "locationTypes": this.locationTypes,
      "orderTypes": this.orderTypes,
      "title": "Customer Specific Route Rate",
      "customerId": this.selectedCustomer.customer_id,
      "modeSelect": false
    };
    let dialogRef = this.dialog.open(EditRouteRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getCustomerRateAccessoriesFromService();
        this.sharedService.openSnackbar("New Route Rate Created Successfully", 2000, "success-msg");
      }
    });
  }

  /* filtering of customer specific route rate on client side */
  onClickRatesColumnFilter() {
    let values = Object.keys(this.ratesColumnFilterValue).map(temp => this.ratesColumnFilterValue[temp]);

    if (values.filter(temp => temp !== "").length !== 0) {
      this.isRouteRatesFilterEnabled = true;

      let needsToBeFilter = [...this.customerRouteRatesData];

      let filterObject = { ...this.ratesColumnFilterValue };

      Object.keys(filterObject).forEach(element => {

        filterObject[element] = filterObject[element].toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "");

        needsToBeFilter = needsToBeFilter.filter(temp => {
          return filterObject[element] !== "" ?
            (
              element === 'start_loc' || element === 'des_loc' ?
                (
                  temp[element] !== null ?
                    temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                      .includes(filterObject[element]) : false
                )
                :
                (
                  temp[element] !== null ?
                    temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                      .indexOf(filterObject[element]) === 0 : false
                )
            ) : true
        })
      });

      this.customerSpecificRouteRateDataSource.data = needsToBeFilter;
      this.customerSpecificRouteRateDataSource.paginator = this.customerSpecificRouteRatePaginator;

    } else if (values.filter(temp => temp === "").length === Object.keys(this.ratesColumnFilterValue).length) {
      this.isRouteRatesFilterEnabled = false;
      this.customerSpecificRouteRateDataSource.data = this.customerRouteRatesData;
      this.customerSpecificRouteRateDataSource.paginator = this.customerSpecificRouteRatePaginator;
    }
  }

  onClickResetRouteRatesColumnFilter() {
    Object.keys(this.ratesColumnFilterValue).forEach(temp => this.ratesColumnFilterValue[temp] = '');

    this.isRouteRatesFilterEnabled = false;

    this.customerSpecificRouteRateDataSource.data = this.customerRouteRatesData;

    this.customerSpecificRouteRateDataSource.paginator = this.customerSpecificRouteRatePaginator;
  }

  /* client side sorting using lodash */
  onChangeSortDirection(event: any) {
    this.customerSpecificRouteRateDataSource.data = _.orderBy(this.customerSpecificRouteRateDataSource.data,
      event.active, event.direction);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllCustomerRouteRateSelected() {
    const numSelected = this.customerRouteRateSelection.selected.length;
    const numRows = this.customerSpecificRouteRateDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  customerRouteRateSelectMaster() {
    this.isAllCustomerRouteRateSelected() ?
      this.customerSpecificRouteRateDataSource.data.forEach(row => row.is_selected = false) :
      this.customerSpecificRouteRateDataSource.data.forEach(row => row.is_selected = true);
    this.isAllCustomerRouteRateSelected() ?
      this.customerRouteRateSelection.clear() :
      this.customerSpecificRouteRateDataSource.data.forEach(row => this.customerRouteRateSelection.select(row));
    this.isBulkUpdateEnabled = this.isAllCustomerRouteRateSelected();
  }

  onSelectCustomerRouteRate(selectedRow: RoutesData, event: MatCheckboxChange) {

    selectedRow.is_selected = event.checked;

    let dataSelection = [];

    this.customerSpecificRouteRateDataSource.data.forEach(row => {
      dataSelection.push(this.customerRouteRateSelection.isSelected(row))
    });

    this.isBulkUpdateEnabled = event.checked ? event.checked : dataSelection.includes(true);
  }

  onClickEditCustomerSpecificRate(selectedRow: RoutesData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      "selectedValue": selectedRow,
      "locationTypes": this.locationTypes,
      "orderTypes": this.orderTypes,
      "title": "Customer Specific Route Rate",
      "customerId": this.selectedCustomer.customer_id,
      "modeSelect": true
    };
    let dialogRef = this.dialog.open(EditRouteRateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getCustomerRateAccessoriesFromService()
        this.sharedService.openSnackbar("Customer Route Rate Updated successfully", 2000, "success-msg");
      }
    });
  }

  onClickDeleteCustomerSpecificRate(routeRateId: number) {

    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Customer Specific Rate',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    });

    dialogRef.subscribe(result => {
      if (result != undefined) {
        this.sharedService.deleteCustomerSpecificRouteRate(this.routeComponentName, routeRateId)
          .subscribe(response => {

            this.customerSpecificRouteRateDataSource.data = this.customerSpecificRouteRateDataSource.data
              .filter(customers => customers.route_rate_id !== routeRateId);

            this.customerSpecificRouteRateDataSource.paginator = this.customerSpecificRouteRatePaginator;

            this.sharedService.openSnackbar("Customer Route Deleted successfully", 2000, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  }

  /* customer accessories table */

  onClickAddAccessoriesCharge() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'accessorialData': this.customerAccessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'modeSelect': false,
      'title': "New Accessorial Charge",
      'customerId': this.selectedCustomer.customer_id,
      'customerFlag': true
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.customer_id = this.selectedCustomer.customer_id;
        this.sharedService.postCustomerAccessories(this.accessoriesComponentName, result)
          .subscribe(response => {
            this.customerAccessoriesDatasource.data.unshift(response);
            this.customerAccessoriesDatasource.paginator = this.customerAccessoriesPaginator;
            this.sharedService.openSnackbar("Customer New Accessories Created Successfully", 2000, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickEditAccessoriesCharge(selectedRow: AccessoriesData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'selectedValue': selectedRow,
      'accessorialData': this.customerAccessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'modeSelect': true,
      'title': "Accessorial Charge",
      'customerId': this.selectedCustomer.customer_id,
      'customerFlag': true
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sharedService.editCustomerAccessories(this.accessoriesComponentName, result)
          .subscribe(response => {

            let index = this.customerAccessoriesDatasource.data
              .findIndex(temp => temp.acc_charge_id === response.acc_charge_id);

            if (index !== -1) {
              this.customerAccessoriesDatasource.data[index] = response;
            }

            this.customerAccessoriesDatasource.paginator = this.customerAccessoriesPaginator;

            this.sharedService.openSnackbar("Customer Accessories Updated successfully", 2000, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteAccessoriesCharge(accessorialChargeId: number) {

    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Accessorial Charges',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteCustomerAccessories(this.accessoriesComponentName, accessorialChargeId)
          .subscribe(response => {

            this.customerAccessoriesDatasource.data = this.customerAccessoriesDatasource.data
              .filter(customers => customers.acc_charge_id !== accessorialChargeId);

            this.customerAccessoriesDatasource.paginator = this.customerAccessoriesPaginator;

            this.sharedService.openSnackbar("Customer Accessories Deleted successfully", 2000, "success-msg");
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  };


}
