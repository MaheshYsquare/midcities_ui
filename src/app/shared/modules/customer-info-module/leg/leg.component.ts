import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';

import { MatTableDataSource, MatDialogConfig, MatDialog, MatSort, MatPaginator } from '@angular/material';

import { Subscription } from 'rxjs';

import * as moment from 'moment';

import { OrderData } from 'app/shared/models/order.model';
import { LegData } from 'app/shared/models/leg.model';
import { AccessoriesData } from 'app/shared/models/accessories.model';
import { loginData } from 'app/session/session.model';
import { SocketService } from 'app/shared/service/socket.service';
import { SharedService } from 'app/shared/service/shared.service';
import { UploadDocsComponent } from '../../order-component/upload-docs/upload-docs.component';
import { EditLegComponent } from '../../order-component/edit-leg/edit-leg.component';
import { PrintLoadSheetComponent } from '../../order-component/print-load-sheet/print-load-sheet.component';
import { EditAccessoriesComponent } from '../../accessories-shared/edit-accessories.component';


@Component({
  selector: 'app-leg',
  templateUrl: './leg.component.html',
  styleUrls: ['./leg.component.scss']
})

export class LegComponent implements OnInit {

  public dropdownComponentName: String = "configurations";

  public legTypes: { label: string; value: number }[];

  public uploadComponentName: string = "uploaddocumentlinks";

  public legComponentName: String = "legs";

  public generalAccessoriesComponent: String = "accessorial_charges";

  public orderComponentName: string = "orders";

  public accessoriesNames: String[];

  public accessoriesCharges: any;

  public transactionAccessoriesComponent = "order_accessorial_charges";

  public invoiceComponentName: string = "invoices";

  public selectedOrder: OrderData;

  public isInvoiceEdit: boolean;

  public isArchivedMode: boolean;

  public uploadedDocumentTemplate: string;

  public isDocumentUploaded: boolean = false;

  public uploadedDocumentList: string[];

  public legDatasource: MatTableDataSource<LegData>;

  @ViewChild('legSort') legSort: MatSort;

  public statuses: string[] = ['Open', 'Assigned', 'Picked Up', 'Delivered'];

  public enableColor: string = '#ff9800';

  public disableColor: string = '#e6bc86';

  public legColumns: string[] = ['leg_number', 'leg_type', 'driver_name', 'pu_name', 'pu_time',
    'dl_name', 'dl_time', 'est_miles', 'dmg', 'container_number', 'chassis_number', 'action'];

  @ViewChild('legsPaginator') legsPaginator: MatPaginator;

  public loadedLegDatasource: MatTableDataSource<any>;

  public loadedLegColumns: string[] = ['leg_number', 'leg_type', 'pu_name', 'dl_name',
    'est_miles', 'rate', 'action'];

  public chargesInlineInput: { miles: any, rate: any } = { miles: 0, rate: 0 };

  public selectedLegId: number = 0;

  public accessoriesDatasource: MatTableDataSource<AccessoriesData>;

  public accessoriesColumns: string[] = ['accessories_name', 'rate'];

  public totalChargesDatasource: MatTableDataSource<{ title: string; total_amount: number, paid: any, outstanding: any }>;

  public totalChargesColumns: string[] = ['title', 'total_amount', 'paid', 'outstanding'];

  public legCount: number = 0;

  public legInjectData: any;

  public loggedInUser: loginData;

  public dialogConfig = new MatDialogConfig();

  public socketSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private router: Router,
    private socketService: SocketService,
    private sharedService: SharedService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.legDatasource = new MatTableDataSource();

    this.loadedLegDatasource = new MatTableDataSource();

    this.accessoriesDatasource = new MatTableDataSource();

    this.loggedInUser = this.sharedService.getUserDetails();

    this.isInvoiceEdit = (window.location.href.includes('invoice-management') ||
      window.location.href.includes('customer'));

    this.isArchivedMode = window.location.href.includes('archived');

    if (this.isArchivedMode) {
      this.loadedLegColumns.pop();
      this.legColumns.pop();
    }

    this.accessoriesColumns = (this.loggedInUser.role === 'Admin' || this.loggedInUser.role === 'Invoice') && !this.isArchivedMode ?
      ['accessories_name', 'rate', 'action'] : this.accessoriesColumns;

    this.route.data.subscribe((data: Data) => {
      this.selectedOrder = data['orderLeg'].length !== 0 ? data['orderLeg'][0] : this.router.navigate(['/error/404'])

      if (this.selectedOrder) {
        this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
          moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
        this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
          moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

        this.loadedLegDatasource.data = [{
          leg_number: 1,
          leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
          pu_name: this.selectedOrder.pu_name,
          pu_loc: this.selectedOrder.pu_loc,
          dl_name: this.selectedOrder.dl_name,
          dl_loc: this.selectedOrder.dl_loc,
          est_miles: this.selectedOrder.miles,
          rate: this.selectedOrder.rate,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          pu_street: this.selectedOrder.pu_street,
          pu_city: this.selectedOrder.pu_city,
          pu_state: this.selectedOrder.pu_state,
          pu_postal_code: this.selectedOrder.pu_postal_code,
          dl_street: this.selectedOrder.dl_street,
          dl_city: this.selectedOrder.dl_city,
          dl_state: this.selectedOrder.dl_state,
          dl_postal_code: this.selectedOrder.dl_postal_code
        }];

        this.totalChargesDatasource = new MatTableDataSource();

        this.totalChargesDatasource.data = [
          {
            title: "Total",
            total_amount: 0,
            paid: this.selectedOrder.paid,
            outstanding: this.selectedOrder.outstanding
          }
        ];
      }
    })

    this.getLegAccessoriesDataFromService();

    this.getUploadedDocumentListFromService();

    this.getAccessorialDropdownNamesFromService();

    this.getAccessorialPrefillChargesFromService();

    this.getLegTypesDataFromService();

    this.dialogConfig.disableClose = true;

    this.dialogConfig.autoFocus = true;
  }

  getLegAccessoriesDataFromService(): void {
    this.sharedService.getLegAccessories(this.legComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.legDatasource.data = response.data.legs;
        this.legDatasource.paginator = this.legsPaginator;
        this.accessoriesDatasource.data = response.data.charges;
        this.legCount = this.legDatasource.data.length;
        this.legInjectData = {
          dl_loc: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_loc : null,
          dl_name: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].dl_name : null,
          leg_number: this.legCount !== 0 ? this.legDatasource.data[this.legCount - 1].leg_number + 1 : 1,
          order_id: this.selectedOrder.order_id,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
          container_number: this.selectedOrder.container_number,
          chassis_number: this.selectedOrder.chassis_number,
          hazmat_req: this.selectedOrder.hazmat_req,
          username: `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
          format_address: this.selectedOrder.format_address,
          customerId: this.selectedOrder.customer_id,
          orderTypeId: this.selectedOrder.order_type_id,
          order_type: this.selectedOrder.order_type,
          isDispatch: null,
          is_brokered: this.selectedOrder.is_brokered,
          carrier_id: this.selectedOrder.carrier_id,
          carrier_name: this.selectedOrder.carrier_name
        }
        this.calculateTotalAmount();
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  calculateTotalAmount() {
    this.totalChargesDatasource.data[0].total_amount = 0;

    this.loadedLegDatasource.data.forEach(element => {
      this.totalChargesDatasource.data[0].total_amount = +(+element.rate + this.totalChargesDatasource.data[0].total_amount).toFixed(2);
    })

    this.accessoriesDatasource.data.forEach(element => {
      this.totalChargesDatasource.data[0].total_amount = +(+element.rate + this.totalChargesDatasource.data[0].total_amount).toFixed(2);
    })
  }

  getUploadedDocumentListFromService(): void {
    this.sharedService.getOrderUploadedDocumentList(this.uploadComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.uploadedDocumentList = response.data;
        this.isDocumentUploaded = response.data.length !== 0;
        let checkFileList = response.data.map(temp => temp.split('/')[2].split('.')[0]);
        this.uploadedDocumentTemplate = this.generateTemplateForTooltip(checkFileList);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialDropdownNamesFromService(): void {
    this.sharedService.getAccessoriesNameList(this.generalAccessoriesComponent)
      .subscribe(accessoriesnamelist => {
        this.accessoriesNames = accessoriesnamelist;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getAccessorialPrefillChargesFromService(): void {
    this.sharedService.getAccessoriesChargeList(this.generalAccessoriesComponent, this.selectedOrder.customer_id)
      .subscribe(accessoriesChargesList => {
        this.accessoriesCharges = accessoriesChargesList;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getLegTypesDataFromService(): void {
    this.sharedService.getDropdownList(this.dropdownComponentName, 'leg')
      .subscribe(response => {
        this.legTypes = response;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      });
  }

  getSelectedOrder() {
    this.sharedService.getSelectedOrder(this.orderComponentName,
      this.selectedOrder.order_number, this.selectedOrder.container_sequence).subscribe((response: OrderData[]) => {
        this.selectedOrder = response.length !== 0 ? response[0] : null;
        if (this.selectedOrder) {
          this.selectedOrder.est_pickup_from_time = this.selectedOrder.est_pickup_from_time ?
            moment(this.selectedOrder.est_pickup_from_time).format('M/D/YY HH:mm') : this.selectedOrder.est_pickup_from_time;
          this.selectedOrder.est_delivery_to_time = this.selectedOrder.est_delivery_to_time ?
            moment(this.selectedOrder.est_delivery_to_time).format('M/D/YY HH:mm') : this.selectedOrder.est_delivery_to_time;

          this.loadedLegDatasource.data = [{
            leg_number: 1,
            leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
            pu_name: this.selectedOrder.pu_name,
            pu_loc: this.selectedOrder.pu_loc,
            dl_name: this.selectedOrder.dl_name,
            dl_loc: this.selectedOrder.dl_loc,
            est_miles: this.selectedOrder.miles,
            rate: this.selectedOrder.rate,
            order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
            pu_street: this.selectedOrder.pu_street,
            pu_city: this.selectedOrder.pu_city,
            pu_state: this.selectedOrder.pu_state,
            pu_postal_code: this.selectedOrder.pu_postal_code,
            dl_street: this.selectedOrder.dl_street,
            dl_city: this.selectedOrder.dl_city,
            dl_state: this.selectedOrder.dl_state,
            dl_postal_code: this.selectedOrder.dl_postal_code
          }];

          this.calculateTotalAmount();
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onClickEditOrder() {
    this.router.navigate(['edit'], { relativeTo: this.route, replaceUrl: true });
  }

  onClickViewOrUpload() {
    this.dialogConfig.data = {
      "containerLevelId": this.selectedOrder.order_container_chassis_id,
      "uploadedDocumentList": this.uploadedDocumentList,
      "fileName": `orders/${this.selectedOrder.order_number}-${this.selectedOrder.container_sequence}/`,
      "isManualInvoice": false
    };
    let dialogRef = this.dialog.open(UploadDocsComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe((result: { fileFormData: FormData; tempPreviewFile: any, isDeleted: boolean, cancel: boolean }) => {
      if (result) {
        if (!result.isDeleted && !result.cancel) {
          let fields = {
            id: this.selectedOrder.order_container_chassis_id,
            order_number: this.selectedOrder.order_number,
            sequence: this.selectedOrder.container_sequence,
            userId: this.loggedInUser.userId
          }

          result.fileFormData.append("fields", JSON.stringify(fields));

          this.sharedService.uploadOrderfile(this.uploadComponentName, result.fileFormData).subscribe(response => {

            let loadingContainer: HTMLElement = document.getElementsByClassName('bg-pre-loader').item(0) as HTMLElement;

            loadingContainer.style.display = 'flex';

            this.socketSubscription = this.socketService.listenServerThroughSocket('fileUpload').subscribe(result => {

              if (result.code !== 200) {
                this.sharedService.openSnackbar(result.error, 5000, 'failure-msg');
              }

              this.getUploadedDocumentListFromService();

              this.socketSubscription.unsubscribe();
            });

          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
        } else if (result.isDeleted && result.cancel) {
          this.getUploadedDocumentListFromService();
        }
      }
    });
  };

  onClickSortLeg(selectedRow: LegData, isUp: boolean) {
    let request = {
      "leg_number": selectedRow.leg_number,
      "order_id": selectedRow.order_id,
      "order_container_chassis_id": selectedRow.order_container_chassis_id,
      isUp
    }
    this.sharedService.rearrangeLegNumber(this.legComponentName, request).subscribe(response => {
      this.getLegAccessoriesDataFromService();
    })
  }

  onClickEditLeg(selectedRow: LegData) {
    this.dialogConfig.data = {
      "selectedValue": selectedRow,
      "modeSelect": true,
      "legTypes": this.legTypes,
      "commonData": this.legInjectData,
      "isLeg": true
    };
    let dialogRef = this.dialog.open(EditLegComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.sharedService.editLeg(this.legComponentName, result).subscribe(
          responseData => {
            this.getLegAccessoriesDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickPrintLoadsheet(selectedRow: LegData) {
    if (selectedRow.driver_id !== null) {
      this.dialogConfig.data = {
        "selectedleg": selectedRow,
        "selectedorder": this.selectedOrder,
        "senderAddress": selectedRow.email
      }
      this.dialog.open(PrintLoadSheetComponent, this.dialogConfig)
    } else {
      this.sharedService.openSnackbar("Please Assign Driver", 2000, "warning-msg");
    }
  }

  onClickCustomerPopup() {
    let selectedCustomer = {
      'business_name': this.selectedOrder.business_name,
      'point_of_contact': this.selectedOrder.point_of_contact,
      'poc_phone': this.selectedOrder.poc_phone,
      'business_phone': this.selectedOrder.business_phone,
      'email': this.selectedOrder.email,
      'b_address': this.selectedOrder.b_address,
      'p_address': this.selectedOrder.p_address,
      'preferred_invoice': this.selectedOrder.preferred_invoice,
      'customer_notes': this.selectedOrder.customer_notes,
      'rate_type': this.selectedOrder.rate_type
    }

    this.sharedService.openInfo({
      view: "list",
      name: "Customer",
      listNames: ['Business Name', 'Point of Contact', 'PoC Phone', 'Business Phone',
        'Email', 'Billing Address', 'Physical Address', 'Preferred Mode Of Invoicing', 'RateType', 'Notes'],
      columnNames: ['business_name', 'point_of_contact', 'poc_phone', 'business_phone', 'email',
        'b_address', 'p_address', 'preferred_invoice', 'rate_type', 'customer_notes'],
      selectedData: selectedCustomer
    })
  };

  onClickEditMilesAndRate(selectedRow: any) {
    this.selectedLegId = selectedRow.order_container_chassis_id;
    this.chargesInlineInput.miles = selectedRow.est_miles;
    this.chargesInlineInput.rate = selectedRow.rate;
  }

  onClickSaveMilesAndRate(selectedRow: any) {
    if (/^[0-9]*$/.test(this.chargesInlineInput.miles) && /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/.test(this.chargesInlineInput.rate)) {
      let request = {
        order_container_chassis_id: selectedRow.order_container_chassis_id,
        miles: this.chargesInlineInput.miles,
        rate: this.chargesInlineInput.rate,
        order_id: this.selectedOrder.order_id,
        customer_id: this.selectedOrder.customer_id
      }
      this.sharedService.updateOrdersMiles(this.orderComponentName, request).subscribe(response => {
        this.selectedLegId = 0;
        this.getSelectedOrder();
        this.getLegAccessoriesDataFromService();
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
    } else {
      this.sharedService.openSnackbar("miles and rate must be number", 2500, "warning-msg");
    }
  }

  onClickAddAccessoriesCharge() {
    this.dialogConfig.data = {
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': false,
      'title': "New Accessorial Charge",
      'orderFlag': true
    }
    let dialogRef = this.dialog.open(EditAccessoriesComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.postAccessories(this.transactionAccessoriesComponent, result, result.order_container_chassis_id).subscribe(
          response => {
            let updatedData = this.accessoriesDatasource.data;
            updatedData.unshift(response);
            this.accessoriesDatasource.data = updatedData;
            this.calculateTotalAmount();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
          });
      }
    });
  };

  onClickEditAccessoriesCharge(selectedRow: AccessoriesData) {
    this.dialogConfig.data = {
      'selectedValue': selectedRow,
      'accessorialData': this.accessoriesDatasource.data,
      'accessorialCharges': this.accessoriesNames,
      'accessorialChargesSpecificData': this.accessoriesCharges,
      'modeSelect': true,
      'title': "Accessorial Charge",
      'orderFlag': true
    };
    let dialogRef = this.dialog.open(EditAccessoriesComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.order_container_chassis_id = this.selectedOrder.order_container_chassis_id;
        this.sharedService.editAccessories(this.transactionAccessoriesComponent, result).subscribe(
          response => {
            this.getLegAccessoriesDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    });
  }

  onClickDeleteAccessorialCharge(accessoryChargeId) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Accessorial Charge',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteAccessories(this.transactionAccessoriesComponent, accessoryChargeId).subscribe(
          response => {
            this.getLegAccessoriesDataFromService();
          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
          });
      }
    })
  };

  // onChangeAccessoriesRate(value: string, selectedRow: AccessoriesData) {
  //   if (/^[0-9]*$/.test(value)) {
  //     let request = {
  //       rate: value,
  //       acc_charge_id: selectedRow.acc_charge_id
  //     }
  //     this.sharedService.updateChargesRate(this.transactionAccessoriesComponent, request).subscribe(response => {
  //       this.getLegAccessoriesDataFromService()
  //     }, error => {
  //       this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
  //     })
  //   } else {
  //     this.sharedService.openSnackbar("the given amount is not valid", 2500, "warning-msg");
  //   }
  // }

  onClickGenerateInvoice() {
    let quickbook_line = [];
    this.loadedLegDatasource.data.forEach(temp => {
      quickbook_line.push({
        "DetailType": "SalesItemLineDetail",
        "Description": temp.leg_type,
        "Amount": +temp.rate,
        "SalesItemLineDetail": {
          "ItemRef": {
            "name": "Services",
            "value": "19"
          }
        }
      })
    })
    this.accessoriesDatasource.data.forEach(temp => {
      quickbook_line.push({
        "DetailType": "SalesItemLineDetail",
        "Description": temp.accessories_name,
        "Amount": +temp.rate,
        "SalesItemLineDetail": {
          "ItemRef": {
            "name": "Services",
            "value": "19"
          }
        }
      })
    });

    if (quickbook_line.length !== 0) {
      let request = {
        'customer_id': this.selectedOrder.customer_id,
        'customer_quickbooks_id': this.selectedOrder.customer_quickbooks_id,
        'created_by': `${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
        'order_container_chassis_id': this.selectedOrder.order_container_chassis_id,
        'order_id': this.selectedOrder.order_id,
        'quickbook_line': quickbook_line
      }
      this.sharedService.postAutoInvoice(this.invoiceComponentName, request).subscribe(response => {
        if (response.code === 200) {
          this.selectedOrder.is_invoice_generated == 0 ?
            this.socketService.sendNotification({
              isMultiple: true,
              multipleRole: ['Admin', 'Invoice'],
              msg: `Invoice for ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} has been generated`,
              hyperlink: `/invoicing/invoice-management/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg/view-invoice`,
              icon: 'info',
              styleClass: 'mat-text-accent'
            }) :
            this.socketService.sendNotification({
              isMultiple: true,
              multipleRole: ['Admin', 'Invoice'],
              msg: `Invoice has been updated on ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} by ${this.loggedInUser.first_name} ${this.loggedInUser.last_name}`,
              hyperlink: `/invoicing/invoice-management/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg/view-invoice`,
              icon: 'info',
              styleClass: 'mat-text-accent'
            })
          this.router.navigate(['view-invoice'], { relativeTo: this.route, replaceUrl: true });
        } else if (response.code == 103) {
          this.sharedService.quickbooksConnection(this.onClickGenerateInvoice.bind(this));
        } else if (response.code == 422) {
          this.sharedService.openSnackbar(response.error, 3000, "warning-msg");
        }
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
    } else {
      this.sharedService.openSnackbar("Please Enter Charges", 2500, "warning-msg");
    }
  }

  generateTemplateForTooltip(checkFileList) {
    let wrong = `❌`;
    let correct = `✔️`;
    return `
    <table>
      <tr>
        <th align="center" colspan="2">Uploaded Documents</th>
      </tr>
      <tr>
      <td align="left">Prenote</td>
      <td align="right">${checkFileList.includes('prenote') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Proof of Delivery</td>
      <td align="right">${checkFileList.includes('proof_of_delivery') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Outgate</td>
      <td align="right">${checkFileList.includes('outgate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Ingate</td>
      <td align="right">${checkFileList.includes('ingate') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Bill of Lading</td>
      <td align="right">${checkFileList.includes('bill_of_landing') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Miscellaneous</td>
      <td align="right">${checkFileList.includes('miscellaneous') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Hazmat</td>
      <td align="right">${checkFileList.includes('hazmat') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Chassis</td>
      <td align="right">${checkFileList.includes('chassis') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Interchange</td>
      <td align="right">${checkFileList.includes('interchange') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Scale Tickets</td>
      <td align="right">${checkFileList.includes('scale_tickets') ? correct : wrong}</td>
      </tr>
      <tr>
      <td align="left">Incidents</td>
      <td align="right">${checkFileList.includes('incidents') ? correct : wrong}</td>
      </tr>
    </table>`
  }
}
