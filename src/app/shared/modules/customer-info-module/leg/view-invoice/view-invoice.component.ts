import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';

import { MatDialog, MatDialogConfig } from '@angular/material';

import { OrderData } from 'app/shared/models/order.model';
import { SharedService } from 'app/shared/service/shared.service';
import { AccessoriesData } from 'app/shared/models/accessories.model';
import { InvoiceMailComponent } from './invoice-mail/invoice-mail.component';


@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})

export class ViewInvoiceComponent implements OnInit {

  public selectedOrder: OrderData;

  public invoiceComponentName: string = 'invoices';

  public legComponentName: string = 'legs';

  public isMultiple: boolean;

  public legsCharges: any;

  public accessoriesCharges: AccessoriesData[];

  public isInvoiceCustomer: boolean;

  public notes: string = `I.C.C. regulations require payment within seven days. 
PLEASE REMIT TO:  MIDCITIES MOTOR FREIGHT, INC. 
PO BOX 4025 
St. Joseph, MO 64504 - 0025`

  constructor(public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.isMultiple = window.location.href.includes('multiple-invoice') ? true : false;

    this.isInvoiceCustomer = (window.location.href.includes('invoice-management') ||
      window.location.href.includes('customer')) ? true : false;

    if (this.isMultiple) {

      this.route.data.subscribe((data: Data) => {
        this.selectedOrder = data['orderData'].req.length !== 0 ? data['orderData'] : this.router.navigate(['/error/404']);
      })

    } else {

      this.route.data.subscribe((data: Data) => {
        this.selectedOrder = data['orderLeg'].length !== 0 ? data['orderLeg'][0] : this.router.navigate(['/error/404']);

        if (this.selectedOrder) {
          this.legsCharges = [{
            leg_number: 1,
            leg_type: this.selectedOrder.order_type === 'One Way Empty - Intermodal' ? 'Empty' : 'Loaded',
            pu_name: this.selectedOrder.pu_name,
            pu_loc: this.selectedOrder.pu_loc,
            dl_name: this.selectedOrder.dl_name,
            dl_loc: this.selectedOrder.dl_loc,
            est_miles: this.selectedOrder.miles,
            rate: this.selectedOrder.rate,
            order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
            pu_street: this.selectedOrder.pu_street,
            pu_city: this.selectedOrder.pu_city,
            pu_state: this.selectedOrder.pu_state,
            pu_postal_code: this.selectedOrder.pu_postal_code,
            dl_street: this.selectedOrder.dl_street,
            dl_city: this.selectedOrder.dl_city,
            dl_state: this.selectedOrder.dl_state,
            dl_postal_code: this.selectedOrder.dl_postal_code
          }];
        }
      })

      this.getLegAccessoriesDataFromService();
    }
  }

  getLegAccessoriesDataFromService() {
    this.sharedService.getLegAccessories(this.legComponentName, this.selectedOrder.order_container_chassis_id)
      .subscribe(response => {
        this.accessoriesCharges = response.data.charges;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      });
  }

  onClickPrint() {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Invoice</title>
          <style>
        
          .c-info {
            display: flex;
            flex-direction: row;
            .c-info-1 {
              flex: 0 0 50%;
            }
            .c-info-2 {
              flex: 1 1 100%;
            }
          }
          .logo {
            width: 30%;
            min-width: 200px;
            min-height: 150px;
            margin-right: 1rem;
            margin-left: 2rem;
          }
          
          .address {
            display: inline-block;
            width: 35%;
            vertical-align: bottom;
          }
          
          .midcities {
            vertical-align: middle;
          }

          .address-container {
            display: flex;
            // justify-content: space-between;
            align-items: flex-start;
          }
          
          th,
          tr {
            text-align: left;
          }
          
          table {
            width: 100%;
            border-collapse: collapse;
          }
          
          tr.header th{
            font-weight: normal;
            background-color: #f8e0ffec;
            border-bottom: 1px solid #ccc;
            height:30px;
          }
          
          tr.data {
            height:30px;
            border-bottom: 1px solid #ccc;
          }
          
          
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  onClickView() {
    let request = {};
    request['id'] = this.selectedOrder.order_container_chassis_id;
    request['multiple'] = this.isMultiple;
    request['isNeedToFetchLeg'] = false;
    request['invoice_type'] = 'auto';
    request['invoiceGenerationData'] = {
      invoice_number: this.selectedOrder.invoice_number,
      order_number: this.selectedOrder.order_number,
      date: new Date(this.selectedOrder.date).toLocaleDateString(),
      customerName: this.selectedOrder.business_name,
      customerAddress: this.selectedOrder.b_address,
      b_street: this.selectedOrder.b_street,
      b_city: this.selectedOrder.b_city,
      b_state: this.selectedOrder.b_state,
      b_postal_code: this.selectedOrder.b_postal_code,
      customerPhone: this.selectedOrder.business_phone,
      customerEmail: this.selectedOrder.email,
      container_number: this.selectedOrder.container_number,
      chassis_number: this.selectedOrder.chassis_number,
      booking_number: this.selectedOrder.booking_number,
      customer_reference: this.selectedOrder.customer_reference,
      orderLegAccessories: this.selectedOrder.orderLegAccessories,
      leg_mgmt: this.legsCharges,
      accessoriesCharge: this.accessoriesCharges,
      random_notes: this.notes,
      due_date: new Date(this.selectedOrder.due_date).toLocaleDateString(),
      total_amount: this.selectedOrder.total_amount
    }
    request['loggedInUser'] = this.sharedService.getUserDetails().userId;

    this.sharedService.viewWithMergeDocs(this.invoiceComponentName, request)
      .subscribe(response => {
        let file = new Blob([response.data], { type: 'application/pdf' });
        let fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
  }

  onClickEmail() {
    const emailDialogConfig = new MatDialogConfig();
    emailDialogConfig.data = {
      to: this.selectedOrder.email,
      prefferedDocuments: this.selectedOrder.preferred_documents !== null ?
        this.selectedOrder.preferred_documents.split(',') : [],
      subject: `Mid Cities Invoice #${this.selectedOrder.invoice_number}`,
      isMultiple: this.isMultiple
    }
    const emailDialogRef = this.dialog.open(InvoiceMailComponent, emailDialogConfig);
    emailDialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.id = this.selectedOrder.order_container_chassis_id;
        result.multiple = this.isMultiple;
        result.isNeedToFetchLeg = false;
        result.invoice_type = 'auto';
        result.invoiceGenerationData = {
          invoice_number: this.selectedOrder.invoice_number,
          order_number: this.selectedOrder.order_number,
          date: new Date(this.selectedOrder.date).toLocaleDateString(),
          customerName: this.selectedOrder.business_name,
          customerAddress: this.selectedOrder.b_address,
          b_street: this.selectedOrder.b_street,
          b_city: this.selectedOrder.b_city,
          b_state: this.selectedOrder.b_state,
          b_postal_code: this.selectedOrder.b_postal_code,
          customerPhone: this.selectedOrder.business_phone,
          customerEmail: this.selectedOrder.email,
          container_number: this.selectedOrder.container_number,
          chassis_number: this.selectedOrder.chassis_number,
          booking_number: this.selectedOrder.booking_number,
          customer_reference: this.selectedOrder.customer_reference,
          orderLegAccessories: this.selectedOrder.orderLegAccessories,
          leg_mgmt: this.legsCharges,
          accessoriesCharge: this.accessoriesCharges,
          random_notes: this.notes,
          due_date: new Date(this.selectedOrder.due_date).toLocaleDateString(),
          total_amount: this.selectedOrder.total_amount
        }
        result.loggedInUser = this.sharedService.getUserDetails().userId;
        if (!result.mergeDocs) {
          this.sharedService.emailInvoiceWithAttachment(this.invoiceComponentName, result)
            .subscribe(response => {
              if (response.code === 200) {
                this.sharedService.openSnackbar(response.data, 2000, "success-msg");
              } else {
                this.sharedService.openSnackbar("Mail Not Sent", 2000, "failure-msg");
              }
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        } else {
          this.sharedService.sendEmailWithMergeDocs(this.invoiceComponentName, result)
            .subscribe(response => {
              this.sharedService.openSnackbar(response.data, 2000, response.code === 103 ? "warning-msg" : "success-msg");
            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
            })
        }
      }
    })
  }

  onClickCancel() {
    let request = this.isMultiple ? { containerToBeDeleted: this.selectedOrder.req } : {
      containerToBeDeleted: [{
        order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
        order_id: this.selectedOrder.order_id,
        invoice_quickbooks_id: this.selectedOrder.invoice_quickbooks_id,
        invoice_quickbooks_sync: this.selectedOrder.invoice_quickbooks_sync
      }]
    }

    let dialogRef = this.sharedService.openConfirmation({
      action: "cancel",
      name: "generated Invoice",
      cancelLabel: "No",
      confirmColor: "red",
      confirmLabel: "Yes"
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteAutoInvoice(this.invoiceComponentName, request).subscribe(response => {
          if (response.code === 200) {
            this.isMultiple ? this.router.navigate(['../../'], { relativeTo: this.route, replaceUrl: true }) :
              this.router.navigate(['../../../../'], { relativeTo: this.route, replaceUrl: true });
          } else if (response.code == 103) {
            this.sharedService.quickbooksConnection(this.onClickCancel.bind(this));
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg');
        })
      }
    })
  }

  converBase64toBlob(content, contentType) {
    contentType = contentType || '';
    let sliceSize = 512;
    let byteCharacters = window.atob(content); //method which converts base64 to binary
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);
      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    let blob = new Blob(byteArrays, {
      type: contentType
    }); //statement which creates the blob
    return blob;
  }
}
