import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA, MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

import { SharedService } from 'app/shared/service/shared.service';
import { email } from '@rxweb/reactive-form-validators';

export interface EmailInterface {
  to: string[];
  cc: string[];
  subject: string;
  body: string;
  mergeDocs: boolean;
  attachmentList?: Object;
}

@Component({
  selector: 'app-invoice-mail',
  templateUrl: './invoice-mail.component.html',
  styleUrls: ['./invoice-mail.component.scss']
})
export class InvoiceMailComponent implements OnInit {

  public invoiceMailForm: FormGroup;

  @ViewChild("toEmailList") toEmailList;

  @ViewChild("chipList") chipList;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  public emailId: Array<string> = [];

  public toEmail: Array<string> = [];

  public sendMail: Array<Object> = [];

  constructor(
    public dialogRef: MatDialogRef<InvoiceMailComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    let emailStr: string =this.injectedData.to;
    let emailArr = [];
    emailStr.split(',').forEach((x,y)=>{
      if(x){
        emailArr.push(x)
      }
    });
    this.injectedData.to = emailArr.join(',');
    this.invoiceMailForm = new FormGroup({
      to: new FormControl(this.injectedData.to),
      cc: new FormControl(),
      subject: new FormControl(this.injectedData.subject),
      body: new FormControl(),
      invoice_pdf: new FormControl(true),
      prenote: new FormControl(this.injectedData.prefferedDocuments.includes('prenote')),
      proof_of_delivery: new FormControl(this.injectedData.prefferedDocuments.includes('proof_of_delivery')),
      outgate: new FormControl(this.injectedData.prefferedDocuments.includes('outgate')),
      ingate: new FormControl(this.injectedData.prefferedDocuments.includes('ingate')),
      bill_of_landing: new FormControl(this.injectedData.prefferedDocuments.includes('bill_of_landing')),
      miscellaneous: new FormControl(this.injectedData.prefferedDocuments.includes('miscellaneous')),
      hazmat: new FormControl(this.injectedData.prefferedDocuments.includes('hazmat')),
      chassis: new FormControl(this.injectedData.prefferedDocuments.includes('chassis')),
      interchange: new FormControl(this.injectedData.prefferedDocuments.includes('interchange')),
      scale_tickets: new FormControl(this.injectedData.prefferedDocuments.includes('scale_tickets')),
      incidents: new FormControl(this.injectedData.prefferedDocuments.includes('incidents'))
    });

    this.toEmail = this.injectedData.to ? this.injectedData.to.split(',') : [];
  }

  onAddEmailId(event: MatChipInputEvent, isTo: boolean): void {
    const input = event.input;
    const value = (event.value || '').trim();

    const email = new FormControl(value, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/));

    if (email.valid) {

      if (value) {
        isTo ? this.toEmail.push(value) : this.emailId.push(value);

        this.invoiceMailForm.get(isTo ? 'to' : 'cc').setErrors(null);
      }

      input ? input.value = '' : null;

    } else if (value) {

      this.invoiceMailForm.get(isTo ? 'to' : 'cc').setErrors({ emailList: true });

    }

    if (!value) {

      this.invoiceMailForm.get(isTo ? 'to' : 'cc').setErrors(null);

    }

    this.checkEmailValid(isTo, value);

  }

  onRemoveEmailId(email, isTo: boolean): void {
    const index = isTo ? this.toEmail.indexOf(email) : this.emailId.indexOf(email);
    if (index >= 0) {
      isTo ? this.toEmail.splice(index, 1) : this.emailId.splice(index, 1);
    }

    this.checkEmailValid(isTo);
  }

  checkEmailValid(isTo: boolean, value?: string) {

    if (isTo) {

      this.toEmail.length ? this.toEmail.forEach(temp => {
        if (!/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/.test(temp)) {
          this.invoiceMailForm.get('to').setErrors({ emailList: true });
        }
      }) : value ? null : this.invoiceMailForm.get('to').setErrors(null);

      this.toEmailList.errorState = this.invoiceMailForm.get('to').hasError('emailList');

      return
    }

    this.emailId.length ? this.emailId.forEach(temp => {
      if (!/^[a-zA-Z0-9._%+-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,}$/.test(temp)) {
        this.invoiceMailForm.get('cc').setErrors({ emailList: true });
      }
    }) : value ? null : this.invoiceMailForm.get('cc').setErrors(null);

    this.chipList.errorState = this.invoiceMailForm.get('cc').hasError('emailList');
  }

  onClickSendEmail(isMerged: boolean) {

    if (this.invoiceMailForm.valid && this.toEmail.length) {

      let request: EmailInterface = {
        to: this.toEmail,
        cc: this.emailId,
        subject: this.invoiceMailForm.get('subject').value,
        body: this.invoiceMailForm.get('body').value,
        mergeDocs: isMerged,
        attachmentList: {
          invoice_pdf: this.invoiceMailForm.get('invoice_pdf').value,
          prenote: this.invoiceMailForm.get('prenote').value,
          proof_of_delivery: this.invoiceMailForm.get('proof_of_delivery').value,
          outgate: this.invoiceMailForm.get('outgate').value,
          ingate: this.invoiceMailForm.get('ingate').value,
          bill_of_landing: this.invoiceMailForm.get('bill_of_landing').value,
          miscellaneous: this.invoiceMailForm.get('miscellaneous').value,
          hazmat: this.invoiceMailForm.get('hazmat').value,
          chassis: this.invoiceMailForm.get('chassis').value,
          interchange: this.invoiceMailForm.get('interchange').value,
          scale_tickets: this.invoiceMailForm.get('scale_tickets').value,
          incidents: this.invoiceMailForm.get('incidents').value
        }
      }

      if (this.invoiceMailForm.get('subject').value) {
        this.dialogRef.close(request);
      } else {
        let dialog = this.sharedService.openConfirmation({
          action: "mailWithoutSubject",
          name: null,
          cancelLabel: "No",
          confirmLabel: "Yes",
          confirmColor: "green"
        })
        dialog.subscribe(result => {
          if (result) {
            this.dialogRef.close(request);
          }
        })
      }
    } else {
      this.sharedService.openSnackbar("Plese Fill the Details Correctly", 5000, "warning-msg");
    }
  }

}
