import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from 'app/shared/demo.module';
import { EditAccessoriesComponent } from './edit-accessories.component';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    EditAccessoriesComponent
  ],
  entryComponents: [
    EditAccessoriesComponent
  ],
  exports: [
    EditAccessoriesComponent
  ]
})
export class AccessoriesSharedModule { }
