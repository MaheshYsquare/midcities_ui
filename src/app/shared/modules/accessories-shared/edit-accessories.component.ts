import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';

import { SharedService } from 'app/shared/service/shared.service';
import { AccessoriesData } from 'app/shared/models/accessories.model';

@Component({
  selector: 'app-edit-accessories',
  templateUrl: './edit-accessories.component.html',
  styleUrls: ['./edit-accessories.component.scss']
})
export class EditAccessoriesComponent implements OnInit {

  public preferredName: string = "fixed";

  public title: string;

  public orderFlag: boolean;

  public customerFlag: boolean;

  public editMode: boolean;

  public data: AccessoriesData;

  public accessorialCharges: string[];

  public accessorialChargesSpecificData: any;

  public disableAccessorialCharges: any = [];

  public userDetails: any;

  public accessorialData: any;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public customerId: number;

  @ViewChild('f') accessorialRateForm: NgForm

  constructor(public dialogRef: MatDialogRef<EditAccessoriesComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private sharedService: SharedService) {
    if (dialogData) {
      this.editMode = dialogData.modeSelect;
      this.data = dialogData.selectedValue;
      this.accessorialCharges = dialogData.accessorialCharges;
      this.accessorialChargesSpecificData = dialogData.accessorialChargesSpecificData;
      this.accessorialData = dialogData.accessorialData;
      this.title = dialogData.title;
      this.orderFlag = dialogData.orderFlag;
      this.customerFlag = dialogData.customerFlag;
      this.customerId = dialogData.customerId;
    }
  }

  ngOnInit() {
    this.initForm();

    if (this.orderFlag) {
      this.userDetails = this.sharedService.getUserDetails()
      if (this.userDetails.role === "Admin" || this.userDetails.role === "Order Entry" || this.userDetails.role === "Invoice") {
        this.accessorialCharges = this.dialogData.accessorialCharges;
      } else if (this.userDetails.role === "Dispatch") {
        this.accessorialCharges = ["Detention"]
      }
    }

    for (var i in this.accessorialData) {
      for (var j = 0; j < this.accessorialCharges.length; j++) {
        if (this.accessorialData[i].accessories_name === this.accessorialCharges[j]) {
          this.disableAccessorialCharges.push(this.accessorialData[i].accessories_name);
        }
      }
    }
  }

  private initForm() {
    if (this.editMode) {
      setTimeout(() => {
        this.accessorialRateForm.form.patchValue({
          'accessories_name': this.data.accessories_name,
          'accessories_interval': this.data.accessories_interval,
          'accessories_value': this.data.accessories_value,
          'description': this.data.description,
          'rate': this.data.rate
        })
      }, 100);

      this.preferredName = this.accessorialCharges && this.accessorialCharges.includes(this.data.accessories_name) ? 'fixed' : 'new';
    }
  }

  onRadioChange() {
    this.accessorialRateForm.form.patchValue({
      'accessories_name': null
    })
  }

  onOptionSelected(event) {
    var accessorialChargesSpecificData = this.accessorialChargesSpecificData.filter(data => data.accessories_name === event)
    var customerSpecific = accessorialChargesSpecificData.filter(temp => temp.customer_id !== 0)[0]
    var general = accessorialChargesSpecificData.filter(temp => temp.customer_id === 0)[0]
    if (customerSpecific) {
      this.accessorialRateForm.form.patchValue({
        'accessories_interval': customerSpecific.accessories_interval,
        'accessories_value': customerSpecific.accessories_value,
        'description': customerSpecific.description,
        'rate': customerSpecific.rate
      })
    } else if (general) {
      this.accessorialRateForm.form.patchValue({
        'accessories_interval': general.accessories_interval,
        'accessories_value': general.accessories_value,
        'description': general.description,
        'rate': general.rate
      })
    }
  }

  onSubmit() {
    if (this.accessorialRateForm.valid) {
      if (this.editMode) {
        var output = {
          'customer_id': this.customerId,
          'acc_charge_id': this.data.acc_charge_id,
          'accessories_name': this.accessorialRateForm.value.accessories_name,
          'accessories_interval': this.accessorialRateForm.value.accessories_interval,
          'accessories_value': this.accessorialRateForm.value.accessories_value,
          'description': this.accessorialRateForm.value.description,
          'rate': this.orderFlag || this.accessorialRateForm.value.accessories_interval !== '%' ?
            this.accessorialRateForm.value.rate : this.data.rate
        }
        this.dialogRef.close(output)
      } else {
        let request = {
          'customer_id': this.customerId,
          'acc_charge_id': null,
          'accessories_name': this.accessorialRateForm.value.accessories_name,
          'accessories_interval': this.accessorialRateForm.value.accessories_interval,
          'accessories_value': this.accessorialRateForm.value.accessories_value,
          'description': this.accessorialRateForm.value.description,
          'rate': this.orderFlag || this.accessorialRateForm.value.accessories_interval !== '%' ?
            this.accessorialRateForm.value.rate : '0'
        }
        this.dialogRef.close(request);
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
