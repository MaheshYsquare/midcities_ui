import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-upload-docs',
  templateUrl: './upload-docs.component.html',
  styleUrls: ['./upload-docs.component.scss']
})
export class UploadDocsComponent implements OnInit {

  public documents: { label: string, value: string, isUploaded: boolean, file_size?: any }[] = [
    { label: 'Prenote', value: 'prenote', isUploaded: false, file_size: 0 },
    { label: 'Proof of Delivery', value: 'proof_of_delivery', isUploaded: false, file_size: 0 },
    { label: 'Outgate', value: 'outgate', isUploaded: false, file_size: 0 },
    { label: 'Ingate', value: 'ingate', isUploaded: false, file_size: 0 },
    { label: 'Bill of Lading', value: 'bill_of_landing', isUploaded: false, file_size: 0 },
    { label: 'Miscellaneous', value: 'miscellaneous', isUploaded: false, file_size: 0 },
    { label: 'Hazmat', value: 'hazmat', isUploaded: false, file_size: 0 },
    { label: 'Chassis', value: 'chassis', isUploaded: false, file_size: 0 },
    { label: 'Interchange', value: 'interchange', isUploaded: false, file_size: 0 },
    { label: 'Scale tickets', value: 'scale_tickets', isUploaded: false, file_size: 0 },
    { label: 'Incidents', value: 'incidents', isUploaded: false, file_size: 0 }
  ];

  public fileUploadFormData: FormData = new FormData();

  public isFilesAdded: boolean = false;

  public uploadComponentName: string = "uploaddocumentlinks";

  public invoiceComponentName: string = "manualInvoices";

  public isDeleted: boolean = false;

  public tempFiles: any = {
    'prenote': null,
    'proof_of_delivery': null,
    'outgate': null,
    'ingate': null,
    'bill_of_landing': null,
    'miscellaneous': null,
    'hazmat': null,
    'chassis': null,
    'interchange': null,
    'scale_tickets': null,
    'incidents': null
  };

  public tempFileSize: any = {
    'prenote': 0,
    'proof_of_delivery': 0,
    'outgate': 0,
    'ingate': 0,
    'bill_of_landing': 0,
    'miscellaneous': 0,
    'hazmat': 0,
    'chassis': 0,
    'interchange': 0,
    'scale_tickets': 0,
    'incidents': 0,
    'total': 0
  };

  constructor(private dialogRef: MatDialogRef<UploadDocsComponent>,
    @Inject(MAT_DIALOG_DATA) private injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    if (this.injectedData.isManualInvoice && !this.injectedData.editMode) {

      this.documents.forEach(temp => {
        temp.isUploaded = this.injectedData.tempPreviewFiles && this.injectedData.tempPreviewFiles[temp.value] ?
          true : false;

        temp.file_size = this.injectedData.tempFileSize ? this.injectedData.tempFileSize[temp.value] : 0;
      });

      this.tempFiles = this.injectedData.tempPreviewFiles ? this.injectedData.tempPreviewFiles : this.tempFiles;

      this.tempFileSize = this.injectedData.tempFileSize ? this.injectedData.tempFileSize : this.tempFileSize;

      this.documents.some(temp => temp.isUploaded) ? this.documents.push({
        label: "Total File Size",
          value: "total_file",
          isUploaded: true,
          file_size: this.documents.map(temp => temp.file_size)
            .reduce((prev, curr) => prev + curr, 0)
      }) : null;

      this.fileUploadFormData = this.injectedData.formData ? this.injectedData.formData : this.fileUploadFormData;

      this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

    } else {

      this.injectedData && this.injectedData.uploadedDocumentList ?
        this.injectedData.uploadedDocumentList.forEach(element => {
          this.documents.forEach(temp => {
            if (element.includes(temp.value)) {
              temp.isUploaded = true;
            }
          })
        }) : null;

      this.documents.some(temp => temp.isUploaded) ? this.getFileSizeFromAws() : null;

    }

  }


  getFileSizeFromAws() {
    this.sharedService.getOrderFileSize(this.uploadComponentName, { fileName: this.injectedData.fileName })
      .subscribe(response => {
        this.injectedData.uploadedDocumentList.forEach(element => {
          this.documents.forEach(temp => {
            if (element.includes(temp.value)) {
              temp.file_size = response.data[element];
            }
          })
        });

        this.documents.push({
          label: "Total File Size",
          value: "total_file",
          isUploaded: true,
          file_size: this.documents.map(temp => temp.file_size)
            .reduce((prev, curr) => prev + curr, 0)
        })
      })
  }

  onChangeFileInput(event: any, documentName: string, fileInput: any): void {

    if (event.target.files && event.target.files.length) {

      if (event.target.files[0].size <= 5000000) {

        let fileExtension = event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1].toLowerCase();

        if (fileExtension === 'pdf' || fileExtension === 'png' || fileExtension === 'jpg' || fileExtension === 'jpeg') {

          this.removeExistingOrUnwantedFile(documentName);

          this.addTempFilesPreview(documentName, event.target.files[0]);

          this.fileUploadFormData.append("files", event.target.files[0], `${documentName}.${fileExtension}`);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        } else {

          fileInput.value = null;

          this.removeExistingOrUnwantedFile(documentName);

          this.addTempFilesPreview(documentName, null);

          this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

          this.sharedService.openSnackbar("File Format Must be pdf,jpg,png", 5000, "warning-msg");
        }

      } else {
        fileInput.value = null;

        this.removeExistingOrUnwantedFile(documentName);

        this.addTempFilesPreview(documentName, null);

        this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

        this.sharedService.openSnackbar("File Size Must be less than 5MB", 5000, "warning-msg");
      }
    }
  }

  removeExistingOrUnwantedFile(documentName: string) {
    let fileIndex = this.fileUploadFormData.getAll("files").findIndex((temp: any) => temp.name.includes(documentName));

    if (fileIndex !== -1) {
      let filesList = this.fileUploadFormData.getAll("files");

      filesList.splice(fileIndex, 1);

      this.fileUploadFormData.delete("files");

      filesList.forEach(temp => {
        this.fileUploadFormData.append("files", temp);
      })
    }
  }

  addTempFilesPreview(documentName: string, selectedFile: any) {
    let reader = new FileReader();


    if (selectedFile) {
      reader.readAsArrayBuffer(selectedFile);

      reader.onload = () => {
        let utf = new Uint8Array(reader.result as ArrayBuffer);
        let file = new Blob([utf], { type: selectedFile.type });
        let fileURL = URL.createObjectURL(file);

        this.tempFiles[documentName] = fileURL;
        this.tempFileSize[documentName] = selectedFile.size;
      };

      reader.onerror = function () {
        console.log(reader.error);
      };


    } else {
      this.tempFiles[documentName] = null;
      this.tempFileSize[documentName] = 0;
    }

  }

  onClickViewFile(documentName: string) {

    if (this.injectedData.isManualInvoice && !this.injectedData.editMode) {

      window.open(this.injectedData.tempPreviewFiles[documentName]);

    } else if (this.injectedData.isManualInvoice && this.injectedData.editMode) {

      this.injectedData.uploadedDocumentList.forEach(element => {
        if (element.includes(documentName)) {
          this.sharedService.getManualInvoiceFile(this.invoiceComponentName, { fileName: element }).subscribe(response => {
            let utf = new Uint8Array(response.buffer.data);
            let file = new Blob([utf], { type: response.ContentType });
            let fileURL = URL.createObjectURL(file);
            window.open(fileURL);
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          })
        }
      });

    } else {
      this.injectedData.uploadedDocumentList.forEach(element => {
        if (element.includes(documentName)) {
          this.sharedService.getOrderFile(this.uploadComponentName, { fileName: element }).subscribe(response => {
            let utf = new Uint8Array(response.buffer.data);
            let file = new Blob([utf], { type: response.ContentType });
            let fileURL = URL.createObjectURL(file);
            window.open(fileURL);
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
          })
        }
      });
    }

  }

  onClickDeleteFile(documentName: string) {

    if (this.injectedData.isManualInvoice && !this.injectedData.editMode) {

      let documentsIndex = this.documents.findIndex(temp => temp.value === documentName);

      this.documents[documentsIndex].isUploaded = false;

      this.documents[this.documents.length - 1].file_size = this.documents[this.documents.length - 1].file_size - this.documents[documentsIndex].file_size;

      this.injectedData.tempPreviewFiles[documentName] = null;

      this.tempFiles[documentName] = null;

      this.tempFileSize[documentName] = 0;

      this.documents.some(temp => temp.isUploaded && temp.value !== 'total_file') ? null : this.documents.pop();

      this.removeExistingOrUnwantedFile(documentName);

      this.isFilesAdded = this.fileUploadFormData.getAll("files").length !== 0;

      this.isDeleted = true;

    } else if (this.injectedData.isManualInvoice && this.injectedData.editMode) {

      this.injectedData.uploadedDocumentList.forEach(element => {
        if (element.includes(documentName)) {
          let request = {
            'fileName': element,
            'id': this.injectedData.manual_invoice_id
          }
          this.sharedService.deleteManualInvoiceFiles(this.invoiceComponentName, request)
            .subscribe(response => {
              let documentsIndex = this.documents.findIndex(temp => temp.value === documentName);

              this.documents[documentsIndex].isUploaded = false;

              this.documents[this.documents.length - 1].file_size = this.documents[this.documents.length - 1].file_size - this.documents[documentsIndex].file_size;

              this.documents[documentsIndex].file_size = 0;

              this.documents.some(temp => temp.isUploaded && temp.value !== 'total_file') ? null : this.documents.pop();

              this.isDeleted = true;
            }, error => {
              this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
            });
        }
      });

    } else {
      this.injectedData.uploadedDocumentList.forEach(element => {
        if (element.includes(documentName)) {
          let request = {
            'fileName': element,
            'order_container_chassis_id': this.injectedData.containerLevelId
          }
          this.sharedService.deleteOrderFile(this.uploadComponentName, request)
            .subscribe(responseData => {
              let documentsIndex = this.documents.findIndex(temp => temp.value === documentName);

              this.documents[documentsIndex].isUploaded = false;

              this.isDeleted = true;
            }, error => {
              this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
            });
        }
      });
    }
  }

  onClickCancel() {
    this.dialogRef.close({
      isDeleted: this.isDeleted,
      cancel: true,
      fileFormData: this.fileUploadFormData,
      tempPreviewFiles: this.tempFiles,
      tempFileSize: this.tempFileSize
    });
  }

  onClickSave() {
    if (this.isFilesAdded) {
      this.dialogRef.close({
        fileFormData: this.fileUploadFormData,
        tempPreviewFiles: this.tempFiles,
        tempFileSize: this.tempFileSize,
        isDeleted: false,
        cancel: false
      });
    }
  }

}
