import { Component, OnInit, Input, ChangeDetectionStrategy, OnDestroy } from '@angular/core';

import { SharedService } from 'app/shared/service/shared.service';
import { NotesData, NotesResponse } from 'app/shared/models/notes.model';
import { loginData } from 'app/session/session.model';
import { MatSnackBarRef } from '@angular/material';
import { SnackbarComponent } from 'app/shared/modules/helper/snackbar/snackbar.component';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotesComponent implements OnInit, OnDestroy {

  @Input() orderContainerChassisId: number;

  public componentName: string = "noteshistories";

  public user: loginData;

  public notesLabel: any = {
    "A": 'Order Notes',
    "B": 'Dispatch Notes',
    "C": 'Invoice Notes'
  }

  public locationRef: string;

  public notes: any = {};

  public selectedNotesId: number = 0;

  public isArchivedMode: boolean;

  public notesValue: string;

  public init: boolean = true;

  public snackbarRef: MatSnackBarRef<SnackbarComponent>;

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.user = this.sharedService.getUserDetails();

    this.isArchivedMode = window.location.href.includes('archived');

    this.locationRef = window.location.href;

    this.getNotesFromService();
  }

  ngOnDestroy() {
    this.snackbarRef ? this.snackbarRef.dismiss() : null;
  }

  getNotesFromService() {
    this.sharedService.getNotes(this.componentName, this.orderContainerChassisId).subscribe((response: NotesResponse[]) => {
      this.selectedNotesId = 0;
      this.notes = response;

      if (response &&
        (response["A"].length ||
          response["B"].length ||
          response["C"].length) &&
        this.init &&
        !this.sharedService.isNewOrderCreated) {

        this.snackbarRef = this.sharedService.openSnackbarWithAction("There are notes for this Order. Please review",
          0, "warning-msg", true, 'close');

      }

      this.init = false;
      this.sharedService.isNewOrderCreated = false;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
    })
  }

  onClickAddNotes() {
    let notes_description = this.locationRef.includes('order') ? "order_notes" :
      (this.locationRef.includes('dispatch') ? 'dispatch_notes' : 'invoice_notes');

    let objectName = this.locationRef.includes('order') ? "A" :
      (this.locationRef.includes('dispatch') ? 'B' : 'C');

    let notesObject = {
      created_by: this.user.first_name + " " + this.user.last_name,
      notes: null,
      notes_description,
      order_container_chassis_id: this.orderContainerChassisId,
      notes_id: null
    }

    this.notesValue = null;

    this.notes[objectName].push(notesObject);
  }

  onSaveNotes(selectedNote: NotesData) {
    if (this.notesValue && this.notesValue.trim()) {

      let request = {
        'notes': this.notesValue,
        'notes_description': selectedNote.notes_description,
        'created_by': selectedNote.created_by,
        'order_container_chassis_id': this.orderContainerChassisId,
        'user_id': this.user.userId
      }
      this.sharedService.postNotes(this.componentName, request).subscribe(response => {
        this.getNotesFromService()
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      })

    } else {
      this.sharedService.openSnackbar("Please enter notes to proceed", 2500, 'warning-msg');
    }
  }

  onCancelNotes(i) {
    let objectName = this.locationRef.includes('order') ? "A" :
      (this.locationRef.includes('dispatch') ? 'B' : 'C');

    this.notes[objectName].splice(i, 1);
  }

  onSaveEditNotes(selectedNote: NotesData) {
    if (this.notesValue && this.notesValue.trim()) {
      let request = {
        'notes': this.notesValue,
        'notes_description': selectedNote.notes_description,
        'lastupdated_by': this.user.first_name + ' ' + this.user.last_name,
        'notes_id': selectedNote.notes_id,
        'order_container_chassis_id': this.orderContainerChassisId
      }
      this.sharedService.editNotes(this.componentName, request).subscribe(response => {
        this.getNotesFromService();
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      })
    }

  }

  onDeleteNotes(notesId: number) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Note',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })

    dialogRef.subscribe(result => {
      if (result) {
        this.sharedService.deleteNotes(this.componentName, notesId, this.orderContainerChassisId).subscribe(response => {
          this.getNotesFromService()
        }, error => {
          this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
        })
      }
    })
  }
}
