import { Component, OnInit, Inject } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  public filterForm: FormGroup;

  public selectedFilterColumns: string[] = [];

  public selectDropdown: any[] = [];

  public allDropdown: any;

  constructor(public dialogRef: MatDialogRef<FilterComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    let filters = new FormArray([]);

    if (this.injectedData.filterData) {

      this.injectedData.filterData.forEach((element, i) => {
        filters.push(
          this.formFilterFormGroup(element.column,
            element.values,
            element.inputType)
        );
        this.onColumnSelectionChange(element.column, i, true, true);
      })

    } else {
      filters = new FormArray([
        this.formFilterFormGroup(null, null, 'text')
      ])
    }

    this.filterForm = new FormGroup({
      'filters': filters
    })
  }

  /**
   * formFilterFormGroup
   * to form filter formgroup for form array
   */
  public formFilterFormGroup(column, value, inputType) {
    return new FormGroup({
      'column': new FormControl(column, Validators.required),
      'values': new FormControl(value, Validators.required),
      'inputType': new FormControl(inputType)
    })
  }

  /**
   * onSubmit
   */
  public onSubmit() {
    if (this.filterForm.valid) {
      this.dialogRef.close(this.filterForm.get('filters').value);
    }
  }

  /**
   * onClickAddFilter
   * trigger when add filter btn is clicked
   */
  public onClickAddFilter() {

    if (this.filterForm.valid) {

      (<FormArray>this.filterForm.get('filters'))
        .push(this.formFilterFormGroup(null, null, 'text'))

      return;
    }

    this.sharedService.openSnackbar("Please Fill the details to add new filter", 2500, 'waring-msg');

  }

  /**
   * getFilter
   */
  public getFilter() {
    return (<FormArray>this.filterForm.get('filters')).controls
  }

  /**
   * onColumnSelectionChange
   * triggers when the column dropdown selected
   */
  public onColumnSelectionChange(selectedValue: string, index: number, init: boolean, isChangeManual: boolean) {

    !isChangeManual ? (<FormArray>this.filterForm.get('filters')).controls[index]
      .get('values').setValue(null) : null;

    this.selectedFilterColumns[index] = selectedValue;

    let filterColumn = this.injectedData.filterColumns.find(temp => temp.value === selectedValue);

    if (filterColumn) {

      !init ? (<FormArray>this.filterForm.get('filters')).controls[index]
        .get('inputType').setValue(filterColumn.inputType) : null;

      if (filterColumn.isDefaultDropdown) {
        this.setDefaultDropdownValues(selectedValue, index);

        return;
      }

      if (filterColumn.inputType === 'text' || filterColumn.inputType === 'date') {
        return;
      }

      if (!this.allDropdown) {

        this.sharedService.getAllDropdown("configurations", "orders,container,orderflags,tags,leg")
          .subscribe(response => {
            this.allDropdown = response;

            if (this.injectedData.isInvoiceMgmt) {
              this.allDropdown['orders'].push({
                label: "Manual Invoice",
                value: "Manual Invoice"
              })
            }

            this.selectDropdown[index] = response[filterColumn.dropdown]
              .map(temp => {
                return {
                  label: temp.label,
                  value: temp.label
                }
              });

          }, error => {
            this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
          })

        return;
      }

      this.selectDropdown[index] = this.allDropdown[filterColumn.dropdown].map(temp => {
        return {
          label: temp.label,
          value: temp.label
        }
      });

    }

  }

  /**
   * setDefaultDropdownValues
   */
  public setDefaultDropdownValues(value: string, i: number) {

    switch (value) {
      case "hazmat_req":
        this.selectDropdown[i] = [
          { label: 'Y', value: '1' },
          { label: 'N', value: '0' }
        ];
        break;

      case 'container_size':
        this.selectDropdown[i] = [
          { label: '20', value: '20' },
          { label: '28', value: '28' },
          { label: '40', value: '40' },
          { label: '45', value: '45' },
          { label: '53', value: '53' },
          { label: '40H', value: '40H' },
          { label: '45H', value: '45H' }
        ]

        break;
      case 'triaxle':
        this.selectDropdown[i] = [
          { label: 'Y', value: 'Y' },
          { label: 'N', value: 'N' }
        ]
        break;

      case 'quickbook_status':

        this.selectDropdown[i] = [
          { label: "Paid", value: "Paid" },
          { label: "Unpaid", value: "Unpaid" },
          { label: "Partial", value: "Partial" }
        ]

        break;

      case 'order_status':

        this.selectDropdown[i] = this.injectedData.isArchived ?
          [
            { label: 'Delivered', value: 'Delivered' },
            { label: 'Cancelled', value: 'Cancelled' }
          ] :
          [
            { label: 'Open', value: 'Open' },
            { label: 'Assigned', value: 'Assigned' },
            { label: 'Picked Up', value: 'Picked Up' },
            { label: 'Delivered', value: 'Delivered' }
          ];

        break;
      case 'driver_status':
        this.selectDropdown[i] = [
          { label: 'Assigned', value: 'Assigned' },
          { label: 'Available', value: 'Available' },
          { label: 'In Route', value: 'In Route' }
        ];

        break;

      case 'leg_status':
        this.selectDropdown[i] = [
          { label: 'Open', value: 'Open' },
          { label: 'Assigned', value: 'Assigned' },
          { label: 'Picked Up', value: 'Picked Up' },
          { label: 'Delivered', value: 'Delivered' }
        ];

        break;

      case 'user_status':
        this.selectDropdown[i] = [
          { label: 'Active', value: 'active' },
          { label: 'Inactive', value: 'inactive' }
        ];

        break;

      case "is_paid":
        this.selectDropdown[i] = [
          { label: 'Y', value: 1 },
          { label: 'N', value: 0 }
        ];
        break;

      default:
        break;
    }

  }

  /**
   * onClickDelete
   */
  public onClickDelete(index: number) {
    this.selectedFilterColumns.splice(index, 1);

    (<FormArray>this.filterForm.get('filters')).removeAt(index);

    this.filterForm.get('filters').value.forEach((element, i) => {
      this.onColumnSelectionChange(element.column, i, false, true);
    });
  }

}
