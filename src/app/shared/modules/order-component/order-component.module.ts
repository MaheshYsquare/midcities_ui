import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OWL_DATE_TIME_FORMATS,
    DateTimeAdapter,
    OWL_DATE_TIME_LOCALE
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

import { NgxFilesizeModule } from 'ngx-filesize';

import { EditOrderComponent } from './edit-order/edit-order.component';
import { AssignDriverComponent } from './assigndriver/assigndriver.component';
import { PrintLoadSheetComponent } from './print-load-sheet/print-load-sheet.component';
import { FilterComponent } from './filter/filter.component';
import { NotesComponent } from './notes/notes.component';
import { EditLegComponent } from './edit-leg/edit-leg.component';
import { CustomerSharedModule } from '../customer-shared/customer-shared.module';
import { AccessoriesSharedModule } from '../accessories-shared/accessories-shared.module';
import { ManageColumnsComponent } from './manage-columns/manage-columns.component';
import { UploadDocsComponent } from './upload-docs/upload-docs.component';
import { MaterialModule } from 'app/shared/demo.module';
import { LocationSharedModule } from '../location-shared/location-shared.module';
import { MY_CUSTOM_FORMATS } from 'app/shared/models/shared.model';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        LocationSharedModule,
        CustomerSharedModule,
        AccessoriesSharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        NgxFilesizeModule
    ],
    declarations: [
        EditOrderComponent,
        EditLegComponent,
        AssignDriverComponent,
        PrintLoadSheetComponent,
        FilterComponent,
        NotesComponent,
        ManageColumnsComponent,
        UploadDocsComponent
    ],
    entryComponents: [
        EditLegComponent,
        AssignDriverComponent,
        PrintLoadSheetComponent,
        FilterComponent,
        ManageColumnsComponent,
        UploadDocsComponent
    ],
    exports: [
        CommonModule,
        MaterialModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        EditOrderComponent,
        EditLegComponent,
        AssignDriverComponent,
        PrintLoadSheetComponent,
        FilterComponent,
        NotesComponent,
        UploadDocsComponent
    ],
    providers: [
        { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
    ]
})
export class OrderSharedModule { }
