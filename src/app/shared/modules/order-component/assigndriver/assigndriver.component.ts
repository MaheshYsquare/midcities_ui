import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {
  MatTableDataSource,
  MatSort,
  MatDialogRef,
  MatPaginator,
  MAT_DIALOG_DATA
} from '@angular/material';

import { SharedService } from 'app/shared/service/shared.service';
import {
  DriverDetailsPostRequest,
  DriverDetailsData,
  DriverDetailsResponse
} from 'app/driver/DriverData';

@Component({
  selector: 'app-assigndriver',
  templateUrl: './assigndriver.component.html',
  styleUrls: ['./assigndriver.component.scss']
})
export class AssignDriverComponent implements OnInit {

  public componentName: String = "drivers"; // api component name for api fetch

  public searchFilterEvent: boolean = false; // filter flag whether search filter is enabled

  public searchFilterValue: string = ""; // store search filter value

  /* table source,sort,pagination and cache data fetch variable  */
  public driverDetailsDatasource: MatTableDataSource<DriverDetailsData>;

  public driverDetailsColumns: string[] = ['select', 'driver_name', 'current_order', 'driver_status',
    'driver_type', 'pu_name', 'dl_name'];

  public currentPageIndex: number = 0;

  public offset: number = 1;

  public limit: number = 5;

  public sortDirection: string;

  public sortColumn: string;

  public pageLength: number = 0;

  @ViewChild('driverDetailSort') driverDetailSort: MatSort;

  @ViewChild('driverDetailPaginator') driverDetailPaginator: MatPaginator;

  public selectedDriver: DriverDetailsData;

  public enabledColor = '#2196f3';

  public disabledColor = '#2196f391';

  constructor(public dialogRef: MatDialogRef<AssignDriverComponent>,
    private sharedService: SharedService,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    this.driverDetailsDatasource = new MatTableDataSource();

    this.getDriverDetailsDataFromService();
  }

  getDriverDetailsDataFromService() {
    let request = this.generateRequest()
    this.sharedService.getDriverDetails(this.componentName, request).subscribe((response: DriverDetailsResponse) => {
      this.driverDetailsDatasource.data = response.data.driverData;
      this.pageLength = response.data.driverPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
    })
  }

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilterEvent = true;
      this.initialPage();
      this.getDriverDetailsDataFromService();
    } else {
      this.searchFilterEvent = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getDriverDetailsDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilterEvent = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getDriverDetailsDataFromService();
  }

  onChangeSortDirection(event) {
    this.sortDirection = event.direction == '' ? null : event.direction;
    this.sortColumn = event.active == 'dl_date' ? 'dl_time' : event.active;
    this.getDriverDetailsDataFromService();
  }

  onChangeRadioButton(selectedRow: DriverDetailsData) {
    this.selectedDriver = selectedRow;
  }

  onSubmit() {
    if (this.injectedData.isHazmat && !this.selectedDriver.hazmat_certified) {
      let dialogRef = this.sharedService.openConfirmation(
        {
          action: "hazmatCertifiedValidation",
          name: this.selectedDriver.driver_name,
          cancelLabel: "No",
          confirmColor: "#4d97f3",
          confirmLabel: "Yes"
        })
      dialogRef.subscribe(result => {
        if (result) {
          this.dialogRef.close(this.selectedDriver);
        }
      });
    } else {
      this.dialogRef.close(this.selectedDriver);
    }
  }

  onChangePage(event) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);
    switch (event.pageSize) {
      case 3:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverDetailsDataFromService()
        break;
      case 4:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverDetailsDataFromService()
        break;
      case 5:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverDetailsDataFromService()
        break;
    }
  }

  generateRequest() {
    let request: DriverDetailsPostRequest = {
      offset: this.offset,
      limit: this.limit,
      role: null,
      userId: null,
      searchFilter: this.searchFilterEvent,
      searchFilterValue: this.searchFilterValue,
      columnFilter: false,
      filterData: null,
      column: this.sortColumn,
      direction: this.sortDirection,
      isNotAllDriver: this.injectedData.driverId !== null ? true : false,
      driverId: this.injectedData.driverId,
      isInactiveIncluded: false,
      isSortByDriver: true
    }
    return request
  }

  initialPage() {
    this.driverDetailPaginator.pageIndex = 0;
    this.driverDetailPaginator.pageSize = this.driverDetailPaginator.pageSize ? this.driverDetailPaginator.pageSize : 5;
    this.currentPageIndex = 0
    this.offset = 1;
    this.limit = this.driverDetailPaginator.pageSize ? this.driverDetailPaginator.pageSize : 5;
  }

}
