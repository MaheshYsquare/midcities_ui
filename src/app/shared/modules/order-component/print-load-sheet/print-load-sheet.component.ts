import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as moment from 'moment';

import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-print-load-sheet',
  templateUrl: './print-load-sheet.component.html',
  styleUrls: ['./print-load-sheet.component.scss']
})

export class PrintLoadSheetComponent implements OnInit {

  public selectedorder: any;

  public selectedleg: any;

  public senderAddress: any;

  public driverComponentName: string = 'drivers';

  public driverNotes: string[] = [];

  constructor(public dialogRef: MatDialogRef<PrintLoadSheetComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private sharedService: SharedService) {
    this.selectedleg = data.selectedleg;
    this.selectedorder = data.selectedorder;
    this.senderAddress = data.senderAddress;
  }

  ngOnInit() {
    this.driverNotes.push(this.selectedorder.driver_notes, this.selectedorder.dispatch_driver_notes);

    this.driverNotes = this.driverNotes.filter(temp => temp !== null && temp !== '');
  }

  onClickPrint() {
    let printContents, popupWin;
    printContents = document.getElementById('printablediv').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Pickup/Delivery Receipt</title>
          <style>
        
          
.logo {
  width: 30%;
  min-width: 270px;
  min-height: 150px;
  margin-right: 1rem;
  margin-left: 2rem;
}

.address {
  display: inline-block;
  width: 35%;
  vertical-align: bottom;
}

.midcities {
  vertical-align: middle;
}

table.remarks {
  width: 50%
}

table.signature {
  width: 50%
}

table {
  border-collapse: collapse;
  width: 100%;
}

th {
  border: 1px solid #ffffff;
  text-align: left;
}

td {
  border: 1px solid #ffffff;
  text-align: left;
}

          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  onClickEmail() {
    if (this.senderAddress !== null) {
      let driver_notes = this.driverNotes.map(temp => {
        return {
          note: temp
        }
      })

      let input = {
        senderAddress: this.senderAddress,
        driverLoadsheet: {
          order_number: `${this.selectedorder.order_number}/${this.selectedorder.container_sequence}`,
          shipping_ref: this.selectedorder.pu_ref,
          client_ref: this.selectedorder.dl_ref,
          client_code: this.selectedorder.business_name.slice(0, 3),
          client_name: this.selectedorder.business_name,
          size: this.selectedorder.container_size,
          type: this.selectedorder.container_type,
          weight: this.selectedorder.weight,
          chassis: this.selectedorder.chassis_number,
          booking_number: this.selectedorder.booking_number,
          driver_name: this.selectedleg.is_brokered ?
            `${this.selectedleg.carrier_name} (Carrier)` : this.selectedleg.driver_name,
          leg: this.selectedleg.leg_number,
          container_number: this.selectedleg.container_number,
          leg_type: this.selectedleg.leg_type,

          pickup_date: this.selectedleg.pickup_time ?
            moment(this.selectedleg.pickup_time).format('M/D/YY HH:mm') :
            ((this.selectedleg.pu_time && this.selectedleg.pu_time_to) ?
              `${moment(this.selectedleg.pu_time).format('M/D/YY HH:mm')} ~ ${moment(this.selectedleg.pu_time_to).format('M/D/YY HH:mm')}` : null),

          pickup: this.selectedleg.pu_loc,
          pickup_ref: this.selectedorder.pu_ref,

          delivery_date: this.selectedleg.delivery_time ?
            moment(this.selectedleg.delivery_time).format('M/D/YY HH:mm') :
            ((this.selectedleg.dl_time && this.selectedleg.dl_time_to) ?
              `${moment(this.selectedleg.dl_time).format('M/D/YY HH:mm')} ~ ${moment(this.selectedleg.dl_time_to).format('M/D/YY HH:mm')}` : null),

          delivery: this.selectedleg.dl_loc,
          driver_notes,
          pickup_name: this.selectedleg.pu_name,
          delivery_name: this.selectedleg.dl_name,
          pu_street: this.selectedleg.pu_street,
          pu_state: this.selectedleg.pu_state,
          pu_city: this.selectedleg.pu_city,
          pu_zip: this.selectedleg.pu_postal_code,
          dl_street: this.selectedleg.dl_street,
          dl_state: this.selectedleg.dl_state,
          dl_city: this.selectedleg.dl_city,
          dl_zip: this.selectedleg.dl_postal_code,
          cust_ref: this.selectedorder.customer_reference,
          order_type: this.selectedorder.order_type,
          container: this.selectedorder.container_name
        }
      }

      this.sharedService.sendDriverLoadsheetEmail(this.driverComponentName, input).subscribe(response => {
        this.sharedService.openSnackbar(response, 2000, "success-msg");
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
      })
    } else {
      this.sharedService.openSnackbar("Driver Mail id is not set", 2000, "warning-msg");
    }
  }

  close() {
    this.dialogRef.close()
  }

}
