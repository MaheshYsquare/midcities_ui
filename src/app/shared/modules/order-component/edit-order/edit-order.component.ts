import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data, Router } from "@angular/router";
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";

import { OrderData } from "app/shared/models/order.model";
import { SharedService } from "app/shared/service/shared.service";
import { DropdownData } from "app/shared/models/dropdown.model";
import { CustomerData } from "app/shared/models/customer.model";
import { OrderService } from "app/order/order.service";
import { LocationData } from "app/shared/models/location.model";
import { feildsMatched } from 'app/shared/utils/app-validators';
import { loginData } from "app/session/session.model";
import { SocketService } from "app/shared/service/socket.service";

@Component({
  selector: "app-edit-order",
  templateUrl: "./edit-order.component.html",
  styleUrls: ["./edit-order.component.scss"]
})
export class EditOrderComponent implements OnInit {
  public loggedInUser: loginData; // to store the current logged in user data

  public orderComponentName: string = "orders" // api endpoint for post & edit

  public dropdownComponentName: string = "configurations"; // api endpoint for dropdown

  public editMode: boolean; // flag which used to determine whether the screen is editable or creatable

  public draftMode: boolean; // flag which used to determine whether the screen is opening of existing saved one

  public isInvoiceMode: boolean; // flag which used to determine whether the screen is opening of invoicing edit

  public selectedOrder: OrderData; // object type variable to store existing data to prefill in edit or draft mode

  public orderSequence: { order_number: number; invoice_number: number }; // retrieve next order sequence gonna create

  public orderForm: FormGroup; // variable for reactive form

  public orderTypes: DropdownData['orders']; // dropDown list for the order type

  public customerAutoSuggestions: CustomerData[]; //autosuugesstion list for validation

  public truckMoveHideOrShowElement: boolean = true; // flag which decide to show element on order type truck move

  public locationTypes: DropdownData['location']; // dropDown list for the location type

  public tags: DropdownData['tags'] = []; // dropDown list for the tags

  public formatTypes: string[] = ["P2P", "C2C", "P2C", "Z2Z", "S2S", "P2S", "S2P", "C2P"]; // address format type

  public addressObject: {
    p_city?: string;
    p_postal_code?: any;
    d_city?: string;
    d_postal_code?: any;
    p_phone?: string;
    p_email?: any;
    d_phone?: string;
    d_email?: any;
  } = {}; // to store the city and postal code of pickup and delivery

  public isPickupInitEmit: boolean = false; // flag to get the pickup address object from server initially

  public pickupLocationData: LocationData; // to store the selected object of pickup address

  public pickupFormatedLoc: string; // to store pickup formated location

  public pickupHint: boolean = false; // flag to show or hide the hint for the date format in pickup date & time

  public isDeliveryInitEmit: boolean; // flag to get the delivery address object from server initially

  public deliveryLocationData: LocationData; // to store the selected object of delivery address

  public deliveryFormatedLoc: string; // to store delivery formated location

  public deliveryHint: boolean = false; // flag to show or hide the hint for the date format in delivery date & time

  public sizes: string[] = ["20", "28", "40", "45", "53", "40H", "45H"]; // list of container size

  public containerTypes: DropdownData['container']; // dropDown list for the container type

  public isBrokeredOrder: boolean[] = []; // flag used for hide or display carrier and carrier rate

  public carriers: { carrier_id: number; carrier_name: string }[];

  public isDispatch: boolean = false; // flag to check the order is in dispatch or pending on Edit order

  public isRedirectToLeg: boolean = true; // flag which determine whether the order redirect to leg or not

  public isOrderTypeChanged: boolean = false; // flag which figure that order type changed or not on edit screen

  public routeDetails: { rate?: any; distance?: any; rateType?: any, isRateNeedToCreate?: boolean, route_rate_id?: any }; // to store fetched routes

  constructor(private routes: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService,
    private orderService: OrderService,
    private socketService: SocketService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {

    /* by using the activated routes can check the current route based on that flag get updated */
    this.editMode = this.routes.snapshot.url.length > 1 ?
      (this.routes.snapshot.url[2].path === "edit" ? true : false) : false;

    this.draftMode = this.routes.snapshot.url.length > 1 ?
      (this.routes.snapshot.url[2].path === "draft" ? true : false) : false;

    this.isInvoiceMode = (window.location.href.includes('invoice-management') ||
      window.location.href.includes('customer'));

    /* when edit or draft using resolve subscription to get the data that need to prefill */
    if (this.editMode || this.draftMode || this.isInvoiceMode) {
      this.routes.data.subscribe((response: Data) => {

        this.selectedOrder = response['editOrder'].length !== 0
          ? response['editOrder'][0] : this.router.navigate(['/error/404']);

        if (this.selectedOrder) {
          this.isDispatch = this.selectedOrder.is_dispatch === 1;
        }
      })
    } else if (!this.editMode && !this.draftMode) { // retrieve the order number sequence from db that gonna get created
      this.routes.data.subscribe((data: Data) => {
        data['sequence'].length !== 0 ? this.orderSequence = data['sequence'][0]
          : { order_number: null, invoice_number: null };
      })
    }

    this.loggedInUser = this.sharedService.getUserDetails(); // retreive logged on user data

    /* To Call neccessary api for the forms */
    this.getAllDropdownFromService();

    this.initForm(); // to initiate form and to prefill existing data for edit

    if (this.editMode) {
      (<any>Object).values(this.orderForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    if (this.editMode || this.draftMode) {
      this.onChangeIsBrokeredCheckbox(this.selectedOrder.is_brokered ? true : false, true, 0);
    }

    if (!this.draftMode && !this.editMode && this.orderService.isOrderCopied) {
      this.onChangeIsBrokeredCheckbox(this.orderService.copiedOrder.is_brokered ? true : false, true, 0);
    }
  }

  private initForm() {
    let order_type = null;
    let order_type_id = null;
    let customer_name = null;
    let customer_id = null;
    let customer_reference = null;
    let booking_number = null;
    let rail_cut = null;
    let tags = null;
    let format_address = null;
    let pu_name = null;
    let pu_loc = null;
    let est_pickup_time = null;
    let dl_name = null;
    let dl_loc = null;
    let est_delivery_time = null;
    let order_container_chassis = new FormArray([this.initFormArray(true)]); // to initiate form array
    let order_notes = null;
    let driver_notes = null;

    /* assign a value when the screen is in edit mode */
    if ((this.editMode || this.draftMode || this.isInvoiceMode) && this.selectedOrder) {

      order_type = this.selectedOrder.order_type;
      order_type_id = this.selectedOrder.order_type_id;
      customer_name = this.selectedOrder.business_name;
      customer_id = this.selectedOrder.customer_id;
      customer_reference = this.selectedOrder.customer_reference;
      booking_number = this.selectedOrder.booking_number;
      rail_cut = this.selectedOrder.rail_cut;

      tags = this.selectedOrder.tag_id !== null ?
        this.selectedOrder.tag_id.split(',').map(temp => +temp) : null;

      format_address = this.selectedOrder.format_address;
      pu_name = this.selectedOrder.pu_name;
      pu_loc = this.selectedOrder.pu_loc;
      est_pickup_time = this.selectedOrder.est_pickup_from_time &&
        this.selectedOrder.est_pickup_to_time ?
        [
          new Date(this.selectedOrder.est_pickup_from_time),
          new Date(this.selectedOrder.est_pickup_to_time)
        ] : null;
      dl_name = this.selectedOrder.dl_name;
      dl_loc = this.selectedOrder.dl_loc;
      est_delivery_time = this.selectedOrder.est_delivery_from_time &&
        this.selectedOrder.est_delivery_to_time ?
        [
          new Date(this.selectedOrder.est_delivery_from_time),
          new Date(this.selectedOrder.est_delivery_to_time)
        ] : null;

      order_notes = this.selectedOrder.order_notes;
      driver_notes = this.selectedOrder.driver_notes;

      this.isPickupInitEmit = true;
      this.isDeliveryInitEmit = true;

      this.addressObject = {
        p_city: this.selectedOrder.p_city,
        p_postal_code: this.selectedOrder.p_zip,
        d_city: this.selectedOrder.d_city,
        d_postal_code: this.selectedOrder.d_zip,
        p_phone: this.selectedOrder.p_phone,
        p_email: this.selectedOrder.p_email,
        d_phone: this.selectedOrder.d_phone,
        d_email: this.selectedOrder.d_email
      }

    }

    /* assign a value when the user try to copy the existing order */
    if (!this.draftMode && !this.editMode && this.orderService.isOrderCopied) {

      order_type = this.orderService.copiedOrder.order_type;
      order_type_id = this.orderService.copiedOrder.order_type_id;
      customer_name = this.orderService.copiedOrder.business_name;
      customer_id = this.orderService.copiedOrder.customer_id;
      customer_reference = this.orderService.copiedOrder.customer_reference;
      booking_number = this.orderService.copiedOrder.booking_number;
      rail_cut = this.orderService.copiedOrder.rail_cut;

      tags = this.orderService.copiedOrder.tag_id.length !== 0 ?
        this.orderService.copiedOrder.tag_id.map(temp => +temp) : null;

      format_address = this.orderService.copiedOrder.format_address;
      pu_name = this.orderService.copiedOrder.order_pu_name;
      pu_loc = this.orderService.copiedOrder.order_pu_loc;
      dl_name = this.orderService.copiedOrder.order_dl_name;
      dl_loc = this.orderService.copiedOrder.order_dl_loc;

      this.isPickupInitEmit = true;
      this.isDeliveryInitEmit = true;

      this.addressObject = {
        p_city: this.orderService.copiedOrder.p_city,
        p_postal_code: this.orderService.copiedOrder.p_zip,
        d_city: this.orderService.copiedOrder.d_city,
        d_postal_code: this.orderService.copiedOrder.d_zip,
        p_phone: this.orderService.copiedOrder.p_phone,
        p_email: this.orderService.copiedOrder.p_email,
        d_phone: this.orderService.copiedOrder.d_phone,
        d_email: this.orderService.copiedOrder.d_email
      }
    }

    /* initialisation of form with prefilled data */
    this.orderForm = this.formBuilder.group({
      order_type: new FormControl({ value: order_type, disabled: this.editMode || this.isInvoiceMode }, Validators.required),
      order_type_id: new FormControl(order_type_id, Validators.required),
      customer_name: new FormControl({ value: customer_name, disabled: this.isInvoiceMode }, Validators.required),
      customer_id: new FormControl(customer_id, Validators.required),
      customer_reference: new FormControl(customer_reference, [Validators.required,
      Validators.pattern(/^[a-zA-Z0-9,'*-_.!#+"\$\&]{0,25}$/)]),
      booking_number: new FormControl(booking_number,
        Validators.pattern(/^[a-zA-Z0-9,'*-_.!#+"\$\&]{0,25}$/)),
      rail_cut: new FormControl({ value: rail_cut, disabled: this.isInvoiceMode }),
      tags: new FormControl({ value: tags, disabled: this.isInvoiceMode }),
      format_address: new FormControl({ value: format_address, disabled: this.isInvoiceMode }, Validators.required),
      pu_name: new FormControl({ value: pu_name, disabled: this.isInvoiceMode }, Validators.required),
      pu_loc: new FormControl({ value: pu_loc, disabled: this.isInvoiceMode }, Validators.required),
      est_pickup_time: new FormControl({ value: est_pickup_time, disabled: this.isInvoiceMode }, Validators.required),
      dl_name: new FormControl({ value: dl_name, disabled: this.isInvoiceMode }, Validators.required),
      dl_loc: new FormControl({ value: dl_loc, disabled: this.isInvoiceMode }, Validators.required),
      est_delivery_time: new FormControl({ value: est_delivery_time, disabled: this.isInvoiceMode }, Validators.required),
      order_container_chassis: order_container_chassis,
      order_notes: new FormControl({
        value: order_notes,
        disabled: !(this.loggedInUser.role === 'Admin' || this.loggedInUser.role === 'Order Entry') || this.isInvoiceMode
      }),
      driver_notes: new FormControl({
        value: driver_notes,
        disabled: !(this.loggedInUser.role === 'Admin' || this.loggedInUser.role === 'Order Entry') || this.isInvoiceMode
      })
    }, { validator: feildsMatched('pu_name', 'dl_name') });

    /* used to check for the order type to hide or show some element */
    if ((this.editMode || this.draftMode) && this.selectedOrder) {
      setTimeout(() => {
        this.truckMoveHideOrShowElement = this.selectedOrder.order_type !== 'Truck Move';
      }, 100);
    }

    if (!this.draftMode && !this.editMode && this.orderService.isOrderCopied) {
      setTimeout(() => {
        this.truckMoveHideOrShowElement = this.orderService.copiedOrder.order_type !== 'Truck Move';
      }, 100);
      this.orderService.isOrderCopied = false;
    }

  }

  /* to initiate form array */
  private initFormArray(init: boolean, sequence?: number) {
    let is_brokered = false;
    let carrier_id = null;
    let carrier_rate = null;
    let container_number = null;
    let container_size = null;
    let weight = null;
    let container_type = null;
    let temperature = null;
    let chassis_number = null;
    let containerNameDetails = {
      container_name: null,
      hire_name: null,
      hire_dehire_loc: null,
      chassis_name: null
    };
    let pu_ref = null;
    let dl_ref = null;
    let lfd_date = null;
    let pieces = null;
    let seal_no = null;
    let hazmat_req = 0;
    let un_number = null;
    let haz_classes = null;
    let haz_contact = null;
    let haz_name = null;
    let haz_page_ref = null;
    let haz_weight = null;
    let is_dispatch = 0;
    let orderType = null;
    let eta = null;
    let isBrokeredDisabled = false;

    /* assign a value when the screen is in edit mode */
    if ((this.editMode || this.draftMode || this.isInvoiceMode) && this.selectedOrder) {
      is_brokered = this.selectedOrder.is_brokered;
      carrier_id = this.selectedOrder.carrier_id;
      carrier_rate = this.selectedOrder.carrier_rate;
      container_number = this.selectedOrder.container_number;
      container_size = this.selectedOrder.container_size;
      weight = this.selectedOrder.weight;
      container_type = this.selectedOrder.container_type;
      temperature = this.selectedOrder.temperature;
      chassis_number = this.selectedOrder.chassis_number;
      containerNameDetails = {
        container_name: this.selectedOrder.container_name,
        hire_name: this.selectedOrder.hire_name,
        hire_dehire_loc: this.selectedOrder.hire_dehire_loc,
        chassis_name: this.selectedOrder.chassis_name
      };
      pu_ref = this.selectedOrder.pu_ref;
      dl_ref = this.selectedOrder.dl_ref;
      lfd_date = this.selectedOrder.lfd_date;
      pieces = this.selectedOrder.pieces;
      seal_no = this.selectedOrder.seal_no;
      hazmat_req = this.selectedOrder.hazmat_req;
      un_number = this.selectedOrder.un_number;
      haz_classes = this.selectedOrder.haz_classes;
      haz_contact = this.selectedOrder.haz_contact;
      haz_name = this.selectedOrder.haz_name;
      haz_page_ref = this.selectedOrder.haz_page_ref;
      haz_weight = this.selectedOrder.haz_weight;
      is_dispatch = this.selectedOrder.is_dispatch;
      orderType = this.selectedOrder.order_type;
      eta = this.selectedOrder.eta;

      isBrokeredDisabled = (this.selectedOrder.order_status !== 'Open' &&
        this.selectedOrder.order_status !== 'Assigned')
    }

    /* assign a value when the user try to copy the existing order only on initial */
    if (!this.draftMode && !this.editMode && this.orderService.isOrderCopied && init) {
      is_brokered = this.orderService.copiedOrder.is_brokered;
      carrier_id = this.orderService.copiedOrder.carrier_id;
      carrier_rate = this.orderService.copiedOrder.carrier_rate;
      container_size = this.orderService.copiedOrder.container_size;
      weight = this.orderService.copiedOrder.weight;
      container_type = this.orderService.copiedOrder.container_type;
      temperature = this.orderService.copiedOrder.temperature;
      containerNameDetails = {
        container_name: this.orderService.copiedOrder.container_name,
        hire_name: this.orderService.copiedOrder.hire_name,
        hire_dehire_loc: this.orderService.copiedOrder.hire_dehire_loc,
        chassis_name: this.orderService.copiedOrder.chassis_name
      };
      lfd_date = this.orderService.copiedOrder.lfd_date;
      pieces = this.orderService.copiedOrder.pieces;
      seal_no = this.orderService.copiedOrder.seal_no;
      hazmat_req = this.orderService.copiedOrder.hazmat_req;
      un_number = this.orderService.copiedOrder.un_number;
      haz_classes = this.orderService.copiedOrder.haz_classes;
      haz_contact = this.orderService.copiedOrder.haz_contact;
      haz_name = this.orderService.copiedOrder.haz_name;
      haz_page_ref = this.orderService.copiedOrder.haz_page_ref;
      haz_weight = this.orderService.copiedOrder.haz_weight;
      is_dispatch = this.orderService.copiedOrder.is_dispatch;
      orderType = this.orderService.copiedOrder.order_type;
      eta = this.orderService.copiedOrder.eta;
    }

    /* assign value for the certain input when user add a new container */
    if (!init) {
      let index = this.orderForm.get('order_container_chassis').value.length - 1; //get index of current container

      let control = (<FormArray>this.orderForm.get('order_container_chassis')).controls[index]; //retreive value of current container

      container_size = control.get('container_size').value;
      container_type = control.get('container_type').value;
      temperature = control.get('temperature') ? control.get('temperature').value : null;
      containerNameDetails = control.get('containerNameDetails').value;

      orderType = this.orderForm.get('order_type').value;
    }

    /* initialisation of form with prefilled data */
    return new FormGroup({
      is_brokered: new FormControl({ value: is_brokered, disabled: isBrokeredDisabled || this.isInvoiceMode }),
      carrier_id: new FormControl({ value: carrier_id, disabled: this.isInvoiceMode }),
      carrier_rate: new FormControl({ value: carrier_rate, disabled: this.isInvoiceMode }),
      container_number: new FormControl({ value: container_number, disabled: this.isInvoiceMode },
        (orderType === 'Truck Move') ?
          Validators.pattern(/(^[a-zA-Z]{2}[- ]?[0-9]{6}$)|(^[a-zA-Z]{4}[- ]?[0-9]{5}$)/) :
          Validators.pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$")),

      container_size: new FormControl({ value: container_size, disabled: this.isInvoiceMode }, Validators.required),
      weight: new FormControl({ value: weight, disabled: this.isInvoiceMode }, Validators.required),
      overweight: new FormControl(),
      container_type: new FormControl({ value: container_type, disabled: this.isInvoiceMode }, Validators.required),

      // temperature: new FormControl({ value: temperature, disabled: this.isInvoiceMode }, [Validators.pattern(/^[+-]?([0-9]{0,9}(\.[0-9]{0,2})?%?)$/),
      // this.temperatureRangePattern]), for later use

      temperature: new FormControl({ value: temperature, disabled: this.isInvoiceMode }),

      chassis_number: new FormControl({ value: chassis_number, disabled: this.isInvoiceMode },
        Validators.pattern("^[a-zA-Z0-9]{4}[- ]?[a-zA-Z0-9]{5}(\[0-9]{1})?$")),

      containerNameDetails: new FormControl({ value: containerNameDetails, disabled: this.isInvoiceMode }),
      pu_ref: new FormControl(pu_ref),
      dl_ref: new FormControl(dl_ref),
      lfd_date: new FormControl({ value: lfd_date, disabled: this.isInvoiceMode }),
      pieces: new FormControl(pieces),
      seal_no: new FormControl(seal_no, Validators.pattern(/^[a-zA-Z0-9]{0,}$/)),
      hazmat_req: new FormControl({ value: hazmat_req, disabled: this.isInvoiceMode }, Validators.required),
      hazmatinfo: new FormGroup({
        un_number: new FormControl({ value: un_number, disabled: this.isInvoiceMode }),
        haz_classes: new FormControl({ value: haz_classes, disabled: this.isInvoiceMode }),
        haz_contact: new FormControl({ value: haz_contact, disabled: this.isInvoiceMode }),
        haz_name: new FormControl({ value: haz_name, disabled: this.isInvoiceMode }),
        haz_page_ref: new FormControl({ value: haz_page_ref, disabled: this.isInvoiceMode }),
        haz_weight: new FormControl({ value: haz_weight, disabled: this.isInvoiceMode })
      }),
      order_status: new FormControl("Open"),
      is_dispatch: new FormControl(is_dispatch),
      is_draft: new FormControl(0),
      previousIsDraft: new FormControl(this.editMode || this.draftMode ? this.selectedOrder.is_draft : null),
      container_sequence: new FormControl(init ? 1 : sequence),
      eta: new FormControl({ value: eta, disabled: this.isInvoiceMode })
    })
  }

  /* for later use */
  // temperatureRangePattern(control: FormControl): { [s: string]: boolean } {
  //   if (!(control.value >= -40 && control.value <= 50)) {
  //     return { 'between': true };
  //   }
  //   return null;
  // }

  /* service call for the necessary dropdown */
  getAllDropdownFromService() {
    this.sharedService.getAllDropdown(this.dropdownComponentName, "orders,container,location,tags")
      .subscribe(response => {
        this.orderTypes = response.orders;
        this.tags = response.tags;
        this.locationTypes = response.location;
        this.containerTypes = response.container;
      }, error => {
        this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
      })
  }

  onChangeOrderType(event: string) {

    /* set vlaue null based on order type to avoid the unnecessary entry of data */
    if (event === 'Truck Move') {
      /* used timeout to avoid conflict error on removing some element */
      setTimeout(() => {
        this.truckMoveHideOrShowElement = false;

        this.orderForm.get('booking_number').setValue(null);

        this.orderForm.get('rail_cut').setValue(null);

        (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
          temp.get('lfd_date').setValue(null);

          temp.get('eta').setValue(null);

          temp.get('containerNameDetails').setValue({
            container_name: null,
            hire_name: null,
            hire_dehire_loc: null,
            chassis_name: null
          });

          temp.get('chassis_number').setValue(null);

          temp.get('container_number').setValue(null);

          temp.get('container_number').setErrors(null);

          temp.get('container_number').setValidators(Validators
            .pattern("(^[a-zA-Z]{2}[- ]?[0-9]{6}$)|(^[a-zA-Z]{4}[- ]?[0-9]{5}$)"));
        });
      }, 100);

      this.orderForm.get('format_address').setValue('P2P');

    } else if (event === 'Outbound Intermodal') {

      this.truckMoveHideOrShowElement = true;

      (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
        temp.get('lfd_date').setValue(null);

        temp.get('eta').setValue(null);

        temp.get('container_number').setErrors(null);

        temp.get('container_number').setValidators(Validators
          .pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$"));
      });

      this.orderForm.get('format_address').setValue('C2P')

    } else if (event === 'Inbound Intermodal') {

      this.truckMoveHideOrShowElement = true;

      this.orderForm.get('rail_cut').setValue(null);

      (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
        temp.get('container_number').setValidators([Validators.required, Validators
          .pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$")]);
      });

      this.orderForm.get('format_address').setValue('P2C');

    } else if (event === 'One Way Empty - Intermodal' || event === 'One Way Loaded - Intermodal') {
      setTimeout(() => {
        this.truckMoveHideOrShowElement = true;

        this.orderForm.get('format_address').setValue(event === 'One Way Empty - Intermodal' ? 'P2C' : 'C2P');

        (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
          temp.get('lfd_date').setValue(null);

          temp.get('eta').setValue(null);

          temp.get('container_number').setErrors(null);

          temp.get('container_number').setValidators(Validators
            .pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$"));
        });
      }, 100);
    }

    let orderType = this.orderTypes.find(temp => temp.label === event); // get the order type object with selected label

    if (orderType) {
      this.orderForm.get('order_type_id').setValue(orderType.value); // order type id for the selected order type
    }

    /* while in edit mode if the order type changed or not to trigger warning dialog */
    this.isOrderTypeChanged = this.editMode ? (this.selectedOrder.order_type !== event) : false;
  }

  /* get and set customer id for the selected customer name */
  onTypeCustomerName(event: CustomerData[]) {
    this.customerAutoSuggestions = event;

    let control = this.orderForm.get('customer_name');

    let customerValidator: any = this.customerAutoSuggestions.length !== 0 ? this.customerAutoSuggestions
      .find(temp => temp.business_name.toLowerCase() === (control.value !== null ?
        control.value.toLowerCase() : control.value))
      : null;

    if (customerValidator) {
      this.orderForm.get('customer_id').setValue(customerValidator.customer_id);
    } else {
      this.orderForm.get('customer_id').setValue(null);
    }
  }

  /* on change pickup date if only range from entered automatically fill range to */
  onChangeOfPickupDateTimePicker(event: any) {
    this.pickupHint = false;
    if (event.value.length !== 0 && event.value[0] !== null && event.value[1] === null) {
      this.orderForm.get('est_pickup_time').setValue([event.value[0], event.value[0]])
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] !== null) {
      this.orderForm.get('est_pickup_time').setValue([event.value[1], event.value[1]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] === null) {
      this.orderForm.get('est_pickup_time').setValue(null);
    }
  }

  /* to show the hint of pickup datetime format when user starts typing.. */
  onPickupKeyUp(event: any) {
    if (event !== null && event !== "") {
      this.pickupHint = true;
    } else {
      this.pickupHint = false;
    }
  }

  /* on change delivery date if only range from entered automatically fill range to */
  onChangeOfDeliveryDateTimePicker(event: any) {
    this.deliveryHint = false;
    if (event.value.length !== 0 && event.value[0] !== null && event.value[1] === null) {
      this.orderForm.get('est_delivery_time').setValue([event.value[0], event.value[0]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] !== null) {
      this.orderForm.get('est_delivery_time').setValue([event.value[1], event.value[1]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] === null) {
      this.orderForm.get('est_delivery_time').setValue(null);
    }
  }

  /* to show the hint of delivery datetime format when user starts typing.. */
  onDeliveryKeyUp(event) {
    if (event !== null && event !== "") {
      this.deliveryHint = true;
    } else {
      this.deliveryHint = false;
    }
  }

  /* dynamically add container by click */
  onClickAddContainer() {
    let index = this.orderForm.get('order_container_chassis').value.length - 1; // to get current container index

    let control = (<FormArray>this.orderForm.get('order_container_chassis')).controls[index]; // fetch current container object

    let sequence = this.orderForm.get('order_container_chassis').value.length + 1; // to set sequence

    /* only certain field it'll get add container and if not show warning msg */
    if (control.get('container_size').valid &&
      control.get('weight').valid &&
      control.get('container_type').valid &&
      control.get('lfd_date').valid &&
      control.get('containerNameDetails').valid &&
      control.get('container_number').valid) {

      (<FormArray>this.orderForm.get('order_container_chassis')).push(this.initFormArray(false, sequence));
    } else {
      this.sharedService.openSnackbar("Please Fill All the Required Feilds In Container Section",
        2500, "warning-msg");
    }
  }

  /* to get the form array to loop through the form control */
  getControls() {
    return (<FormArray>this.orderForm.get('order_container_chassis')).controls;
  }

  onChangeIsBrokeredCheckbox(event: boolean, init?: boolean, index?: number) {

    setTimeout(() => this.isBrokeredOrder[index] = event, 200);

    if (event && !this.carriers) { // retreive and set the carriers list in case checkbox is selected and carriers list is empty

      this.sharedService.getCarriersList('carriers').subscribe(response => {
        this.carriers = response.map(temp => {
          return {
            carrier_id: temp.carrier_id,
            carrier_name: temp.carrier_name
          }
        }).sort((a, b) => a.carrier_name - b.carrier_name);
      }, error => this.sharedService.openSnackbar("Something Went Wrong", 3000, 'failure-msg'));

    }

    let brokerTag = this.tags.find(temp => temp.label.toLowerCase() === 'brokered');

    let existingTag = this.orderForm.get('tags').value ?
      this.orderForm.get('tags').value : [];

    if (event && !init) { // add "BROKERED" tag in case the ceckbox is selected

      if (brokerTag) {

        !existingTag.includes(+brokerTag.value) ?
          existingTag.push(+brokerTag.value) : null;

        this.orderForm.get('tags').setValue(existingTag);
      }
    }

    if (!event && !init) { // reset the carrier value in case checkbox not selected

      let container = (<FormArray>this.orderForm.get('order_container_chassis')).controls[index];

      container.get('carrier_id').setValue(null);

      container.get('carrier_rate').setValue(null);

      if (brokerTag) {

        let updatedTag = existingTag.filter(temp => +temp !== +brokerTag.value);

        this.orderForm.get('tags').setValue(updatedTag);
      }
    }
  }

  /* to delete the container and adjust the container sequence */
  onDeleteContainer(index: number) {
    let orderContainerChassis = (<FormArray>this.orderForm.get('order_container_chassis'));

    orderContainerChassis.removeAt(index); // removing of current clicked container

    orderContainerChassis.controls[index] ?
      orderContainerChassis.controls[index].get('container_sequence').setValue(index + 1)
      : null; // assign the correct sequence to container
  }

  /* return the mask for container number */
  containerMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);
    let alphanumericLength = 0;
    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }
    if (alphanumericLength > 10) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/];
    } else if (alphanumericLength > 9) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    }
  }

  /* return the mask for trailer number when the order type is truck move */
  trailerMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);

    let alphanumericLength = 0;

    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }

    if (alphanumericLength > 8) {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    }
  }

  chassisMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);

    let alphanumericLength = 0;

    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }

    if (alphanumericLength > 9) {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/];
    }
  }

  /* to set up a weight value  */
  onClickOverWeightYes(index: number) {
    const orderContainerChassis = (<FormArray>this.orderForm.get('order_container_chassis')).controls[index];

    const value = orderContainerChassis.get('container_size').value === '20' ? 39000 : 44001;

    orderContainerChassis.get('weight').setValue(value);
  }

  /* cancelling of created order with confirm dialog */
  onClickCancelOrder() {
    if (this.editMode) {
      let dialogRef = this.sharedService.openConfirmation({
        action: "cancel",
        name: "order",
        cancelLabel: "No",
        confirmColor: "red",
        confirmLabel: "Yes"
      });

      dialogRef.subscribe(result => {
        if (result) {
          this.sharedService.cancelOrder(this.orderComponentName,
            {
              order_container_chassis_id: this.selectedOrder.order_container_chassis_id,
              isDispatch: this.selectedOrder.is_dispatch
            }).subscribe(response => {

              response.code === 200 ? this.router.navigate(['../../../'], { relativeTo: this.routes, replaceUrl: true })
                : this.sharedService.openSnackbar(response.data, 2500, "warning-msg");

            }, error => {
              this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
            })
        }
      })
    }
  }

  /* to check for validation except pickup and delivery time */
  isValid() {
    if (this.isDispatch && this.editMode) {
      return this.orderForm.valid
    } else {
      let valid = [];

      if (this.editMode) {
        (<any>Object).keys(this.orderForm.controls).forEach(key => {
          key !== 'est_delivery_time' && key !== 'est_pickup_time' && key !== 'order_type' ?
            (valid.push(this.orderForm.controls[key].valid)) : null;
        });
      } else {
        (<any>Object).keys(this.orderForm.controls).forEach(key => {
          key !== 'est_delivery_time' && key !== 'est_pickup_time' ?
            (valid.push(this.orderForm.controls[key].valid)) : null;
        });
      }

      return valid.every(temp => temp === true)
    }

  }

  /* onClick save button on html check which flag to assign to save desired table in ui */
  onClickSaveToPending() {

    let valid = [];

    if (this.isInvoiceMode) { // invoice edit ref #

      if (this.orderForm.valid) {

        let request = {
          pieces: (<FormArray>this.orderForm.get('order_container_chassis')).controls[0].get('pieces').value,
          seal_no: (<FormArray>this.orderForm.get('order_container_chassis')).controls[0].get('seal_no').value,
          pu_ref: (<FormArray>this.orderForm.get('order_container_chassis')).controls[0].get('pu_ref').value,
          dl_ref: (<FormArray>this.orderForm.get('order_container_chassis')).controls[0].get('dl_ref').value,
          customer_reference: this.orderForm.get('customer_reference').value,
          booking_number: this.orderForm.get('booking_number').value,
          order_container_chassis_id: this.selectedOrder.order_container_chassis_id
        }
        this.sharedService.afterInvoiceEdit(this.orderComponentName, request).subscribe(response => {
          this.router.navigate(['../'], { relativeTo: this.routes, replaceUrl: true });
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg");
        })

      }

    } else {

      let isValid = (this.editMode && this.isDispatch) ?
        this.orderForm.valid : this.isValid();

      if (isValid) {

        this.isRedirectToLeg = true;

        this.formatAddressBasedOnRouteFormat(); // to set formated pickup and delivery address

        let pickup = this.orderForm.get('pu_loc').value;

        let delivery = this.orderForm.get('dl_loc').value;

        let customer = this.orderForm.get('customer_name').value;

        let customerRef = this.orderForm.get('customer_reference').value;

        let formatAddress = this.orderForm.get('format_address').value;

        let orderType = this.orderForm.get('order_type').value;

        if (this.editMode || this.draftMode) { //edit or draft mode

          if (this.selectedOrder.pu_loc !== pickup || this.selectedOrder.dl_loc !== delivery ||
            this.selectedOrder.business_name !== customer || this.selectedOrder.format_address !== formatAddress ||
            this.selectedOrder.order_type !== orderType ||
            this.selectedOrder.customer_reference !== customerRef) { // if any of the field changed route check is enabled

            this.checkRouteServiceCall(this.editOrderServiceCall.bind(this, this.selectedOrder.is_dispatch, 0));

          } else { // else direct service call triggered
            this.editOrderServiceCall(this.selectedOrder.is_dispatch, 0);
          }

        } else { // for post always check the routes
          this.checkRouteServiceCall(this.postOrderServiceCall.bind(this, 0, 0));
        }

      }
    }

  }

  /* onClick save as dispatch button on html check which flag to assign to save desired table in ui */
  onClickSaveToDispatch() {
    if (this.orderForm.valid) {

      this.isRedirectToLeg = true;

      this.formatAddressBasedOnRouteFormat();

      let pickup = this.orderForm.get('pu_loc').value;

      let delivery = this.orderForm.get('dl_loc').value;

      let customer = this.orderForm.get('customer_name').value;

      let customerRef = this.orderForm.get('customer_reference').value;

      let formatAddress = this.orderForm.get('format_address').value;

      let orderType = this.orderForm.get('order_type').value;

      if (this.editMode || this.draftMode) { //edit or draft mode

        if (this.selectedOrder.pu_loc !== pickup || this.selectedOrder.dl_loc !== delivery ||
          this.selectedOrder.business_name !== customer || this.selectedOrder.format_address !== formatAddress ||
          this.selectedOrder.order_type !== orderType ||
          this.selectedOrder.customer_reference !== customerRef) { // if any of the field changed route check is enabled

          this.checkRouteServiceCall(this.editOrderServiceCall.bind(this, 1, 0));

        } else { // else direct service call triggered
          this.editOrderServiceCall(1, 0);
        }

      } else { // for post always check the routes
        this.checkRouteServiceCall(this.postOrderServiceCall.bind(this, 1, 0));
      }

    }
  }

  /* onClick save as draft button on html check which flag to assign to save desired table in ui */
  onClickSaveAsDraft() {
    let pickup = this.orderForm.get('pu_loc').value;

    let delivery = this.orderForm.get('dl_loc').value;

    let customer = this.orderForm.get('customer_name').value;

    let customerRef = this.orderForm.get('customer_reference').value;

    let formatAddress = this.orderForm.get('format_address').value;

    let orderType = this.orderForm.get('order_type').value;

    if (this.orderForm.get('order_type').valid &&
      this.orderForm.get('order_type_id').valid &&
      this.orderForm.get('customer_name').valid &&
      this.orderForm.get('customer_id').valid) {

      this.isRedirectToLeg = false;

      if (this.draftMode) { // draft mode

        if (pickup && pickup.trim() && delivery && delivery.trim()) { //check the pickup and delivery fields available

          this.formatAddressBasedOnRouteFormat(); // make the address formated

          if (this.selectedOrder.pu_loc !== pickup || this.selectedOrder.dl_loc !== delivery ||
            this.selectedOrder.business_name !== customer || this.selectedOrder.format_address !== formatAddress ||
            this.selectedOrder.order_type !== orderType ||
            this.selectedOrder.customer_reference !== customerRef) { // if any fields changes will proceed with routes check

            this.checkRouteServiceCall(this.editOrderServiceCall.bind(this, 0, 1));

          } else { // else direct edit service triggered
            this.editOrderServiceCall(0, 1);
          }

        } else { // if no pickup and delivery fields directly service call triggered
          this.editOrderServiceCall(0, 1);
        }

      } else { // post as draft

        if (pickup && pickup.trim() && delivery && delivery.trim()) { // check for the pickup and delivery fields

          this.formatAddressBasedOnRouteFormat(); // make the address formated

          this.checkRouteServiceCall(this.postOrderServiceCall.bind(this, 0, 1)); // service call route check

        } else { // else direct service call triggered
          this.postOrderServiceCall(0, 1);
        }

      }

    } else {
      this.sharedService.openSnackbar("Atleast Fill the order type & customer to save as draft", 2500, "warning-msg");
    }
  }

  checkRouteServiceCall(postOrEditFn: Function) {

    let isCustomerRefCheck = true;

    if (this.editMode || this.draftMode) {
      isCustomerRefCheck = this.selectedOrder.customer_reference !== this.orderForm.get('customer_reference').value;
    }

    let request = {
      order_type_id: this.orderForm.get('order_type_id').value,
      customer_id: this.orderForm.get('customer_id').value,
      format_address: this.orderForm.get('format_address').value,
      pu_formated_loc: this.pickupFormatedLoc,
      dl_formated_loc: this.deliveryFormatedLoc,
      customer_reference: this.orderForm.get('customer_reference').value,
      isCustomerRefCheck
    }

    if (this.pickupFormatedLoc && this.deliveryFormatedLoc) { // fomated location is present 

      this.sharedService.checkRouteRate(this.orderComponentName, request)
        .subscribe(response => {

          if (response.code === 103) {
            let customerRefCheck = this.sharedService.openConfirmation({
              action: "customerRefCheck",
              confirmColor: "green",
              confirmLabel: "Yes",
              cancelLabel: "No"
            })

            customerRefCheck.subscribe(result => {
              if (result) {
                this.checkAndProceedPostOrEdit(response, postOrEditFn);
              }
            })
          } else {
            this.checkAndProceedPostOrEdit(response, postOrEditFn);
          }

        }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, "failure-msg"));

    } else { // show warning
      this.sharedService.openSnackbar("Route Format not valid for Choosen Location", 3000, "warning-msg");
    }

  }

  checkAndProceedPostOrEdit(response, postOrEditFn: Function) {
    if (response.data.length) {

      this.routeDetails = {
        rate: response.data[0].rate,
        distance: response.data[0].distance,
        rateType: response.data[0].customer_id === 0 ? 'General Route Rate'
          : 'Customer Route Rate',
        isRateNeedToCreate: false,
        route_rate_id: response.data[0].route_rate_id
      };

      postOrEditFn();

    } else {

      let dialogRef = this.sharedService.openConfirmation({
        action: "rateCheck",
        name: this.pickupFormatedLoc,
        previousName: this.deliveryFormatedLoc,
        confirmColor: "green",
        confirmLabel: "Yes",
        cancelLabel: "No"
      })

      dialogRef.subscribe(result => {
        if (result) {
          this.routeDetails = {
            rate: 0,
            distance: 0,
            rateType: 'General Route Rate',
            isRateNeedToCreate: true,
            route_rate_id: null
          }

          postOrEditFn();
        }
      })

    }
  }


  /* to get back to order/dispatch screen */
  onClickCancel() {
    if (this.editMode || this.draftMode) {
      this.router.navigate(['../../../'], { relativeTo: this.routes });
    } else {
      this.router.navigate(['../'], { relativeTo: this.routes });
    }
  }

  /* to send the input to the backend for post */
  postOrderServiceCall(isDispatch: number, isDraft: number) {
    (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
      temp.get('is_dispatch').setValue(isDispatch);
      temp.get('is_draft').setValue(isDraft);
    }); // assign a flag based on user needs to view on which table

    let request = this.generateRequest(); // to get the formated to object to send backend

    this.sharedService.postOrder(this.orderComponentName, request).subscribe(
      (response: any) => {
        if (response.code === 200) {
          this.socketService.sendChangesInDriverFromClient('order');

          if (this.isRedirectToLeg) {

            this.socketService.sendNotification({
              isMultiple: true,
              multipleRole: ['Admin', 'Order Entry'],
              msg: `Order ${response.data[0][0].order_number} has been successfully created by ${this.loggedInUser.first_name + " " + this.loggedInUser.last_name}`,
              hyperlink: `/order-management/${response.data[0][0].order_number}/1/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            });

            isDispatch === 0 ?
              this.socketService.sendNotification({
                role: 'Order Entry',
                msg: `Order ${response.data[0][0].order_number} has been saved in Pending`,
                hyperlink: `/order-management/${response.data[0][0].order_number}/1/leg`,
                icon: 'event_note',
                styleClass: 'mat-deep-purple'
              }) :
              this.socketService.sendNotification({
                isMultiple: true,
                multipleRole: ['Order Entry', 'Dispatch'],
                msg: `Order ${response.data[0][0].order_number} has been submitted for Dispatch`,
                hyperlink: {
                  'Order Entry': `/order-management/${response.data[0][0].order_number}/1/leg`,
                  'Dispatch': `/dispatch/${response.data[0][0].order_number}/1/leg`
                },
                icon: 'event_note',
                styleClass: 'mat-deep-purple'
              });

            this.sharedService.isNewOrderCreated = true;

            this.router.navigate(['../', response.data[0][0].order_number, '1', 'leg'], { relativeTo: this.routes, replaceUrl: true });

          } else {
            this.socketService.sendNotification({
              role: 'Order Entry',
              msg: `Order ${response.data[0][0].order_number} has been saved as a Draft`,
              hyperlink: `/order-management/${response.data[0][0].order_number}/1/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            })
            this.router.navigate(['../'], { relativeTo: this.routes, replaceUrl: true });
          }
        } else if (response.code === 103) {
          this.sharedService.openSnackbar(response.error, 2500, 'warning-msg');
        }
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
  }

  /* call to the backend with confirmation */
  editOrderServiceCall(isDispatch: number, isDraft: number) {
    (<FormArray>this.orderForm.get('order_container_chassis')).controls.forEach(temp => {
      temp.get('is_dispatch').setValue(isDispatch);
      temp.get('is_draft').setValue(isDraft);
    }); // assign a flag based on user needs to view on which table

    let request = this.generateRequest(); // to get the formated to object to send backend

    /* open a dialog if the order change on edit */
    if (this.editMode) {
      let dialogRef = this.sharedService.openConfirmation({
        action: "orderEdit"
      });

      dialogRef.subscribe(result => {
        if (result) {
          request['isUpdateAll'] = result;

          this.editService(request);
        }
      })
    } else {
      this.editService(request);
    }
  }

  /* to send the input to the backend for edit */
  editService(request) {
    this.sharedService.putOrder(this.orderComponentName, request).subscribe(
      response => {
        if (response.code === 200) {

          this.socketService.sendChangesInDriverFromClient('order');

          !this.draftMode ?
            this.socketService.sendNotification({
              role: 'Dispatch',
              msg: `Order ${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence} information has been updated`,
              hyperlink: `/dispatch/${this.selectedOrder.order_number}/${this.selectedOrder.container_sequence}/leg`,
              icon: 'event_note',
              styleClass: 'mat-deep-purple'
            }) : null;

          setTimeout(() => {
            this.isRedirectToLeg ?
              this.router.navigate(['../../../', this.selectedOrder.order_number,
                this.selectedOrder.container_sequence, 'leg'], { relativeTo: this.routes, replaceUrl: true }) :
              this.router.navigate(['../../../'], { relativeTo: this.routes, replaceUrl: true });
          }, 800)

        } else if (response.code === 103) {
          this.sharedService.openSnackbar(response.error, 2500, 'warning-msg');
        }
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
  }

  /* setting up formated address based on route format */
  formatAddressBasedOnRouteFormat() {
    switch (this.orderForm.get('format_address').value) {
      case "P2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
      case "C2C":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ?
          (
            this.pickupLocationData.city && this.pickupLocationData.city.trim() &&
              this.pickupLocationData.state && this.pickupLocationData.state.trim() ?
              `${this.pickupLocationData.city} ${this.pickupLocationData.state}`.trim()
              : null
          ) : null,
          this.deliveryLocationData ?
            (
              this.deliveryLocationData.city && this.deliveryLocationData.city.trim()
                && this.deliveryLocationData.state && this.deliveryLocationData.state.trim() ?
                `${this.deliveryLocationData.city} ${this.deliveryLocationData.state}`.trim()
                : null
            ) : null);
        break;
      case "P2C":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ?
            (
              this.deliveryLocationData.city && this.deliveryLocationData.city.trim()
                && this.deliveryLocationData.state && this.deliveryLocationData.state.trim() ?
                `${this.deliveryLocationData.city} ${this.deliveryLocationData.state}`.trim()
                : null
            ) : null);
        break;
      case "Z2Z":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.postal_code : null,
          this.deliveryLocationData ? this.deliveryLocationData.postal_code : null);
        break;
      case "S2S":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.state : null,
          this.deliveryLocationData ? this.deliveryLocationData.state : null);
        break;
      case "P2S":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ? this.deliveryLocationData.state : null);
        break;
      case "S2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.state : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
      case "C2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ?
          (
            this.pickupLocationData.city && this.pickupLocationData.city.trim() &&
              this.pickupLocationData.state && this.pickupLocationData.state.trim() ?
              `${this.pickupLocationData.city} ${this.pickupLocationData.state}`.trim()
              : null
          ) : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
    }
  }

  setPickupAndDeliveryAddress(pickup: string, delivery: string) {
    this.pickupFormatedLoc = pickup;
    this.deliveryFormatedLoc = delivery;
  }

  /* form the entered input to send to the backend */
  generateRequest() {
    let request = { ...this.orderForm.getRawValue() }; // copy the  existing object to variable

    Object.keys(request).forEach(temp => {
      request[temp] = request[temp] !== "" ? request[temp] : null;

      temp === 'order_container_chassis' ?
        request[temp].forEach(element => {

          Object.keys(element).forEach(elem => {

            element[elem] = element[elem] !== "" ? element[elem] : null;

            elem === 'containerNameDetails' ?
              (element[elem].container_name = (element[elem].container_name !== "" ?
                element[elem].container_name : null)) : null

          })
        }) : null;
    }); // change the value to null if it is empty string ""

    /* adding some other extra detail apart from reactive form input */
    request['est_pickup_from_time'] = this.orderForm.get('est_pickup_time').value !== null ?
      this.orderForm.get('est_pickup_time').value[0] : null;
    request['est_pickup_to_time'] = this.orderForm.get('est_pickup_time').value !== null ?
      this.orderForm.get('est_pickup_time').value[1] : null;
    request['est_delivery_from_time'] = this.orderForm.get('est_delivery_time').value !== null ?
      this.orderForm.get('est_delivery_time').value[0] : null;
    request['est_delivery_to_time'] = this.orderForm.get('est_delivery_time').value !== null ?
      this.orderForm.get('est_delivery_time').value[1] : null;
    request['previousPuFromTime'] = this.editMode ? this.selectedOrder.est_pickup_from_time : null;
    request['previousDlFromTime'] = this.editMode ? this.selectedOrder.est_delivery_from_time : null;
    request['previousPuToTime'] = this.editMode ? this.selectedOrder.est_pickup_to_time : null;
    request['previousDlToTime'] = this.editMode ? this.selectedOrder.est_delivery_to_time : null;
    request['previousPuLoc'] = this.editMode ? this.selectedOrder.pu_loc : null;
    request['previousDlLoc'] = this.editMode ? this.selectedOrder.dl_loc : null;
    request['previousHireLoc'] = this.editMode ? this.selectedOrder.hire_dehire_loc : null;
    request['p_city'] = this.addressObject.p_city;
    request['p_zip'] = this.addressObject.p_postal_code;
    request['p_phone'] = this.addressObject.p_phone;
    request['p_email'] = this.addressObject.p_email;
    request['d_city'] = this.addressObject.d_city;
    request['d_zip'] = this.addressObject.d_postal_code;
    request['d_phone'] = this.addressObject.d_phone;
    request['d_email'] = this.addressObject.d_email;
    request['loggedInUser'] = this.loggedInUser.first_name + " " + this.loggedInUser.last_name;
    request['loggedInUserId'] = this.loggedInUser.userId;
    request['role'] = this.loggedInUser.role;
    request['pu_formated_loc'] = this.pickupFormatedLoc;
    request['dl_formated_loc'] = this.deliveryFormatedLoc;
    request['order_id'] = (this.editMode || this.draftMode) ? this.selectedOrder.order_id : null;

    request['order_container_chassis_id'] = (this.editMode || this.draftMode) ?
      this.selectedOrder.order_container_chassis_id : null;

    request['previousTriaxleValue'] = (this.editMode || this.draftMode) ? this.selectedOrder.triaxle : null;
    request['previousContainerSize'] = (this.editMode || this.draftMode) ?
      this.selectedOrder.container_size : null;
    request['previousWeight'] = (this.editMode || this.draftMode) ? this.selectedOrder.weight : null;
    request['previousHazmat'] = (this.editMode || this.draftMode) ? this.selectedOrder.hazmat_req : null;
    request['previousNotes'] = (this.editMode || this.draftMode) ? this.selectedOrder.order_notes : null;

    request['fetchedRate'] = this.routeDetails ? this.routeDetails.rate :
      ((this.editMode || this.draftMode) ? this.selectedOrder.rate : null);

    request['loadedLegMiles'] = this.routeDetails ? this.routeDetails.distance :
      ((this.editMode || this.draftMode) ? this.selectedOrder.miles : 0);

    request['rateType'] = this.routeDetails ? this.routeDetails.rateType :
      ((this.editMode || this.draftMode) ? this.selectedOrder.rate_type : null);

    request['isRouteCreate'] = this.routeDetails ? this.routeDetails.isRateNeedToCreate : null;

    request['routeRateId'] = this.routeDetails ? this.routeDetails.route_rate_id : null;

    request['previousIsBrokered'] = (this.editMode || this.draftMode) ? this.selectedOrder.is_brokered : null;

    request['previousCarrier'] = (this.editMode || this.draftMode) ? this.selectedOrder.carrier_id : null;

    return request;
  }

}
