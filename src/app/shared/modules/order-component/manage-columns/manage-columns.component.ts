import { Component, OnInit, Inject, } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange, } from '@angular/material';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { FormControl } from '@angular/forms';
import { columnInterface } from 'app/shared/models/display-column.model';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-manage-columns',
  templateUrl: './manage-columns.component.html',
  styleUrls: ['./manage-columns.component.scss']
})
export class ManageColumnsComponent implements OnInit {

  public listColumns: columnInterface[];

  public addColumnOptions: columnInterface[];

  public hideOrShowDragIcon: boolean[] = [];

  public optionColumn: FormControl = new FormControl();

  constructor(public dialogRef: MatDialogRef<ManageColumnsComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) { }

  ngOnInit() {
    let selectedDispatchColumns = localStorage.getItem(this.injectedData.tableView) ?
      JSON.parse(localStorage.getItem(this.injectedData.tableView)) : [];

    this.listColumns = this.injectedData.displayColumns.filter(temp => selectedDispatchColumns.map(elem => elem.field).includes(temp.value))
      .sort((a, b) =>
        (selectedDispatchColumns.map(elem => elem.field).indexOf(a.value) < selectedDispatchColumns.map(elem => elem.field).indexOf(b.value)) ? -1 : 0);

    this.addColumnOptions = this.injectedData.displayColumns.filter(temp => !selectedDispatchColumns.map(elem => elem.field).includes(temp.value));

    // this.listColumns.length >= 10 ? this.optionColumn.disable() : this.optionColumn.enable();
  }

  onDragAndDropSort(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.listColumns, event.previousIndex, event.currentIndex);
  }

  onHoverMouseEnter(index: number) {
    this.hideOrShowDragIcon[index] = true;
  }

  onHoverMouseLeave(index: number) {
    this.hideOrShowDragIcon[index] = false;
  }

  onClickRemoveColumns(index: number, selectedColumn: columnInterface) {
    this.listColumns.splice(index, 1);

    this.addColumnOptions.push(selectedColumn);

    this.optionColumn.setValue(null);

    // this.listColumns.length >= 10 ? this.optionColumn.disable() : this.optionColumn.enable();
  }

  onSelectAddColumnOption(event: MatSelectChange) {
    let value = this.addColumnOptions.filter(temp => temp.value == event.value)[0]

    this.listColumns.push(value);

    this.addColumnOptions = this.addColumnOptions.filter(temp => temp.value !== event.value);

    this.optionColumn.setValue(null);

    // this.listColumns.length >= 10 ? this.optionColumn.disable() : this.optionColumn.enable();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onClickSave() {
    let columnNeedToAdded = this.listColumns.map(temp => {
      return {
        field: temp.value,
        width: 100
      }
    });

    if (this.injectedData.tableView === 'dispatch_view') {

      columnNeedToAdded.unshift({ field: 'serialNo', width: 50 });

      columnNeedToAdded.push({ field: 'action', width: 50 });

    } else if (this.injectedData.tableView === 'orders_dispatch' ||
      this.injectedData.tableView === 'orders_pending') {

      columnNeedToAdded.unshift({ field: 'select', width: 30 });

      columnNeedToAdded.push({ field: 'action', width: 85 });

    } else if (this.injectedData.tableView === 'invoice_to_generated_view') {

      columnNeedToAdded.unshift({ field: 'select', width: 30 });

      columnNeedToAdded.push({ field: 'action', width: 50 });

    } else if (this.injectedData.tableView === 'invoice_view' ||
      this.injectedData.tableView === 'orders_draft') {

      columnNeedToAdded.push({ field: 'action', width: 50 });

    }

    let oldPreference = localStorage.getItem(this.injectedData.tableView) ? JSON.parse(localStorage.getItem(this.injectedData.tableView)) : [];

    columnNeedToAdded.forEach(temp => {
      if (oldPreference.some(elem => elem.field === temp.field)) {
        temp.width = oldPreference.find(elem => elem.field === temp.field).width
      }
    });

    let request = {
      column: this.injectedData.tableView,
      value: JSON.stringify(columnNeedToAdded),
      user_id: this.sharedService.getUserDetails().userId,
      noloading: false
    }

    this.sharedService.updateUiSettings('users', request).subscribe(response => {

      localStorage.setItem(this.injectedData.tableView, JSON.stringify(columnNeedToAdded));

      this.dialogRef.close(columnNeedToAdded);
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
    })
  }
}
