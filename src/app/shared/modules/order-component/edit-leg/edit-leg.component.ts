import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange, MatRadioChange } from '@angular/material';

import { feildsMatched } from 'app/shared/utils/app-validators';
import { GeneralAutoComplete } from 'app/shared/models/autocomplete.model';
import { SharedService } from 'app/shared/service/shared.service';
import { LegData } from 'app/shared/models/leg.model';
import { loginData } from 'app/session/session.model';

@Component({
  selector: 'app-edit-leg',
  templateUrl: './edit-leg.component.html',
  styleUrls: ['./edit-leg.component.scss']
})
export class EditLegComponent implements OnInit {

  public editMode: boolean;

  public data: any;

  public legTypes: {
    label: string;
    value: number;
  }[];

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public legForm: FormGroup;

  public driverData: GeneralAutoComplete[];

  public orderData: GeneralAutoComplete[];

  public commonData: any;

  public pickupMinimumDate: Date = null;

  public deliveryMinumumDate: Date = null;

  public isMilesEditable: boolean = false;

  public isLocationChanged: boolean = false;

  public legNumber: number;

  public selectedAccessoriesName: string[] = [];

  public orderType: string;

  public loggedInUser: loginData;

  public pickupHint: boolean;

  public deliveryHint: boolean;

  constructor(public dialogRef: MatDialogRef<EditLegComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public injectedData,
    private sharedService: SharedService) {
    if (injectedData) {
      this.editMode = injectedData.modeSelect;
      this.data = injectedData.selectedValue;
      this.legTypes = injectedData.legTypes;
      this.commonData = injectedData.commonData;
    }
  }

  ngOnInit() {
    this.orderType = this.injectedData.isLeg ? this.commonData.order_type : this.injectedData.orderType;

    this.loggedInUser = this.sharedService.getUserDetails();

    this.pickupMinimumDate = this.injectedData.isLeg ? new Date() : null;

    this.deliveryMinumumDate = this.injectedData.isLeg ? new Date() : null;

    this.initForm();

    if (this.editMode) {
      (<any>Object).values(this.legForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }
  }

  private initForm() {
    let leg_number = this.injectedData.isLeg ? this.commonData.leg_number : null;
    let leg_type = null;
    let driver_name = !this.injectedData.isLeg ? this.injectedData.driverName : null;
    let pu_loc = this.injectedData.isLeg ? this.commonData.dl_loc : null;
    let pu_name = this.injectedData.isLeg ? this.commonData.dl_name : null;
    let pu_time = null;
    let dl_loc = null;
    let dl_name = null;
    let dl_time = null;
    let est_miles = null;
    let container_number = this.injectedData.isLeg ? this.commonData.container_number : null;
    let chassis_number = this.injectedData.isLeg ? this.commonData.chassis_number : null;
    let order_number = null;
    let is_paid = 0;
    let rate = this.editMode ? (this.injectedData.isLeg ? null : this.data.driver_rate) : null;
    let is_charges_added = !this.injectedData.isLeg ? true : null;
    let driver_charges = !this.injectedData.isLeg ? new FormArray([this.accessoriesChargesArray(null, null)]) : null;
    let legType = null;
    let legStatus = null;

    if (this.editMode) {
      this.pickupMinimumDate = (this.loggedInUser.role !== 'Admin' && this.loggedInUser.role !== 'Dispatch' && this.loggedInUser.role !== 'Driver Pay') ?
        (this.data.pu_time !== null ? new Date(this.data.pu_time) : new Date()) : null;

      this.deliveryMinumumDate = (this.loggedInUser.role !== 'Admin' && this.loggedInUser.role !== 'Dispatch' && this.loggedInUser.role !== 'Driver Pay') ?
        (
          this.data.pu_time !== null ? new Date(this.data.pu_time)
            : (this.data.dl_time !== null ? new Date(this.data.dl_time) : new Date())
        ) : null;

      leg_number = this.data.leg_number;
      leg_type = this.data.leg_type_id;
      driver_name = this.data.driver_name;
      pu_loc = this.data.pu_loc;
      pu_name = this.data.pu_name;

      if (this.injectedData.isLeg && (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered' && this.data.leg_status !== 'Picked Up'))) {
        pu_time = this.data.pu_time &&
          this.data.pu_time_to ?
          [
            new Date(this.data.pu_time),
            new Date(this.data.pu_time_to)
          ] : null;
      } else if (!this.injectedData.isLeg || (this.injectedData.isLeg && this.editMode && this.data.leg_status !== 'Open' && this.data.leg_status !== 'Assigned')) {
        pu_time = this.data.pu_time ? new Date(this.data.pu_time) : null;
      }

      if (this.injectedData.isLeg && (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered'))) {

        dl_time = this.data.dl_time &&
          this.data.dl_time_to ?
          [
            new Date(this.data.dl_time),
            new Date(this.data.dl_time_to)
          ] : null;
      } else if (!this.injectedData.isLeg || (this.injectedData.isLeg && this.editMode && this.data.leg_status === 'Delivered')) {

        dl_time = this.data.dl_time ? new Date(this.data.dl_time) : null;
      }

      dl_loc = this.data.dl_loc;
      dl_name = this.data.dl_name;
      est_miles = this.injectedData.isLeg ? this.data.est_miles : this.data.miles;
      legType = this.data.leg_type;
      container_number = (legType === 'Bobtail' || legType === 'Chassis') ? null : this.data.container_number;
      chassis_number = (legType === 'Bobtail' || this.orderType === 'Truck Move') ? null : this.data.chassis_number;
      order_number = this.data.order_sequence
      is_paid = this.injectedData.isLeg ? 0 : this.data.is_paid;
      is_charges_added = !this.injectedData.isLeg ? true : null;
      legStatus = this.injectedData.isLeg ? this.data.leg_status : null;

      if (!this.injectedData.isLeg) {

        driver_charges = new FormArray([]);

        this.data.driver_charges && this.data.driver_charges.length !== 0 ? this.data.driver_charges.forEach(element => {
          this.selectedAccessoriesName.push(element.accessories_name);
          driver_charges.push(this.accessoriesChargesArray(element.accessories_name, element.rate));
        }) : driver_charges.push(this.accessoriesChargesArray(null, null));

      }

    }

    this.legForm = this.formBuilder.group({
      'leg_number': new FormControl({
        value: leg_number, disabled: !this.injectedData.isLeg || (this.injectedData.isLeg && this.editMode) ?
          (leg_number !== null) : false
      },
        [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/)]),
      'leg_type': new FormControl(leg_type, Validators.required),
      'driver_name': new FormControl({
        value: driver_name,
        disabled: !this.injectedData.isLeg ? true :
          (
            (this.editMode && this.data.is_brokered) || this.commonData.is_brokered
              ? true : (legStatus === 'Open' || legStatus === 'Assigned')
                ? false : driver_name !== null
          )
      }),
      'pickup': new FormControl(pu_loc, Validators.required),
      'pu_name': new FormControl(pu_name, Validators.required),
      'pu_time': new FormControl(pu_time),
      'delivery': new FormControl(dl_loc, Validators.required),
      'dl_name': new FormControl(dl_name, Validators.required),
      'dl_time': new FormControl(dl_time),
      'est_miles': new FormControl({ value: est_miles, disabled: this.editMode }),
      'container_number': new FormControl({
        value: container_number,
        disabled: this.editMode ? (legType === 'Bobtail' || legType === 'Chassis') : false
      },
        (this.orderType === 'Truck Move') ?
          Validators.pattern("(^[a-zA-Z]{2}[- ]?[0-9]{6}$)|(^[a-zA-Z]{4}[- ]?[0-9]{5}$)") :
          Validators.pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$")
      ),
      'chassis_number': new FormControl({
        value: chassis_number,
        disabled: this.editMode ? (legType === 'Bobtail' || this.orderType === 'Truck Move') : (this.orderType === 'Truck Move')
      }, Validators.pattern("^[a-zA-Z0-9]{4}[- ]?[a-zA-Z0-9]{5}(\[0-9]{1})?$")),
      'order_number': new FormControl({ value: order_number, disabled: (order_number !== null) }),
      'is_paid': new FormControl(is_paid),
      'rate': new FormControl(rate, Validators.pattern(/^[0-9]*(\.[0-9]+)?$/)),
      'is_charges_added': new FormControl(is_charges_added, !this.injectedData.isLeg ? Validators.required : null),
      'driver_charges': driver_charges
    }, { validator: feildsMatched('pickup', 'delivery') })

    this.injectedData.isLeg ? this.legForm.get('leg_number')
      .setValidators([Validators.required,
      Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/)]) : null;
  }

  accessoriesChargesArray(accessories_name?, rate?) {
    return new FormGroup({
      'accessories_name': new FormControl(accessories_name),
      'rate': new FormControl(rate, [Validators.pattern(/^[0-9]*(\.[0-9]+)?$/)])
    })
  }

  getAccessoriesCharges() {
    return (<FormArray>this.legForm.get('driver_charges')).controls;
  }

  onChangeRadioButton(event: MatRadioChange) {
    if (event.value) {
      (<FormArray>this.legForm.get('driver_charges')).push(this.accessoriesChargesArray());
    } else {
      (<FormArray>this.legForm.get('driver_charges')).value.forEach((element, i) => {
        (<FormArray>this.legForm.get('driver_charges')).removeAt(i);
      });
    }
  }

  onAddAccessoriesCharges() {
    (<FormArray>this.legForm.get('driver_charges')).push(this.accessoriesChargesArray());
  }

  onDeleteAccessoriesCharges(index: number) {
    this.selectedAccessoriesName.length !== 0 ? this.selectedAccessoriesName.splice(index, 1) : null;
    (<FormArray>this.legForm.get('driver_charges')).removeAt(index);
  }

  onSelectAccessoriesCharge(event: MatSelectChange, index: number) {
    this.selectedAccessoriesName[index] = event.value;
  }

  onOrderNumberValueChanges(event: GeneralAutoComplete[]) {
    this.orderData = event;

    let orderNumber = this.legForm.get('order_number').value;

    let orderSuggest = this.orderData.filter(temp => temp.searchValue === orderNumber);

    let orderSelectedData = this.orderData.find(temp => temp.searchValue === orderNumber);

    if (orderSelectedData) {
      this.orderType = orderSelectedData.orderType;
      if (this.orderType === 'Truck Move') {
        this.legForm.get('container_number').setValue(this.editMode ? this.data.container_number : null);
        this.legForm.get('container_number').setValidators(Validators.pattern("(^[a-zA-Z]{2}[- ]?[0-9]{6}$)|(^[a-zA-Z]{4}[- ]?[0-9]{5}$)"));
        this.legForm.get('chassis_number').setValue(null);
        this.legForm.get('chassis_number').disable();
      } else {
        this.legForm.get('container_number')
          .setValidators(Validators.pattern("^[a-zA-Z]{4}[- ]?([a-zA-Z]{1}[0-9]{5}|[0-9]{5}|[0-9]{6})(\[- ]?[0-9]{1})?$"));
        this.legForm.get('chassis_number').enable();
      }
    }

    if (orderNumber === null || orderNumber === '' || orderSuggest.length === 0) {
      this.legForm.get('leg_number').setValue(this.editMode ? this.data.leg_number : this.legNumber);
      this.editMode ? this.legForm.get('leg_number').disable() : this.legForm.get('leg_number').enable();
    }
  }

  onOrderNumberSelected(event: number) {
    this.legForm.get('leg_number').setValue(event);
    this.legForm.get('leg_number').disable();
  }

  onSelectLegnumber(event: MatSelectChange) {
    let legData: LegData = event.value === 1 ?
      this.injectedData.legDuplicate.find(temp => temp.leg_number === event.value)
      : this.injectedData.legDuplicate.find(temp => temp.leg_number === (event.value - 1));

    let legType = this.legTypes.find(temp => temp.value === this.legForm.get('leg_type').value);

    if (legData) {
      this.legForm.get('chassis_number').setValue(legType && legType.label === 'Bobtail' ? null :
        legData.chassis_number);
      this.legForm.get('container_number').setValue(legType && (legType.label === 'Bobtail' || legType.label === 'Chassis') ?
        null : legData.container_number);
      this.legForm.get('driver_name').setValue(legData.driver_name);
      if (event.value === 1) {
        this.legForm.get('pu_name').setValue(null);
        this.legForm.get('pickup').setValue(null);
        this.legForm.get('dl_name').setValue(legData.pu_name);
        this.legForm.get('delivery').setValue(legData.pu_loc);
      } else {
        this.legForm.get('pu_name').setValue(legData.dl_name);
        this.legForm.get('pickup').setValue(legData.dl_loc);
        this.legForm.get('dl_name').setValue(null);
        this.legForm.get('delivery').setValue(null);
      }
    }

    if (legType) {
      if (legType.label === 'Bobtail') {
        this.legForm.get('container_number').disable();
        this.legForm.get('chassis_number').disable();
      } else if (legType.label === 'Chassis') {
        this.legForm.get('container_number').disable();
        this.legForm.get('chassis_number').enable();
      } else {
        this.legForm.get('container_number').enable();
        this.legForm.get('chassis_number').enable();
      }
    }

    if (this.orderType === 'Truck Move') {
      this.legForm.get('chassis_number').setValue(null);
      this.legForm.get('chassis_number').disable();
    }
  }

  onSelectLegType(event: MatSelectChange) {
    let legType = this.legTypes.find(temp => temp.value === event.value);

    if (legType) {

      if (legType.label === 'Bobtail') {
        this.legForm.get('container_number').setValue(null);
        this.legForm.get('container_number').disable();
        this.legForm.get('chassis_number').setValue(null);
        this.legForm.get('chassis_number').disable();
      } else if (legType.label === 'Chassis') {
        this.legForm.get('container_number').setValue(null);
        this.legForm.get('container_number').disable();
        this.legForm.get('chassis_number').setValue(this.editMode ? this.data.chassis_number :
          this.injectedData.isLeg ? this.commonData.chassis_number : null);
        this.legForm.get('chassis_number').enable();
      } else {
        this.legForm.get('container_number').setValue(this.editMode ? this.data.container_number :
          this.injectedData.isLeg ? this.commonData.container_number : null);
        this.legForm.get('container_number').enable();
        this.legForm.get('chassis_number').setValue(this.editMode ? this.data.chassis_number :
          this.injectedData.isLeg ? this.commonData.chassis_number : null);
        this.legForm.get('chassis_number').enable();
      }

    }

    if (this.orderType === 'Truck Move') {
      this.legForm.get('chassis_number').setValue(null);
      this.legForm.get('chassis_number').disable();
    }

  }

  /* on change pickup date if only range from entered automatically fill range to */
  onChangeOfPickupDateTimePicker(event: any) {
    this.pickupHint = false;
    if (event.value.length !== 0 && event.value[0] !== null && event.value[1] === null) {
      this.legForm.get('pu_time').setValue([event.value[0], event.value[0]])
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] !== null) {
      this.legForm.get('pu_time').setValue([event.value[1], event.value[1]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] === null) {
      this.legForm.get('pu_time').setValue(null);
    }
  }

  /* to show the hint of pickup datetime format when user starts typing.. */
  onPickupKeyUp(event: any) {
    if (event !== null && event !== "") {
      this.pickupHint = true;
    } else {
      this.pickupHint = false;
    }
  }

  /* on change delivery date if only range from entered automatically fill range to */
  onChangeOfDeliveryDateTimePicker(event: any) {
    this.deliveryHint = false;
    if (event.value.length !== 0 && event.value[0] !== null && event.value[1] === null) {
      this.legForm.get('dl_time').setValue([event.value[0], event.value[0]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] !== null) {
      this.legForm.get('dl_time').setValue([event.value[1], event.value[1]]);
    } else if (event.value.length !== 0 && event.value[0] === null && event.value[1] === null) {
      this.legForm.get('dl_time').setValue(null);
    }
  }

  /* to show the hint of delivery datetime format when user starts typing.. */
  onDeliveryKeyUp(event) {
    if (event !== null && event !== "") {
      this.deliveryHint = true;
    } else {
      this.deliveryHint = false;
    }
  }

  containerMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);
    let alphanumericLength = 0;
    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }
    if (alphanumericLength > 10) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/];
    } else if (alphanumericLength > 9) {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {
      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-',
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    }
  }

  /* return the mask for trailer number when the order type is truck move */
  trailerMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);

    let alphanumericLength = 0;

    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }

    if (alphanumericLength > 8) {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    }
  }

  chassisMask(event) {
    let alphanumeric = event.match(/[a-zA-Z0-9]/g);

    let alphanumericLength = 0;

    if (alphanumeric) {
      alphanumericLength = alphanumeric.join("").length;
    }

    if (alphanumericLength > 9) {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/, /[a-zA-Z0-9]/];
    } else {

      return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        '-', /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/,
        /[a-zA-Z0-9]/];
    }
  }

  onLocationChange() {
    this.isLocationChanged = true;
    this.onClickDisableMiles();
  }

  deliveryValidator(control: FormControl): { [s: string]: boolean } {
    if ((this.legForm.get('pickup').value !== null || control.value !== null) && (this.legForm.get('pickup').value === control.value)) {
      return { 'sameAddress': true };
    }
    return null;
  }

  onClickEnableMiles() {
    this.legForm.get('est_miles').enable();
    this.isMilesEditable = true;
  }

  onClickDisableMiles() {
    this.legForm.get('est_miles').disable();
    this.isMilesEditable = false;
  }

  onSubmit() {

    let driverName = this.legForm.get('driver_name').value;

    let orderNumber = this.legForm.get('order_number').value;

    let driverSuggest = this.driverData.filter(temp => temp.searchValue === driverName);

    let orderSuggest = this.injectedData.isLeg ? [] :
      this.orderData.filter(temp => temp.searchValue === orderNumber);

    if (this.legForm.valid) {
      let request;

      if (this.injectedData.isLeg) {

        request = this.generateRequest({
          driver_id: driverName && driverSuggest.length ?
            driverSuggest[0].id : null,
          legStatus: driverName && driverSuggest.length ?
            driverSuggest[0].legStatus : null
        });

        let isValid = true;

        let reason = "";

        if (!this.editMode && this.commonData.leg_number !== this.legForm.get('leg_number').value) {
          request.isAddLegInbetween = true;

          let legData: LegData = this.injectedData.legDuplicate.find(temp => temp.leg_number === this.legForm.get('leg_number').value);

          request.nextLegDeliveryLocation = legData.dl_loc;

          request.nextLegDeliveryLocationName = legData.dl_name;

          if (legData && (legData.leg_status === 'Delivered' || legData.leg_status === 'Picked Up')) {
            isValid = false;
            reason = "Cannot Add Leg Inbetween. Please Select Leg Which is Open or Assigned";
          }
        } else {
          request.isAddLegInbetween = false;
        }


        if (!driverName) {

          if (this.editMode) {
            this.dialogRef.close(request);
          } else if (isValid) {
            this.dialogRef.close(request);
          } else {
            this.sharedService.openSnackbar(reason, 3000, 'warning-msg');
          }


        } else if (driverName && driverSuggest.length) {

          if (this.commonData.hazmat_req && !driverSuggest[0].hazardousConfirm) {

            if (this.editMode) {
              this.closeWithConfirmDialog(request, driverName);
            } else if (isValid) {
              this.closeWithConfirmDialog(request, driverName);
            } else {
              this.sharedService.openSnackbar(reason, 3000, 'warning-msg');
            }

          } else {
            if (this.editMode) {
              this.dialogRef.close(request);
            } else if (isValid) {
              this.dialogRef.close(request);
            } else {
              this.sharedService.openSnackbar(reason, 3000, 'warning-msg');
            }
          }

        } else if (driverName && !driverSuggest.length) {
          this.sharedService.openSnackbar("Please Select Driver Name from the autoSuggest list", 2500, 'warning-msg');
        }

      } else {

        request = this.generateRequest({
          driver_id: driverSuggest.length !== 0 ? driverSuggest[0].id : null,
          order_id: orderSuggest.length !== 0 ? orderSuggest[0].orderLevelId : null,
          order_container_id: orderSuggest.length !== 0 ? orderSuggest[0].containerLevelId : null
        });

        if (driverSuggest.length && (orderNumber || (orderNumber && orderSuggest.length))) {

          if (orderSuggest.length && orderSuggest[0].hazmat && !driverSuggest[0].hazardousConfirm) {
            this.closeWithConfirmDialog(request, driverName);
          } else {
            this.dialogRef.close(request);
          }

        } else {
          this.sharedService.openSnackbar("Please Select Order Number & Driver Name from the autosuggest list", 2500, 'warning-msg');
        }

      }

    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  generateRequest(selectedId) {
    return {
      chassis_number: this.legForm.get('chassis_number').value,
      container_number: this.legForm.get('container_number').value,
      pu_loc: this.legForm.get('pickup').value,
      pu_name: this.legForm.get('pu_name').value,

      pu_time: (this.injectedData.isLeg &&
        (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered' && this.data.leg_status !== 'Picked Up'))) ?
        (this.legForm.get('pu_time').value ? this.legForm.get('pu_time').value[0] : null) : this.legForm.get('pu_time').value,

      pu_time_to: (this.injectedData.isLeg &&
        (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered' && this.data.leg_status !== 'Picked Up'))) ?
        (this.legForm.get('pu_time').value ? this.legForm.get('pu_time').value[1] : null) : this.legForm.get('pu_time').value,

      dl_loc: this.legForm.get('delivery').value,
      dl_name: this.legForm.get('dl_name').value,

      dl_time: (this.injectedData.isLeg &&
        (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered'))) ?
        (this.legForm.get('dl_time').value ? this.legForm.get('dl_time').value[0] : null) : this.legForm.get('dl_time').value,

      dl_time_to: (this.injectedData.isLeg &&
        (!this.editMode || (this.editMode && this.data.leg_status !== 'Delivered'))) ?
        (this.legForm.get('dl_time').value ? this.legForm.get('dl_time').value[1] : null) : this.legForm.get('dl_time').value,

      dmg: null,
      driver_id: selectedId.driver_id,
      miles: this.legForm.get('est_miles').value,
      loggedInUser: `${this.sharedService.getUserDetails().first_name} ${this.sharedService.getUserDetails().last_name}`,
      leg_number: this.legForm.get('leg_number').value,
      leg_type_id: this.legForm.get('leg_type').value,

      order_id: this.injectedData.isLeg ?
        (this.editMode ? this.data.order_id : this.commonData.order_id) :
        selectedId.order_id,

      order_container_chassis_id: this.injectedData.isLeg ?
        (this.editMode ? this.data.order_container_chassis_id : this.commonData.order_container_chassis_id) :
        selectedId.order_container_id,

      id: this.editMode ? (this.injectedData.isLeg ? this.data.leg_id : this.data.driver_pay_id) : null,
      legStatus: this.injectedData.isLeg ? selectedId.legStatus : null,
      isMilesEditable: this.isMilesEditable,
      is_paid: this.legForm.get('is_paid').value,
      previousDeliveryAddress: this.editMode ? this.data.dl_loc : null,
      previousPickupAddress: this.editMode ? this.data.pu_loc : null,
      rate: this.legForm.get('rate').value,
      format_address: this.injectedData.isLeg ? this.commonData.format_address : null,
      leg_status: this.editMode ? (
        this.injectedData.isLeg ?
          (
            (!this.data.is_brokered &&
              ((this.data.leg_status === 'Open' && !this.data.driver_name) || this.data.leg_status === 'Assigned')
            ) ?
              (
                (this.legForm.get('driver_name').value && this.legForm.get('driver_name').value.trim()) ?
                  'Assigned' : 'Open'
              ) : this.data.leg_status
          ) : null)
        : null,
      isRateEdited: this.editMode ? (
        this.injectedData.isLeg ? null
          : (this.data.driver_rate !== this.legForm.get('rate').value && !this.data.is_paid ? true :
            this.legForm.get('is_paid').value)
      ) : false,
      customerId: this.injectedData.isLeg ? this.commonData.customerId : null,
      orderTypeId: this.injectedData.isLeg ? this.commonData.orderTypeId : null,
      is_charges_added: !this.injectedData.isLeg ? this.legForm.get('is_charges_added').value : null,
      driver_charges: !this.injectedData.isLeg ? this.legForm.get('driver_charges').value : null,
      isDispatch: this.injectedData.isLeg ? this.commonData.isDispatch : null,

      is_brokered: this.injectedData.isLeg ?
        (this.editMode ? this.data.is_brokered : this.commonData.is_brokered) : null,

      carrier_id: this.injectedData.isLeg ?
        (this.editMode ? this.data.carrier_id : this.commonData.carrier_id) : null
    }
  }

  closeWithConfirmDialog(request, driverName) {
    let dialogRef = this.sharedService.openConfirmation(
      {
        action: "hazmatCertifiedValidation",
        name: driverName,
        cancelLabel: "No",
        confirmColor: "#4d97f3",
        confirmLabel: "Yes"
      })
    dialogRef.subscribe(result => {
      if (result) {
        this.dialogRef.close(request);
      }
    });
  }

}
