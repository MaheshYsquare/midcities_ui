import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmationData } from 'app/shared/models/shared.model';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  public confirmMessages = [];

  public password: FormControl = new FormControl();

  public passwordShowHide: boolean = true;

  constructor(public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData: ConfirmationData) { }

  ngOnInit() {
    this.confirmMessages.push(
      {
        for: "Delete",
        message: `Are you sure want to ${this.injectedData.action} the ${this.injectedData.name} ?`
      },
      {
        for: "Deactivate",
        message: `Are you sure want to ${this.injectedData.action} the ${this.injectedData.name} ?`
      },
      {
        for: "inactive",
        message: `Are you sure you want to make ${this.injectedData.name} inactive ?`
      },
      {
        for: "verification",
        message: `Are you sure you want to resend the verification email to ${this.injectedData.name} ?`
      },
      {
        for: "mailWithoutSubject",
        message: `Are you sure you want to send email without subject ?`
      },
      {
        for: "mailConfirmation",
        message: `Are you sure you want to resend the invoice ?`
      },
      {
        for: "hazmatCertifiedValidation",
        message: `Are you sure you want to hire same driver ${this.injectedData.name} who is not hazmat certified ?`
      },
      {
        for: "sameDriver",
        message: `Are you sure you want to hire same driver ${this.injectedData.name} ?`
      },
      {
        for: "bottomTop",
        message: `Are you sure you want to change status from "${this.injectedData.previousName}" to "${this.injectedData.name}"?, it will reset ${this.injectedData.reset}`
      },
      {
        for: "bottomTopPickedupUndo",
        message: `Are you sure you want to change status from "${this.injectedData.previousName}" to "${this.injectedData.name}"?, it will reset ${this.injectedData.reset}`
      },
      {
        for: "bobtail",
        message: `Are you sure you want "${this.injectedData.reset}" to "${this.injectedData.action}" from "${this.injectedData.previousName}" to "${this.injectedData.name}"`
      },
      {
        for: "closeOrder",
        message: `Do you want to mark this Order ready for invoicing?`
      },
      {
        for: "closeOrderManual",
        message: `Do you want to mark this Order ready for invoicing?`
      },
      {
        for: "streetTurnWithoutLeg",
        message: `Are you sure you want to mark this Order complete and perform Street Turn?`
      },
      {
        for: "streetTurnWithLeg",
        message: `Are you sure you want to delete legs under the selected leg and mark this Order complete?`
      },
      {
        for: "cancel",
        message: `Are you sure to want to cancel this ${this.injectedData.name}?`
      },
      {
        for: "orderEdit",
        message: "Do you want to update all corresponding legs with the updated information?"
      },
      {
        for: "moveOrderToDispatch",
        message: "Are you sure to want to move back the order to dispatch?"
      },
      {
        for: "sendEmail",
        message: `Do you want to send email to the following address? \n\t${this.injectedData.name ? this.injectedData.name.split(",").join("\n\t") : null}`
      },
      {
        for: "moveOrderToArchive",
        message: "Are you sure to want to move order to archived?"
      },
      {
        for: "moveOrderToInvoice",
        message: "Are you sure to want to move order back to invoice?"
      },
      {
        for: "rateCheck",
        message: `No Rates configured from "${this.injectedData.name}" to "${this.injectedData.previousName}" for this order type and customer, Do you want to proceed anyway?`
      },
      {
        for: "confirmWithPassword",
        message: "Please enter your password to delete this Order. This action cannot be undone."
      },
      {
        for: 'customerRefCheck',
        message: "This reference number has been used before. Do you want to proceed?"
      },
      {
        for: 'assignDriverConfirm',
        message: "Assign same driver to next leg?"
      },
      {
        for: 'chassisCreate',
        message: "Did they keep the chassis?"
      }
    );
  }

  onSubmit() {
    if (this.injectedData.action === "confirmWithPassword") {

      if (this.password.valid) {
        this.dialogRef.close(this.password.value);
      }

    } else {
      this.dialogRef.close('yes')
    }
  }

}
