import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss']
})
export class FilterDialogComponent implements OnInit {

  public searchBy: FormControl = new FormControl();

  constructor(public dialogRef: MatDialogRef<FilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public injectedData) { }

  ngOnInit() {
    const rightPosition = +window.innerWidth - +this.injectedData.leftPosition;
    this.dialogRef.updatePosition({
      top: `${this.injectedData.topPosition}px`,
      right: `${rightPosition}px`
    })
  }

  onChangeOfDateTimePicker(event: any) {
    if (event.value && event.value.length && event.value[0] !== null && event.value[1] === null) {
      this.searchBy.setValue([event.value[0], event.value[0]]);
    } else if (event.value && event.value.length && event.value[0] === null && event.value[1] !== null) {
      this.searchBy.setValue([event.value[1], event.value[1]]);
    }
  }

  onSubmit() {
    if (this.searchBy.value && this.searchBy.value.trim()) {
      this.dialogRef.close(this.searchBy.value);
    } else {
      this.dialogRef.close();
    }
  }

}
