import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, DateTimeAdapter, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

import { SnackbarComponent } from './snackbar/snackbar.component';
import { MaterialModule } from 'app/shared/demo.module';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { InformationComponent } from './information/information.component';
import { FilterDialogComponent } from './filter-dialog/filter-dialog.component';
import { MY_CUSTOM_FORMATS } from '../../models/shared.model';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  declarations: [
    SnackbarComponent,
    ConfirmationComponent,
    InformationComponent,
    FilterDialogComponent,
  ],
  entryComponents: [
    SnackbarComponent,
    ConfirmationComponent,
    InformationComponent,
    FilterDialogComponent
  ],
  exports: [
    SnackbarComponent,
    ConfirmationComponent,
    InformationComponent,
    MaterialModule,
    FilterDialogComponent
  ],
  providers: [
      { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
      { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ]
})
export class HelperModule { }
