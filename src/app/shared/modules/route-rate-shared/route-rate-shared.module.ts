import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRouteRateComponent } from './edit-route-rate/edit-route-rate.component';
import { BulkUpdateComponent } from './bulk-update/bulk-update.component';
import { MaterialModule } from '../../demo.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    EditRouteRateComponent,
    BulkUpdateComponent
  ],
  entryComponents: [
    EditRouteRateComponent,
    BulkUpdateComponent
  ],
  exports: [
    EditRouteRateComponent,
    BulkUpdateComponent
  ],
})
export class RouteRateSharedModule { }
