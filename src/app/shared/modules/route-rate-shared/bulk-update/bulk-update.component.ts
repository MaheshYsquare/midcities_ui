import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-bulk-update',
  templateUrl: './bulk-update.component.html',
  styleUrls: ['./bulk-update.component.scss']
})
export class BulkUpdateComponent implements OnInit {

  public title: string;

  @ViewChild('f') bulkUpdateForm: NgForm

  constructor(public dialogRef: MatDialogRef<BulkUpdateComponent>, @Inject(MAT_DIALOG_DATA) data) {
    if (data) {
      this.title = data.title
    }
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.bulkUpdateForm.valid) {
      this.dialogRef.close(this.bulkUpdateForm.value)
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

}
