import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { MAT_DIALOG_DATA, MatDialogRef, MatSelectChange } from '@angular/material';

import { feildsMatched } from 'app/shared/utils/app-validators';
import { SharedService } from 'app/shared/service/shared.service';
import { LocationData } from 'app/shared/models/location.model';
import { RoutesData } from 'app/shared/models/routes.model';

@Component({
  selector: 'app-edit-route-rate',
  templateUrl: './edit-route-rate.component.html',
  styleUrls: ['./edit-route-rate.component.scss']
})
export class EditRouteRateComponent implements OnInit {

  public editMode: boolean;

  public data: RoutesData;

  public title: string;

  public formatTypes: string[] = ["P2P", "C2C", "P2C", "Z2Z", "S2S", "P2S", "S2P", "C2P"];

  public orderTypes: { label: string; value: number }[];

  public locationTypes: { label: string; value: number }[];

  public customerId: number;

  public routeRateForm: FormGroup;

  public addButtonColor = '#2196f3';

  public editButtonColor = 'green';

  public isMilesEditable: boolean = false;

  public isLocationChanged: boolean = false;

  public pickupLocationData: LocationData;

  public deliveryLocationData: LocationData;

  public isPickupInitEmit: boolean = false;

  public isDeliveryInitEmit: boolean = false;

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditRouteRateComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private sharedService: SharedService) {
    if (data) {
      this.data = data.selectedValue;
      this.editMode = data.modeSelect;
      this.title = data.title;
      this.orderTypes = data.orderTypes;
      this.customerId = data.customerId;
      this.locationTypes = data.locationTypes;
    }
  }

  ngOnInit() {
    this.initForm()

    if (this.editMode) {
      (<any>Object).values(this.routeRateForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }
  }

  private initForm() {
    let pu_name = null;
    let delivery_name = null;
    let pickupAddress = null;
    let DeliverAdress = null;
    let orderType = null;
    let distance = null;
    let rate = null;
    let format_address = "P2P";

    if (this.editMode) {
      pickupAddress = this.data.start_loc;
      DeliverAdress = this.data.des_loc;
      orderType = this.data.order_type_id;
      distance = this.data.distance;
      rate = this.data.rate;
      pu_name = this.data.pu_name;
      delivery_name = this.data.dl_name;
      format_address = this.data.format_address;
      this.isPickupInitEmit = true;
      this.isDeliveryInitEmit = true;
    }

    this.routeRateForm = this.formBuilder.group({
      'pu_name': new FormControl(pu_name, Validators.required),
      'delivery_name': new FormControl(delivery_name, Validators.required),
      'pickupAddress': new FormControl(pickupAddress, [Validators.required]),
      'deliveryAddress': new FormControl(DeliverAdress, [Validators.required]),
      'order_type': new FormControl(orderType, Validators.required),
      'distance': new FormControl({ value: distance, disabled: this.editMode }, [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/)]),
      'rate': new FormControl(rate, [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/)]),
      'format_address': new FormControl(format_address, Validators.required)
    }, { validator: feildsMatched('pickupAddress', 'deliveryAddress') });
  }

  onSelectionChangeFormat(event: MatSelectChange) {
    switch (event.value) {
      case "P2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
      case "C2C":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? `${this.pickupLocationData.city} ${this.pickupLocationData.state}`.trim() : null,
          this.deliveryLocationData ? `${this.deliveryLocationData.city} ${this.deliveryLocationData.state}`.trim() : null);
        break;
      case "P2C":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ? `${this.deliveryLocationData.city} ${this.deliveryLocationData.state}`.trim() : null);
        break;
      case "Z2Z":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.postal_code : null,
          this.deliveryLocationData ? this.deliveryLocationData.postal_code : null);
        break;
      case "S2S":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.state : null,
          this.deliveryLocationData ? this.deliveryLocationData.state : null);
        break;
      case "P2S":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.address : null,
          this.deliveryLocationData ? this.deliveryLocationData.state : null);
        break;
      case "S2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? this.pickupLocationData.state : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
      case "C2P":
        this.setPickupAndDeliveryAddress(this.pickupLocationData ? `${this.pickupLocationData.city} ${this.pickupLocationData.state}`.trim() : null,
          this.deliveryLocationData ? this.deliveryLocationData.address : null);
        break;
    }
  }

  setPickupAndDeliveryAddress(puloc: string, dlloc: string) {
    this.pickupLocationData ? this.routeRateForm.get('pickupAddress').setValue(puloc) : null;

    this.deliveryLocationData ? this.routeRateForm.get('deliveryAddress').setValue(dlloc) : null;
  }

  onPickupSelectedLocation(event: LocationData) {

    this.pickupLocationData = event;

    this.isPickupInitEmit = false;

    if (event) {
      switch (this.routeRateForm.get('format_address').value) {
        case "P2P":
          this.routeRateForm.get('pickupAddress').setValue(event.address);
          break;
        case "C2C":
          this.routeRateForm.get('pickupAddress').setValue(`${event.city} ${event.state}`.trim());
          break;
        case "P2C":
          this.routeRateForm.get('pickupAddress').setValue(event.address);
          break;
        case "Z2Z":
          this.routeRateForm.get('pickupAddress').setValue(event.postal_code);
          break;
        case "S2S":
          this.routeRateForm.get('pickupAddress').setValue(event.state);
          break;
        case "P2S":
          this.routeRateForm.get('pickupAddress').setValue(event.address);
          break;
        case "S2P":
          this.routeRateForm.get('pickupAddress').setValue(event.state);
          break;
        case "C2P":
          this.routeRateForm.get('pickupAddress').setValue(`${event.city} ${event.state}`.trim());
          break;
      }
    }
  }

  onDeliverySelectedLocation(event: LocationData) {

    this.deliveryLocationData = event;

    this.isDeliveryInitEmit = false;

    if (event) {
      switch (this.routeRateForm.get('format_address').value) {
        case "P2P":
          this.routeRateForm.get('deliveryAddress').setValue(event.address);
          break;
        case "C2C":
          this.routeRateForm.get('deliveryAddress').setValue(`${event.city} ${event.state}`.trim());
          break;
        case "P2C":
          this.routeRateForm.get('deliveryAddress').setValue(`${event.city} ${event.state}`.trim());
          break;
        case "Z2Z":
          this.routeRateForm.get('deliveryAddress').setValue(event.postal_code);
          break;
        case "S2S":
          this.routeRateForm.get('deliveryAddress').setValue(event.state);
          break;
        case "P2S":
          this.routeRateForm.get('deliveryAddress').setValue(event.state);
          break;
        case "S2P":
          this.routeRateForm.get('deliveryAddress').setValue(event.address);
          break;
        case "C2P":
          this.routeRateForm.get('deliveryAddress').setValue(event.address);
          break;
      }
    }

  }

  onLocationChange() {
    this.isLocationChanged = true;
    this.onClickDisableMiles()
  }

  onClickEnableMiles() {
    this.routeRateForm.get('distance').enable();
    this.isMilesEditable = true;
  }

  onClickDisableMiles() {
    this.routeRateForm.get('distance').disable();
    this.isMilesEditable = false;
  }

  onSubmit() {
    if (this.routeRateForm.valid) {
      if (this.editMode) {
        var editOutput = {
          'customer_id': this.customerId,
          'route_rate_id': this.data.route_rate_id,
          'start_loc': this.routeRateForm.get('pickupAddress').value,
          'des_loc': this.routeRateForm.get('deliveryAddress').value,
          'order_type_id': this.routeRateForm.get('order_type').value,
          'distance': this.routeRateForm.get('distance').value,
          'rate': this.routeRateForm.get('rate').value,
          'pu_name': this.routeRateForm.get('pu_name').value,
          'dl_name': this.routeRateForm.get('delivery_name').value,
          'format_address': this.routeRateForm.get('format_address').value,
          previousPickupAddress: this.data.start_loc,
          previousDeliveryAddress: this.data.des_loc,
          isMilesEditable: this.isMilesEditable
        }
        this.sharedService.updateRouteData('route_rates', editOutput).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.routeRateForm.get('pickupAddress').reset(this.data.start_loc);
            this.routeRateForm.get('deliveryAddress').reset(this.data.des_loc);
            this.routeRateForm.get('pu_name').reset(this.data.pu_name);
            this.routeRateForm.get('delivery_name').reset(this.data.dl_name);
            this.sharedService.openSnackbar("Route Already Exists", 2000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
      } else {
        var addOutput = {
          'customer_id': this.customerId,
          'start_loc': this.routeRateForm.get('pickupAddress').value,
          'des_loc': this.routeRateForm.get('deliveryAddress').value,
          'order_type_id': this.routeRateForm.get('order_type').value,
          'rate': this.routeRateForm.get('rate').value,
          'pu_name': this.routeRateForm.get('pu_name').value,
          'dl_name': this.routeRateForm.get('delivery_name').value,
          'format_address': this.routeRateForm.get('format_address').value,
        }
        this.sharedService.postRouteData('route_rates', addOutput).subscribe((response: any) => {
          if (response.code == 200) {
            this.dialogRef.close('success')
          } else if (response.code == 422) {
            this.routeRateForm.get('pickupAddress').reset();
            this.routeRateForm.get('deliveryAddress').reset();
            this.routeRateForm.get('pu_name').reset();
            this.routeRateForm.get('delivery_name').reset();
            this.sharedService.openSnackbar("Route Already Exists", 2000, "warning-msg");
          }
        }, error => {
          this.sharedService.openSnackbar("Something Went Wrong", 2000, "failure-msg");
        })
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }
}
