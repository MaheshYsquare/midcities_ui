export interface DriverInfoData{
    order_number:Number;
    leg_number:Number;
    leg_type:String;
    pu_loc:String;
    dl_loc:String;
    est_miles:Number;
    container_number:String;
    chassis_number:String;
    pay:String;
  }
  
  