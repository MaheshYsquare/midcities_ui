export interface DriverMoreInfoData {
        truck_number:string,
        driver_notes: string,
        phone_number: number,
        fuel_card_number: string,
          ssn: string,
            street: string,
            city: string,
            state: string,
            zip: number,
            phone: number,
            email: string,
            lic_exp_date:"", 
            hazmat_certified:"",
            hazmat_exp_date :"",
            med_exp_date:"",
            driver_id :"",
            specialEndorsement: [
        {
        special_endorsement_name:"",
        special_endorsement_exp_date:""
        }
        ],
            attendenceVacation: [
        {
        vacation_name:"",
        vacation_from_date:"",
        vacattion_date:"",
        vacation_to_date:""
        }
        ],
            attendenceSickday : [
        {
        sickday_name:"",
        sickday_from_date:"",
        sickdayto_date:"",
        sickday_date
        }
        ],
            attendenceNcns: [
        {
        ncns_name:"",
        ncns_from_date:"",
        ncns_to_date:"",
        ncns_date
        }
        ],
            incident : [
        {
        name:"",
        description:"",
        action_taken:"",
        upload_document:""
        }
        ]
     
   
          
        }
