import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialogConfig, MatDialog, MatSort } from '@angular/material';
import { driverPayFilterColumns } from 'app/shared/models/display-column.model';

import { DriverPayData, DriverPayRequest, DriverPayResponse } from 'app/shared/models/driver.model';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';
import { SharedService } from 'app/shared/service/shared.service';

@Component({
  selector: 'app-driver-pay',
  templateUrl: './driver-pay.component.html',
  styleUrls: ['./driver-pay.component.scss']
})
export class DriverPayComponent implements OnInit {

  public driverPayDatasource: MatTableDataSource<DriverPayData>;

  public driverPayColumns: string[] = ['driver_name', 'order_sequence', 'leg_type',
    'pu_name', 'pu_time', 'dl_name', 'dl_time', 'miles', 'container_number', 'chassis_number',
    'driver_rate', 'driver_extra_charge', 'total_driver_rate', 'is_paid'];

  public componentName: string = 'drivers';

  public isPaid: boolean = false;

  public driverPayData: DriverPayData[];

  public userDetails: any;

  public userId: number;

  public userRole: string;

  public offset: number = 1;

  public limit: number = 10;

  public sortDirection: string;

  public sortColumn: string;

  public pageLength: number = 0;

  public filterData: DriverPayRequest['filterData'];

  public searchFilterEvent: boolean = false;

  public searchValue: string = "";

  public columnFilterEvent: boolean = false;

  public filterDate: Date;

  @ViewChild('driverPaySort') driverPaySort: MatSort;

  @ViewChild('driverPayPaginator') driverPayPaginator: MatPaginator;

  constructor(private sharedService: SharedService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.driverPayDatasource = new MatTableDataSource();

    this.userDetails = this.sharedService.getUserDetails();

    this.userId = this.userDetails.userId;

    this.userRole = this.userDetails.role;

    this.getDriverPayDataFromService();
  }

  getDriverPayDataFromService(): void {
    let request = this.generateRequest()

    this.sharedService.getDriverPayData(this.componentName, request)
      .subscribe((response: DriverPayResponse) => {
        if (response.code == 200) {
          this.driverPayDatasource.data = response.data.driverPayData;
          this.pageLength = response.data.driverPayPaginationLength;
        } else {
          this.sharedService.openSnackbar('Something Went Wrong', 500, 'failure-msg')
        }
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
  }

  /*   Fliter Dispatch Table through Date */
  onChangeDate() {
    this.initialPage();
    this.getDriverPayDataFromService();
  }

  /* clearing the date filter */
  onClickClear() {
    this.filterDate = null;
    this.initialPage();
    this.getDriverPayDataFromService();
  }

  onKeyupFilter(event) {
    if (event === "") {
      this.initialPage();
      this.searchFilterEvent = false;
      this.searchValue = event;
      this.getDriverPayDataFromService()
    } else {
      this.initialPage();
      this.searchFilterEvent = true;
      this.searchValue = event;
      this.getDriverPayDataFromService()
    }
  }

  /* api service call for search */
  onSearch() {
    if (this.searchValue !== "") {
      this.searchFilterEvent = true;
      this.getDriverPayDataFromService();
    } else {
      this.searchFilterEvent = false;
      this.searchValue = "";
      this.getDriverPayDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilterEvent = false;
    this.searchValue = "";
    this.getDriverPayDataFromService();
  }

  onClickDriverFilter() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'filterData': this.filterData,
      filterColumns: driverPayFilterColumns,
      "key": "driverPayData"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.initialPage();
        this.columnFilterEvent = true;
        this.filterData = result;
        this.getDriverPayDataFromService()
      }
    });
  }

  onClickReset() {
    this.initialPage();
    this.columnFilterEvent = false;
    this.filterData = undefined;
    this.getDriverPayDataFromService()
  }

  onChangeSortDirection(event) {
    this.sortDirection = event.direction == '' ? null : event.direction;
    this.sortColumn = event.active;
    this.getDriverPayDataFromService();
  }

  onChangePage(event) {
    switch (event.pageSize) {
      case 10:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverPayDataFromService()
        break;
      case 25:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverPayDataFromService()
        break;
      case 50:
        this.offset = event.pageIndex == 0 ? ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) : ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1)
        this.limit = event.pageIndex == 0 ? ((event.pageIndex + 1) * event.pageSize) : ((event.pageIndex + 1) * event.pageSize)
        this.getDriverPayDataFromService()
        break;
    }
  }

  generateRequest() {
    let request: DriverPayRequest = {
      offset: this.offset,
      limit: this.limit,
      searchFilter: this.searchFilterEvent,
      searchFilterValue: this.searchValue,
      role: this.userRole,
      user_id: this.userId,
      isListById: false,
      driver_id: null,
      is_paid: null,
      columnFilter: this.columnFilterEvent,
      filterData: this.filterData,
      column: this.sortColumn,
      direction: this.sortDirection,
      roleDirection: this.userRole == 'Dispatch' ? 'desc' : 'asc',
      filterDate: this.filterDate ? this.filterDate.toDateString() : null,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }

    return request
  }

  initialPage() {
    this.driverPayPaginator.pageIndex = 0;
    this.driverPayPaginator.pageSize = this.driverPayPaginator.pageSize ? this.driverPayPaginator.pageSize : 10;
    this.offset = 1;
    this.limit = this.driverPayPaginator.pageSize ? this.driverPayPaginator.pageSize : 10;
  }

}
