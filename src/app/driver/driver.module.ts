import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { loadingInterceptor } from '../shared/interceptor/loading.interceptor';
import { DriverRoutingModule } from './driver-routing.module';
import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { DriverDetailComponent } from './driver-detail/driver-detail.component';
import { DriverPayComponent } from './driver-pay/driver-pay.component';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverInfoModule } from 'app/shared/modules/driver-info-module/driver-info-module.module';

@NgModule({
  imports: [
    HttpClientModule,
    DriverInfoModule,
    DriverRoutingModule
  ],
  declarations: [
    DriverDetailComponent,
    DriverPayComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: loadingInterceptor,
      multi: true,
    },
    DriverInfoResolver,
    OrderLegResolver
  ]
})

export class DriverModule { }
