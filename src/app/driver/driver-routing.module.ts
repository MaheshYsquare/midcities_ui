import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DriverInfoResolver } from 'app/shared/resolver/driver-info.resolver';
import { DriverDetailComponent } from './driver-detail/driver-detail.component';
import { DriverPayComponent } from './driver-pay/driver-pay.component';
import { SubMenuGuard } from 'app/shared/guard/sub-menu.guard';
import { OrderLegResolver } from 'app/shared/resolver/order-leg.resolver';
import { DriverinfoComponent } from 'app/shared/modules/driver-info-module/driverinfo.component';
import { LegComponent } from 'app/shared/modules/driver-info-module/leg/leg.component';

const DriverRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'driver-details',
        children: [
          {
            path: '',
            canActivate: [SubMenuGuard],
            component: DriverDetailComponent
          },
          {
            path: 'driver-info',
            canActivate: [SubMenuGuard],
            component: DriverinfoComponent,
            resolve: {
              'driverData': DriverInfoResolver
            }
          },
          {
            path: ':orderNumber/:sequence/leg',
            canActivate: [SubMenuGuard],
            component: LegComponent,
            resolve: {
              'orderLeg': OrderLegResolver
            }
          }
        ]
      }, {
        path: 'driver-pay',
        canActivate: [SubMenuGuard],
        component: DriverPayComponent
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(DriverRoutes)],
  exports: [RouterModule]
})

export class DriverRoutingModule { }
