
export interface DriverData {
  driver_id: number;
  first_name: string;
  last_name: string;
  driver_name: string;
  truck_number: string;
  order_number: string;
  leg_status: string;
  driver_type: string;
  business_name: string;
  dl_loc: string;
  d_city: string;
  a_delivery_date: string;
  a_delivery_time: string;
  container_type: string;
  phone_number: number;
  nextel_id: number;
  truck: string;
  shift: string;
  hazmat_req: string;
  lic_exp_date: string;
  ins_exp_date: string;
  med_exp_date: string;
  amount: number;
  driver_notes: string;
  hazmat_certified: boolean;
  selected_drivers: any;
  profile_image: any
}

export class DriverDetailsResponse {
  code: number
  error: string
  data: {
    driverData: DriverDetailsData[]
    driverPaginationLength: number
  }
}

export class DriverDetailsData {
  selected_drivers: SelectedDriverData[];
  serialNo?: number;
  pu_loc: string;
  pu_name: string;
  dl_loc: string;
  dl_name: string;
  est_pickup_from_time: string;
  est_pickup_to_time: string;
  est_delivery_from_time: string;
  est_delivery_to_time: string;
  container_type: string;
  current_order: string;
  d_city: string;
  leg_status: string;
  driver_status: string;
  pu_time: string;
  dl_time: string;
  business_name: string
  driver_id: number;
  user_id: number;
  phone_number: string;
  truck_number: string
  driver_name: string
  leg_type: string
  hazmat_req: any
  hazmat_certified: any
  order_id: number
  order_container_chassis_id: number
  paid: any;
  user_status: any;
  last_known_location_name: string;
  last_known_location: string;
  vehicle_id?: any;
}


export class SelectedDriverData {
  pu_loc: string;
  pu_name: string;
  dl_loc: string;
  dl_name: string;
  pu_time: any;
  dl_time: any;
  leg_type: string;
  hazmat_req: any;
  leg_status: string;
  order_number: number;
}

export class DriverDetailsPostRequest {
  offset: number
  limit: number
  searchFilter: boolean
  searchFilterValue: string
  role: string
  userId: number
  columnFilter: boolean
  filterData: {
    column: string
    filters: string
    values: string
  }[]
  column: string
  direction: string;
  isNotAllDriver: boolean;
  driverId?: number;
  isInactiveIncluded?: boolean;
  isDriverBoard?: boolean;
  filterDate?: any;
  isSortByDriver?: boolean;
  timeZone?: any;
}