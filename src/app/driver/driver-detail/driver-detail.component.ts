import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialogConfig, MatDialog, MatSort, MatSelectChange } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { DriverDetailsData, DriverDetailsPostRequest, DriverDetailsResponse } from '../DriverData';
import { DriverService } from '../driver.service';
import { SharedService } from 'app/shared/service/shared.service';
import { SocketService } from 'app/shared/service/socket.service';
import { loginData } from 'app/session/session.model';
import { FilterComponent } from 'app/shared/modules/order-component/filter/filter.component';
import { driverFilterColumns } from 'app/shared/models/display-column.model';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.scss']
})
export class DriverDetailComponent implements OnInit {

  public userDetails: loginData; // store logged in user

  public userId: number; // store userId

  public userRole: string; // store user role

  public driverRole: boolean; // flag to check whether the logged in user is driver or not

  public componentName: String = "drivers"; // api component name for api fetch

  public searchFilterEvent: boolean = false; // filter flag whether search filter is enabled

  public searchFilterValue: string = ""; // store search filter value

  public columnFilterEvent: boolean = false; // filter flag whether column wise filter is enabled

  public filterData: DriverDetailsPostRequest['filterData']; // store column wise filter data

  /* table source,sort,pagination and cache data fetch variable  */
  public driverDetailsDatasource: MatTableDataSource<DriverDetailsData>;

  public driverDetailsColumns: string[] = ['s_no', 'truck_number', 'driver_name', 'user_status', 'phone_number',
    'email', 'current_order', 'driver_status', 'leg_type', 'pu_name', 'dl_name',
    'container_type', 'hazmat_certified', 'action'];

  public currentPageIndex: number = 0;

  public driverDetailInlineInput: {
    truck_number?: string;
    phone_number?: any;
    vehicle_id?: any;
  } = {};

  public selectedDriverId: number;

  public offset: number = 1;

  public limit: number = 50;

  public sortDirection: string;

  public sortColumn: string;

  public pageLength: number = 0;

  public phoneNumberMask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  @ViewChild('driverDetailSort') driverDetailSort: MatSort;

  @ViewChild('driverDetailPaginator') driverDetailPaginator: MatPaginator;

  public truckList: any[] = [];

  constructor(private driverService: DriverService,
    private socketService: SocketService,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.driverDetailsDatasource = new MatTableDataSource();

    this.userDetails = this.sharedService.getUserDetails();

    this.userId = this.userDetails.userId;

    this.userRole = this.userDetails.role;

    this.driverRole = this.userRole == 'Driver' ? true : false;

    this.getDriverDetailsDataFromService();
  }

  getDriverDetailsDataFromService() {
    let request = this.generateRequest()
    this.sharedService.getDriverDetails(this.componentName, request).subscribe((response: DriverDetailsResponse) => {
      this.driverDetailsDatasource.data = response.data.driverData;
      this.pageLength = response.data.driverPaginationLength;
    }, error => {
      this.sharedService.openSnackbar('Something Went Wrong', 3000, 'failure-msg')
    })
  }

  /* api service call for search */
  onSearch() {
    if (this.searchFilterValue !== "") {
      this.searchFilterEvent = true;
      this.initialPage();
      this.getDriverDetailsDataFromService();
    } else {
      this.searchFilterEvent = false;
      this.searchFilterValue = "";
      this.initialPage();
      this.getDriverDetailsDataFromService();
    }
  }

  /* api service call for clear search */
  onClearSearch() {
    this.searchFilterEvent = false;
    this.searchFilterValue = "";
    this.initialPage();
    this.getDriverDetailsDataFromService();
  }

  onClickReset() {
    this.initialPage()
    this.columnFilterEvent = false;
    this.filterData = undefined;
    this.getDriverDetailsDataFromService()
  }

  onClickDriverFilter() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      'filterData': this.filterData,
      filterColumns: driverFilterColumns,
      "key": "driverDetailsData"
    }
    let dialogRef = this.dialog.open(FilterComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.initialPage()
        this.columnFilterEvent = true;
        this.filterData = result;
        this.getDriverDetailsDataFromService()
      }
    });
  }

  onChangeSortDirection(event) {
    this.sortDirection = event.direction == '' ? null : event.direction;
    this.sortColumn = event.active == 'dl_date' ? 'dl_time' : event.active;
    this.getDriverDetailsDataFromService();
  }

  onClickSerialNumber(selectedRow: DriverDetailsData) {

    this.sharedService.openInfo({
      view: "table",
      name: "Driver",
      secondaryName: selectedRow.driver_name,
      secondaryColumnNames: ["S.No", "Current Order", "Status", "Leg Type", "Shipper",
        "Consignee", "Hazmat Required"],
      selectedArray: selectedRow.selected_drivers,
      selectedData: selectedRow
    })
  }

  onClickDriverName(selectedRow: DriverDetailsData) {
    this.router.navigate(['driver-info'], { relativeTo: this.route, queryParams: { driverId: selectedRow.driver_id } });
  }

  onSelectTruck(event: any) {
    let truck = this.truckList.find(temp => temp.truck_no === event.target.value);

    if (truck) {
      this.driverDetailInlineInput.vehicle_id = truck.vehicle_id;
    } else {
      this.driverDetailInlineInput.vehicle_id = null;
    }
  }

  onClickEditDriver(selectedRow: DriverDetailsData) {
    this.selectedDriverId = selectedRow.driver_id;
    this.driverDetailInlineInput.phone_number = selectedRow.phone_number;
    this.driverDetailInlineInput.truck_number = selectedRow.truck_number;
    this.driverDetailInlineInput.vehicle_id = selectedRow.vehicle_id;
    this.getTruckListFromService();
  }

  getTruckListFromService() {
    this.sharedService.getTruckNumberList('equipments').subscribe(response => {
      this.truckList = response.data;
    }, error => this.sharedService.openSnackbar("Something Went Wrong", 2500, 'failure-msg'));
  }

  onClickSaveDriver(selectedRow: DriverDetailsData) {
    var input = {
      "driver_id": selectedRow.driver_id,
      "phone_number": this.driverDetailInlineInput.phone_number,
      "truck_number": this.driverDetailInlineInput.truck_number === 'null' ? null : this.driverDetailInlineInput.truck_number,
      "vehicle_id": this.driverDetailInlineInput.vehicle_id
    }
    if (/^(\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/.test(this.driverDetailInlineInput.phone_number)) {
      this.driverService.updateDriverData(this.componentName, input).subscribe(response => {
        if (response.code == 200) {
          this.selectedDriverId = 0;
          this.socketService.sendChangesInDriverFromClient('driver')
          this.getDriverDetailsDataFromService();
        } else {
          this.sharedService.openSnackbar('Something Went Wrong', 5000, 'failure-msg')
        }
      }, error => {
        this.sharedService.openSnackbar('Something Went Wrong', 2500, 'failure-msg');
      });
    } else {
      this.sharedService.openSnackbar('Invalid Phone Number', 5000, 'warning-msg')
    }
  }

  onClickDeleteDriver(selectedRow: DriverDetailsData) {
    let dialogRef = this.sharedService.openConfirmation({
      action: 'Delete',
      name: 'Driver',
      cancelLabel: 'Cancel',
      confirmLabel: 'Delete',
      confirmColor: 'red'
    })
    dialogRef.subscribe(result => {
      if (result) {
        this.driverService.deleteDriver(this.componentName, selectedRow.driver_id, selectedRow.user_id).subscribe(
          response => {
            if (response.code == 200) {
              this.socketService.sendChangesInDriverFromClient('driver');
              this.getDriverDetailsDataFromService();
              this.sharedService.openSnackbar('Driver Deleted Successfully', 500, 'success-msg')
            } else if (response.code == 400 && response.error == 'Driver is Referenced') {
              this.sharedService.openSnackbar('Driver Is Assigned', 5000, 'warning-msg')
            }
          }, error => {
            this.sharedService.openSnackbar('Something Went Wrong', 5000, 'failure-msg')
          });
      }
    })
  }

  onChangePage(event) {
    this.currentPageIndex = (event.pageIndex * event.pageSize);

    this.offset = event.pageIndex == 0 ?
      ((event.pageSize + 1) - ((event.pageIndex + 1) * event.pageSize)) :
      ((event.pageIndex + 1) * event.pageSize) - (event.pageSize - 1);

    this.limit = event.pageIndex == 0 ?
      ((event.pageIndex + 1) * event.pageSize) :
      ((event.pageIndex + 1) * event.pageSize);

    this.getDriverDetailsDataFromService();
  }

  generateRequest() {
    let request: DriverDetailsPostRequest = {
      offset: this.offset,
      limit: this.limit,
      role: this.userRole,
      userId: this.userId,
      searchFilter: this.searchFilterEvent,
      searchFilterValue: this.searchFilterValue,
      columnFilter: this.columnFilterEvent,
      filterData: this.filterData,
      column: this.sortColumn,
      direction: this.sortDirection,
      isNotAllDriver: false,
      isInactiveIncluded: true,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    return request
  }

  initialPage() {
    this.driverDetailPaginator.pageIndex = 0;
    this.driverDetailPaginator.pageSize = this.driverDetailPaginator.pageSize ? this.driverDetailPaginator.pageSize : 50;
    this.currentPageIndex = 0
    this.offset = 1;
    this.limit = this.driverDetailPaginator.pageSize ? this.driverDetailPaginator.pageSize : 50;
  }
}
