import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment'
import { SharedService } from 'app/shared/service/shared.service';

@Injectable({
    providedIn: 'root'
})

export class DriverService {

    HttpUploadOptions = {}

    private baseURL = environment.baseURL;
    httpOptions = {

    };

    constructor(private http: HttpClient,
        private sharedService: SharedService) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'access_token': this.sharedService.getAuthToken()
            })
        };

    }

    /** DELETE: delete the driver from the server */
    deleteDriver(actionComponentName: String, driverId: number, userId: number) {
        let url = userId == null ?
            `${this.baseURL + actionComponentName}/deleteDriver?driver_id=${driverId}` :
            `${this.baseURL + actionComponentName}/deleteDriver?driver_id=${driverId}&user_id=${userId}`
        return this.http.delete<any>(url, this.httpOptions)
    }

    updateDriverData(actionComponentName: String, newDriverInfoData: any) {
        const url = `${this.baseURL + actionComponentName}/updateDriverData`;
        return this.http.put<any>(url, newDriverInfoData, this.httpOptions)
    }

    deactivateDriver(actionComponentName: String, data: any) {
        const url = `${this.baseURL + actionComponentName}/deactivateDriver`;
        return this.http.post<any>(url, data, this.httpOptions)
    }
}
